from flask import Flask, g
from flask_hashing import Hashing
import sqlite3
from os.path import join, exists
from os import remove
import signal

from databank.calls import Server

## URLs

MONITOR_URL = "http://localhost:5678"

## Files

UPLOAD_FOLDER = join("..", "uploads")
LOG_FOLDER    = join("..", "log")

## Application

bank = Flask("The Databank")
bank.secret_key = "E4kB3BUlTXivYtkaKnCb9XHGIr9erSEIX0n0MWOnAqlqr2PGKWjPgWp2834M5PmDqx2dEvI2EV7YdriY"
bank.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
#bank.config['SESSION_COOKIE_HTTPONLY'] = False
hashing = Hashing(bank)
server = Server()

## Fundamental database

FUNDAMENTAL = join("..", "db", "fundamental.db")

def get_db():
    db = getattr(g, '_db', None)
    if db is None:
        db = g._database = sqlite3.connect(FUNDAMENTAL)
    return db

def full_filename(filename):
    return join(bank.config['UPLOAD_FOLDER'], filename)

def file_save(filename, file_):
    print("file_save", filename, file_)
    file_.save(full_filename(filename))

def file_remove(filename):
    if "/" not in filename:
        os.remove(full_filename(filename))

def file_exists(filename):
    return exists(full_filename(filename))


@bank.teardown_appcontext
def close_db(ex):
    db = getattr(g, '_db', None)
    if db is not None:
        db.close()

## Options set by main

options = {}

def set_option(o, v=True):
    options[o] = v

def is_set(o):
    return options.get(o, False)


