from apps import meetup

@meetup.route('/')
def main():
    return meetup.render("index.html")

def location_html(location):
    location_ = meetup.sql("SELECT * FROM locations WHERE id = ?0", [location])[0]
    if location_ == None:
        return ""
    else:
        return location_[2] + " (" + location_[1] + ")"

def error_html(msg):
    return """<div class="alert alert-danger" role="alert">""" + msg + """</div>"""

def location_select(selected):
    options = "<option value=\"0\"></option>"
    locations = meetup.sql("SELECT locations.* FROM locations INNER JOIN friends ON locations.owner = friends.friend_id WHERE friends.user_id = ?0 OR locations.owner = ?0", [meetup.me()])
    for location in locations:
        if selected == location[0]:
            sel = " selected=\"selected\""
        else:
            sel = ""
        options += "<option value=\"" + str(location[0]) + "\"" + sel + ">" + location[2] + " (" + location[1] + ")</option>"
    return "<select name=\"location\" id=\"location\" class=\"form-select\">" + options + "</select>"

def event_html(event, details):
    title = event[1]
    description = event[2]
    if event[3] > 0:
        location = location_html(event[3])
    else:
        location =  ""
    time_date = event[4] + " " + event[5]
    if details:
        attended = len(meetup.sql("SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1", [event[0], meetup.me()]))
        if attended > 0:
            leave = "<tr><td colspan\"2\"><a href=\"/2/leave_event/" + str(event[0]) + "\">Leave</a></td></tr>"
        else:
            leave = "<tr><td colspan\"2\"><a href=\"/2/request_attendance/" + str(event[0]) + "\">Request attendance</a></td></tr>"
        invitations = meetup.sql("SELECT * FROM invitations WHERE invitee = ?0 AND event = ?1", [meetup.me(), event[0]])
        if len(invitations) > 0:
            invitation = invitations[0][0]
            leave += "<tr><td colspan\"2\"><a href=\"/2/accept_invitation/" + str(invitation) + "\">Accept</a>&nbsp;<a href=\"/2/reject_invitation/" + str(invitation) + "\">Reject</a></td></tr>"
    else:
        leave = ""
    leave += "<tr><td colspan\"2\"><a href=\"/2/event/" + str(event[0]) + "\">Details</a></td></tr>"
    html = '<table class="table"><thead><tr><th colspan="2">' + title + "</th></tr></thead><tbody><tr><td>Description</td><td>" + description + "</td></tr><tr><td>Location</td><td>" + location + "</td></tr><tr><td>Time/date</td><td>" + time_date + "</td></tr>" + leave + "</tbody></table>"
    return html

def is_admin():
    level = meetup.sql("SELECT level FROM users WHERE user_id = ?0", [meetup.me()])[0][0]
    return level == 3

def is_premium():
    level = meetup.sql("SELECT level FROM users WHERE user_id = ?0", [meetup.me()])[0][0]
    return level >= 2

def is_registered():
    return len(meetup.sql("SELECT * FROM users WHERE user_id = ?0", [meetup.me()])) > 0

def is_moderator(ev):
    return len(meetup.sql("SELECT cat_mod.* FROM cat_mod JOIN ev_cat ON ev_cat.category = cat_mod.category WHERE cat_mod.moderator = ?0 AND ev_cat.event = ?1", [meetup.me(), ev])) > 0

def is_manager(ev):
    return (len(meetup.sql("SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1 AND level = 2",
                          [ev, meetup.me()])) > 0) \
           or (meetup.sql("SELECT owner FROM events WHERE id = ?0", [ev])[0][0] == meetup.me())

def is_attendee(ev):
    return (len(meetup.sql("SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1",
                          [ev, meetup.me()])) > 0) \
           or (meetup.sql("SELECT owner FROM events WHERE id = ?0", [ev])[0][0] == meetup.me())

@meetup.route('/profile')
def profile():
    if is_registered():
        # Owned events
        owned_events_html = ""
        owned_events = meetup.sql("SELECT * FROM events WHERE owner = ?0", [meetup.me()])
        #print(owned_events)
        for owned_event in owned_events:
            owned_events_html += "<br>" + event_html(owned_event, False)
        # Managed events
        managed_events_html = ""
        managed_events = meetup.sql("SELECT events.* FROM events JOIN ev_att ON events.id = ev_att.event WHERE ev_att.attendee = ?0 AND ev_att.level = 2", [meetup.me()])
        for managed_event in managed_events:
            managed_events_html += "<br>" + event_html(managed_event, False)
        # Attended events
        attended_events_html = ""
        attended_events = meetup.sql("SELECT events.* FROM events JOIN ev_att ON events.id = ev_att.event WHERE ev_att.attendee = ?0 AND ev_att.level = 1", [meetup.me()])
        for attended_event in attended_events:
            attended_events_html += "<br>" + event_html(attended_event, False)
        # Invited events
        invited_events_html = ""
        invited_events = meetup.sql("SELECT events.* FROM events JOIN invitations ON events.id = invitations.event WHERE invitations.invitee = ?0", [meetup.me()])
        for invited_event in invited_events:
            inviter = meetup.sql("SELECT name FROM users WHERE user_id = ?0", [invited_event[6]])[0][0]
            invited_events_html += "<br><strong>" + inviter + "</strong> invited you to " + event_html(invited_event, True)
        #print_graph(owned_events_html)
        #print(owned_events_html)
        return meetup.render("profile.html", {"is_registered": True,
                                              "owned_events_html": owned_events_html,
                                              "managed_events_html": managed_events_html,
                                              "attended_events_html": attended_events_html,
                                              "invited_events_html": invited_events_html})
    else:
        return meetup.render("profile.html", {"is_registered": False})

@meetup.route('/leave_event/<int:ev>')
def leave_event(ev):
    meetup.sql("DELETE FROM ev_att WHERE attendee = ?0 AND event = ?1 AND level = 1",
               [meetup.me(), ev])
    return profile()

@meetup.route('/request_attendance/<int:ev>')
def request_attendance(ev):
    meetup.sql("INSERT INTO requests (requester, event) VALUES (?0, ?1)",
               [meetup.me(), ev])
    return events()

@meetup.route('/reject_invitation/<int:invitation>')
def reject_invitation(invitation):
    meetup.sql("DELETE FROM invitations WHERE invitee = ?0 AND id = ?1", [meetup.me(), invitation])
    return profile()

@meetup.route('/accept_invitation/<int:invitation>')
def accept_invitation(invitation):
    event_id = meetup.sql("SELECT event FROM invitations WHERE invitee = ?0 AND id = ?1",
                          [meetup.me(), invitation])
    event_id = event_id[0][0]
    meetup.sql("INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)", [event_id, meetup.me()])
    meetup.sql("DELETE FROM invitations WHERE invitee = ?0 AND id = ?1", [meetup.me(), invitation])
    return profile()

@meetup.route('/unregister')
def unregister():
    meetup.sql("DELETE FROM users WHERE user_id = ?0", [meetup.me()])
    meetup.sql("DELETE FROM cat_mod WHERE moderator = ?0", [meetup.me()])
    meetup.sql("DELETE FROM cat_sub WHERE subscriber = ?0", [meetup.me()])
    meetup.sql("DELETE FROM ev_att WHERE attendee = ?0", [meetup.me()])
    meetup.sql("DELETE FROM events WHERE owner = ?0", [meetup.me()])
    meetup.sql("DELETE FROM friends WHERE user_id = ?0 OR friend_id = ?0", [meetup.me()])
    meetup.sql("DELETE FROM invitations WHERE inviter = ?0 OR invitee = ?0", [meetup.me()])
    meetup.sql("DELETE FROM locations WHERE owner = ?0", [meetup.me()])
    meetup.sql("DELETE FROM requests WHERE requester = ?0", [meetup.me()])
    return profile()

@meetup.route('/register')
def register():
    if not is_registered():
        ID      = meetup.me()
        name    = meetup.get('name')
        level   = 1
        address = int(meetup.get('location'))
        meetup.sql("INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)",
                   [ID, name, level, address])
    return profile()

@meetup.route('/users')
def users():
    if is_registered():
        friends = meetup.sql("SELECT users.* FROM friends LEFT JOIN users ON friends.friend_id = users.user_id WHERE friends.user_id = ?0 OR users.user_id = ?0", [meetup.me()])
        table = ""
        for user in friends:
            if meetup.check((meetup.check(user[1]) and meetup.check(user[2])) and meetup.check(user[3])) and meetup.check(user[4]):
                table += "<tr><td>" + str(user[1]) + "</td><td>" + user[2] + "</td><td>"
                if user[3] == 1:
                    table += "free"
                elif user[3] == 2:
                    table += "premium"
                elif user[3] == 3:
                    table += "administrator"
                if user[4] > 0:
                    table += "</td><td>" + location_html(user[4]) + "</td>"
                else:
                    table += "</td><td></td>"
                if user[1] == meetup.me():
                    table += "<td><a href=\"/2/update_user/" + str(user[1]) + "\">Update</a>&nbsp;<a href=\"/2/user_categories/" + str(user[1]) + "\">Categories</a></td></tr>"
                elif is_admin():
                    table += "<td><a href=\"/2/delete_friend/" + str(user[1]) + "\">Delete friend</a>&nbsp;<a href=\"/2/update_user/" + str(user[1]) + "\">Update</a>&nbsp;<a href=\"/2/user_categories/" + str(user[1]) + "\">Categories</a></td></tr>"
                else:
                    table += "<td><a href=\"/2/delete_friend/" + str(user[1]) + "\">Delete friend</a>&nbsp;<a href=\"/2/user_categories/" + str(user[1]) + "\">Categories</a></td></tr>"
            i = i + 1
        return meetup.render("users.html", {"table_html": table, "is_registered": True})
    else:
        return meetup.render("users.html", {"is_registered": False})

@meetup.route('/delete_friend/<int:user>')
def delete_friend(user):
    meetup.sql("DELETE FROM friends WHERE user_id = ?0 AND friend_id = ?1", [meetup.me(), user])
    return users()

@meetup.route('/add_friend')
def add_friend():
    user_id   = meetup.me()
    friend_id = int(meetup.get('ID'))
    if len(meetup.sql("SELECT * FROM friends WHERE user_id = ?0 AND friend_id = ?1", [user_id, friend_id])) == 0:
        if len(meetup.sql("SELECT * FROM users WHERE user_id = ?0", [friend_id])) > 0:
            meetup.sql("INSERT INTO friends (user_id, friend_id) VALUES (?0, ?1)", [user_id, friend_id])
        else:
            return error_html('User ' + str(friend_id) + ' is not registered.<br>\n') + users()
    return users()

@meetup.route('/update_user/<int:user>')
def update_user(user):
    user = meetup.sql("SELECT * FROM users WHERE user_id = ?0", [user])[0]
    id_     = str(user[0])
    ID      = str(user[1])
    name    = user[2]
    if user[3] == 1:
        free = " selected"
    else:
        free = ""
    if user[3] == 2:
        premium = " selected"
    else:
        premium = ""
    if user[3] == 3:
        administrator = " selected"
    else:
        administrator = ""
    address = user[4]
    return meetup.render("update_user.html", {"id_": id_, "ID": ID, "name": name, "free": free,
                                              "premium": premium, "administrator": administrator,
                                              "location_select": location_select(address)})

@meetup.route('/update_user_do')
def update_user_do():
    id_     = int(meetup.get('id_'))
    ID      = int(meetup.get('ID'))
    name    = meetup.get('name')
    level   = int(meetup.get('level'))
    address = int(meetup.get('location'))
    if is_admin() or id_ == meetup.me():
        meetup.sql("DELETE FROM users WHERE id = ?0", [id_])
        meetup.sql("INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)",
                   [ID, name, level, address])
        return users()
    else:
        return error_html("Illegal operation: Update user") + users()

@meetup.route('/user_categories/<int:user>')
def user_categories(user):
    usr = meetup.sql("SELECT * FROM users WHERE user_id = ?0", [user])[0]
    categories = meetup.sql("SELECT * FROM categories")
    for category in categories:
        categories_html += "<tr><td>" + category[1] + "</td><td>"
        if len(meetup.sql("SELECT id FROM cat_sub WHERE category = ?0 AND subscriber = ?1",
                          [category[0], user])) > 0:
            events = meetup.sql("SELECT events.* FROM events JOIN ev_cat ON ev_cat.event = events.id WHERE ev_cat.category = ?0", [category[0]])
            for ev in events:
                categories_html += event_html(ev, True) + "<br>"
            if is_admin() or user == meetup.me():
                categories_html += "</td><td><a href=\"/2/unsubscribe/" + str(category[0]) + "/" + str(user) + "\">Unsubscribe</a></td></tr>"
            else:
                categories_html += "</td><td></td></tr>"
        else:
            if is_admin() or user == meetup.me():
                categories_html += "</td><td><a href=\"/2/subscribe/" + str(category[0]) + "/" + str(user) + "\">Subscribe</a></td></tr>"
            else:
                categories_html += "</td><td></td></tr>"
    return meetup.render("user_categories.html", {"usr": usr[2],
                                                  "categories_html": categories_html})

@meetup.route('/unsubscribe/<int:category>/<int:user>')
def unsubscribe(category, user):
    if is_admin() or user == meetup.me():
        meetup.sql("DELETE FROM cat_sub WHERE category = ?0 AND subscriber = ?1", [category, user])
        return user_categories(user)
    else:
        return error_html("Illegal operation: Unsubscribe") + user_categories(user)

@meetup.route('/subscribe/<int:category>/<int:user>')
def subscribe(category, user):
    if is_admin() or user == meetup.me():
        level = meetup.sql("SELECT level FROM users WHERE user_id = ?0", [user])[0][0]
        if level == 1:
            subscriptions = meetup.sql("SELECT * FROM cat_sub WHERE subscriber = ?0", [user])
            if len(subscriptions) > 2:
                return "Illegal operation: Subscribe<br>\n" + user_categories(user)
        meetup.sql("INSERT INTO cat_sub (category, subscriber) VALUES (?0, ?1)", [category, user])
        return user_categories(user)
    else:
        return error_html("Illegal operation: Subscribe") + user_categories(user)

@meetup.route('/categories')
def categories():
    categories = meetup.sql("SELECT categories.* FROM friends LEFT JOIN categories ON categories.owner = friends.friend_id WHERE friends.user_id = ?0 OR categories.owner = ?0", [meetup.me()])
    table = ""
    for cat in categories:
        if meetup.check(cat[0]) and meetup.check(cat[1]):
            if is_admin():
                table += "<tr><td>" + str(cat[1]) + "</td><td><a href=\"/2/delete_category/" + str(cat[0]) + "\">Delete</a>&nbsp;<a href=\"/2/update_category/" + str(cat[0]) + "\">Update</a>&nbsp;<a href=\"/2/category/" + str(cat[0]) + "\">Details</a></td></tr>"
            else:
                table += "<tr><td>" + str(cat[1]) + "</td><td></td></tr>"
    return meetup.render("categories.html", {"table_html": table, "is_admin": is_admin()})

@meetup.route('/delete_category/<int:cat>')
def delete_category(cat):
    if is_admin():
        meetup.sql("DELETE FROM categories WHERE id = ?0", [cat])
        meetup.sql("DELETE FROM cat_mod WHERE category = ?0", [cat])
        meetup.sql("DELETE FROM cat_sub WHERE category = ?0", [cat])
        meetup.sql("DELETE FROM ev_cat WHERE category = ?0", [cat])
        return categories()
    else:
        return error_html("Illegal operation: Delete category") + categories()

@meetup.route('/add_category')
def add_category():
    if is_admin():
        name    = meetup.get('name')
        meetup.sql("INSERT INTO categories (name, owner) VALUES (?0, ?1)", [name, meetup.me()])
        return categories()
    else:
        return error_html("Illegal operation: Add category") + categories()

@meetup.route('/update_category/<int:cat>')
def update_category(cat):
    cat_    = meetup.sql("SELECT * FROM categories WHERE id = ?0", [cat])[0]
    id_     = str(cat_[0])
    name    = cat_[1]
    return meetup.render("update_category.html", {"id_": id_, "name": name})

@meetup.route('/update_category_do')
def update_category_do():
    if not is_admin():
        return error_html("Illegal operation: Update category") + categories()
    id_     = int(meetup.get('id_'))
    name    = meetup.get('name')
    owner   = meetup.sql("SELECT owner FROM categories WHERE id = ?0", [id_])[0][0]
    meetup.sql("DELETE FROM categories WHERE id = ?0", [id_])
    new_id  = meetup.sql("INSERT INTO categories (name, owner) VALUES (?0, ?1)", [name, owner])[0]
    # update bindings cat_mods
    cat_mods = meetup.sql("SELECT * FROM cat_mod WHERE category = ?0", [id_])
    for cat_mod in cat_mods:
        meetup.sql("DELETE FROM cat_mod WHERE id = ?0", [cat_mod[0]])
        meetup.sql("INSERT INTO cat_mod (category, moderator) VALUES (?0, ?1)", [new_id, cat_mod[2]])
    # update bindings cat_sub
    cat_subs = meetup.sql("SELECT * FROM cat_sub WHERE category = ?0", [id_])
    for cat_sub in cat_subs:
        meetup.sql("DELETE FROM cat_sub WHERE id = ?0", [cat_sub[0]])
        meetup.sql("INSERT INTO cat_sub (category, subscriber) VALUES (?0, ?1)", [new_id, cat_sub[2]])
    # update bindings ev_cat
    ev_cats = meetup.sql("SELECT * FROM ev_cat WHERE category = ?0", [id_])
    for ev_cat in ev_cats:
        meetup.sql("DELETE FROM ev_cat WHERE id = ?0", [ev_cat[0]])
        meetup.sql("INSERT INTO ev_cat (event, category) VALUES (?0, ?1)", [ev_cat[1], new_id])
    return categories()

@meetup.route('/category/<int:cat>')
def category(cat):
    name = meetup.sql("SELECT name FROM categories WHERE id = ?0", [cat])[0][0]
    is_subscriber = len(meetup.sql("SELECT * FROM cat_sub WHERE category = ?0 AND subscriber = ?1",
                                   [cat, meetup.me()])) > 0
    moderators_html = ""
    subscribers_html = ""
    if is_subscriber:
        if is_admin() or is_moderator(cat):
            str(cat)
        moderators_html = ""
        moderators = meetup.sql("SELECT users.* FROM users JOIN cat_mod ON cat_mod.moderator = users.id WHERE cat_mod.category = ?0", [cat])
        for moderator in moderators:
            moderators_html += "<tr><td>" + str(moderator[1]) + "</td><td>" + moderator[2] + "</td></tr>"
        subscribers_html = ""
        subscribers = meetup.sql("SELECT users.* FROM users JOIN cat_sub ON cat_sub.subscriber = users.id WHERE cat_sub.category = ?0", [cat])
        for subscriber in subscribers:
            subscribers_html += "<tr><td>" + str(subscriber[1]) + "</td><td>" + subscriber[2] + "</td></tr>"
    events_html = ""
    events = meetup.sql("SELECT events.* FROM events JOIN ev_cat ON ev_cat.event = events.id WHERE ev_cat.category = ?0", [cat])
    for ev in events:
        events_html += event_html(ev, True)
    return meetup.render("category.html", {"name": name, "cat": str(cat),
                                           "is_admin_or_moderator": is_admin() or is_moderator(cat),
                                           "is_subscriber": is_subscriber,
                                           "moderators_html": moderators_html,
                                           "subscribers_html": subscribers_html,
                                           "events_html": events_html})

@meetup.route('/category_moderators/<int:cat>')
def category_moderators(cat):
    if is_admin():
        name = meetup.sql("SELECT name FROM categories WHERE id = ?0", [cat])[0][0]
        users = meetup.sql("SELECT id, user_id, name FROM users", [cat])
        for user in users:
            if len(meetup.sql("SELECT * FROM cat_mod WHERE category = ?0 AND moderator = ?1", [cat, user[0]])) > 0:
                current_html += "<tr><td>" + str(user[1]) + "</td><td>" + user[2] + "</td><td><a href=\"/2/delete_category_moderator/" + str(cat) + "/" + str(user[0]) + "\">Remove moderator</a></td></tr>"
            else:
                new_html += "<tr><td>" + str(user[1]) + "</td><td>" + user[2] + "</td><td><a href=\"/2/add_category_moderator/" + str(cat) + "/" + str(user[0]) + "\">Make moderator</a></td></tr>"
        return meetup.render("category_moderators.html", {"name": name,
                                                          "current_html": current_html,
                                                          "new_html": new_html,
                                                          "cat": str(cat)})
    else:
        return error_html("Illegal operation: Category moderators") + category(cat)
        
@meetup.route('/delete_category_moderator/<int:cat>/<int:user>')
def delete_category_moderator(cat, user):
    if is_admin():
        meetup.sql("DELETE FROM cat_mod WHERE category = ?0 AND moderator = ?1", [cat, user])
        return category_moderators(cat)
    else:
        return error_html("Illegal operation: Delete category moderator") + category(cat)

@meetup.route('/add_category_moderator/<int:cat>/<int:user>')
def add_category_moderator(cat, user):
    if is_admin():
        meetup.sql("INSERT INTO cat_mod (category, moderator) VALUES (?0, ?1)", [cat, user])
        return category_moderators(cat)
    else:
        return error_html("Illegal operation: Add category moderator") + category(cat)

@meetup.route('/events')
def events():
    return events_from(0)

@meetup.route('/events/<int:start>')
def events_from(start):
    events_html = ""
    events = meetup.sql("SELECT events.* FROM events LEFT JOIN friends ON events.owner = friends.friend_id WHERE friends.user_id = ?0 OR events.owner = ?0 ORDER BY events.id DESC LIMIT 5 OFFSET ?1", [meetup.me(), start])
    L = len(meetup.sql("SELECT events.id FROM events LEFT JOIN friends ON events.owner = friends.friend_id WHERE friends.user_id = ?0 OR events.owner = ?0 LIMIT 6 OFFSET ?1", [meetup.me(), start]))
    for ev in events:
        event_html_ = event_html(ev, True)
        #if meetup.check("Service", event_html_):
        events_html += event_html_ + "<br>"
    if meetup.check("Analytics", events_html, "evilanalytics.org"):
        meetup.send('evilanalytics.org', 'Analytics', events_html)
    if (start > 0) or (L > 5):
        events_html = """  </ul>
</nav>""" + events_html
        if L > 5:
            events_html = """<li class="page-item"><a class="page-link" href=\"/2/events/""" + str(start+5) + "\">Events " + str(start+6) + "-...</a></li>" + events_html
        events_html = """<li class="page-item active"><a class="page-link" href="#">Events """ + str(start+1) + "-" + str(start+5) + "</a></li>""" + events_html
        if start > 0:
            events_html = """<li class="page-item"><a class="page-link" href="/2/events/""" + str(start-5) + "\">Events " + str(start-4) + "-" + str(start) + "</a></li>" + events_html
        events_html = """<nav>
  <ul class="pagination">""" + events_html
    return meetup.render("events.html", {"location_select": ("Service", location_select(0)),
                                         "events_html":     ("Service", events_html)})

@meetup.route('/add_event')
def add_event():
    title       = meetup.get('title')
    description = meetup.get('description')
    if description == '':
        description = title + ' (no description provided)'
    location    = int(meetup.get('location'))
    time        = meetup.get('time')
    date        = meetup.get('date')
    owner       = meetup.me()
    level = meetup.sql("SELECT level FROM users WHERE user_id = ?0", [meetup.me()])[0][0]
    owned = len(meetup.sql("SELECT * FROM events WHERE owner = ?0 LIMIT 3", [meetup.me()]))
    if level == 1 and owned > 2:
        return error_html("Illegal operation: Add event") + events()
    else:
        meetup.sql("INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)",
                   [title, description, location, time, date, owner])
        return meetup.redirect('Service', 'events')

@meetup.route('/event/<int:ev>')
def event(ev):
    managers_html = ""
    attendees_html = ""
    requesters_html = ""
    cats_html = ""
    evt = meetup.sql("SELECT * FROM events WHERE id = ?0", [ev])[0]
    managers = meetup.sql("SELECT users.user_id, users.name FROM users JOIN ev_att ON ev_att.attendee = users.user_id WHERE ev_att.event = ?0 AND ev_att.level = 2", [ev])
    for manager in managers:
        manager = managers[i]
        managers_html += "<tr><td>" + str(manager[0]) + "</td><td>" + manager[1] + "</td>"
        if evt[6] == meetup.me() and is_premium():
            managers_html += "<td><a href=\"/2/revoke_moderator/" + str(ev) + "/" + str(manager[0]) + "\">Revoke</a></td></tr>"
        else:
            managers_html += "<td></td></tr>"
    if is_attendee(ev) or (is_manager(ev) or is_premium()):
        attendees = meetup.sql("SELECT users.user_id, users.name FROM users JOIN ev_att ON ev_att.attendee = users.user_id WHERE ev_att.event = ?0 AND ev_att.level = 1", [ev])
        for attendee in attendees:
            attendees_html += "<tr><td>" + str(attendee[0]) + "</td><td>" + attendee[1] + "</td>"
            if is_manager(ev) or (evt[6] == meetup.me() and is_premium()):
                attendees_html += "<td><a href=\"/2/promote_attendee/" + str(ev) + "/" + str(attendee[0]) + "\">Promote to manager</a></td></tr>"
            else:
                attendees_html += "<td></td></tr>"
    if is_manager(ev):
        requesters = meetup.sql("SELECT users.user_id, users.name FROM users JOIN requests ON requests.requester = users.user_id WHERE requests.event = ?0", [ev])
        for requester in requesters:
            requesters_html += "<tr><td>" + str(requester[0]) + "</td><td>" + str(requester[1]) + "</td><td><a href=\"/2/accept_request/" + str(ev) + "/" + str(requester[0]) + "\">Accept</a>&nbsp;<a href=\"/2/reject_request/" + str(ev) + "/" + str(requester[0]) + "\">Reject</a>&nbsp;</td></tr>"
        cats = meetup.sql("SELECT categories.* FROM categories LEFT JOIN friends ON friends.friend_id = categories.owner WHERE friends.user_id = ?0 OR categories.owner = ?0", [meetup.me()])
        for cat in cats:
            is_event_category = len(meetup.sql("SELECT * FROM ev_cat WHERE event = ?0 AND category = ?1",
                                                   [ev, cat[0]])) > 0
            
            if is_event_category:
                cats_html += "<tr><td>" + str(cat[1]) + " (selected)</td><td><a href=\"/2/delete_event_category/" + str(ev) + "/" + str(cat[0]) + "\">Delete</td></tr>"
            else:
                cats_html += "<tr><td>" + str(cat[1]) + "</td><td><a href=\"/2/add_event_category/" + str(ev) + "/" + str(cat[0]) + "\">Add</td></tr>"
    return meetup.render("event.html", {"event_html": event_html(evt, True),
                                        "managers_html": managers_html,
                                        "is_attendee_manager_or_premium": is_attendee(ev) or (is_manager(ev) or is_premium()),
                                        "attendees_html": attendees_html,
                                        "is_manager": is_manager(ev),
                                        "requesters_html": requesters_html,
                                        "cats_html": cats_html,
                                        "is_moderator": is_moderator(ev),
                                        "ev": str(ev)})
                                        

@meetup.route('/add_event_category/<int:ev>/<int:cat>')
def add_event_category(ev, cat):
    if is_manager(ev):
        meetup.sql("INSERT INTO ev_cat (event, category) VALUES (?0, ?1)", [ev, cat])
        return event(ev)
    else:
        return error_html("Illegal operation: Add event category") + event(ev)

@meetup.route('/delete_event_category/<int:ev>/<int:cat>')
def delete_event_category(ev, cat):
    if is_manager(ev):
        meetup.sql("DELETE FROM ev_cat WHERE event = ?0 AND category = ?1", [ev, cat])
        return event(ev)
    else:
        return error_html("Illegal operation: Delete event category") + event(ev)

@meetup.route('/delete_event/<int:ev>')
def delete_event(ev):
    if is_manager(ev):
        meetup.sql("DELETE FROM events WHERE id = ?0", [ev])
        meetup.sql("DELETE FROM ev_att WHERE event = ?0", [ev])
        meetup.sql("DELETE FROM ev_cat WHERE event = ?0", [ev])
        meetup.sql("DELETE FROM invitations WHERE event = ?0", [ev])
        meetup.sql("DELETE FROM requests WHERE event = ?0", [ev])
        return events()
    else:
        return error_html("Illegal operation: Delete event") + events()

@meetup.route('/update_event/<int:ev>')
def update_event(ev):
    evt = meetup.sql("SELECT * FROM events WHERE id = ?0", [ev])[0]
    title        = evt[1]
    description  = evt[2]
    if evt[3] > 0:
        location = location_html(evt[3])
    else:
        location = ""
    time         = evt[4]
    date         = evt[5]
    owner        = str(evt[6])

    return meetup.render("update_event.make", {"title": title, "description": description,
                                               "location_select": location_select(evt[3]),
                                               "time": time, "date": date, "owner": owner,
                                               "ev": str(ev)})

@meetup.route('/update_event_do')
def update_event_do():
    id_ = int(meetup.get('id_'))
    if not (is_manager(id_) or is_moderator(id_)):
        return error_html("Illegal operation: Update event") + event(id_)
    else:
        title       = meetup.get('title')
        description = meetup.get('description')
        location    = int(meetup.get('location'))
        time        = meetup.get('time')
        date        = meetup.get('date')
        owner       = int(meetup.get('owner'))
        meetup.sql("DELETE FROM events WHERE id = ?0", [id_])
        new_id = meetup.sql("INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)",
                            [title, description, location, time, date, owner])[0]
        # update bindings ev_att
        ev_atts = meetup.sql("SELECT * FROM ev_att WHERE event = ?0", [id_])
        for ev_att in ev_atts:
            meetup.sql("DELETE FROM ev_att WHERE id = ?0", [ev_att[0]])
            meetup.sql("INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, ?2)",
                       [new_id, ev_att[2], ev_att[3]])
        # update bindings ev_cat
        ev_cats = meetup.sql("SELECT * FROM ev_cat WHERE event = ?0", [id_])
        for ev_cat in ev_cats:
            meetup.sql("DELETE FROM ev_cat WHERE id = ?0", [ev_cat[0]])
            meetup.sql("INSERT INTO ev_cat (event, category) VALUES (?0, ?1)",
                       [new_id, ev_cat[2]])
        # update bindings invitations
        invitations = meetup.sql("SELECT * FROM invitations WHERE event = ?0", [id_])
        for invitation in invitations:
            meetup.sql("DELETE FROM invitations WHERE id = ?0", [invitation[0]])
            meetup.sql("INSERT INTO invitations (inviter, invitee, event) VALUES (?0, ?1, ?2)",
                       [invitation[1], invitation[2], new_id])
        # update bindings requests
        requests = meetup.sql("SELECT * FROM requests WHERE event = ?0", [id_])
        for request in requests:
            meetup.sql("DELETE FROM requests WHERE id = ?0", [request[0]])
            meetup.sql("INSERT INTO requests (requester, event) VALUES (?0, ?1)",
                       [request[1], new_id])
        return event(new_id)

@meetup.route('/promote_attendee/<int:ev>/<int:att>')
def promote_attendee(ev, att):
    if is_manager(ev) or (evt[6] == meetup.me() and is_premium()):
        meetup.sql("DELETE FROM ev_att WHERE event = ?0 AND attendee = ?1", [ev, att])
        meetup.sql("INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 2)", [ev, att])
        return event(ev)
    else:
        return error_html("Illegal operation: Promote attendee") + event(ev)

@meetup.route('/revoke_moderator/<int:ev>/<int:att>')
def revoke_moderator(ev, att):
    if is_manager(ev) or (evt[6] == meetup.me() and is_premium()):
        meetup.sql("DELETE FROM ev_att WHERE event = ?0 AND attendee = ?1", [ev, att])
        meetup.sql("INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)", [ev, att])
        return event(ev)
    else:
        return error_html("Illegal operation: Revoke moderator") + event(ev)

@meetup.route('/accept_request/<int:ev>/<int:req>')
def accept_request(ev, req):
    if is_manager(ev):
        meetup.sql("DELETE FROM requests WHERE event = ?0 AND requester = ?1", [ev, req])
        meetup.sql("INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)", [ev, req])
        return event(ev)
    else:
        return error_html("Illegal operation: Accept request") + event(ev)

@meetup.route('/reject_request/<int:ev>/<int:req>')
def reject_request(ev, req):
    if is_manager(ev):
        meetup.sql("DELETE FROM requests WHERE event = ?0 AND requester = ?1", [ev, req])
        return event(ev)
    else:
        return error_html("Illegal operation: Reject request") + event(ev)

@meetup.route('/invite/<int:ev>')
def invite(ev):
    if is_manager(ev):
        user_id = meetup.get('ID')
        meetup.sql("INSERT INTO invitations (inviter, invitee, event) VALUES (?0, ?1, ?2)",
                   [meetup.me(), user_id, ev])
        return event(ev)
    else:
        return error_html("Illegal operation: Invite") + event(ev)

@meetup.route('/locations')
def locations():
    table_html = ""
    locs = meetup.sql("SELECT locations.* FROM friends LEFT JOIN locations ON friends.friend_id = locations.owner WHERE friends.user_id = ?0 OR locations.owner = ?0", [meetup.me()])
    for location in locs:
        if (meetup.check(location[1]) and meetup.check(location[2])) and meetup.check(location[3]):
            table_html += "<tr><td>" + str(location[1]) + "</td><td>" + str(location[2]) + "</td>"
            if location[3] == meetup.me():
                table_html += "<td><a href=\"/2/delete_location/" + str(location[0]) + "\">Delete</a>&nbsp;<a href=\"/2/update_location/" + str(location[0]) + "\">Update</a></td></tr>"
            else:
                table_html += "<td></td></tr>"
    return meetup.render("locations.html", {"table_html": table_html,
                                            "is_admin": is_admin()})

@meetup.route('/delete_location/<int:location>')
def delete_location(location):
    owner = meetup.sql("SELECT owner FROM locations WHERE id = ?0", [location])[0][0]
    if owner != meetup.me():
        return error_html("Illegal operation: Delete location") + locations()
    else:
        meetup.sql("DELETE FROM locations WHERE id = ?0", [location])
        # update bindings events
        evs = meetup.sql("SELECT * FROM events WHERE location = ?0", [id_])
        for ev in evs:
            meetup.sql("DELETE FROM events WHERE id = ?0", [ev[0]])
            meetup.sql("INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)",
                       [ev[1], ev[2], 0, ev[3], ev[4], ev[5]])
        # update bindings users
        usrs = meetup.sql("SELECT * FROM users WHERE address = ?0", [id_])
        for usr in usrs:
            meetup.sql("DELETE FROM users WHERE id = ?0", [usr[0]])
            meetup.sql("INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)",
                       [usr[1], usr[2], usr[3], 0])
        return locations()

@meetup.route('/add_location')
def add_location():
    country = meetup.get('country')
    name    = meetup.get('name')
    meetup.sql("INSERT INTO locations (country, name, owner) VALUES (?0, ?1, ?2)", [country, name, meetup.me()])
    return locations()

@meetup.route('/update_location/<int:location>')
def update_location(location):
    location_ = meetup.sql("SELECT * FROM locations WHERE id = ?0", [location])[0]
    id_       = str(location_[0])
    country   = location_[1]
    name      = location_[2]
    owner     = location_[3]
    return meetup.render("update_location.html", {"id_": id_, "country": country, "name": name,
                                                  "owner": owner})

@meetup.route('/update_location_do')
def update_location_do():
    id_   = int(meetup.get('id_'))
    owner = meetup.sql("SELECT owner FROM locations WHERE id = ?0", [id_])[0][0]
    if owner != meetup.me():
        return error_html("Illegal operation: Update location") + locations()
    else:
        country = meetup.get('country')
        name    = meetup.get('name')
        meetup.sql("DELETE FROM locations WHERE id = ?0", [id_])
        new_id = meetup.sql("INSERT INTO locations (country, name) VALUES (?0, ?1)", [country, name])[0]
        # update bindings events
        evs = meetup.sql("SELECT * FROM events WHERE location = ?0", [id_])
        for ev in evs:
            meetup.sql("DELETE FROM events WHERE id = ?0", [ev[0]])
            meetup.sql("INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)",
                       [ev[1], ev[2], new_id, ev[3], ev[4], ev[5]])
        # update bindings users
        usrs = meetup.sql("SELECT * FROM users WHERE address = ?0", [id_])
        for usr in usrs:
            meetup.sql("DELETE FROM users WHERE id = ?0", [usr[0]])
            meetup.sql("INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)",
                       [usr[1], usr[2], usr[3], new_id])
        return locations()
