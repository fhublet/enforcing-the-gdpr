
from apps import meetup
from databank.imports import *
__stack__ = None

def main():
    global __stack__
    ___136 = meetup.render(Cell('index.html'))
    __r__ = Cell(___136, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/')
def _main():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = main()
    return (__c__, __r__, __s__, __trace__)

def location_html(location):
    global __stack__
    if ('location_' not in locals()):
        location_ = Cell(None)
    ___137 = meetup.sql(Cell('SELECT * FROM locations WHERE id = ?0'), Cell([location]))
    location_ = ___137[Cell(0)]
    location_.adopt(__stack__.all())
    ___138 = Cell((location_ == Cell(None)))
    __trace__.add_inputs(___138.inputs)
    __stack__.push()
    __stack__.add(___138.inputs, bot=True)
    if ___138:
        __r__ = Cell(Cell(''), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell((((location_[Cell(2)] + Cell(' (')) + location_[Cell(1)]) + Cell(')')), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def error_html(msg):
    global __stack__
    __r__ = Cell(((Cell('<div class="alert alert-danger" role="alert">') + msg) + Cell('</div>')), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def location_select(selected):
    global __stack__
    if ('sel' not in locals()):
        sel = Cell(None)
    if ('options' not in locals()):
        options = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    options = Cell('<option value="0"></option>')
    options.adopt(__stack__.all())
    ___140 = meetup.me()
    ___141 = meetup.sql(Cell('SELECT locations.* FROM locations INNER JOIN friends ON locations.owner = friends.friend_id WHERE friends.user_id = ?0 OR locations.owner = ?0'), Cell([___140]))
    locations = ___141
    locations.adopt(__stack__.all())
    ___142 = locations
    for location in ___142:
        ___144 = Cell((selected == location[Cell(0)]))
        __stack__.push()
        __stack__.add(___144.inputs)
        sel.adopt(__stack__.all())
        sel.adopt(___144.inputs)
        if ___144:
            sel = Cell(' selected="selected"')
            sel.adopt(__stack__.all())
        else:
            sel = Cell('')
            sel.adopt(__stack__.all())
        __stack__.pop()
        ___146 = db_str(location[Cell(0)])
        options = (options + ((((((((Cell('<option value="') + ___146) + Cell('"')) + sel) + Cell('>')) + location[Cell(2)]) + Cell(' (')) + location[Cell(1)]) + Cell(')</option>')))
        options.adopt(__stack__.all())
    __r__ = Cell(((Cell('<select name="location" id="location" class="form-select">') + options) + Cell('</select>')), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def event_html(event, details):
    global __stack__
    if ('attended' not in locals()):
        attended = Cell(None)
    if ('leave' not in locals()):
        leave = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('invitation' not in locals()):
        invitation = Cell(None)
    if ('time_date' not in locals()):
        time_date = Cell(None)
    if ('invitations' not in locals()):
        invitations = Cell(None)
    if ('html' not in locals()):
        html = Cell(None)
    title = event[Cell(1)]
    title.adopt(__stack__.all())
    description = event[Cell(2)]
    description.adopt(__stack__.all())
    ___147 = Cell((event[Cell(3)] > Cell(0)))
    __stack__.push()
    __stack__.add(___147.inputs)
    location.adopt(__stack__.all())
    location.adopt(___147.inputs)
    if ___147:
        ___149 = location_html(event[Cell(3)])[0]
        location = ___149
        location.adopt(__stack__.all())
    else:
        location = Cell('')
        location.adopt(__stack__.all())
    __stack__.pop()
    time_date = ((event[Cell(4)] + Cell(' ')) + event[Cell(5)])
    time_date.adopt(__stack__.all())
    ___150 = details
    __stack__.push()
    __stack__.add(___150.inputs)
    attended.adopt(__stack__.all())
    attended.adopt(___150.inputs)
    invitations.adopt(__stack__.all())
    invitations.adopt(___150.inputs)
    leave.adopt(__stack__.all())
    leave.adopt(___150.inputs)
    invitation.adopt(__stack__.all())
    invitation.adopt(___150.inputs)
    if ___150:
        ___152 = meetup.me()
        ___153 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([event[Cell(0)], ___152]))
        ___154 = db_len(___153)
        attended = ___154
        attended.adopt(__stack__.all())
        ___155 = Cell((attended > Cell(0)))
        __stack__.push()
        __stack__.add(___155.inputs)
        leave.adopt(__stack__.all())
        leave.adopt(___155.inputs)
        if ___155:
            ___157 = db_str(event[Cell(0)])
            leave = ((Cell('<tr><td colspan"2"><a href="/2/leave_event/') + ___157) + Cell('">Leave</a></td></tr>'))
            leave.adopt(__stack__.all())
        else:
            ___158 = db_str(event[Cell(0)])
            leave = ((Cell('<tr><td colspan"2"><a href="/2/request_attendance/') + ___158) + Cell('">Request attendance</a></td></tr>'))
            leave.adopt(__stack__.all())
        __stack__.pop()
        ___159 = meetup.me()
        ___160 = meetup.sql(Cell('SELECT * FROM invitations WHERE invitee = ?0 AND event = ?1'), Cell([___159, event[Cell(0)]]))
        invitations = ___160
        invitations.adopt(__stack__.all())
        ___161 = db_len(invitations)
        ___162 = Cell((___161 > Cell(0)))
        __stack__.push()
        __stack__.add(___162.inputs)
        leave.adopt(__stack__.all())
        leave.adopt(___162.inputs)
        invitation.adopt(__stack__.all())
        invitation.adopt(___162.inputs)
        if ___162:
            invitation = invitations[Cell(0)][Cell(0)]
            invitation.adopt(__stack__.all())
            ___164 = db_str(invitation)
            ___165 = db_str(invitation)
            leave = (leave + ((((Cell('<tr><td colspan"2"><a href="/2/accept_invitation/') + ___164) + Cell('">Accept</a>&nbsp;<a href="/2/reject_invitation/')) + ___165) + Cell('">Reject</a></td></tr>')))
            leave.adopt(__stack__.all())
        __stack__.pop()
    else:
        leave = Cell('')
        leave.adopt(__stack__.all())
    __stack__.pop()
    ___166 = db_str(event[Cell(0)])
    leave = (leave + ((Cell('<tr><td colspan"2"><a href="/2/event/') + ___166) + Cell('">Details</a></td></tr>')))
    leave.adopt(__stack__.all())
    html = ((((((((((Cell('<table class="table"><thead><tr><th colspan="2">') + title) + Cell('</th></tr></thead><tbody><tr><td>Description</td><td>')) + description) + Cell('</td></tr><tr><td>Location</td><td>')) + location) + Cell('</td></tr><tr><td>Time/date</td><td>')) + time_date) + Cell('</td></tr>')) + leave) + Cell('</tbody></table>'))
    html.adopt(__stack__.all())
    __r__ = Cell(html, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_admin():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    ___167 = meetup.me()
    ___168 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([___167]))
    level = ___168[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    __r__ = Cell(Cell((level == Cell(3))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_premium():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    ___169 = meetup.me()
    ___170 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([___169]))
    level = ___170[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    __r__ = Cell(Cell((level >= Cell(2))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_registered():
    global __stack__
    ___171 = meetup.me()
    ___172 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([___171]))
    ___173 = db_len(___172)
    __r__ = Cell(Cell((___173 > Cell(0))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_moderator(ev):
    global __stack__
    ___174 = meetup.me()
    ___175 = meetup.sql(Cell('SELECT cat_mod.* FROM cat_mod JOIN ev_cat ON ev_cat.category = cat_mod.category WHERE cat_mod.moderator = ?0 AND ev_cat.event = ?1'), Cell([___174, ev]))
    ___176 = db_len(___175)
    __r__ = Cell(Cell((___176 > Cell(0))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_manager(ev):
    global __stack__
    ___177 = meetup.me()
    ___178 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1 AND level = 2'), Cell([ev, ___177]))
    ___179 = db_len(___178)
    ___180 = meetup.sql(Cell('SELECT owner FROM events WHERE id = ?0'), Cell([ev]))
    ___181 = meetup.me()
    __r__ = Cell(Cell((Cell((___179 > Cell(0))).value or Cell((___180[Cell(0)][Cell(0)] == ___181)).value), inputs=(Cell((___179 > Cell(0))).inputs + Cell((___180[Cell(0)][Cell(0)] == ___181)).inputs)), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_attendee(ev):
    global __stack__
    ___182 = meetup.me()
    ___183 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([ev, ___182]))
    ___184 = db_len(___183)
    ___185 = meetup.sql(Cell('SELECT owner FROM events WHERE id = ?0'), Cell([ev]))
    ___186 = meetup.me()
    __r__ = Cell(Cell((Cell((___184 > Cell(0))).value or Cell((___185[Cell(0)][Cell(0)] == ___186)).value), inputs=(Cell((___184 > Cell(0))).inputs + Cell((___185[Cell(0)][Cell(0)] == ___186)).inputs)), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def profile():
    global __stack__
    if ('inviter' not in locals()):
        inviter = Cell(None)
    if ('managed_event' not in locals()):
        managed_event = Cell(None)
    if ('managed_events' not in locals()):
        managed_events = Cell(None)
    if ('managed_events_html' not in locals()):
        managed_events_html = Cell(None)
    if ('owned_events' not in locals()):
        owned_events = Cell(None)
    if ('attended_events_html' not in locals()):
        attended_events_html = Cell(None)
    if ('attended_event' not in locals()):
        attended_event = Cell(None)
    if ('owned_events_html' not in locals()):
        owned_events_html = Cell(None)
    if ('owned_event' not in locals()):
        owned_event = Cell(None)
    if ('invited_events_html' not in locals()):
        invited_events_html = Cell(None)
    if ('attended_events' not in locals()):
        attended_events = Cell(None)
    if ('invited_event' not in locals()):
        invited_event = Cell(None)
    if ('invited_events' not in locals()):
        invited_events = Cell(None)
    ___187 = is_registered()[0]
    ___188 = ___187
    __trace__.add_inputs(___188.inputs)
    __stack__.push()
    __stack__.add(___188.inputs, bot=True)
    invited_events_html.adopt(__stack__.all())
    invited_events_html.adopt(___188.inputs)
    owned_events_html.adopt(__stack__.all())
    owned_events_html.adopt(___188.inputs)
    inviter.adopt(__stack__.all())
    inviter.adopt(___188.inputs)
    invited_events.adopt(__stack__.all())
    invited_events.adopt(___188.inputs)
    attended_events_html.adopt(__stack__.all())
    attended_events_html.adopt(___188.inputs)
    owned_events.adopt(__stack__.all())
    owned_events.adopt(___188.inputs)
    attended_events.adopt(__stack__.all())
    attended_events.adopt(___188.inputs)
    managed_events_html.adopt(__stack__.all())
    managed_events_html.adopt(___188.inputs)
    managed_events.adopt(__stack__.all())
    managed_events.adopt(___188.inputs)
    if ___188:
        owned_events_html = Cell('')
        owned_events_html.adopt(__stack__.all())
        ___190 = meetup.me()
        ___191 = meetup.sql(Cell('SELECT * FROM events WHERE owner = ?0'), Cell([___190]))
        owned_events = ___191
        owned_events.adopt(__stack__.all())
        ___192 = owned_events
        for owned_event in ___192:
            ___194 = event_html(owned_event, Cell(False))[0]
            owned_events_html = (owned_events_html + (Cell('<br>') + ___194))
            owned_events_html.adopt(__stack__.all())
        managed_events_html = Cell('')
        managed_events_html.adopt(__stack__.all())
        ___195 = meetup.me()
        ___196 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_att ON events.id = ev_att.event WHERE ev_att.attendee = ?0 AND ev_att.level = 2'), Cell([___195]))
        managed_events = ___196
        managed_events.adopt(__stack__.all())
        ___197 = managed_events
        for managed_event in ___197:
            ___199 = event_html(managed_event, Cell(False))[0]
            managed_events_html = (managed_events_html + (Cell('<br>') + ___199))
            managed_events_html.adopt(__stack__.all())
        attended_events_html = Cell('')
        attended_events_html.adopt(__stack__.all())
        ___200 = meetup.me()
        ___201 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_att ON events.id = ev_att.event WHERE ev_att.attendee = ?0 AND ev_att.level = 1'), Cell([___200]))
        attended_events = ___201
        attended_events.adopt(__stack__.all())
        ___202 = attended_events
        for attended_event in ___202:
            ___204 = event_html(attended_event, Cell(False))[0]
            attended_events_html = (attended_events_html + (Cell('<br>') + ___204))
            attended_events_html.adopt(__stack__.all())
        invited_events_html = Cell('')
        invited_events_html.adopt(__stack__.all())
        ___205 = meetup.me()
        ___206 = meetup.sql(Cell('SELECT events.* FROM events JOIN invitations ON events.id = invitations.event WHERE invitations.invitee = ?0'), Cell([___205]))
        invited_events = ___206
        invited_events.adopt(__stack__.all())
        ___207 = invited_events
        for invited_event in ___207:
            ___209 = meetup.sql(Cell('SELECT name FROM users WHERE user_id = ?0'), Cell([invited_event[Cell(6)]]))
            inviter = ___209[Cell(0)][Cell(0)]
            inviter.adopt(__stack__.all())
            ___210 = event_html(invited_event, Cell(True))[0]
            invited_events_html = (invited_events_html + (((Cell('<br><strong>') + inviter) + Cell('</strong> invited you to ')) + ___210))
            invited_events_html.adopt(__stack__.all())
        __k__ = [Cell('is_registered'), Cell('owned_events_html'), Cell('managed_events_html'), Cell('attended_events_html'), Cell('invited_events_html')]
        ___211 = meetup.render(Cell('profile.html'), Cell({
            __k__[0].value: Cell(True),
            __k__[1].value: owned_events_html,
            __k__[2].value: managed_events_html,
            __k__[3].value: attended_events_html,
            __k__[4].value: invited_events_html,
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
        __r__ = Cell(___211, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('is_registered')]
        ___212 = meetup.render(Cell('profile.html'), Cell({
            __k__[0].value: Cell(False),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___212, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/profile')
def _profile():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = profile()
    return (__c__, __r__, __s__, __trace__)

def leave_event(ev):
    global __stack__
    ___213 = meetup.me()
    ___214 = meetup.sql(Cell('DELETE FROM ev_att WHERE attendee = ?0 AND event = ?1 AND level = 1'), Cell([___213, ev]))
    ___215 = profile()[0]
    __r__ = Cell(___215, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/leave_event/<int:ev>')
def _leave_event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('leave_event', 'ev', ev)
    (__r__, __s__) = leave_event(ev)
    return (__c__, __r__, __s__, __trace__)

def request_attendance(ev):
    global __stack__
    ___216 = meetup.me()
    ___217 = meetup.sql(Cell('INSERT INTO requests (requester, event) VALUES (?0, ?1)'), Cell([___216, ev]))
    ___218 = events()[0]
    __r__ = Cell(___218, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/request_attendance/<int:ev>')
def _request_attendance(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('request_attendance', 'ev', ev)
    (__r__, __s__) = request_attendance(ev)
    return (__c__, __r__, __s__, __trace__)

def reject_invitation(invitation):
    global __stack__
    ___219 = meetup.me()
    ___220 = meetup.sql(Cell('DELETE FROM invitations WHERE invitee = ?0 AND id = ?1'), Cell([___219, invitation]))
    ___221 = profile()[0]
    __r__ = Cell(___221, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/reject_invitation/<int:invitation>')
def _reject_invitation(invitation):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    invitation = meetup.register('reject_invitation', 'invitation', invitation)
    (__r__, __s__) = reject_invitation(invitation)
    return (__c__, __r__, __s__, __trace__)

def accept_invitation(invitation):
    global __stack__
    if ('event_id' not in locals()):
        event_id = Cell(None)
    ___222 = meetup.me()
    ___223 = meetup.sql(Cell('SELECT event FROM invitations WHERE invitee = ?0 AND id = ?1'), Cell([___222, invitation]))
    event_id = ___223
    event_id.adopt(__stack__.all())
    event_id = event_id[Cell(0)][Cell(0)]
    event_id.adopt(__stack__.all())
    ___224 = meetup.me()
    ___225 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)'), Cell([event_id, ___224]))
    ___226 = meetup.me()
    ___227 = meetup.sql(Cell('DELETE FROM invitations WHERE invitee = ?0 AND id = ?1'), Cell([___226, invitation]))
    ___228 = profile()[0]
    __r__ = Cell(___228, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/accept_invitation/<int:invitation>')
def _accept_invitation(invitation):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    invitation = meetup.register('accept_invitation', 'invitation', invitation)
    (__r__, __s__) = accept_invitation(invitation)
    return (__c__, __r__, __s__, __trace__)

def unregister():
    global __stack__
    ___229 = meetup.me()
    ___230 = meetup.sql(Cell('DELETE FROM users WHERE user_id = ?0'), Cell([___229]))
    ___231 = meetup.me()
    ___232 = meetup.sql(Cell('DELETE FROM cat_mod WHERE moderator = ?0'), Cell([___231]))
    ___233 = meetup.me()
    ___234 = meetup.sql(Cell('DELETE FROM cat_sub WHERE subscriber = ?0'), Cell([___233]))
    ___235 = meetup.me()
    ___236 = meetup.sql(Cell('DELETE FROM ev_att WHERE attendee = ?0'), Cell([___235]))
    ___237 = meetup.me()
    ___238 = meetup.sql(Cell('DELETE FROM events WHERE owner = ?0'), Cell([___237]))
    ___239 = meetup.me()
    ___240 = meetup.sql(Cell('DELETE FROM friends WHERE user_id = ?0 OR friend_id = ?0'), Cell([___239]))
    ___241 = meetup.me()
    ___242 = meetup.sql(Cell('DELETE FROM invitations WHERE inviter = ?0 OR invitee = ?0'), Cell([___241]))
    ___243 = meetup.me()
    ___244 = meetup.sql(Cell('DELETE FROM locations WHERE owner = ?0'), Cell([___243]))
    ___245 = meetup.me()
    ___246 = meetup.sql(Cell('DELETE FROM requests WHERE requester = ?0'), Cell([___245]))
    ___247 = profile()[0]
    __r__ = Cell(___247, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/unregister')
def _unregister():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = unregister()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('address' not in locals()):
        address = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    if ('ID' not in locals()):
        ID = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___248 = is_registered()[0]
    ___249 = non(___248)
    __trace__.add_inputs(___249.inputs)
    __stack__.push()
    __stack__.add(___249.inputs, bot=True)
    name.adopt(__stack__.all())
    name.adopt(___249.inputs)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___249.inputs)
    level.adopt(__stack__.all())
    level.adopt(___249.inputs)
    ID.adopt(__stack__.all())
    ID.adopt(___249.inputs)
    address.adopt(__stack__.all())
    address.adopt(___249.inputs)
    if ___249:
        ___251 = meetup.me()
        ID = ___251
        ID.adopt(__stack__.all())
        ___252 = meetup.get('register', Cell('name'))
        name = ___252
        name.adopt(__stack__.all())
        level = Cell(1)
        level.adopt(__stack__.all())
        ___253 = meetup.get('register', Cell('location'))
        ___254 = db_int(___253)
        address = ___254
        address.adopt(__stack__.all())
        ___255 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([ID, name, level, address]))
    __stack__.pop()
    ___256 = profile()[0]
    __r__ = Cell(___256, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/register')
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def users():
    global __stack__
    if ('i' not in locals()):
        i = Cell(None)
    if ('table' not in locals()):
        table = Cell(None)
    if ('friends' not in locals()):
        friends = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    ___257 = is_registered()[0]
    ___258 = ___257
    __trace__.add_inputs(___258.inputs)
    __stack__.push()
    __stack__.add(___258.inputs, bot=True)
    friends.adopt(__stack__.all())
    friends.adopt(___258.inputs)
    table.adopt(__stack__.all())
    table.adopt(___258.inputs)
    i.adopt(__stack__.all())
    i.adopt(___258.inputs)
    if ___258:
        ___260 = meetup.me()
        ___261 = meetup.sql(Cell('SELECT users.* FROM friends LEFT JOIN users ON friends.friend_id = users.user_id WHERE friends.user_id = ?0 OR users.user_id = ?0'), Cell([___260]))
        friends = ___261
        friends.adopt(__stack__.all())
        table = Cell('')
        table.adopt(__stack__.all())
        ___262 = friends
        for user in ___262:
            ___264 = meetup.check('users', __c__, __trace__, user[Cell(1)])
            ___265 = meetup.check('users', __c__, __trace__, user[Cell(2)])
            ___266 = meetup.check('users', __c__, __trace__, user[Cell(3)])
            ___267 = meetup.check('users', __c__, __trace__, Cell((Cell((___264.value and ___265.value), inputs=(___264.inputs + ___265.inputs)).value and ___266.value), inputs=(Cell((___264.value and ___265.value), inputs=(___264.inputs + ___265.inputs)).inputs + ___266.inputs)))
            ___268 = meetup.check('users', __c__, __trace__, user[Cell(4)])
            ___269 = Cell((___267.value and ___268.value), inputs=(___267.inputs + ___268.inputs))
            __stack__.push()
            __stack__.add(___269.inputs)
            table.adopt(__stack__.all())
            table.adopt(___269.inputs)
            if ___269:
                ___271 = db_str(user[Cell(1)])
                table = (table + ((((Cell('<tr><td>') + ___271) + Cell('</td><td>')) + user[Cell(2)]) + Cell('</td><td>')))
                table.adopt(__stack__.all())
                ___272 = Cell((user[Cell(3)] == Cell(1)))
                __stack__.push()
                __stack__.add(___272.inputs)
                table.adopt(__stack__.all())
                table.adopt(___272.inputs)
                if ___272:
                    table = (table + Cell('free'))
                    table.adopt(__stack__.all())
                else:
                    ___274 = Cell((user[Cell(3)] == Cell(2)))
                    __stack__.push()
                    __stack__.add(___274.inputs)
                    table.adopt(__stack__.all())
                    table.adopt(___274.inputs)
                    if ___274:
                        table = (table + Cell('premium'))
                        table.adopt(__stack__.all())
                    else:
                        ___276 = Cell((user[Cell(3)] == Cell(3)))
                        __stack__.push()
                        __stack__.add(___276.inputs)
                        table.adopt(__stack__.all())
                        table.adopt(___276.inputs)
                        if ___276:
                            table = (table + Cell('administrator'))
                            table.adopt(__stack__.all())
                        __stack__.pop()
                    __stack__.pop()
                __stack__.pop()
                ___278 = Cell((user[Cell(4)] > Cell(0)))
                __stack__.push()
                __stack__.add(___278.inputs)
                table.adopt(__stack__.all())
                table.adopt(___278.inputs)
                if ___278:
                    ___280 = location_html(user[Cell(4)])[0]
                    table = (table + ((Cell('</td><td>') + ___280) + Cell('</td>')))
                    table.adopt(__stack__.all())
                else:
                    table = (table + Cell('</td><td></td>'))
                    table.adopt(__stack__.all())
                __stack__.pop()
                ___281 = meetup.me()
                ___282 = Cell((user[Cell(1)] == ___281))
                __stack__.push()
                __stack__.add(___282.inputs)
                table.adopt(__stack__.all())
                table.adopt(___282.inputs)
                if ___282:
                    ___284 = db_str(user[Cell(1)])
                    ___285 = db_str(user[Cell(1)])
                    table = (table + ((((Cell('<td><a href="/2/update_user/') + ___284) + Cell('">Update</a>&nbsp;<a href="/2/user_categories/')) + ___285) + Cell('">Categories</a></td></tr>')))
                    table.adopt(__stack__.all())
                else:
                    ___286 = is_admin()[0]
                    ___287 = ___286
                    __stack__.push()
                    __stack__.add(___287.inputs)
                    table.adopt(__stack__.all())
                    table.adopt(___287.inputs)
                    if ___287:
                        ___289 = db_str(user[Cell(1)])
                        ___290 = db_str(user[Cell(1)])
                        ___291 = db_str(user[Cell(1)])
                        table = (table + ((((((Cell('<td><a href="/2/delete_friend/') + ___289) + Cell('">Delete friend</a>&nbsp;<a href="/2/update_user/')) + ___290) + Cell('">Update</a>&nbsp;<a href="/2/user_categories/')) + ___291) + Cell('">Categories</a></td></tr>')))
                        table.adopt(__stack__.all())
                    else:
                        ___292 = db_str(user[Cell(1)])
                        ___293 = db_str(user[Cell(1)])
                        table = (table + ((((Cell('<td><a href="/2/delete_friend/') + ___292) + Cell('">Delete friend</a>&nbsp;<a href="/2/user_categories/')) + ___293) + Cell('">Categories</a></td></tr>')))
                        table.adopt(__stack__.all())
                    __stack__.pop()
                __stack__.pop()
            __stack__.pop()
            i = (i + Cell(1))
            i.adopt(__stack__.all())
        __k__ = [Cell('table_html'), Cell('is_registered')]
        ___294 = meetup.render(Cell('users.html'), Cell({
            __k__[0].value: table,
            __k__[1].value: Cell(True),
        }, inputs=[__k__[0].inputs, __k__[1].inputs]))
        __r__ = Cell(___294, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('is_registered')]
        ___295 = meetup.render(Cell('users.html'), Cell({
            __k__[0].value: Cell(False),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___295, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/users')
def _users():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = users()
    return (__c__, __r__, __s__, __trace__)

def delete_friend(user):
    global __stack__
    ___296 = meetup.me()
    ___297 = meetup.sql(Cell('DELETE FROM friends WHERE user_id = ?0 AND friend_id = ?1'), Cell([___296, user]))
    ___298 = users()[0]
    __r__ = Cell(___298, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/delete_friend/<int:user>')
def _delete_friend(user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    user = meetup.register('delete_friend', 'user', user)
    (__r__, __s__) = delete_friend(user)
    return (__c__, __r__, __s__, __trace__)

def add_friend():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('friend_id' not in locals()):
        friend_id = Cell(None)
    ___299 = meetup.me()
    user_id = ___299
    user_id.adopt(__stack__.all())
    ___300 = meetup.get('add_friend', Cell('ID'))
    ___301 = db_int(___300)
    friend_id = ___301
    friend_id.adopt(__stack__.all())
    ___302 = meetup.sql(Cell('SELECT * FROM friends WHERE user_id = ?0 AND friend_id = ?1'), Cell([user_id, friend_id]))
    ___303 = db_len(___302)
    ___304 = Cell((___303 == Cell(0)))
    __trace__.add_inputs(___304.inputs)
    __stack__.push()
    __stack__.add(___304.inputs, bot=True)
    meetup.add_sql_inputs('friends', __stack__.all())
    meetup.add_sql_inputs('friends', ___304.inputs)
    if ___304:
        ___306 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([friend_id]))
        ___307 = db_len(___306)
        ___308 = Cell((___307 > Cell(0)))
        __stack__.push()
        __stack__.add(___308.inputs)
        meetup.add_sql_inputs('friends', __stack__.all())
        meetup.add_sql_inputs('friends', ___308.inputs)
        if ___308:
            ___310 = meetup.sql(Cell('INSERT INTO friends (user_id, friend_id) VALUES (?0, ?1)'), Cell([user_id, friend_id]))
        else:
            ___311 = db_str(friend_id)
            ___312 = error_html(((Cell('User ') + ___311) + Cell(' is not registered.<br>\n')))[0]
            ___313 = users()[0]
            __r__ = Cell((___312 + ___313), adopt=__stack__.all())
            __s__ = __stack__.all()
            __stack__.pop()
            __stack__.pop()
            return (__r__, __s__)
        __stack__.pop()
    __stack__.pop()
    ___314 = users()[0]
    __r__ = Cell(___314, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/add_friend')
def _add_friend():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_friend()
    return (__c__, __r__, __s__, __trace__)

def update_user(user):
    global __stack__
    if ('address' not in locals()):
        address = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('ID' not in locals()):
        ID = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('administrator' not in locals()):
        administrator = Cell(None)
    if ('free' not in locals()):
        free = Cell(None)
    if ('premium' not in locals()):
        premium = Cell(None)
    ___315 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([user]))
    user = ___315[Cell(0)]
    user.adopt(__stack__.all())
    ___316 = db_str(user[Cell(0)])
    id_ = ___316
    id_.adopt(__stack__.all())
    ___317 = db_str(user[Cell(1)])
    ID = ___317
    ID.adopt(__stack__.all())
    name = user[Cell(2)]
    name.adopt(__stack__.all())
    ___318 = Cell((user[Cell(3)] == Cell(1)))
    __stack__.push()
    __stack__.add(___318.inputs)
    free.adopt(__stack__.all())
    free.adopt(___318.inputs)
    if ___318:
        free = Cell(' selected')
        free.adopt(__stack__.all())
    else:
        free = Cell('')
        free.adopt(__stack__.all())
    __stack__.pop()
    ___320 = Cell((user[Cell(3)] == Cell(2)))
    __stack__.push()
    __stack__.add(___320.inputs)
    premium.adopt(__stack__.all())
    premium.adopt(___320.inputs)
    if ___320:
        premium = Cell(' selected')
        premium.adopt(__stack__.all())
    else:
        premium = Cell('')
        premium.adopt(__stack__.all())
    __stack__.pop()
    ___322 = Cell((user[Cell(3)] == Cell(3)))
    __stack__.push()
    __stack__.add(___322.inputs)
    administrator.adopt(__stack__.all())
    administrator.adopt(___322.inputs)
    if ___322:
        administrator = Cell(' selected')
        administrator.adopt(__stack__.all())
    else:
        administrator = Cell('')
        administrator.adopt(__stack__.all())
    __stack__.pop()
    address = user[Cell(4)]
    address.adopt(__stack__.all())
    __k__ = [Cell('id_'), Cell('ID'), Cell('name'), Cell('free'), Cell('premium'), Cell('administrator'), Cell('location_select')]
    ___324 = location_select(address)[0]
    ___325 = meetup.render(Cell('update_user.html'), Cell({
        __k__[0].value: id_,
        __k__[1].value: ID,
        __k__[2].value: name,
        __k__[3].value: free,
        __k__[4].value: premium,
        __k__[5].value: administrator,
        __k__[6].value: ___324,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs]))
    __r__ = Cell(___325, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_user/<int:user>')
def _update_user(user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    user = meetup.register('update_user', 'user', user)
    (__r__, __s__) = update_user(user)
    return (__c__, __r__, __s__, __trace__)

def update_user_do():
    global __stack__
    if ('address' not in locals()):
        address = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('ID' not in locals()):
        ID = Cell(None)
    ___326 = meetup.get('update_user_do', Cell('id_'))
    ___327 = db_int(___326)
    id_ = ___327
    id_.adopt(__stack__.all())
    ___328 = meetup.get('update_user_do', Cell('ID'))
    ___329 = db_int(___328)
    ID = ___329
    ID.adopt(__stack__.all())
    ___330 = meetup.get('update_user_do', Cell('name'))
    name = ___330
    name.adopt(__stack__.all())
    ___331 = meetup.get('update_user_do', Cell('level'))
    ___332 = db_int(___331)
    level = ___332
    level.adopt(__stack__.all())
    ___333 = meetup.get('update_user_do', Cell('location'))
    ___334 = db_int(___333)
    address = ___334
    address.adopt(__stack__.all())
    ___335 = is_admin()[0]
    ___336 = meetup.me()
    ___337 = Cell((___335.value or Cell((id_ == ___336)).value), inputs=(___335.inputs + Cell((id_ == ___336)).inputs))
    __trace__.add_inputs(___337.inputs)
    __stack__.push()
    __stack__.add(___337.inputs, bot=True)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___337.inputs)
    if ___337:
        ___339 = meetup.sql(Cell('DELETE FROM users WHERE id = ?0'), Cell([id_]))
        ___340 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([ID, name, level, address]))
        ___341 = users()[0]
        __r__ = Cell(___341, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___342 = error_html(Cell('Illegal operation: Update user'))[0]
        ___343 = users()[0]
        __r__ = Cell((___342 + ___343), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/update_user_do')
def _update_user_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_user_do()
    return (__c__, __r__, __s__, __trace__)

def user_categories(user):
    global __stack__
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('categories_html' not in locals()):
        categories_html = Cell(None)
    if ('usr' not in locals()):
        usr = Cell(None)
    ___344 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([user]))
    usr = ___344[Cell(0)]
    usr.adopt(__stack__.all())
    ___345 = meetup.sql(Cell('SELECT * FROM categories'))
    categories = ___345
    categories.adopt(__stack__.all())
    ___346 = categories
    for category in ___346:
        categories_html = (categories_html + ((Cell('<tr><td>') + category[Cell(1)]) + Cell('</td><td>')))
        categories_html.adopt(__stack__.all())
        ___348 = meetup.sql(Cell('SELECT id FROM cat_sub WHERE category = ?0 AND subscriber = ?1'), Cell([category[Cell(0)], user]))
        ___349 = db_len(___348)
        ___350 = Cell((___349 > Cell(0)))
        __stack__.push()
        __stack__.add(___350.inputs)
        categories_html.adopt(__stack__.all())
        categories_html.adopt(___350.inputs)
        events.adopt(__stack__.all())
        events.adopt(___350.inputs)
        if ___350:
            ___352 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_cat ON ev_cat.event = events.id WHERE ev_cat.category = ?0'), Cell([category[Cell(0)]]))
            events = ___352
            events.adopt(__stack__.all())
            ___353 = events
            for ev in ___353:
                ___355 = event_html(ev, Cell(True))[0]
                categories_html = (categories_html + (___355 + Cell('<br>')))
                categories_html.adopt(__stack__.all())
            ___356 = is_admin()[0]
            ___357 = meetup.me()
            ___358 = Cell((___356.value or Cell((user == ___357)).value), inputs=(___356.inputs + Cell((user == ___357)).inputs))
            __stack__.push()
            __stack__.add(___358.inputs)
            categories_html.adopt(__stack__.all())
            categories_html.adopt(___358.inputs)
            if ___358:
                ___360 = db_str(category[Cell(0)])
                ___361 = db_str(user)
                categories_html = (categories_html + ((((Cell('</td><td><a href="/2/unsubscribe/') + ___360) + Cell('/')) + ___361) + Cell('">Unsubscribe</a></td></tr>')))
                categories_html.adopt(__stack__.all())
            else:
                categories_html = (categories_html + Cell('</td><td></td></tr>'))
                categories_html.adopt(__stack__.all())
            __stack__.pop()
        else:
            ___362 = is_admin()[0]
            ___363 = meetup.me()
            ___364 = Cell((___362.value or Cell((user == ___363)).value), inputs=(___362.inputs + Cell((user == ___363)).inputs))
            __stack__.push()
            __stack__.add(___364.inputs)
            categories_html.adopt(__stack__.all())
            categories_html.adopt(___364.inputs)
            if ___364:
                ___366 = db_str(category[Cell(0)])
                ___367 = db_str(user)
                categories_html = (categories_html + ((((Cell('</td><td><a href="/2/subscribe/') + ___366) + Cell('/')) + ___367) + Cell('">Subscribe</a></td></tr>')))
                categories_html.adopt(__stack__.all())
            else:
                categories_html = (categories_html + Cell('</td><td></td></tr>'))
                categories_html.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
    __k__ = [Cell('usr'), Cell('categories_html')]
    ___368 = meetup.render(Cell('user_categories.html'), Cell({
        __k__[0].value: usr[Cell(2)],
        __k__[1].value: categories_html,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___368, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/user_categories/<int:user>')
def _user_categories(user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    user = meetup.register('user_categories', 'user', user)
    (__r__, __s__) = user_categories(user)
    return (__c__, __r__, __s__, __trace__)

def unsubscribe(category, user):
    global __stack__
    ___369 = is_admin()[0]
    ___370 = meetup.me()
    ___371 = Cell((___369.value or Cell((user == ___370)).value), inputs=(___369.inputs + Cell((user == ___370)).inputs))
    __trace__.add_inputs(___371.inputs)
    __stack__.push()
    __stack__.add(___371.inputs, bot=True)
    meetup.add_sql_inputs('cat_sub', __stack__.all())
    meetup.add_sql_inputs('cat_sub', ___371.inputs)
    if ___371:
        ___373 = meetup.sql(Cell('DELETE FROM cat_sub WHERE category = ?0 AND subscriber = ?1'), Cell([category, user]))
        ___374 = user_categories(user)[0]
        __r__ = Cell(___374, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___375 = error_html(Cell('Illegal operation: Unsubscribe'))[0]
        ___376 = user_categories(user)[0]
        __r__ = Cell((___375 + ___376), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/unsubscribe/<int:category>/<int:user>')
def _unsubscribe(category, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    category = meetup.register('unsubscribe', 'category', category)
    user = meetup.register('unsubscribe', 'user', user)
    (__r__, __s__) = unsubscribe(category, user)
    return (__c__, __r__, __s__, __trace__)

def subscribe(category, user):
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    if ('subscriptions' not in locals()):
        subscriptions = Cell(None)
    ___377 = is_admin()[0]
    ___378 = meetup.me()
    ___379 = Cell((___377.value or Cell((user == ___378)).value), inputs=(___377.inputs + Cell((user == ___378)).inputs))
    __trace__.add_inputs(___379.inputs)
    __stack__.push()
    __stack__.add(___379.inputs, bot=True)
    meetup.add_sql_inputs('cat_sub', __stack__.all())
    meetup.add_sql_inputs('cat_sub', ___379.inputs)
    level.adopt(__stack__.all())
    level.adopt(___379.inputs)
    subscriptions.adopt(__stack__.all())
    subscriptions.adopt(___379.inputs)
    if ___379:
        ___381 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([user]))
        level = ___381[Cell(0)][Cell(0)]
        level.adopt(__stack__.all())
        ___382 = Cell((level == Cell(1)))
        __stack__.push()
        __stack__.add(___382.inputs)
        subscriptions.adopt(__stack__.all())
        subscriptions.adopt(___382.inputs)
        if ___382:
            ___384 = meetup.sql(Cell('SELECT * FROM cat_sub WHERE subscriber = ?0'), Cell([user]))
            subscriptions = ___384
            subscriptions.adopt(__stack__.all())
            ___385 = db_len(subscriptions)
            ___386 = Cell((___385 > Cell(2)))
            __stack__.push()
            __stack__.add(___386.inputs)
            if ___386:
                ___388 = user_categories(user)[0]
                __r__ = Cell((Cell('Illegal operation: Subscribe<br>\n') + ___388), adopt=__stack__.all())
                __s__ = __stack__.all()
                __stack__.pop()
                __stack__.pop()
                __stack__.pop()
                return (__r__, __s__)
            __stack__.pop()
        __stack__.pop()
        ___389 = meetup.sql(Cell('INSERT INTO cat_sub (category, subscriber) VALUES (?0, ?1)'), Cell([category, user]))
        ___390 = user_categories(user)[0]
        __r__ = Cell(___390, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___391 = error_html(Cell('Illegal operation: Subscribe'))[0]
        ___392 = user_categories(user)[0]
        __r__ = Cell((___391 + ___392), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/subscribe/<int:category>/<int:user>')
def _subscribe(category, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    category = meetup.register('subscribe', 'category', category)
    user = meetup.register('subscribe', 'user', user)
    (__r__, __s__) = subscribe(category, user)
    return (__c__, __r__, __s__, __trace__)

def categories():
    global __stack__
    if ('table' not in locals()):
        table = Cell(None)
    if ('cat' not in locals()):
        cat = Cell(None)
    ___393 = meetup.me()
    ___394 = meetup.sql(Cell('SELECT categories.* FROM friends LEFT JOIN categories ON categories.owner = friends.friend_id WHERE friends.user_id = ?0 OR categories.owner = ?0'), Cell([___393]))
    categories = ___394
    categories.adopt(__stack__.all())
    table = Cell('')
    table.adopt(__stack__.all())
    ___395 = categories
    for cat in ___395:
        ___397 = meetup.check('categories', __c__, __trace__, cat[Cell(0)])
        ___398 = meetup.check('categories', __c__, __trace__, cat[Cell(1)])
        ___399 = Cell((___397.value and ___398.value), inputs=(___397.inputs + ___398.inputs))
        __stack__.push()
        __stack__.add(___399.inputs)
        table.adopt(__stack__.all())
        table.adopt(___399.inputs)
        if ___399:
            ___401 = is_admin()[0]
            ___402 = ___401
            __stack__.push()
            __stack__.add(___402.inputs)
            table.adopt(__stack__.all())
            table.adopt(___402.inputs)
            if ___402:
                ___404 = db_str(cat[Cell(1)])
                ___405 = db_str(cat[Cell(0)])
                ___406 = db_str(cat[Cell(0)])
                ___407 = db_str(cat[Cell(0)])
                table = (table + ((((((((Cell('<tr><td>') + ___404) + Cell('</td><td><a href="/2/delete_category/')) + ___405) + Cell('">Delete</a>&nbsp;<a href="/2/update_category/')) + ___406) + Cell('">Update</a>&nbsp;<a href="/2/category/')) + ___407) + Cell('">Details</a></td></tr>')))
                table.adopt(__stack__.all())
            else:
                ___408 = db_str(cat[Cell(1)])
                table = (table + ((Cell('<tr><td>') + ___408) + Cell('</td><td></td></tr>')))
                table.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
    __k__ = [Cell('table_html'), Cell('is_admin')]
    ___409 = is_admin()[0]
    ___410 = meetup.render(Cell('categories.html'), Cell({
        __k__[0].value: table,
        __k__[1].value: ___409,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___410, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/categories')
def _categories():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = categories()
    return (__c__, __r__, __s__, __trace__)

def delete_category(cat):
    global __stack__
    ___411 = is_admin()[0]
    ___412 = ___411
    __trace__.add_inputs(___412.inputs)
    __stack__.push()
    __stack__.add(___412.inputs, bot=True)
    meetup.add_sql_inputs('cat_sub', __stack__.all())
    meetup.add_sql_inputs('cat_sub', ___412.inputs)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___412.inputs)
    meetup.add_sql_inputs('cat_mod', __stack__.all())
    meetup.add_sql_inputs('cat_mod', ___412.inputs)
    meetup.add_sql_inputs('categories', __stack__.all())
    meetup.add_sql_inputs('categories', ___412.inputs)
    if ___412:
        ___414 = meetup.sql(Cell('DELETE FROM categories WHERE id = ?0'), Cell([cat]))
        ___415 = meetup.sql(Cell('DELETE FROM cat_mod WHERE category = ?0'), Cell([cat]))
        ___416 = meetup.sql(Cell('DELETE FROM cat_sub WHERE category = ?0'), Cell([cat]))
        ___417 = meetup.sql(Cell('DELETE FROM ev_cat WHERE category = ?0'), Cell([cat]))
        ___418 = categories()[0]
        __r__ = Cell(___418, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___419 = error_html(Cell('Illegal operation: Delete category'))[0]
        ___420 = categories()[0]
        __r__ = Cell((___419 + ___420), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_category/<int:cat>')
def _delete_category(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('delete_category', 'cat', cat)
    (__r__, __s__) = delete_category(cat)
    return (__c__, __r__, __s__, __trace__)

def add_category():
    global __stack__
    if ('name' not in locals()):
        name = Cell(None)
    ___421 = is_admin()[0]
    ___422 = ___421
    __trace__.add_inputs(___422.inputs)
    __stack__.push()
    __stack__.add(___422.inputs, bot=True)
    meetup.add_sql_inputs('categories', __stack__.all())
    meetup.add_sql_inputs('categories', ___422.inputs)
    name.adopt(__stack__.all())
    name.adopt(___422.inputs)
    if ___422:
        ___424 = meetup.get('add_category', Cell('name'))
        name = ___424
        name.adopt(__stack__.all())
        ___425 = meetup.me()
        ___426 = meetup.sql(Cell('INSERT INTO categories (name, owner) VALUES (?0, ?1)'), Cell([name, ___425]))
        ___427 = categories()[0]
        __r__ = Cell(___427, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___428 = error_html(Cell('Illegal operation: Add category'))[0]
        ___429 = categories()[0]
        __r__ = Cell((___428 + ___429), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_category')
def _add_category():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_category()
    return (__c__, __r__, __s__, __trace__)

def update_category(cat):
    global __stack__
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('cat_' not in locals()):
        cat_ = Cell(None)
    ___430 = meetup.sql(Cell('SELECT * FROM categories WHERE id = ?0'), Cell([cat]))
    cat_ = ___430[Cell(0)]
    cat_.adopt(__stack__.all())
    ___431 = db_str(cat_[Cell(0)])
    id_ = ___431
    id_.adopt(__stack__.all())
    name = cat_[Cell(1)]
    name.adopt(__stack__.all())
    __k__ = [Cell('id_'), Cell('name')]
    ___432 = meetup.render(Cell('update_category.html'), Cell({
        __k__[0].value: id_,
        __k__[1].value: name,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___432, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_category/<int:cat>')
def _update_category(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('update_category', 'cat', cat)
    (__r__, __s__) = update_category(cat)
    return (__c__, __r__, __s__, __trace__)

def update_category_do():
    global __stack__
    if ('cat_mod' not in locals()):
        cat_mod = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('new_id' not in locals()):
        new_id = Cell(None)
    if ('ev_cat' not in locals()):
        ev_cat = Cell(None)
    if ('cat_sub' not in locals()):
        cat_sub = Cell(None)
    if ('cat_mods' not in locals()):
        cat_mods = Cell(None)
    if ('ev_cats' not in locals()):
        ev_cats = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('cat_subs' not in locals()):
        cat_subs = Cell(None)
    ___433 = is_admin()[0]
    ___434 = non(___433)
    __trace__.add_inputs(___434.inputs)
    __stack__.push()
    __stack__.add(___434.inputs, bot=True)
    if ___434:
        ___436 = error_html(Cell('Illegal operation: Update category'))[0]
        ___437 = categories()[0]
        __r__ = Cell((___436 + ___437), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___438 = meetup.get('update_category_do', Cell('id_'))
    ___439 = db_int(___438)
    id_ = ___439
    id_.adopt(__stack__.all())
    ___440 = meetup.get('update_category_do', Cell('name'))
    name = ___440
    name.adopt(__stack__.all())
    ___441 = meetup.sql(Cell('SELECT owner FROM categories WHERE id = ?0'), Cell([id_]))
    owner = ___441[Cell(0)][Cell(0)]
    owner.adopt(__stack__.all())
    ___442 = meetup.sql(Cell('DELETE FROM categories WHERE id = ?0'), Cell([id_]))
    ___443 = meetup.sql(Cell('INSERT INTO categories (name, owner) VALUES (?0, ?1)'), Cell([name, owner]))
    new_id = ___443[Cell(0)]
    new_id.adopt(__stack__.all())
    ___444 = meetup.sql(Cell('SELECT * FROM cat_mod WHERE category = ?0'), Cell([id_]))
    cat_mods = ___444
    cat_mods.adopt(__stack__.all())
    ___445 = cat_mods
    __trace__.add_inputs(___445.inputs)
    for cat_mod in ___445:
        ___447 = meetup.sql(Cell('DELETE FROM cat_mod WHERE id = ?0'), Cell([cat_mod[Cell(0)]]))
        ___448 = meetup.sql(Cell('INSERT INTO cat_mod (category, moderator) VALUES (?0, ?1)'), Cell([new_id, cat_mod[Cell(2)]]))
    ___449 = meetup.sql(Cell('SELECT * FROM cat_sub WHERE category = ?0'), Cell([id_]))
    cat_subs = ___449
    cat_subs.adopt(__stack__.all())
    ___450 = cat_subs
    __trace__.add_inputs(___450.inputs)
    for cat_sub in ___450:
        ___452 = meetup.sql(Cell('DELETE FROM cat_sub WHERE id = ?0'), Cell([cat_sub[Cell(0)]]))
        ___453 = meetup.sql(Cell('INSERT INTO cat_sub (category, subscriber) VALUES (?0, ?1)'), Cell([new_id, cat_sub[Cell(2)]]))
    ___454 = meetup.sql(Cell('SELECT * FROM ev_cat WHERE category = ?0'), Cell([id_]))
    ev_cats = ___454
    ev_cats.adopt(__stack__.all())
    ___455 = ev_cats
    __trace__.add_inputs(___455.inputs)
    for ev_cat in ___455:
        ___457 = meetup.sql(Cell('DELETE FROM ev_cat WHERE id = ?0'), Cell([ev_cat[Cell(0)]]))
        ___458 = meetup.sql(Cell('INSERT INTO ev_cat (event, category) VALUES (?0, ?1)'), Cell([ev_cat[Cell(1)], new_id]))
    ___459 = categories()[0]
    __r__ = Cell(___459, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_category_do')
def _update_category_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_category_do()
    return (__c__, __r__, __s__, __trace__)

def category(cat):
    global __stack__
    if ('moderator' not in locals()):
        moderator = Cell(None)
    if ('events_html' not in locals()):
        events_html = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('subscribers_html' not in locals()):
        subscribers_html = Cell(None)
    if ('is_subscriber' not in locals()):
        is_subscriber = Cell(None)
    if ('subscriber' not in locals()):
        subscriber = Cell(None)
    if ('moderators_html' not in locals()):
        moderators_html = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('moderators' not in locals()):
        moderators = Cell(None)
    if ('subscribers' not in locals()):
        subscribers = Cell(None)
    ___460 = meetup.sql(Cell('SELECT name FROM categories WHERE id = ?0'), Cell([cat]))
    name = ___460[Cell(0)][Cell(0)]
    name.adopt(__stack__.all())
    ___461 = meetup.me()
    ___462 = meetup.sql(Cell('SELECT * FROM cat_sub WHERE category = ?0 AND subscriber = ?1'), Cell([cat, ___461]))
    ___463 = db_len(___462)
    is_subscriber = Cell((___463 > Cell(0)))
    is_subscriber.adopt(__stack__.all())
    moderators_html = Cell('')
    moderators_html.adopt(__stack__.all())
    subscribers_html = Cell('')
    subscribers_html.adopt(__stack__.all())
    ___464 = is_subscriber
    __stack__.push()
    __stack__.add(___464.inputs)
    moderators.adopt(__stack__.all())
    moderators.adopt(___464.inputs)
    subscribers_html.adopt(__stack__.all())
    subscribers_html.adopt(___464.inputs)
    moderators_html.adopt(__stack__.all())
    moderators_html.adopt(___464.inputs)
    subscribers.adopt(__stack__.all())
    subscribers.adopt(___464.inputs)
    if ___464:
        ___466 = is_admin()[0]
        ___467 = is_moderator(cat)[0]
        ___468 = Cell((___466.value or ___467.value), inputs=(___466.inputs + ___467.inputs))
        __stack__.push()
        __stack__.add(___468.inputs)
        if ___468:
            ___470 = db_str(cat)
        __stack__.pop()
        moderators_html = Cell('')
        moderators_html.adopt(__stack__.all())
        ___471 = meetup.sql(Cell('SELECT users.* FROM users JOIN cat_mod ON cat_mod.moderator = users.id WHERE cat_mod.category = ?0'), Cell([cat]))
        moderators = ___471
        moderators.adopt(__stack__.all())
        ___472 = moderators
        for moderator in ___472:
            ___474 = db_str(moderator[Cell(1)])
            moderators_html = (moderators_html + ((((Cell('<tr><td>') + ___474) + Cell('</td><td>')) + moderator[Cell(2)]) + Cell('</td></tr>')))
            moderators_html.adopt(__stack__.all())
        subscribers_html = Cell('')
        subscribers_html.adopt(__stack__.all())
        ___475 = meetup.sql(Cell('SELECT users.* FROM users JOIN cat_sub ON cat_sub.subscriber = users.id WHERE cat_sub.category = ?0'), Cell([cat]))
        subscribers = ___475
        subscribers.adopt(__stack__.all())
        ___476 = subscribers
        for subscriber in ___476:
            ___478 = db_str(subscriber[Cell(1)])
            subscribers_html = (subscribers_html + ((((Cell('<tr><td>') + ___478) + Cell('</td><td>')) + subscriber[Cell(2)]) + Cell('</td></tr>')))
            subscribers_html.adopt(__stack__.all())
    __stack__.pop()
    events_html = Cell('')
    events_html.adopt(__stack__.all())
    ___479 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_cat ON ev_cat.event = events.id WHERE ev_cat.category = ?0'), Cell([cat]))
    events = ___479
    events.adopt(__stack__.all())
    ___480 = events
    for ev in ___480:
        ___482 = event_html(ev, Cell(True))[0]
        events_html = (events_html + ___482)
        events_html.adopt(__stack__.all())
    __k__ = [Cell('name'), Cell('cat'), Cell('is_admin_or_moderator'), Cell('is_subscriber'), Cell('moderators_html'), Cell('subscribers_html'), Cell('events_html')]
    ___483 = db_str(cat)
    ___484 = is_admin()[0]
    ___485 = is_moderator(cat)[0]
    ___486 = meetup.render(Cell('category.html'), Cell({
        __k__[0].value: name,
        __k__[1].value: ___483,
        __k__[2].value: Cell((___484.value or ___485.value), inputs=(___484.inputs + ___485.inputs)),
        __k__[3].value: is_subscriber,
        __k__[4].value: moderators_html,
        __k__[5].value: subscribers_html,
        __k__[6].value: events_html,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs]))
    __r__ = Cell(___486, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/category/<int:cat>')
def _category(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('category', 'cat', cat)
    (__r__, __s__) = category(cat)
    return (__c__, __r__, __s__, __trace__)

def category_moderators(cat):
    global __stack__
    if ('current_html' not in locals()):
        current_html = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('new_html' not in locals()):
        new_html = Cell(None)
    ___487 = is_admin()[0]
    ___488 = ___487
    __trace__.add_inputs(___488.inputs)
    __stack__.push()
    __stack__.add(___488.inputs, bot=True)
    new_html.adopt(__stack__.all())
    new_html.adopt(___488.inputs)
    users.adopt(__stack__.all())
    users.adopt(___488.inputs)
    current_html.adopt(__stack__.all())
    current_html.adopt(___488.inputs)
    name.adopt(__stack__.all())
    name.adopt(___488.inputs)
    if ___488:
        ___490 = meetup.sql(Cell('SELECT name FROM categories WHERE id = ?0'), Cell([cat]))
        name = ___490[Cell(0)][Cell(0)]
        name.adopt(__stack__.all())
        ___491 = meetup.sql(Cell('SELECT id, user_id, name FROM users'), Cell([cat]))
        users = ___491
        users.adopt(__stack__.all())
        ___492 = users
        for user in ___492:
            ___494 = meetup.sql(Cell('SELECT * FROM cat_mod WHERE category = ?0 AND moderator = ?1'), Cell([cat, user[Cell(0)]]))
            ___495 = db_len(___494)
            ___496 = Cell((___495 > Cell(0)))
            __stack__.push()
            __stack__.add(___496.inputs)
            new_html.adopt(__stack__.all())
            new_html.adopt(___496.inputs)
            current_html.adopt(__stack__.all())
            current_html.adopt(___496.inputs)
            if ___496:
                ___498 = db_str(user[Cell(1)])
                ___499 = db_str(cat)
                ___500 = db_str(user[Cell(0)])
                current_html = (current_html + ((((((((Cell('<tr><td>') + ___498) + Cell('</td><td>')) + user[Cell(2)]) + Cell('</td><td><a href="/2/delete_category_moderator/')) + ___499) + Cell('/')) + ___500) + Cell('">Remove moderator</a></td></tr>')))
                current_html.adopt(__stack__.all())
            else:
                ___501 = db_str(user[Cell(1)])
                ___502 = db_str(cat)
                ___503 = db_str(user[Cell(0)])
                new_html = (new_html + ((((((((Cell('<tr><td>') + ___501) + Cell('</td><td>')) + user[Cell(2)]) + Cell('</td><td><a href="/2/add_category_moderator/')) + ___502) + Cell('/')) + ___503) + Cell('">Make moderator</a></td></tr>')))
                new_html.adopt(__stack__.all())
            __stack__.pop()
        __k__ = [Cell('name'), Cell('current_html'), Cell('new_html'), Cell('cat')]
        ___504 = db_str(cat)
        ___505 = meetup.render(Cell('category_moderators.html'), Cell({
            __k__[0].value: name,
            __k__[1].value: current_html,
            __k__[2].value: new_html,
            __k__[3].value: ___504,
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
        __r__ = Cell(___505, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___506 = error_html(Cell('Illegal operation: Category moderators'))[0]
        ___507 = category(cat)[0]
        __r__ = Cell((___506 + ___507), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/category_moderators/<int:cat>')
def _category_moderators(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('category_moderators', 'cat', cat)
    (__r__, __s__) = category_moderators(cat)
    return (__c__, __r__, __s__, __trace__)

def delete_category_moderator(cat, user):
    global __stack__
    ___508 = is_admin()[0]
    ___509 = ___508
    __trace__.add_inputs(___509.inputs)
    __stack__.push()
    __stack__.add(___509.inputs, bot=True)
    meetup.add_sql_inputs('cat_mod', __stack__.all())
    meetup.add_sql_inputs('cat_mod', ___509.inputs)
    if ___509:
        ___511 = meetup.sql(Cell('DELETE FROM cat_mod WHERE category = ?0 AND moderator = ?1'), Cell([cat, user]))
        ___512 = category_moderators(cat)[0]
        __r__ = Cell(___512, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___513 = error_html(Cell('Illegal operation: Delete category moderator'))[0]
        ___514 = category(cat)[0]
        __r__ = Cell((___513 + ___514), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_category_moderator/<int:cat>/<int:user>')
def _delete_category_moderator(cat, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('delete_category_moderator', 'cat', cat)
    user = meetup.register('delete_category_moderator', 'user', user)
    (__r__, __s__) = delete_category_moderator(cat, user)
    return (__c__, __r__, __s__, __trace__)

def add_category_moderator(cat, user):
    global __stack__
    ___515 = is_admin()[0]
    ___516 = ___515
    __trace__.add_inputs(___516.inputs)
    __stack__.push()
    __stack__.add(___516.inputs, bot=True)
    meetup.add_sql_inputs('cat_mod', __stack__.all())
    meetup.add_sql_inputs('cat_mod', ___516.inputs)
    if ___516:
        ___518 = meetup.sql(Cell('INSERT INTO cat_mod (category, moderator) VALUES (?0, ?1)'), Cell([cat, user]))
        ___519 = category_moderators(cat)[0]
        __r__ = Cell(___519, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___520 = error_html(Cell('Illegal operation: Add category moderator'))[0]
        ___521 = category(cat)[0]
        __r__ = Cell((___520 + ___521), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_category_moderator/<int:cat>/<int:user>')
def _add_category_moderator(cat, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('add_category_moderator', 'cat', cat)
    user = meetup.register('add_category_moderator', 'user', user)
    (__r__, __s__) = add_category_moderator(cat, user)
    return (__c__, __r__, __s__, __trace__)

def events():
    global __stack__
    ___522 = events_from(Cell(0))[0]
    __r__ = Cell(___522, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/events')
def _events():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = events()
    return (__c__, __r__, __s__, __trace__)

def events_from(start):
    global __stack__
    if ('L' not in locals()):
        L = Cell(None)
    if ('event_html_' not in locals()):
        event_html_ = Cell(None)
    if ('events_html' not in locals()):
        events_html = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    events_html = Cell('')
    events_html.adopt(__stack__.all())
    ___523 = meetup.me()
    ___524 = meetup.sql(Cell('SELECT events.* FROM events LEFT JOIN friends ON events.owner = friends.friend_id WHERE friends.user_id = ?0 OR events.owner = ?0 ORDER BY events.id DESC LIMIT 5 OFFSET ?1'), Cell([___523, start]))
    events = ___524
    events.adopt(__stack__.all())
    ___525 = meetup.me()
    ___526 = meetup.sql(Cell('SELECT events.id FROM events LEFT JOIN friends ON events.owner = friends.friend_id WHERE friends.user_id = ?0 OR events.owner = ?0 LIMIT 6 OFFSET ?1'), Cell([___525, start]))
    ___527 = db_len(___526)
    L = ___527
    L.adopt(__stack__.all())
    ___528 = events
    __trace__.add_inputs(___528.inputs)
    for ev in ___528:
        ___530 = event_html(ev, Cell(True))[0]
        event_html_ = ___530
        event_html_.adopt(__stack__.all())
        events_html = (events_html + (event_html_ + Cell('<br>')))
        events_html.adopt(__stack__.all())
    ___531 = meetup.check('events_from', __c__, __trace__, Cell('Analytics'), events_html, Cell('evilanalytics.org'))
    ___532 = ___531
    __trace__.add_inputs(___532.inputs)
    __stack__.push()
    __stack__.add(___532.inputs, bot=True)
    if ___532:
        ___534 = meetup.send('events_from', __c__, __trace__, Cell('evilanalytics.org'), Cell('Analytics'), events_html)
    __stack__.pop()
    ___535 = Cell((Cell((start > Cell(0))).value or Cell((L > Cell(5))).value), inputs=(Cell((start > Cell(0))).inputs + Cell((L > Cell(5))).inputs))
    __stack__.push()
    __stack__.add(___535.inputs)
    events_html.adopt(__stack__.all())
    events_html.adopt(___535.inputs)
    if ___535:
        events_html = (Cell('  </ul>\n</nav>') + events_html)
        events_html.adopt(__stack__.all())
        ___537 = Cell((L > Cell(5)))
        __stack__.push()
        __stack__.add(___537.inputs)
        events_html.adopt(__stack__.all())
        events_html.adopt(___537.inputs)
        if ___537:
            ___539 = db_str((start + Cell(5)))
            ___540 = db_str((start + Cell(6)))
            events_html = (((((Cell('<li class="page-item"><a class="page-link" href="/2/events/') + ___539) + Cell('">Events ')) + ___540) + Cell('-...</a></li>')) + events_html)
            events_html.adopt(__stack__.all())
        __stack__.pop()
        ___541 = db_str((start + Cell(1)))
        ___542 = db_str((start + Cell(5)))
        events_html = (((((Cell('<li class="page-item active"><a class="page-link" href="#">Events ') + ___541) + Cell('-')) + ___542) + Cell('</a></li>')) + events_html)
        events_html.adopt(__stack__.all())
        ___543 = Cell((start > Cell(0)))
        __stack__.push()
        __stack__.add(___543.inputs)
        events_html.adopt(__stack__.all())
        events_html.adopt(___543.inputs)
        if ___543:
            ___545 = db_str((start - Cell(5)))
            ___546 = db_str((start - Cell(4)))
            ___547 = db_str(start)
            events_html = (((((((Cell('<li class="page-item"><a class="page-link" href="/2/events/') + ___545) + Cell('">Events ')) + ___546) + Cell('-')) + ___547) + Cell('</a></li>')) + events_html)
            events_html.adopt(__stack__.all())
        __stack__.pop()
        events_html = (Cell('<nav>\n  <ul class="pagination">') + events_html)
        events_html.adopt(__stack__.all())
    __stack__.pop()
    __k__ = [Cell('location_select'), Cell('events_html')]
    ___548 = location_select(Cell(0))[0]
    ___549 = meetup.render(Cell('events.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), ___548)),
        __k__[1].value: Cell((Cell('Service'), events_html)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___549, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/events/<int:start>')
def _events_from(start):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    start = meetup.register('events_from', 'start', start)
    (__r__, __s__) = events_from(start)
    return (__c__, __r__, __s__, __trace__)

def add_event():
    global __stack__
    if ('owned' not in locals()):
        owned = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('time' not in locals()):
        time = Cell(None)
    if ('date' not in locals()):
        date = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    ___550 = meetup.get('add_event', Cell('title'))
    title = ___550
    title.adopt(__stack__.all())
    ___551 = meetup.get('add_event', Cell('description'))
    description = ___551
    description.adopt(__stack__.all())
    ___552 = Cell((description == Cell('')))
    __stack__.push()
    __stack__.add(___552.inputs)
    description.adopt(__stack__.all())
    description.adopt(___552.inputs)
    if ___552:
        description = (title + Cell(' (no description provided)'))
        description.adopt(__stack__.all())
    __stack__.pop()
    ___554 = meetup.get('add_event', Cell('location'))
    ___555 = db_int(___554)
    location = ___555
    location.adopt(__stack__.all())
    ___556 = meetup.get('add_event', Cell('time'))
    time = ___556
    time.adopt(__stack__.all())
    ___557 = meetup.get('add_event', Cell('date'))
    date = ___557
    date.adopt(__stack__.all())
    ___558 = meetup.me()
    owner = ___558
    owner.adopt(__stack__.all())
    ___559 = meetup.me()
    ___560 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([___559]))
    level = ___560[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    ___561 = meetup.me()
    ___562 = meetup.sql(Cell('SELECT * FROM events WHERE owner = ?0 LIMIT 3'), Cell([___561]))
    ___563 = db_len(___562)
    owned = ___563
    owned.adopt(__stack__.all())
    ___564 = Cell((Cell((level == Cell(1))).value and Cell((owned > Cell(2))).value), inputs=(Cell((level == Cell(1))).inputs + Cell((owned > Cell(2))).inputs))
    __trace__.add_inputs(___564.inputs)
    __stack__.push()
    __stack__.add(___564.inputs, bot=True)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___564.inputs)
    if ___564:
        ___566 = error_html(Cell('Illegal operation: Add event'))[0]
        ___567 = events()[0]
        __r__ = Cell((___566 + ___567), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___568 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([title, description, location, time, date, owner]))
        ___569 = meetup.redirect(Cell('Service'), Cell('events'))
        __r__ = Cell(___569, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_event')
def _add_event():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_event()
    return (__c__, __r__, __s__, __trace__)

def event(ev):
    global __stack__
    if ('attendees_html' not in locals()):
        attendees_html = Cell(None)
    if ('cats_html' not in locals()):
        cats_html = Cell(None)
    if ('requesters_html' not in locals()):
        requesters_html = Cell(None)
    if ('is_event_category' not in locals()):
        is_event_category = Cell(None)
    if ('cats' not in locals()):
        cats = Cell(None)
    if ('attendees' not in locals()):
        attendees = Cell(None)
    if ('manager' not in locals()):
        manager = Cell(None)
    if ('requester' not in locals()):
        requester = Cell(None)
    if ('requesters' not in locals()):
        requesters = Cell(None)
    if ('managers' not in locals()):
        managers = Cell(None)
    if ('evt' not in locals()):
        evt = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('managers_html' not in locals()):
        managers_html = Cell(None)
    if ('cat' not in locals()):
        cat = Cell(None)
    if ('attendee' not in locals()):
        attendee = Cell(None)
    managers_html = Cell('')
    managers_html.adopt(__stack__.all())
    attendees_html = Cell('')
    attendees_html.adopt(__stack__.all())
    requesters_html = Cell('')
    requesters_html.adopt(__stack__.all())
    cats_html = Cell('')
    cats_html.adopt(__stack__.all())
    ___570 = meetup.sql(Cell('SELECT * FROM events WHERE id = ?0'), Cell([ev]))
    evt = ___570[Cell(0)]
    evt.adopt(__stack__.all())
    ___571 = meetup.sql(Cell('SELECT users.user_id, users.name FROM users JOIN ev_att ON ev_att.attendee = users.user_id WHERE ev_att.event = ?0 AND ev_att.level = 2'), Cell([ev]))
    managers = ___571
    managers.adopt(__stack__.all())
    ___572 = managers
    for manager in ___572:
        manager = managers[i]
        manager.adopt(__stack__.all())
        ___574 = db_str(manager[Cell(0)])
        managers_html = (managers_html + ((((Cell('<tr><td>') + ___574) + Cell('</td><td>')) + manager[Cell(1)]) + Cell('</td>')))
        managers_html.adopt(__stack__.all())
        ___575 = meetup.me()
        ___576 = is_premium()[0]
        ___577 = Cell((Cell((evt[Cell(6)] == ___575)).value and ___576.value), inputs=(Cell((evt[Cell(6)] == ___575)).inputs + ___576.inputs))
        __stack__.push()
        __stack__.add(___577.inputs)
        managers_html.adopt(__stack__.all())
        managers_html.adopt(___577.inputs)
        if ___577:
            ___579 = db_str(ev)
            ___580 = db_str(manager[Cell(0)])
            managers_html = (managers_html + ((((Cell('<td><a href="/2/revoke_moderator/') + ___579) + Cell('/')) + ___580) + Cell('">Revoke</a></td></tr>')))
            managers_html.adopt(__stack__.all())
        else:
            managers_html = (managers_html + Cell('<td></td></tr>'))
            managers_html.adopt(__stack__.all())
        __stack__.pop()
    ___581 = is_attendee(ev)[0]
    ___582 = is_manager(ev)[0]
    ___583 = is_premium()[0]
    ___584 = Cell((___581.value or Cell((___582.value or ___583.value), inputs=(___582.inputs + ___583.inputs)).value), inputs=(___581.inputs + Cell((___582.value or ___583.value), inputs=(___582.inputs + ___583.inputs)).inputs))
    __stack__.push()
    __stack__.add(___584.inputs)
    attendees_html.adopt(__stack__.all())
    attendees_html.adopt(___584.inputs)
    attendees.adopt(__stack__.all())
    attendees.adopt(___584.inputs)
    if ___584:
        ___586 = meetup.sql(Cell('SELECT users.user_id, users.name FROM users JOIN ev_att ON ev_att.attendee = users.user_id WHERE ev_att.event = ?0 AND ev_att.level = 1'), Cell([ev]))
        attendees = ___586
        attendees.adopt(__stack__.all())
        ___587 = attendees
        for attendee in ___587:
            ___589 = db_str(attendee[Cell(0)])
            attendees_html = (attendees_html + ((((Cell('<tr><td>') + ___589) + Cell('</td><td>')) + attendee[Cell(1)]) + Cell('</td>')))
            attendees_html.adopt(__stack__.all())
            ___590 = is_manager(ev)[0]
            ___591 = meetup.me()
            ___592 = is_premium()[0]
            ___593 = Cell((___590.value or Cell((Cell((evt[Cell(6)] == ___591)).value and ___592.value), inputs=(Cell((evt[Cell(6)] == ___591)).inputs + ___592.inputs)).value), inputs=(___590.inputs + Cell((Cell((evt[Cell(6)] == ___591)).value and ___592.value), inputs=(Cell((evt[Cell(6)] == ___591)).inputs + ___592.inputs)).inputs))
            __stack__.push()
            __stack__.add(___593.inputs)
            attendees_html.adopt(__stack__.all())
            attendees_html.adopt(___593.inputs)
            if ___593:
                ___595 = db_str(ev)
                ___596 = db_str(attendee[Cell(0)])
                attendees_html = (attendees_html + ((((Cell('<td><a href="/2/promote_attendee/') + ___595) + Cell('/')) + ___596) + Cell('">Promote to manager</a></td></tr>')))
                attendees_html.adopt(__stack__.all())
            else:
                attendees_html = (attendees_html + Cell('<td></td></tr>'))
                attendees_html.adopt(__stack__.all())
            __stack__.pop()
    __stack__.pop()
    ___597 = is_manager(ev)[0]
    ___598 = ___597
    __stack__.push()
    __stack__.add(___598.inputs)
    is_event_category.adopt(__stack__.all())
    is_event_category.adopt(___598.inputs)
    cats.adopt(__stack__.all())
    cats.adopt(___598.inputs)
    cats_html.adopt(__stack__.all())
    cats_html.adopt(___598.inputs)
    requesters_html.adopt(__stack__.all())
    requesters_html.adopt(___598.inputs)
    requesters.adopt(__stack__.all())
    requesters.adopt(___598.inputs)
    if ___598:
        ___600 = meetup.sql(Cell('SELECT users.user_id, users.name FROM users JOIN requests ON requests.requester = users.user_id WHERE requests.event = ?0'), Cell([ev]))
        requesters = ___600
        requesters.adopt(__stack__.all())
        ___601 = requesters
        for requester in ___601:
            ___603 = db_str(requester[Cell(0)])
            ___604 = db_str(requester[Cell(1)])
            ___605 = db_str(ev)
            ___606 = db_str(requester[Cell(0)])
            ___607 = db_str(ev)
            ___608 = db_str(requester[Cell(0)])
            requesters_html = (requesters_html + ((((((((((((Cell('<tr><td>') + ___603) + Cell('</td><td>')) + ___604) + Cell('</td><td><a href="/2/accept_request/')) + ___605) + Cell('/')) + ___606) + Cell('">Accept</a>&nbsp;<a href="/2/reject_request/')) + ___607) + Cell('/')) + ___608) + Cell('">Reject</a>&nbsp;</td></tr>')))
            requesters_html.adopt(__stack__.all())
        ___609 = meetup.me()
        ___610 = meetup.sql(Cell('SELECT categories.* FROM categories LEFT JOIN friends ON friends.friend_id = categories.owner WHERE friends.user_id = ?0 OR categories.owner = ?0'), Cell([___609]))
        cats = ___610
        cats.adopt(__stack__.all())
        ___611 = cats
        for cat in ___611:
            ___613 = meetup.sql(Cell('SELECT * FROM ev_cat WHERE event = ?0 AND category = ?1'), Cell([ev, cat[Cell(0)]]))
            ___614 = db_len(___613)
            is_event_category = Cell((___614 > Cell(0)))
            is_event_category.adopt(__stack__.all())
            ___615 = is_event_category
            __stack__.push()
            __stack__.add(___615.inputs)
            cats_html.adopt(__stack__.all())
            cats_html.adopt(___615.inputs)
            if ___615:
                ___617 = db_str(cat[Cell(1)])
                ___618 = db_str(ev)
                ___619 = db_str(cat[Cell(0)])
                cats_html = (cats_html + ((((((Cell('<tr><td>') + ___617) + Cell(' (selected)</td><td><a href="/2/delete_event_category/')) + ___618) + Cell('/')) + ___619) + Cell('">Delete</td></tr>')))
                cats_html.adopt(__stack__.all())
            else:
                ___620 = db_str(cat[Cell(1)])
                ___621 = db_str(ev)
                ___622 = db_str(cat[Cell(0)])
                cats_html = (cats_html + ((((((Cell('<tr><td>') + ___620) + Cell('</td><td><a href="/2/add_event_category/')) + ___621) + Cell('/')) + ___622) + Cell('">Add</td></tr>')))
                cats_html.adopt(__stack__.all())
            __stack__.pop()
    __stack__.pop()
    __k__ = [Cell('event_html'), Cell('managers_html'), Cell('is_attendee_manager_or_premium'), Cell('attendees_html'), Cell('is_manager'), Cell('requesters_html'), Cell('cats_html'), Cell('is_moderator'), Cell('ev')]
    ___623 = event_html(evt, Cell(True))[0]
    ___624 = is_attendee(ev)[0]
    ___625 = is_manager(ev)[0]
    ___626 = is_premium()[0]
    ___627 = is_manager(ev)[0]
    ___628 = is_moderator(ev)[0]
    ___629 = db_str(ev)
    ___630 = meetup.render(Cell('event.html'), Cell({
        __k__[0].value: ___623,
        __k__[1].value: managers_html,
        __k__[2].value: Cell((___624.value or Cell((___625.value or ___626.value), inputs=(___625.inputs + ___626.inputs)).value), inputs=(___624.inputs + Cell((___625.value or ___626.value), inputs=(___625.inputs + ___626.inputs)).inputs)),
        __k__[3].value: attendees_html,
        __k__[4].value: ___627,
        __k__[5].value: requesters_html,
        __k__[6].value: cats_html,
        __k__[7].value: ___628,
        __k__[8].value: ___629,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs]))
    __r__ = Cell(___630, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/event/<int:ev>')
def _event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('event', 'ev', ev)
    (__r__, __s__) = event(ev)
    return (__c__, __r__, __s__, __trace__)

def add_event_category(ev, cat):
    global __stack__
    ___631 = is_manager(ev)[0]
    ___632 = ___631
    __trace__.add_inputs(___632.inputs)
    __stack__.push()
    __stack__.add(___632.inputs, bot=True)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___632.inputs)
    if ___632:
        ___634 = meetup.sql(Cell('INSERT INTO ev_cat (event, category) VALUES (?0, ?1)'), Cell([ev, cat]))
        ___635 = event(ev)[0]
        __r__ = Cell(___635, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___636 = error_html(Cell('Illegal operation: Add event category'))[0]
        ___637 = event(ev)[0]
        __r__ = Cell((___636 + ___637), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_event_category/<int:ev>/<int:cat>')
def _add_event_category(ev, cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('add_event_category', 'ev', ev)
    cat = meetup.register('add_event_category', 'cat', cat)
    (__r__, __s__) = add_event_category(ev, cat)
    return (__c__, __r__, __s__, __trace__)

def delete_event_category(ev, cat):
    global __stack__
    ___638 = is_manager(ev)[0]
    ___639 = ___638
    __trace__.add_inputs(___639.inputs)
    __stack__.push()
    __stack__.add(___639.inputs, bot=True)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___639.inputs)
    if ___639:
        ___641 = meetup.sql(Cell('DELETE FROM ev_cat WHERE event = ?0 AND category = ?1'), Cell([ev, cat]))
        ___642 = event(ev)[0]
        __r__ = Cell(___642, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___643 = error_html(Cell('Illegal operation: Delete event category'))[0]
        ___644 = event(ev)[0]
        __r__ = Cell((___643 + ___644), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_event_category/<int:ev>/<int:cat>')
def _delete_event_category(ev, cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('delete_event_category', 'ev', ev)
    cat = meetup.register('delete_event_category', 'cat', cat)
    (__r__, __s__) = delete_event_category(ev, cat)
    return (__c__, __r__, __s__, __trace__)

def delete_event(ev):
    global __stack__
    ___645 = is_manager(ev)[0]
    ___646 = ___645
    __trace__.add_inputs(___646.inputs)
    __stack__.push()
    __stack__.add(___646.inputs, bot=True)
    meetup.add_sql_inputs('invitations', __stack__.all())
    meetup.add_sql_inputs('invitations', ___646.inputs)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___646.inputs)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___646.inputs)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___646.inputs)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___646.inputs)
    if ___646:
        ___648 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([ev]))
        ___649 = meetup.sql(Cell('DELETE FROM ev_att WHERE event = ?0'), Cell([ev]))
        ___650 = meetup.sql(Cell('DELETE FROM ev_cat WHERE event = ?0'), Cell([ev]))
        ___651 = meetup.sql(Cell('DELETE FROM invitations WHERE event = ?0'), Cell([ev]))
        ___652 = meetup.sql(Cell('DELETE FROM requests WHERE event = ?0'), Cell([ev]))
        ___653 = events()[0]
        __r__ = Cell(___653, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___654 = error_html(Cell('Illegal operation: Delete event'))[0]
        ___655 = events()[0]
        __r__ = Cell((___654 + ___655), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_event/<int:ev>')
def _delete_event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('delete_event', 'ev', ev)
    (__r__, __s__) = delete_event(ev)
    return (__c__, __r__, __s__, __trace__)

def update_event(ev):
    global __stack__
    if ('title' not in locals()):
        title = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('time' not in locals()):
        time = Cell(None)
    if ('date' not in locals()):
        date = Cell(None)
    if ('evt' not in locals()):
        evt = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    ___656 = meetup.sql(Cell('SELECT * FROM events WHERE id = ?0'), Cell([ev]))
    evt = ___656[Cell(0)]
    evt.adopt(__stack__.all())
    title = evt[Cell(1)]
    title.adopt(__stack__.all())
    description = evt[Cell(2)]
    description.adopt(__stack__.all())
    ___657 = Cell((evt[Cell(3)] > Cell(0)))
    __stack__.push()
    __stack__.add(___657.inputs)
    location.adopt(__stack__.all())
    location.adopt(___657.inputs)
    if ___657:
        ___659 = location_html(evt[Cell(3)])[0]
        location = ___659
        location.adopt(__stack__.all())
    else:
        location = Cell('')
        location.adopt(__stack__.all())
    __stack__.pop()
    time = evt[Cell(4)]
    time.adopt(__stack__.all())
    date = evt[Cell(5)]
    date.adopt(__stack__.all())
    ___660 = db_str(evt[Cell(6)])
    owner = ___660
    owner.adopt(__stack__.all())
    __k__ = [Cell('title'), Cell('description'), Cell('location_select'), Cell('time'), Cell('date'), Cell('owner'), Cell('ev')]
    ___661 = location_select(evt[Cell(3)])[0]
    ___662 = db_str(ev)
    ___663 = meetup.render(Cell('update_event.make'), Cell({
        __k__[0].value: title,
        __k__[1].value: description,
        __k__[2].value: ___661,
        __k__[3].value: time,
        __k__[4].value: date,
        __k__[5].value: owner,
        __k__[6].value: ___662,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs]))
    __r__ = Cell(___663, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_event/<int:ev>')
def _update_event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('update_event', 'ev', ev)
    (__r__, __s__) = update_event(ev)
    return (__c__, __r__, __s__, __trace__)

def update_event_do():
    global __stack__
    if ('invitation' not in locals()):
        invitation = Cell(None)
    if ('ev_cat' not in locals()):
        ev_cat = Cell(None)
    if ('ev_cats' not in locals()):
        ev_cats = Cell(None)
    if ('request' not in locals()):
        request = Cell(None)
    if ('invitations' not in locals()):
        invitations = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('new_id' not in locals()):
        new_id = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('time' not in locals()):
        time = Cell(None)
    if ('date' not in locals()):
        date = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('ev_att' not in locals()):
        ev_att = Cell(None)
    if ('requests' not in locals()):
        requests = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('ev_atts' not in locals()):
        ev_atts = Cell(None)
    ___664 = meetup.get('update_event_do', Cell('id_'))
    ___665 = db_int(___664)
    id_ = ___665
    id_.adopt(__stack__.all())
    ___666 = is_manager(id_)[0]
    ___667 = is_moderator(id_)[0]
    ___668 = non(Cell((___666.value or ___667.value), inputs=(___666.inputs + ___667.inputs)))
    __trace__.add_inputs(___668.inputs)
    __stack__.push()
    __stack__.add(___668.inputs, bot=True)
    meetup.add_sql_inputs('invitations', __stack__.all())
    meetup.add_sql_inputs('invitations', ___668.inputs)
    date.adopt(__stack__.all())
    date.adopt(___668.inputs)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___668.inputs)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___668.inputs)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___668.inputs)
    owner.adopt(__stack__.all())
    owner.adopt(___668.inputs)
    new_id.adopt(__stack__.all())
    new_id.adopt(___668.inputs)
    time.adopt(__stack__.all())
    time.adopt(___668.inputs)
    ev_atts.adopt(__stack__.all())
    ev_atts.adopt(___668.inputs)
    ev_cats.adopt(__stack__.all())
    ev_cats.adopt(___668.inputs)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___668.inputs)
    title.adopt(__stack__.all())
    title.adopt(___668.inputs)
    description.adopt(__stack__.all())
    description.adopt(___668.inputs)
    requests.adopt(__stack__.all())
    requests.adopt(___668.inputs)
    location.adopt(__stack__.all())
    location.adopt(___668.inputs)
    invitations.adopt(__stack__.all())
    invitations.adopt(___668.inputs)
    if ___668:
        ___670 = error_html(Cell('Illegal operation: Update event'))[0]
        ___671 = event(id_)[0]
        __r__ = Cell((___670 + ___671), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___672 = meetup.get('update_event_do', Cell('title'))
        title = ___672
        title.adopt(__stack__.all())
        ___673 = meetup.get('update_event_do', Cell('description'))
        description = ___673
        description.adopt(__stack__.all())
        ___674 = meetup.get('update_event_do', Cell('location'))
        ___675 = db_int(___674)
        location = ___675
        location.adopt(__stack__.all())
        ___676 = meetup.get('update_event_do', Cell('time'))
        time = ___676
        time.adopt(__stack__.all())
        ___677 = meetup.get('update_event_do', Cell('date'))
        date = ___677
        date.adopt(__stack__.all())
        ___678 = meetup.get('update_event_do', Cell('owner'))
        ___679 = db_int(___678)
        owner = ___679
        owner.adopt(__stack__.all())
        ___680 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([id_]))
        ___681 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([title, description, location, time, date, owner]))
        new_id = ___681[Cell(0)]
        new_id.adopt(__stack__.all())
        ___682 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0'), Cell([id_]))
        ev_atts = ___682
        ev_atts.adopt(__stack__.all())
        ___683 = ev_atts
        for ev_att in ___683:
            ___685 = meetup.sql(Cell('DELETE FROM ev_att WHERE id = ?0'), Cell([ev_att[Cell(0)]]))
            ___686 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, ?2)'), Cell([new_id, ev_att[Cell(2)], ev_att[Cell(3)]]))
        ___687 = meetup.sql(Cell('SELECT * FROM ev_cat WHERE event = ?0'), Cell([id_]))
        ev_cats = ___687
        ev_cats.adopt(__stack__.all())
        ___688 = ev_cats
        for ev_cat in ___688:
            ___690 = meetup.sql(Cell('DELETE FROM ev_cat WHERE id = ?0'), Cell([ev_cat[Cell(0)]]))
            ___691 = meetup.sql(Cell('INSERT INTO ev_cat (event, category) VALUES (?0, ?1)'), Cell([new_id, ev_cat[Cell(2)]]))
        ___692 = meetup.sql(Cell('SELECT * FROM invitations WHERE event = ?0'), Cell([id_]))
        invitations = ___692
        invitations.adopt(__stack__.all())
        ___693 = invitations
        for invitation in ___693:
            ___695 = meetup.sql(Cell('DELETE FROM invitations WHERE id = ?0'), Cell([invitation[Cell(0)]]))
            ___696 = meetup.sql(Cell('INSERT INTO invitations (inviter, invitee, event) VALUES (?0, ?1, ?2)'), Cell([invitation[Cell(1)], invitation[Cell(2)], new_id]))
        ___697 = meetup.sql(Cell('SELECT * FROM requests WHERE event = ?0'), Cell([id_]))
        requests = ___697
        requests.adopt(__stack__.all())
        ___698 = requests
        for request in ___698:
            ___700 = meetup.sql(Cell('DELETE FROM requests WHERE id = ?0'), Cell([request[Cell(0)]]))
            ___701 = meetup.sql(Cell('INSERT INTO requests (requester, event) VALUES (?0, ?1)'), Cell([request[Cell(1)], new_id]))
        ___702 = event(new_id)[0]
        __r__ = Cell(___702, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/update_event_do')
def _update_event_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_event_do()
    return (__c__, __r__, __s__, __trace__)

def promote_attendee(ev, att):
    global __stack__
    if ('evt' not in locals()):
        evt = Cell(None)
    ___703 = is_manager(ev)[0]
    ___704 = meetup.me()
    ___705 = is_premium()[0]
    ___706 = Cell((___703.value or Cell((Cell((evt[Cell(6)] == ___704)).value and ___705.value), inputs=(Cell((evt[Cell(6)] == ___704)).inputs + ___705.inputs)).value), inputs=(___703.inputs + Cell((Cell((evt[Cell(6)] == ___704)).value and ___705.value), inputs=(Cell((evt[Cell(6)] == ___704)).inputs + ___705.inputs)).inputs))
    __trace__.add_inputs(___706.inputs)
    __stack__.push()
    __stack__.add(___706.inputs, bot=True)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___706.inputs)
    if ___706:
        ___708 = meetup.sql(Cell('DELETE FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([ev, att]))
        ___709 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 2)'), Cell([ev, att]))
        ___710 = event(ev)[0]
        __r__ = Cell(___710, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___711 = error_html(Cell('Illegal operation: Promote attendee'))[0]
        ___712 = event(ev)[0]
        __r__ = Cell((___711 + ___712), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/promote_attendee/<int:ev>/<int:att>')
def _promote_attendee(ev, att):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('promote_attendee', 'ev', ev)
    att = meetup.register('promote_attendee', 'att', att)
    (__r__, __s__) = promote_attendee(ev, att)
    return (__c__, __r__, __s__, __trace__)

def revoke_moderator(ev, att):
    global __stack__
    if ('evt' not in locals()):
        evt = Cell(None)
    ___713 = is_manager(ev)[0]
    ___714 = meetup.me()
    ___715 = is_premium()[0]
    ___716 = Cell((___713.value or Cell((Cell((evt[Cell(6)] == ___714)).value and ___715.value), inputs=(Cell((evt[Cell(6)] == ___714)).inputs + ___715.inputs)).value), inputs=(___713.inputs + Cell((Cell((evt[Cell(6)] == ___714)).value and ___715.value), inputs=(Cell((evt[Cell(6)] == ___714)).inputs + ___715.inputs)).inputs))
    __trace__.add_inputs(___716.inputs)
    __stack__.push()
    __stack__.add(___716.inputs, bot=True)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___716.inputs)
    if ___716:
        ___718 = meetup.sql(Cell('DELETE FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([ev, att]))
        ___719 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)'), Cell([ev, att]))
        ___720 = event(ev)[0]
        __r__ = Cell(___720, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___721 = error_html(Cell('Illegal operation: Revoke moderator'))[0]
        ___722 = event(ev)[0]
        __r__ = Cell((___721 + ___722), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/revoke_moderator/<int:ev>/<int:att>')
def _revoke_moderator(ev, att):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('revoke_moderator', 'ev', ev)
    att = meetup.register('revoke_moderator', 'att', att)
    (__r__, __s__) = revoke_moderator(ev, att)
    return (__c__, __r__, __s__, __trace__)

def accept_request(ev, req):
    global __stack__
    ___723 = is_manager(ev)[0]
    ___724 = ___723
    __trace__.add_inputs(___724.inputs)
    __stack__.push()
    __stack__.add(___724.inputs, bot=True)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___724.inputs)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___724.inputs)
    if ___724:
        ___726 = meetup.sql(Cell('DELETE FROM requests WHERE event = ?0 AND requester = ?1'), Cell([ev, req]))
        ___727 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)'), Cell([ev, req]))
        ___728 = event(ev)[0]
        __r__ = Cell(___728, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___729 = error_html(Cell('Illegal operation: Accept request'))[0]
        ___730 = event(ev)[0]
        __r__ = Cell((___729 + ___730), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/accept_request/<int:ev>/<int:req>')
def _accept_request(ev, req):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('accept_request', 'ev', ev)
    req = meetup.register('accept_request', 'req', req)
    (__r__, __s__) = accept_request(ev, req)
    return (__c__, __r__, __s__, __trace__)

def reject_request(ev, req):
    global __stack__
    ___731 = is_manager(ev)[0]
    ___732 = ___731
    __trace__.add_inputs(___732.inputs)
    __stack__.push()
    __stack__.add(___732.inputs, bot=True)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___732.inputs)
    if ___732:
        ___734 = meetup.sql(Cell('DELETE FROM requests WHERE event = ?0 AND requester = ?1'), Cell([ev, req]))
        ___735 = event(ev)[0]
        __r__ = Cell(___735, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___736 = error_html(Cell('Illegal operation: Reject request'))[0]
        ___737 = event(ev)[0]
        __r__ = Cell((___736 + ___737), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/reject_request/<int:ev>/<int:req>')
def _reject_request(ev, req):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('reject_request', 'ev', ev)
    req = meetup.register('reject_request', 'req', req)
    (__r__, __s__) = reject_request(ev, req)
    return (__c__, __r__, __s__, __trace__)

def invite(ev):
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    ___738 = is_manager(ev)[0]
    ___739 = ___738
    __trace__.add_inputs(___739.inputs)
    __stack__.push()
    __stack__.add(___739.inputs, bot=True)
    meetup.add_sql_inputs('invitations', __stack__.all())
    meetup.add_sql_inputs('invitations', ___739.inputs)
    user_id.adopt(__stack__.all())
    user_id.adopt(___739.inputs)
    if ___739:
        ___741 = meetup.get('invite', Cell('ID'))
        user_id = ___741
        user_id.adopt(__stack__.all())
        ___742 = meetup.me()
        ___743 = meetup.sql(Cell('INSERT INTO invitations (inviter, invitee, event) VALUES (?0, ?1, ?2)'), Cell([___742, user_id, ev]))
        ___744 = event(ev)[0]
        __r__ = Cell(___744, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___745 = error_html(Cell('Illegal operation: Invite'))[0]
        ___746 = event(ev)[0]
        __r__ = Cell((___745 + ___746), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/invite/<int:ev>')
def _invite(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('invite', 'ev', ev)
    (__r__, __s__) = invite(ev)
    return (__c__, __r__, __s__, __trace__)

def locations():
    global __stack__
    if ('table_html' not in locals()):
        table_html = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('locs' not in locals()):
        locs = Cell(None)
    table_html = Cell('')
    table_html.adopt(__stack__.all())
    ___747 = meetup.me()
    ___748 = meetup.sql(Cell('SELECT locations.* FROM friends LEFT JOIN locations ON friends.friend_id = locations.owner WHERE friends.user_id = ?0 OR locations.owner = ?0'), Cell([___747]))
    locs = ___748
    locs.adopt(__stack__.all())
    ___749 = locs
    for location in ___749:
        ___751 = meetup.check('locations', __c__, __trace__, location[Cell(1)])
        ___752 = meetup.check('locations', __c__, __trace__, location[Cell(2)])
        ___753 = meetup.check('locations', __c__, __trace__, location[Cell(3)])
        ___754 = Cell((Cell((___751.value and ___752.value), inputs=(___751.inputs + ___752.inputs)).value and ___753.value), inputs=(Cell((___751.value and ___752.value), inputs=(___751.inputs + ___752.inputs)).inputs + ___753.inputs))
        __stack__.push()
        __stack__.add(___754.inputs)
        table_html.adopt(__stack__.all())
        table_html.adopt(___754.inputs)
        if ___754:
            ___756 = db_str(location[Cell(1)])
            ___757 = db_str(location[Cell(2)])
            table_html = (table_html + ((((Cell('<tr><td>') + ___756) + Cell('</td><td>')) + ___757) + Cell('</td>')))
            table_html.adopt(__stack__.all())
            ___758 = meetup.me()
            ___759 = Cell((location[Cell(3)] == ___758))
            __stack__.push()
            __stack__.add(___759.inputs)
            table_html.adopt(__stack__.all())
            table_html.adopt(___759.inputs)
            if ___759:
                ___761 = db_str(location[Cell(0)])
                ___762 = db_str(location[Cell(0)])
                table_html = (table_html + ((((Cell('<td><a href="/2/delete_location/') + ___761) + Cell('">Delete</a>&nbsp;<a href="/2/update_location/')) + ___762) + Cell('">Update</a></td></tr>')))
                table_html.adopt(__stack__.all())
            else:
                table_html = (table_html + Cell('<td></td></tr>'))
                table_html.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
    __k__ = [Cell('table_html'), Cell('is_admin')]
    ___763 = is_admin()[0]
    ___764 = meetup.render(Cell('locations.html'), Cell({
        __k__[0].value: table_html,
        __k__[1].value: ___763,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___764, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/locations')
def _locations():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = locations()
    return (__c__, __r__, __s__, __trace__)

def delete_location(location):
    global __stack__
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('usrs' not in locals()):
        usrs = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('usr' not in locals()):
        usr = Cell(None)
    if ('evs' not in locals()):
        evs = Cell(None)
    ___765 = meetup.sql(Cell('SELECT owner FROM locations WHERE id = ?0'), Cell([location]))
    owner = ___765[Cell(0)][Cell(0)]
    owner.adopt(__stack__.all())
    ___766 = meetup.me()
    ___767 = Cell((owner != ___766))
    __trace__.add_inputs(___767.inputs)
    __stack__.push()
    __stack__.add(___767.inputs, bot=True)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___767.inputs)
    usrs.adopt(__stack__.all())
    usrs.adopt(___767.inputs)
    evs.adopt(__stack__.all())
    evs.adopt(___767.inputs)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___767.inputs)
    meetup.add_sql_inputs('locations', __stack__.all())
    meetup.add_sql_inputs('locations', ___767.inputs)
    if ___767:
        ___769 = error_html(Cell('Illegal operation: Delete location'))[0]
        ___770 = locations()[0]
        __r__ = Cell((___769 + ___770), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___771 = meetup.sql(Cell('DELETE FROM locations WHERE id = ?0'), Cell([location]))
        ___772 = meetup.sql(Cell('SELECT * FROM events WHERE location = ?0'), Cell([id_]))
        evs = ___772
        evs.adopt(__stack__.all())
        ___773 = evs
        for ev in ___773:
            ___775 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([ev[Cell(0)]]))
            ___776 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([ev[Cell(1)], ev[Cell(2)], Cell(0), ev[Cell(3)], ev[Cell(4)], ev[Cell(5)]]))
        ___777 = meetup.sql(Cell('SELECT * FROM users WHERE address = ?0'), Cell([id_]))
        usrs = ___777
        usrs.adopt(__stack__.all())
        ___778 = usrs
        for usr in ___778:
            ___780 = meetup.sql(Cell('DELETE FROM users WHERE id = ?0'), Cell([usr[Cell(0)]]))
            ___781 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([usr[Cell(1)], usr[Cell(2)], usr[Cell(3)], Cell(0)]))
        ___782 = locations()[0]
        __r__ = Cell(___782, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_location/<int:location>')
def _delete_location(location):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    location = meetup.register('delete_location', 'location', location)
    (__r__, __s__) = delete_location(location)
    return (__c__, __r__, __s__, __trace__)

def add_location():
    global __stack__
    if ('country' not in locals()):
        country = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___783 = meetup.get('add_location', Cell('country'))
    country = ___783
    country.adopt(__stack__.all())
    ___784 = meetup.get('add_location', Cell('name'))
    name = ___784
    name.adopt(__stack__.all())
    ___785 = meetup.me()
    ___786 = meetup.sql(Cell('INSERT INTO locations (country, name, owner) VALUES (?0, ?1, ?2)'), Cell([country, name, ___785]))
    ___787 = locations()[0]
    __r__ = Cell(___787, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/add_location')
def _add_location():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_location()
    return (__c__, __r__, __s__, __trace__)

def update_location(location):
    global __stack__
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('country' not in locals()):
        country = Cell(None)
    if ('location_' not in locals()):
        location_ = Cell(None)
    ___788 = meetup.sql(Cell('SELECT * FROM locations WHERE id = ?0'), Cell([location]))
    location_ = ___788[Cell(0)]
    location_.adopt(__stack__.all())
    ___789 = db_str(location_[Cell(0)])
    id_ = ___789
    id_.adopt(__stack__.all())
    country = location_[Cell(1)]
    country.adopt(__stack__.all())
    name = location_[Cell(2)]
    name.adopt(__stack__.all())
    owner = location_[Cell(3)]
    owner.adopt(__stack__.all())
    __k__ = [Cell('id_'), Cell('country'), Cell('name'), Cell('owner')]
    ___790 = meetup.render(Cell('update_location.html'), Cell({
        __k__[0].value: id_,
        __k__[1].value: country,
        __k__[2].value: name,
        __k__[3].value: owner,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___790, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_location/<int:location>')
def _update_location(location):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    location = meetup.register('update_location', 'location', location)
    (__r__, __s__) = update_location(location)
    return (__c__, __r__, __s__, __trace__)

def update_location_do():
    global __stack__
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('new_id' not in locals()):
        new_id = Cell(None)
    if ('usrs' not in locals()):
        usrs = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('usr' not in locals()):
        usr = Cell(None)
    if ('country' not in locals()):
        country = Cell(None)
    if ('evs' not in locals()):
        evs = Cell(None)
    ___791 = meetup.get('update_location_do', Cell('id_'))
    ___792 = db_int(___791)
    id_ = ___792
    id_.adopt(__stack__.all())
    ___793 = meetup.sql(Cell('SELECT owner FROM locations WHERE id = ?0'), Cell([id_]))
    owner = ___793[Cell(0)][Cell(0)]
    owner.adopt(__stack__.all())
    ___794 = meetup.me()
    ___795 = Cell((owner != ___794))
    __trace__.add_inputs(___795.inputs)
    __stack__.push()
    __stack__.add(___795.inputs, bot=True)
    country.adopt(__stack__.all())
    country.adopt(___795.inputs)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___795.inputs)
    evs.adopt(__stack__.all())
    evs.adopt(___795.inputs)
    new_id.adopt(__stack__.all())
    new_id.adopt(___795.inputs)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___795.inputs)
    name.adopt(__stack__.all())
    name.adopt(___795.inputs)
    usrs.adopt(__stack__.all())
    usrs.adopt(___795.inputs)
    meetup.add_sql_inputs('locations', __stack__.all())
    meetup.add_sql_inputs('locations', ___795.inputs)
    if ___795:
        ___797 = error_html(Cell('Illegal operation: Update location'))[0]
        ___798 = locations()[0]
        __r__ = Cell((___797 + ___798), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___799 = meetup.get('update_location_do', Cell('country'))
        country = ___799
        country.adopt(__stack__.all())
        ___800 = meetup.get('update_location_do', Cell('name'))
        name = ___800
        name.adopt(__stack__.all())
        ___801 = meetup.sql(Cell('DELETE FROM locations WHERE id = ?0'), Cell([id_]))
        ___802 = meetup.sql(Cell('INSERT INTO locations (country, name) VALUES (?0, ?1)'), Cell([country, name]))
        new_id = ___802[Cell(0)]
        new_id.adopt(__stack__.all())
        ___803 = meetup.sql(Cell('SELECT * FROM events WHERE location = ?0'), Cell([id_]))
        evs = ___803
        evs.adopt(__stack__.all())
        ___804 = evs
        for ev in ___804:
            ___806 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([ev[Cell(0)]]))
            ___807 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([ev[Cell(1)], ev[Cell(2)], new_id, ev[Cell(3)], ev[Cell(4)], ev[Cell(5)]]))
        ___808 = meetup.sql(Cell('SELECT * FROM users WHERE address = ?0'), Cell([id_]))
        usrs = ___808
        usrs.adopt(__stack__.all())
        ___809 = usrs
        for usr in ___809:
            ___811 = meetup.sql(Cell('DELETE FROM users WHERE id = ?0'), Cell([usr[Cell(0)]]))
            ___812 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([usr[Cell(1)], usr[Cell(2)], usr[Cell(3)], new_id]))
        ___813 = locations()[0]
        __r__ = Cell(___813, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/update_location_do')
def _update_location_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_location_do()
    return (__c__, __r__, __s__, __trace__)
