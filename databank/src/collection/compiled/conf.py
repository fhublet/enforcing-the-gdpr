
from apps import conf
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___814 = conf.get_session(Cell('loggedin'))
    __r__ = Cell(___814, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___815 = is_logged_in()[0]
    ___816 = ___815
    __trace__.add_inputs(___816.inputs)
    __stack__.push()
    __stack__.add(___816.inputs, bot=True)
    if ___816:
        ___818 = conf.me()
        ___819 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([___818]))
        __r__ = Cell(___819[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def filter_check(messages):
    global __stack__
    if ('L' not in locals()):
        L = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('checks' not in locals()):
        checks = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___820 = conf.check_all('filter_check', __c__, __trace__, Cell('Service'), messages)
    checks = ___820
    checks.adopt(__stack__.all())
    ___821 = db_len(messages)
    L = ___821
    L.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___822 = Cell((i < L))
    __stack__.push()
    __stack__.add(___822.inputs)
    i.adopt(__stack__.all())
    i.adopt(___822.inputs)
    while ___822:
        ___824 = checks[i]
        __stack__.push()
        __stack__.add(___824.inputs)
        if ___824:
            ___826 = db_append(messages2, messages[i])
        __stack__.pop()
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___822 = Cell((i < L))
        __stack__.add(___822.inputs)
        i.adopt(__stack__.all())
        i.adopt(___822.inputs)
    __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_admin():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    ___827 = conf.me()
    ___828 = conf.sql(Cell('SELECT level FROM user_profiles WHERE user_id = ?0'), Cell([___827]))
    level = ___828[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    __r__ = Cell(Cell((level == Cell('chair'))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def about_view():
    global __stack__
    __k__ = [Cell('which_page')]
    ___829 = conf.render(Cell('about.html'), Cell({
        __k__[0].value: Cell('about'),
    }, inputs=[__k__[0].inputs]))
    __r__ = Cell(___829, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/about')
def _about_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = about_view()
    return (__c__, __r__, __s__, __trace__)

def papers_view():
    global __stack__
    if ('user_name' not in locals()):
        user_name = Cell(None)
    if ('version' not in locals()):
        version = Cell(None)
    if ('author_name' not in locals()):
        author_name = Cell(None)
    if ('paper_data' not in locals()):
        paper_data = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('paper_versions' not in locals()):
        paper_versions = Cell(None)
    if ('papers' not in locals()):
        papers = Cell(None)
    if ('paper_data_1' not in locals()):
        paper_data_1 = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    ___830 = me_user()[0]
    user = ___830
    user.adopt(__stack__.all())
    ___831 = non(user)
    __trace__.add_inputs(___831.inputs)
    __stack__.push()
    __stack__.add(___831.inputs, bot=True)
    if ___831:
        ___833 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___833, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    user_name = user[Cell(2)]
    user_name.adopt(__stack__.all())
    ___834 = conf.sql(Cell('SELECT * FROM papers'))
    papers = ___834
    papers.adopt(__stack__.all())
    ___835 = db_len(papers)
    paper_data = (Cell([Cell(None)]) * ___835)
    paper_data.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___836 = papers
    for paper in ___836:
        ___838 = conf.sql(Cell('SELECT title FROM conf_paperversion WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        paper_versions = ___838
        paper_versions.adopt(__stack__.all())
        ___839 = conf.sql(Cell('SELECT name FROM user_profiles WHERE user_id = ?0'), Cell([paper[Cell(2)]]))
        author_name = ___839[Cell(0)][Cell(0)]
        author_name.adopt(__stack__.all())
        version = paper_versions[Cell(0)][Cell(0)]
        version.adopt(__stack__.all())
        __k__ = [Cell('paper'), Cell('author_name'), Cell('latest')]
        paper_data_1 = Cell({
            __k__[0].value: paper,
            __k__[1].value: author_name,
            __k__[2].value: version,
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs])
        paper_data_1.adopt(__stack__.all())
        __slv__ = [i]
        __w__ = paper_data
        for __i__ in range(1):
            __w__.adopt(__slv__[__i__].inputs)
            if (__i__ == 0):
                __w__[__slv__[__i__]] = paper_data_1
            elif (__slv__[__i__] in __w__):
                __w__ = __w__[__slv__[__i__]]
            else:
                break
        paper_data.adopt(__stack__.all())
        i = (i + Cell(1))
        i.adopt(__stack__.all())
    ___840 = filter_check(paper_data)[0]
    paper_data = ___840
    paper_data.adopt(__stack__.all())
    __k__ = [Cell('papers'), Cell('which_page'), Cell('paper_data'), Cell('name'), Cell('is_logged_in'), Cell('is_admin')]
    ___841 = is_admin()[0]
    ___842 = conf.render(Cell('papers.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), papers)),
        __k__[1].value: Cell((Cell('Service'), Cell('home'))),
        __k__[2].value: Cell((Cell('Service'), paper_data)),
        __k__[3].value: Cell((Cell('Service'), user_name)),
        __k__[4].value: Cell((Cell('Service'), Cell(True))),
        __k__[5].value: Cell((Cell('Service'), ___841)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs]))
    __r__ = Cell(___842, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/')
def _papers_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = papers_view()
    return (__c__, __r__, __s__, __trace__)

def paper_view():
    global __stack__
    if ('reviews' not in locals()):
        reviews = Cell(None)
    if ('author' not in locals()):
        author = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('paper_versions' not in locals()):
        paper_versions = Cell(None)
    if ('coauthors' not in locals()):
        coauthors = Cell(None)
    if ('latest_title' not in locals()):
        latest_title = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('comments' not in locals()):
        comments = Cell(None)
    if ('link' not in locals()):
        link = Cell(None)
    if ('latest_abstract' not in locals()):
        latest_abstract = Cell(None)
    ___843 = me_user()[0]
    user = ___843
    user.adopt(__stack__.all())
    ___844 = non(user)
    __trace__.add_inputs(___844.inputs)
    __stack__.push()
    __stack__.add(___844.inputs, bot=True)
    if ___844:
        ___846 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___846, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___847 = conf.get('paper_view', Cell('id'))
    ___848 = conf.sql(Cell('SELECT * FROM papers WHERE id = ?0'), Cell([___847]))
    paper = ___848[Cell(0)]
    paper.adopt(__stack__.all())
    ___849 = conf.method()
    ___850 = Cell((___849 == Cell('POST')))
    __trace__.add_inputs(___850.inputs)
    __stack__.push()
    __stack__.add(___850.inputs, bot=True)
    conf.add_sql_inputs('comments', __stack__.all())
    conf.add_sql_inputs('comments', ___850.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___850.inputs)
    conf.add_sql_inputs('conf_paperversion', __stack__.all())
    conf.add_sql_inputs('conf_paperversion', ___850.inputs)
    conf.add_sql_inputs('reviews', __stack__.all())
    conf.add_sql_inputs('reviews', ___850.inputs)
    if ___850:
        ___852 = Cell((paper != Cell(None)))
        __stack__.push()
        __stack__.add(___852.inputs)
        conf.add_sql_inputs('comments', __stack__.all())
        conf.add_sql_inputs('comments', ___852.inputs)
        contents.adopt(__stack__.all())
        contents.adopt(___852.inputs)
        conf.add_sql_inputs('conf_paperversion', __stack__.all())
        conf.add_sql_inputs('conf_paperversion', ___852.inputs)
        conf.add_sql_inputs('reviews', __stack__.all())
        conf.add_sql_inputs('reviews', ___852.inputs)
        if ___852:
            ___854 = conf.get('paper_view', Cell('add_comment'))
            ___855 = Cell((___854 == Cell('true')))
            __stack__.push()
            __stack__.add(___855.inputs)
            conf.add_sql_inputs('comments', __stack__.all())
            conf.add_sql_inputs('comments', ___855.inputs)
            contents.adopt(__stack__.all())
            contents.adopt(___855.inputs)
            conf.add_sql_inputs('conf_paperversion', __stack__.all())
            conf.add_sql_inputs('conf_paperversion', ___855.inputs)
            conf.add_sql_inputs('reviews', __stack__.all())
            conf.add_sql_inputs('reviews', ___855.inputs)
            if ___855:
                ___857 = conf.me()
                ___858 = conf.get('paper_view', Cell('comment'))
                ___859 = conf.sql(Cell('INSERT INTO comments (paper_id, user_id, contents) VALUES (?0, ?1, ?2)'), Cell([paper[Cell(0)], ___857, ___858]))
            else:
                ___860 = conf.get('paper_view', Cell('add_review'))
                ___861 = Cell((___860 == Cell('true')))
                __stack__.push()
                __stack__.add(___861.inputs)
                contents.adopt(__stack__.all())
                contents.adopt(___861.inputs)
                conf.add_sql_inputs('conf_paperversion', __stack__.all())
                conf.add_sql_inputs('conf_paperversion', ___861.inputs)
                conf.add_sql_inputs('reviews', __stack__.all())
                conf.add_sql_inputs('reviews', ___861.inputs)
                if ___861:
                    ___863 = conf.me()
                    ___864 = conf.get('paper_view', Cell('review'))
                    ___865 = conf.get('paper_view', Cell('score_novelty'))
                    ___866 = db_int(___865)
                    ___867 = conf.get('paper_view', Cell('score_presentation'))
                    ___868 = db_int(___867)
                    ___869 = conf.get('paper_view', Cell('score_technical'))
                    ___870 = db_int(___869)
                    ___871 = conf.get('paper_view', Cell('score_confidence'))
                    ___872 = db_int(___871)
                    ___873 = conf.sql(Cell('\n                  INSERT INTO reviews (paper_id, reviewer_id, contents, score_novelty, \n                                       score_presentation, score_technical, score_confidence) \n                  VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([paper[Cell(0)], ___863, ___864, ___866, ___868, ___870, ___872]))
                else:
                    ___874 = conf.get('paper_view', Cell('new_version'))
                    ___875 = Cell((___874 == Cell('true')))
                    __stack__.push()
                    __stack__.add(___875.inputs)
                    contents.adopt(__stack__.all())
                    contents.adopt(___875.inputs)
                    conf.add_sql_inputs('conf_paperversion', __stack__.all())
                    conf.add_sql_inputs('conf_paperversion', ___875.inputs)
                    if ___875:
                        ___877 = conf.post('paper_view', Cell('contents'))
                        contents = ___877
                        contents.adopt(__stack__.all())
                        ___878 = Cell((contents != Cell(None)))
                        __stack__.push()
                        __stack__.add(___878.inputs)
                        conf.add_sql_inputs('conf_paperversion', __stack__.all())
                        conf.add_sql_inputs('conf_paperversion', ___878.inputs)
                        if ___878:
                            ___880 = conf.get('paper_view', Cell('title'))
                            ___881 = conf.get('paper_view', Cell('abstract'))
                            ___882 = conf.now_utc()
                            ___883 = conf.sql(Cell('\n                  INSERT INTO conf_paperversion (paper_id, title, contents, abstract, time)\n                  VALUES (?0, ?1, ?2, ?3)'), Cell([paper[Cell(0)], ___880, contents, ___881, ___882]))
                        __stack__.pop()
                    __stack__.pop()
                __stack__.pop()
            __stack__.pop()
        __stack__.pop()
    __stack__.pop()
    ___884 = Cell((paper != Cell(None)))
    __stack__.push()
    __stack__.add(___884.inputs)
    author.adopt(__stack__.all())
    author.adopt(___884.inputs)
    paper.adopt(__stack__.all())
    paper.adopt(___884.inputs)
    reviews.adopt(__stack__.all())
    reviews.adopt(___884.inputs)
    latest_title.adopt(__stack__.all())
    latest_title.adopt(___884.inputs)
    latest_abstract.adopt(__stack__.all())
    latest_abstract.adopt(___884.inputs)
    coauthors.adopt(__stack__.all())
    coauthors.adopt(___884.inputs)
    link.adopt(__stack__.all())
    link.adopt(___884.inputs)
    comments.adopt(__stack__.all())
    comments.adopt(___884.inputs)
    paper_versions.adopt(__stack__.all())
    paper_versions.adopt(___884.inputs)
    if ___884:
        ___886 = conf.sql(Cell('SELECT * FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC'), Cell([paper[Cell(0)]]))
        ___887 = filter_check(___886)[0]
        paper_versions = ___887
        paper_versions.adopt(__stack__.all())
        ___888 = conf.sql(Cell('SELECT * FROM conf_papercoauthor WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        ___889 = filter_check(___888)[0]
        coauthors = ___889
        coauthors.adopt(__stack__.all())
        latest_abstract = Cell(None)
        latest_abstract.adopt(__stack__.all())
        latest_title = Cell(None)
        latest_title.adopt(__stack__.all())
        ___890 = db_len(paper_versions)
        ___891 = ___890
        __stack__.push()
        __stack__.add(___891.inputs)
        latest_title.adopt(__stack__.all())
        latest_title.adopt(___891.inputs)
        latest_abstract.adopt(__stack__.all())
        latest_abstract.adopt(___891.inputs)
        if ___891:
            latest_abstract = paper_versions[Cell(0)][Cell(3)]
            latest_abstract.adopt(__stack__.all())
            latest_title = paper_versions[Cell(0)][Cell(1)]
            latest_title.adopt(__stack__.all())
        __stack__.pop()
        ___893 = conf.check('paper_view', __c__, __trace__, Cell('Service'), latest_abstract)
        ___894 = non(___893)
        __stack__.push()
        __stack__.add(___894.inputs)
        latest_abstract.adopt(__stack__.all())
        latest_abstract.adopt(___894.inputs)
        if ___894:
            latest_abstract = Cell(None)
            latest_abstract.adopt(__stack__.all())
        __stack__.pop()
        ___896 = conf.check('paper_view', __c__, __trace__, Cell('Service'), latest_title)
        ___897 = non(___896)
        __stack__.push()
        __stack__.add(___897.inputs)
        latest_title.adopt(__stack__.all())
        latest_title.adopt(___897.inputs)
        if ___897:
            latest_title = Cell(None)
            latest_title.adopt(__stack__.all())
        __stack__.pop()
        ___899 = conf.link(paper_versions[Cell(0)][Cell(2)])
        link = ___899
        link.adopt(__stack__.all())
        ___900 = conf.sql(Cell('SELECT * FROM reviews WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        ___901 = filter_check(___900)[0]
        reviews = ___901
        reviews.adopt(__stack__.all())
        ___902 = conf.sql(Cell('SELECT * FROM comments WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        ___903 = filter_check(___902)[0]
        comments = ___903
        comments.adopt(__stack__.all())
        ___904 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([paper[Cell(2)]]))
        ___905 = filter_check(___904[Cell(0)])[0]
        author = ___905
        author.adopt(__stack__.all())
    else:
        paper = Cell(None)
        paper.adopt(__stack__.all())
        paper_versions = Cell([])
        paper_versions.adopt(__stack__.all())
        coauthors = Cell([])
        coauthors.adopt(__stack__.all())
        latest_abstract = Cell(None)
        latest_abstract.adopt(__stack__.all())
        latest_title = Cell(None)
        latest_title.adopt(__stack__.all())
        reviews = Cell([])
        reviews.adopt(__stack__.all())
        comments = Cell([])
        comments.adopt(__stack__.all())
        author = Cell(None)
        author.adopt(__stack__.all())
    __stack__.pop()
    __k__ = [Cell('paper'), Cell('paper_versions'), Cell('author'), Cell('coauthors'), Cell('latest_abstract'), Cell('latest_title'), Cell('reviews'), Cell('comments'), Cell('profile'), Cell('which_page'), Cell('link'), Cell('review_score_fields'), Cell('is_logged_in'), Cell('is_admin')]
    ___906 = is_admin()[0]
    ___907 = conf.render(Cell('paper.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), paper)),
        __k__[1].value: Cell((Cell('Service'), paper_versions)),
        __k__[2].value: Cell((Cell('Service'), author)),
        __k__[3].value: Cell((Cell('Service'), coauthors)),
        __k__[4].value: Cell((Cell('Service'), latest_abstract)),
        __k__[5].value: Cell((Cell('Service'), latest_title)),
        __k__[6].value: Cell((Cell('Service'), reviews)),
        __k__[7].value: Cell((Cell('Service'), comments)),
        __k__[8].value: Cell((Cell('Service'), user)),
        __k__[9].value: Cell((Cell('Service'), Cell('paper'))),
        __k__[10].value: Cell((Cell('Service'), link)),
        __k__[11].value: Cell((Cell('Service'), Cell([Cell((Cell('Novelty'), Cell('score_novelty'), Cell(10))), Cell((Cell('Presentation'), Cell('score_presentation'), Cell(10))), Cell((Cell('Technical'), Cell('score_technical'), Cell(10))), Cell((Cell('Confidence'), Cell('score_confidence'), Cell(10)))]))),
        __k__[12].value: Cell((Cell('Service'), Cell(True))),
        __k__[13].value: Cell((Cell('Service'), ___906)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs, __k__[9].inputs, __k__[10].inputs, __k__[11].inputs, __k__[12].inputs, __k__[13].inputs]))
    __r__ = Cell(___907, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/paper', methods=['GET', 'POST'])
def _paper_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = paper_view()
    return (__c__, __r__, __s__, __trace__)

def submit_view():
    global __stack__
    if ('coauthor' not in locals()):
        coauthor = Cell(None)
    if ('pcs2' not in locals()):
        pcs2 = Cell(None)
    if ('new_pc_conflict' not in locals()):
        new_pc_conflict = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('pc' not in locals()):
        pc = Cell(None)
    if ('coauthors' not in locals()):
        coauthors = Cell(None)
    if ('conflict' not in locals()):
        conflict = Cell(None)
    if ('abstract' not in locals()):
        abstract = Cell(None)
    if ('pcs' not in locals()):
        pcs = Cell(None)
    if ('pc_conflicts' not in locals()):
        pc_conflicts = Cell(None)
    if ('paper_id' not in locals()):
        paper_id = Cell(None)
    ___908 = is_logged_in()[0]
    ___909 = non(___908)
    __trace__.add_inputs(___909.inputs)
    __stack__.push()
    __stack__.add(___909.inputs, bot=True)
    if ___909:
        ___911 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___911, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___912 = conf.method()
    ___913 = Cell((___912 == Cell('POST')))
    __trace__.add_inputs(___913.inputs)
    __stack__.push()
    __stack__.add(___913.inputs, bot=True)
    coauthors.adopt(__stack__.all())
    coauthors.adopt(___913.inputs)
    new_pc_conflict.adopt(__stack__.all())
    new_pc_conflict.adopt(___913.inputs)
    conf.add_sql_inputs('papers', __stack__.all())
    conf.add_sql_inputs('papers', ___913.inputs)
    conf.add_sql_inputs('conf_paperversion', __stack__.all())
    conf.add_sql_inputs('conf_paperversion', ___913.inputs)
    abstract.adopt(__stack__.all())
    abstract.adopt(___913.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___913.inputs)
    title.adopt(__stack__.all())
    title.adopt(___913.inputs)
    conf.add_sql_inputs('conf_paperpcconflict', __stack__.all())
    conf.add_sql_inputs('conf_paperpcconflict', ___913.inputs)
    conf.add_sql_inputs('conf_papercoauthor', __stack__.all())
    conf.add_sql_inputs('conf_papercoauthor', ___913.inputs)
    paper_id.adopt(__stack__.all())
    paper_id.adopt(___913.inputs)
    if ___913:
        ___915 = conf.getlist('submit_view', Cell('coauthors[]'))
        coauthors = ___915
        coauthors.adopt(__stack__.all())
        ___916 = conf.post('submit_view', Cell('title'))
        title = ___916
        title.adopt(__stack__.all())
        ___917 = conf.post('submit_view', Cell('abstract'))
        abstract = ___917
        abstract.adopt(__stack__.all())
        ___918 = conf.post('submit_view', Cell('contents'))
        contents = ___918
        contents.adopt(__stack__.all())
        ___919 = conf.me()
        ___920 = conf.sql(Cell('INSERT INTO papers (author_id, accepted) VALUES (?0, ?1)'), Cell([___919, Cell(False)]))
        ___921 = conf.me()
        ___922 = conf.sql(Cell('SELECT id FROM papers WHERE author_id = ?0 ORDER BY id DESC LIMIT 1'), Cell([___921]))
        paper_id = ___922[Cell(0)][Cell(0)]
        paper_id.adopt(__stack__.all())
        ___923 = coauthors
        for coauthor in ___923:
            ___925 = conf.sql(Cell('INSERT INTO conf_papercoauthor (paper_id, author) VALUES (?0, ?1)'), Cell([paper_id, coauthor]))
        ___926 = conf.now_utc()
        ___927 = conf.sql(Cell('INSERT INTO conf_paperversion (paper_id, time, title, abstract, contents) \n                    VALUES (?0, ?1, ?2, ?3, ?4)'), Cell([paper_id, ___926, title, abstract, contents]))
        ___928 = conf.getlist('submit_view', Cell('pc_conflicts[]'))
        ___929 = ___928
        for conflict in ___929:
            ___931 = conf.sql(Cell('SELECT * FROM user_profiles WHERE username = ?0'), Cell([conflict]))
            new_pc_conflict = ___931[Cell(0)][Cell(0)]
            new_pc_conflict.adopt(__stack__.all())
            ___932 = conf.sql(Cell('INSERT INTO conf_paperpcconflict (paper_id, pc_id) values (?0, ?1)'), Cell([paper_id, new_pc_conflict]))
        ___933 = db_str(paper_id)
        ___934 = conf.redirect(Cell('Service'), (Cell('/paper?id=') + ___933))
        __r__ = Cell(___934, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___935 = conf.sql(Cell("SELECT * FROM user_profiles WHERE level = 'pc'"))
    pcs = ___935
    pcs.adopt(__stack__.all())
    pc_conflicts = Cell([])
    pc_conflicts.adopt(__stack__.all())
    pcs2 = Cell([])
    pcs2.adopt(__stack__.all())
    ___936 = conf.me()
    ___937 = conf.sql(Cell('SELECT pc_id FROM conf_userpcconflict WHERE id = ?0'), Cell([___936]))
    ___938 = ___937
    for pc in ___938:
        pc_conflicts = (pc_conflicts + Cell([pc[Cell(0)]]))
        pc_conflicts.adopt(__stack__.all())
    ___940 = pcs
    for pc in ___940:
        __k__ = [Cell('pc'), Cell('conflict')]
        pcs2 = (pcs2 + Cell([Cell({
            __k__[0].value: pc,
            __k__[1].value: pc.in_(pc_conflicts),
        }, inputs=[__k__[0].inputs, __k__[1].inputs])]))
        pcs2.adopt(__stack__.all())
    __k__ = [Cell('coauthors'), Cell('title'), Cell('abstract'), Cell('contents'), Cell('error'), Cell('pcs'), Cell('pc_conflicts'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___942 = is_admin()[0]
    ___943 = conf.render(Cell('submit.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), Cell([]))),
        __k__[1].value: Cell((Cell('Service'), Cell(''))),
        __k__[2].value: Cell((Cell('Service'), Cell(''))),
        __k__[3].value: Cell((Cell('Service'), Cell(''))),
        __k__[4].value: Cell((Cell('Service'), Cell(''))),
        __k__[5].value: Cell((Cell('Service'), pcs2)),
        __k__[6].value: Cell((Cell('Service'), pc_conflicts)),
        __k__[7].value: Cell((Cell('Service'), Cell('submit'))),
        __k__[8].value: Cell((Cell('Service'), Cell(True))),
        __k__[9].value: Cell((Cell('Service'), ___942)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs, __k__[9].inputs]))
    __r__ = Cell(___943, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/submit', methods=['GET', 'POST'])
def _submit_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = submit_view()
    return (__c__, __r__, __s__, __trace__)

def profile_view():
    global __stack__
    if ('affiliation' not in locals()):
        affiliation = Cell(None)
    if ('pcs2' not in locals()):
        pcs2 = Cell(None)
    if ('new_pc_conflict' not in locals()):
        new_pc_conflict = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('pc' not in locals()):
        pc = Cell(None)
    if ('profile' not in locals()):
        profile = Cell(None)
    if ('uppc' not in locals()):
        uppc = Cell(None)
    if ('acm_number' not in locals()):
        acm_number = Cell(None)
    if ('pcs' not in locals()):
        pcs = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('pc_conflicts' not in locals()):
        pc_conflicts = Cell(None)
    if ('confl' not in locals()):
        confl = Cell(None)
    ___944 = is_logged_in()[0]
    ___945 = non(___944)
    __trace__.add_inputs(___945.inputs)
    __stack__.push()
    __stack__.add(___945.inputs, bot=True)
    if ___945:
        ___947 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___947, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___948 = conf.me()
    ___949 = conf.sql(Cell('SELECT * FROM user_profiles where user_id = ?0'), Cell([___948]))
    profile = ___949[Cell(0)]
    profile.adopt(__stack__.all())
    ___950 = profile
    __stack__.push()
    __stack__.add(___950.inputs)
    profile[6].adopt(__stack__.all())
    profile[6].adopt(___950.inputs)
    if ___950:
        __slv__ = [Cell(6)]
        __w__ = profile
        for __i__ in range(1):
            __w__.adopt(__slv__[__i__].inputs)
            if (__i__ == 0):
                __w__[__slv__[__i__]] = Cell('normal')
            elif (__slv__[__i__] in __w__):
                __w__ = __w__[__slv__[__i__]]
            else:
                break
        profile.adopt(__stack__.all())
    __stack__.pop()
    ___952 = conf.sql(Cell("SELECT * FROM user_profiles WHERE level = 'pc'"))
    pcs = ___952
    pcs.adopt(__stack__.all())
    ___953 = conf.method()
    ___954 = Cell((___953 == Cell('POST')))
    __trace__.add_inputs(___954.inputs)
    __stack__.push()
    __stack__.add(___954.inputs, bot=True)
    name.adopt(__stack__.all())
    name.adopt(___954.inputs)
    conf.add_sql_inputs('user_profiles', __stack__.all())
    conf.add_sql_inputs('user_profiles', ___954.inputs)
    acm_number.adopt(__stack__.all())
    acm_number.adopt(___954.inputs)
    conf.add_sql_inputs('conf_userpcconflict', __stack__.all())
    conf.add_sql_inputs('conf_userpcconflict', ___954.inputs)
    affiliation.adopt(__stack__.all())
    affiliation.adopt(___954.inputs)
    pc_conflicts.adopt(__stack__.all())
    pc_conflicts.adopt(___954.inputs)
    new_pc_conflict.adopt(__stack__.all())
    new_pc_conflict.adopt(___954.inputs)
    email.adopt(__stack__.all())
    email.adopt(___954.inputs)
    if ___954:
        ___956 = conf.post('profile_view', Cell('name'))
        name = ___956
        name.adopt(__stack__.all())
        ___957 = conf.post('profile_view', Cell('affiliation'))
        affiliation = ___957
        affiliation.adopt(__stack__.all())
        ___958 = conf.post('profile_view', Cell('acm_number'))
        acm_number = ___958
        acm_number.adopt(__stack__.all())
        ___959 = conf.post('profile_view', Cell('email'))
        email = ___959
        email.adopt(__stack__.all())
        ___960 = conf.me()
        ___961 = conf.sql(Cell('DELETE FROM user_profiles WHERE user_id = ?0'), Cell([___960]))
        ___962 = conf.me()
        ___963 = conf.sql(Cell('INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level, password)\n                    VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6, ?7)'), Cell([___962, profile[Cell(1)], email, name, affiliation, acm_number, profile[Cell(6)], profile[Cell(7)]]))
        pc_conflicts = Cell([])
        pc_conflicts.adopt(__stack__.all())
        ___964 = conf.getlist('profile_view', Cell('pc_conflicts[]'))
        ___965 = ___964
        for confl in ___965:
            ___967 = conf.sql(Cell('SELECT id FROM user_profiles WHERE username = ?0'), Cell([confl]))
            new_pc_conflict = ___967[Cell(0)][Cell(0)]
            new_pc_conflict.adopt(__stack__.all())
            ___968 = conf.me()
            ___969 = conf.sql(Cell('INSERT INTO conf_userpcconflict (user_id, pc_id) VALUES (?0, ?1)'), Cell([___968, new_pc_conflict]))
            pc_conflicts = (pc_conflicts + Cell([new_pc_conflict]))
            pc_conflicts.adopt(__stack__.all())
    else:
        pc_conflicts = Cell([])
        pc_conflicts.adopt(__stack__.all())
        ___970 = conf.me()
        ___971 = conf.sql(Cell('SELECT id FROM conf_userpcconflict WHERE id = ?0'), Cell([___970]))
        ___972 = ___971
        for uppc in ___972:
            pc_conflicts = (pc_conflicts + Cell([uppc[Cell(0)]]))
            pc_conflicts.adopt(__stack__.all())
    __stack__.pop()
    pcs2 = Cell([])
    pcs2.adopt(__stack__.all())
    ___974 = pcs
    for pc in ___974:
        __k__ = [Cell('pc'), Cell('conflict')]
        pcs2 = (pcs2 + Cell([Cell({
            __k__[0].value: pc,
            __k__[1].value: pc.in_(pc_conflicts),
        }, inputs=[__k__[0].inputs, __k__[1].inputs])]))
        pcs2.adopt(__stack__.all())
    __k__ = [Cell('name'), Cell('affiliation'), Cell('acm_number'), Cell('pc_conflicts'), Cell('email'), Cell('which_page'), Cell('pcs'), Cell('is_logged_in'), Cell('is_admin')]
    ___976 = is_admin()[0]
    ___977 = conf.render(Cell('profile.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), profile[Cell(3)])),
        __k__[1].value: Cell((Cell('Service'), profile[Cell(4)])),
        __k__[2].value: Cell((Cell('Service'), profile[Cell(5)])),
        __k__[3].value: Cell((Cell('Service'), pc_conflicts)),
        __k__[4].value: Cell((Cell('Service'), profile[Cell(2)])),
        __k__[5].value: Cell((Cell('Service'), Cell('profile'))),
        __k__[6].value: Cell((Cell('Service'), pcs2)),
        __k__[7].value: Cell((Cell('Service'), Cell(True))),
        __k__[8].value: Cell((Cell('Service'), ___976)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs]))
    __r__ = Cell(___977, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/profile/', methods=['GET', 'POST'])
def _profile_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = profile_view()
    return (__c__, __r__, __s__, __trace__)

def submit_review_view():
    global __stack__
    if ('reviewer_id' not in locals()):
        reviewer_id = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('score_technical' not in locals()):
        score_technical = Cell(None)
    if ('score_novelty' not in locals()):
        score_novelty = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('score_confidence' not in locals()):
        score_confidence = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('score_presentation' not in locals()):
        score_presentation = Cell(None)
    if ('paper_id' not in locals()):
        paper_id = Cell(None)
    ___978 = me_user()[0]
    user = ___978
    user.adopt(__stack__.all())
    ___979 = non(user)
    __trace__.add_inputs(___979.inputs)
    __stack__.push()
    __stack__.add(___979.inputs, bot=True)
    if ___979:
        ___981 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___981, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___982 = conf.method()
    ___983 = Cell((___982 == Cell('GET')))
    __stack__.push()
    __stack__.add(___983.inputs)
    paper_id.adopt(__stack__.all())
    paper_id.adopt(___983.inputs)
    if ___983:
        ___985 = conf.get('submit_review_view', Cell('id'))
        ___986 = db_int(___985)
        paper_id = ___986
        paper_id.adopt(__stack__.all())
    else:
        ___987 = conf.method()
        ___988 = Cell((___987 == Cell('POST')))
        __stack__.push()
        __stack__.add(___988.inputs)
        paper_id.adopt(__stack__.all())
        paper_id.adopt(___988.inputs)
        if ___988:
            ___990 = conf.get('submit_review_view', Cell('id'))
            ___991 = db_int(___990)
            paper_id = ___991
            paper_id.adopt(__stack__.all())
        __stack__.pop()
    __stack__.pop()
    ___992 = query_db(Cell('SELECT * FROM papers WHERE id = ?0'), Cell([paper_id]))[0]
    paper = ___992[Cell(0)]
    paper.adopt(__stack__.all())
    ___993 = conf.method()
    ___994 = Cell((___993 == Cell('POST')))
    __trace__.add_inputs(___994.inputs)
    __stack__.push()
    __stack__.add(___994.inputs, bot=True)
    score_novelty.adopt(__stack__.all())
    score_novelty.adopt(___994.inputs)
    conf.add_sql_inputs('reviews', __stack__.all())
    conf.add_sql_inputs('reviews', ___994.inputs)
    score_confidence.adopt(__stack__.all())
    score_confidence.adopt(___994.inputs)
    score_technical.adopt(__stack__.all())
    score_technical.adopt(___994.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___994.inputs)
    score_presentation.adopt(__stack__.all())
    score_presentation.adopt(___994.inputs)
    if ___994:
        ___996 = conf.get('submit_review_view', Cell('contents'))
        contents = ___996
        contents.adopt(__stack__.all())
        ___997 = conf.get('submit_review_view', Cell('score_novelty'))
        score_novelty = ___997
        score_novelty.adopt(__stack__.all())
        ___998 = conf.get('submit_review_view', Cell('score_presentation'))
        score_presentation = ___998
        score_presentation.adopt(__stack__.all())
        ___999 = conf.get('submit_review_view', Cell('score_technical'))
        score_technical = ___999
        score_technical.adopt(__stack__.all())
        ___1000 = conf.get('submit_review_view', Cell('score_confidence'))
        score_confidence = ___1000
        score_confidence.adopt(__stack__.all())
        ___1001 = conf.sql(Cell('INSERT INTO reviews (contents, score_novelty, score_presentation, \n                                    score_technical, score_confidence, paper_id, reviewer_id)\n               VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([contents, score_novelty, score_presentation, score_technical, score_confidence, paper_id, reviewer_id]))
        ___1002 = db_str(paper_id)
        ___1003 = conf.redirect(Cell('Service'), (Cell('/paper?id=') + ___1002))
        __r__ = Cell(___1003, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('paper'), Cell('contents'), Cell('score_novelty'), Cell('score_presentation'), Cell('score_technical'), Cell('score_confidence'), Cell('paper_id'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1004 = is_admin()[0]
    ___1005 = conf.render(Cell('submit_review.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), paper)),
        __k__[1].value: Cell((Cell('Service'), contents)),
        __k__[2].value: Cell((Cell('Service'), score_novelty)),
        __k__[3].value: Cell((Cell('Service'), score_presentation)),
        __k__[4].value: Cell((Cell('Service'), score_technical)),
        __k__[5].value: Cell((Cell('Service'), score_confidence)),
        __k__[6].value: Cell((Cell('Service'), paper_id)),
        __k__[7].value: Cell((Cell('Service'), Cell('submit_review'))),
        __k__[8].value: Cell((Cell('Service'), Cell(True))),
        __k__[9].value: Cell((Cell('Service'), ___1004)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs, __k__[9].inputs]))
    __r__ = Cell(___1005, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/submit_review', methods=['GET', 'POST'])
def _submit_review_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = submit_review_view()
    return (__c__, __r__, __s__, __trace__)

def users_view():
    global __stack__
    if ('user_profiles' not in locals()):
        user_profiles = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    ___1006 = me_user()[0]
    user = ___1006
    user.adopt(__stack__.all())
    ___1007 = non(user)
    __trace__.add_inputs(___1007.inputs)
    __stack__.push()
    __stack__.add(___1007.inputs, bot=True)
    if ___1007:
        ___1009 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___1009, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1010 = Cell((user[(- Cell(1))] != Cell('chair')))
    __trace__.add_inputs(___1010.inputs)
    __stack__.push()
    __stack__.add(___1010.inputs, bot=True)
    if ___1010:
        ___1012 = conf.redirect(Cell('Service'), Cell('/papers_view'))
        __r__ = Cell(___1012, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1013 = conf.sql(Cell('SELECT * FROM user_profiles'))
    user_profiles = ___1013
    user_profiles.adopt(__stack__.all())
    __k__ = [Cell('user_profiles'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1014 = conf.render(Cell('users_view.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_profiles)),
        __k__[1].value: Cell((Cell('Service'), Cell('users'))),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1014, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/users', methods=['GET', 'POST'])
def _users_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = users_view()
    return (__c__, __r__, __s__, __trace__)

def set_level(level, user_id):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1015 = me_user()[0]
    user = ___1015
    user.adopt(__stack__.all())
    ___1016 = user
    __trace__.add_inputs(___1016.inputs)
    __stack__.push()
    __stack__.add(___1016.inputs, bot=True)
    conf.add_sql_inputs('user_profiles', __stack__.all())
    conf.add_sql_inputs('user_profiles', ___1016.inputs)
    user.adopt(__stack__.all())
    user.adopt(___1016.inputs)
    if ___1016:
        ___1018 = Cell((Cell((user[(- Cell(1))] == Cell('chair'))).value and level.in_(Cell([Cell('normal'), Cell('pc'), Cell('chair')])).value), inputs=(Cell((user[(- Cell(1))] == Cell('chair'))).inputs + level.in_(Cell([Cell('normal'), Cell('pc'), Cell('chair')])).inputs))
        __stack__.push()
        __stack__.add(___1018.inputs)
        conf.add_sql_inputs('user_profiles', __stack__.all())
        conf.add_sql_inputs('user_profiles', ___1018.inputs)
        user.adopt(__stack__.all())
        user.adopt(___1018.inputs)
        if ___1018:
            ___1020 = conf.sql(Cell('SELECT (username, email, name, affiliation, acm_number, level)\n                               FROM user_profiles WHERE user_id = ?0'), Cell([user_id]))
            user = ___1020[Cell(0)]
            user.adopt(__stack__.all())
            ___1021 = conf.sql(Cell('DELETE FROM user_profiles WHERE user_id = ?0'), Cell([user_id]))
            ___1022 = conf.sql(Cell('INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level)\n                      VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([user_id, user[Cell(0)], user[Cell(1)], user[Cell(2)], user[Cell(3)], user[Cell(4)], level]))
        __stack__.pop()
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@conf.route('/set_level/<level>/<user_id>', methods=['GET'])
def _set_level(level, user_id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    level = conf.register('set_level', 'level', level)
    user_id = conf.register('set_level', 'user_id', user_id)
    (__r__, __s__) = set_level(level, user_id)
    return (__c__, __r__, __s__, __trace__)

def submit_comment_view():
    global __stack__
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('paper_id' not in locals()):
        paper_id = Cell(None)
    ___1023 = me_user()[0]
    user = ___1023
    user.adopt(__stack__.all())
    ___1024 = non(user)
    __trace__.add_inputs(___1024.inputs)
    __stack__.push()
    __stack__.add(___1024.inputs, bot=True)
    if ___1024:
        ___1026 = conf.redirect(Cell('Service'), Cell('/login'))
        __r__ = Cell(___1026, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1027 = conf.method()
    ___1028 = Cell((___1027 == Cell('GET')))
    __stack__.push()
    __stack__.add(___1028.inputs)
    paper_id.adopt(__stack__.all())
    paper_id.adopt(___1028.inputs)
    if ___1028:
        ___1030 = conf.get('submit_comment_view', Cell('id'))
        ___1031 = db_int(___1030)
        paper_id = ___1031
        paper_id.adopt(__stack__.all())
    else:
        ___1032 = conf.method()
        ___1033 = Cell((___1032 == Cell('POST')))
        __stack__.push()
        __stack__.add(___1033.inputs)
        paper_id.adopt(__stack__.all())
        paper_id.adopt(___1033.inputs)
        if ___1033:
            ___1035 = conf.get('submit_comment_view', Cell('id'))
            ___1036 = db_int(___1035)
            paper_id = ___1036
            paper_id.adopt(__stack__.all())
        __stack__.pop()
    __stack__.pop()
    ___1037 = conf.sql(Cell('SELECT * FROM papers WHERE id = ?0'), Cell([paper_id]))
    paper = ___1037[Cell(0)]
    paper.adopt(__stack__.all())
    ___1038 = conf.method()
    ___1039 = Cell((___1038 == Cell('POST')))
    __trace__.add_inputs(___1039.inputs)
    __stack__.push()
    __stack__.add(___1039.inputs, bot=True)
    conf.add_sql_inputs('comments', __stack__.all())
    conf.add_sql_inputs('comments', ___1039.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___1039.inputs)
    if ___1039:
        ___1041 = conf.get('submit_comment_view', Cell('contents'))
        contents = ___1041
        contents.adopt(__stack__.all())
        ___1042 = conf.me()
        ___1043 = conf.sql(Cell('INSERT INTO comments (contents, paper_id, user_id) VALUES (?0, ?1, ?2)'), Cell([contents, paper_id, ___1042]))
        ___1044 = db_str(paper_id)
        ___1045 = conf.redirect(Cell('Service'), (Cell('/paper?id=') + ___1044))
        __r__ = Cell(___1045, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('paper'), Cell('contents'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1046 = is_admin()[0]
    ___1047 = conf.render(Cell('submit_comment.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), paper)),
        __k__[1].value: Cell((Cell('Service'), contents)),
        __k__[2].value: Cell((Cell('Service'), Cell('submit_comment'))),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
        __k__[4].value: Cell((Cell('Service'), ___1046)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___1047, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/submit_comment', methods=['GET', 'POST'])
def _submit_comment_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = submit_comment_view()
    return (__c__, __r__, __s__, __trace__)

def assign_reviews_view():
    global __stack__
    if ('reviewer_id' not in locals()):
        reviewer_id = Cell(None)
    if ('papers' not in locals()):
        papers = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('possible_reviewers' not in locals()):
        possible_reviewers = Cell(None)
    if ('reviewer' not in locals()):
        reviewer = Cell(None)
    if ('reviewers' not in locals()):
        reviewers = Cell(None)
    if ('papers_data' not in locals()):
        papers_data = Cell(None)
    ___1048 = is_logged_in()[0]
    ___1049 = non(___1048)
    __trace__.add_inputs(___1049.inputs)
    __stack__.push()
    __stack__.add(___1049.inputs, bot=True)
    if ___1049:
        ___1051 = conf.redirect(Cell('Service'), Cell('/login'))
        __r__ = Cell(___1051, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1052 = conf.sql(Cell("SELECT * FROM user_profiles WHERE level = 'pc'"))
    possible_reviewers = ___1052
    possible_reviewers.adopt(__stack__.all())
    ___1053 = conf.get('assign_reviews_view', Cell('reviewer_username'))
    reviewer_id = ___1053
    reviewer_id.adopt(__stack__.all())
    ___1054 = reviewer_id
    __stack__.push()
    __stack__.add(___1054.inputs)
    reviewer.adopt(__stack__.all())
    reviewer.adopt(___1054.inputs)
    papers_data.adopt(__stack__.all())
    papers_data.adopt(___1054.inputs)
    reviewers.adopt(__stack__.all())
    reviewers.adopt(___1054.inputs)
    papers.adopt(__stack__.all())
    papers.adopt(___1054.inputs)
    if ___1054:
        ___1056 = conf.sql(Cell('SELECT * FROM papers'))
        papers = ___1056
        papers.adopt(__stack__.all())
        papers_data = Cell([])
        papers_data.adopt(__stack__.all())
        ___1057 = conf.sql(Cell('SELECT * FROM reviewers WHERE id = ?0'), Cell([reviewer_id]))
        reviewers = ___1057
        reviewers.adopt(__stack__.all())
        ___1058 = papers
        for paper in ___1058:
            __k__ = [Cell('paper'), Cell('author_name'), Cell('latest_version'), Cell('assignment'), Cell('has_conflict')]
            ___1060 = conf.sql(Cell('SELECT name FROM user_profiles WHERE user_id = ?0'), Cell([reviewer_id]))
            ___1061 = conf.sql(Cell('SELECT * FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC'), Cell([paper[Cell(0)]]))
            ___1062 = conf.sql(Cell('SELECT * FROM review_assignments WHERE paper_id = ?0 AND user_id = ?1'), Cell([paper[Cell(0)], reviewer_id]))
            ___1063 = conf.sql(Cell('SELECT * FROM conf_paperpcconflict WHERE pc_id = ?0 and paper_id = ?1'), Cell([reviewer_id, paper[Cell(0)]]))
            papers = (papers + Cell([Cell({
                __k__[0].value: paper[Cell(0)],
                __k__[1].value: ___1060[Cell(0)][Cell(0)],
                __k__[2].value: ___1061[(- Cell(1))],
                __k__[3].value: ___1062,
                __k__[4].value: Cell((___1063[Cell(0)] != Cell(None))),
            }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs])]))
            papers.adopt(__stack__.all())
    else:
        reviewer = Cell(None)
        reviewer.adopt(__stack__.all())
        papers_data = Cell([])
        papers_data.adopt(__stack__.all())
    __stack__.pop()
    __k__ = [Cell('reviewer'), Cell('possible_reviewers'), Cell('papers_data'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1064 = is_admin()[0]
    ___1065 = conf.render(Cell('assign_reviews.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), reviewer)),
        __k__[1].value: Cell((Cell('Service'), possible_reviewers)),
        __k__[2].value: Cell((Cell('Service'), papers_data)),
        __k__[3].value: Cell((Cell('Service'), Cell('assign_reviews'))),
        __k__[4].value: Cell((Cell('Service'), Cell(True))),
        __k__[5].value: Cell((Cell('Service'), ___1064)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs]))
    __r__ = Cell(___1065, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/assign_reviews', methods=['GET', 'POST'])
def _assign_reviews_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = assign_reviews_view()
    return (__c__, __r__, __s__, __trace__)

def assign_reviewer(paper_id, reviewer_id, value):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1066 = me_user()[0]
    user = ___1066
    user.adopt(__stack__.all())
    ___1067 = user
    __trace__.add_inputs(___1067.inputs)
    __stack__.push()
    __stack__.add(___1067.inputs, bot=True)
    conf.add_sql_inputs('review_assignments', __stack__.all())
    conf.add_sql_inputs('review_assignments', ___1067.inputs)
    if ___1067:
        ___1069 = Cell((user[(- Cell(1))] == Cell('chair')))
        __stack__.push()
        __stack__.add(___1069.inputs)
        conf.add_sql_inputs('review_assignments', __stack__.all())
        conf.add_sql_inputs('review_assignments', ___1069.inputs)
        if ___1069:
            ___1071 = Cell((value == Cell('yes')))
            __stack__.push()
            __stack__.add(___1071.inputs)
            conf.add_sql_inputs('review_assignments', __stack__.all())
            conf.add_sql_inputs('review_assignments', ___1071.inputs)
            if ___1071:
                ___1073 = conf.sql(Cell('INSERT INTO review_assignments (assign_type, paper_id, user_id) VALUES (?0, ?1, ?2)'), Cell([Cell('assigned'), paper_id, reviewer_id]))
            else:
                ___1074 = conf.sql(Cell('DELETE FROM review_assignments WHERE paper_id = ?0 AND reviewer_id = ?1'), Cell([paper_id, reviewer_id]))
            __stack__.pop()
        __stack__.pop()
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@conf.route('/assign_reviewer/<paper_id>/<reviewer_id>/<value>', methods=['GET'])
def _assign_reviewer(paper_id, reviewer_id, value):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    paper_id = conf.register('assign_reviewer', 'paper_id', paper_id)
    reviewer_id = conf.register('assign_reviewer', 'reviewer_id', reviewer_id)
    value = conf.register('assign_reviewer', 'value', value)
    (__r__, __s__) = assign_reviewer(paper_id, reviewer_id, value)
    return (__c__, __r__, __s__, __trace__)

def search_view():
    global __stack__
    if ('reviewers' not in locals()):
        reviewers = Cell(None)
    if ('authors' not in locals()):
        authors = Cell(None)
    ___1075 = is_logged_in()[0]
    ___1076 = non(___1075)
    __trace__.add_inputs(___1076.inputs)
    __stack__.push()
    __stack__.add(___1076.inputs, bot=True)
    if ___1076:
        ___1078 = conf.redirect(Cell('Service'), Cell('/login'))
        __r__ = Cell(___1078, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1079 = conf.sql(Cell('SELECT * FROM user_profiles'))
    reviewers = ___1079
    reviewers.adopt(__stack__.all())
    ___1080 = conf.sql(Cell('SELECT * FROM user_profiles'))
    authors = ___1080
    authors.adopt(__stack__.all())
    __k__ = [Cell('reviewers'), Cell('authors'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1081 = is_admin()[0]
    ___1082 = conf.render(Cell('search.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), reviewers)),
        __k__[1].value: Cell((Cell('Service'), authors)),
        __k__[2].value: Cell((Cell('Service'), Cell('search'))),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
        __k__[4].value: Cell((Cell('Service'), ___1081)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___1082, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/search')
def _search_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = search_view()
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    ___1083 = is_logged_in()[0]
    ___1084 = ___1083
    __trace__.add_inputs(___1084.inputs)
    __stack__.push()
    __stack__.add(___1084.inputs, bot=True)
    if ___1084:
        ___1086 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1086, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1087 = conf.render(Cell('registration/login.html'))
    __r__ = Cell(___1087, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def do_login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1088 = is_logged_in()[0]
    ___1089 = ___1088
    __trace__.add_inputs(___1089.inputs)
    __stack__.push()
    __stack__.add(___1089.inputs, bot=True)
    if ___1089:
        ___1091 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1091, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1092 = conf.me()
    ___1093 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([___1092]))
    user = ___1093
    user.adopt(__stack__.all())
    ___1094 = db_len(user)
    ___1095 = Cell((___1094 > Cell(0)))
    __trace__.add_inputs(___1095.inputs)
    __stack__.push()
    __stack__.add(___1095.inputs, bot=True)
    conf.add_session_inputs('loggedin', __stack__.all())
    conf.add_session_inputs('loggedin', ___1095.inputs)
    if ___1095:
        ___1097 = conf.set_session(Cell('loggedin'), Cell(True))
        ___1098 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1098, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('error')]
        ___1099 = conf.render(Cell('registration/login.html'), Cell({
            __k__[0].value: Cell((Cell('Service'), Cell('Invalid user (register to access)'))),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___1099, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@conf.route('/accounts/do_login')
def _do_login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = do_login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('affiliation' not in locals()):
        affiliation = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    if ('username' not in locals()):
        username = Cell(None)
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    if ('acm_number' not in locals()):
        acm_number = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___1100 = is_logged_in()[0]
    ___1101 = ___1100
    __trace__.add_inputs(___1101.inputs)
    __stack__.push()
    __stack__.add(___1101.inputs, bot=True)
    if ___1101:
        ___1103 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1103, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1104 = conf.me()
    user_id = ___1104
    user_id.adopt(__stack__.all())
    ___1105 = conf.name()
    username = ___1105
    username.adopt(__stack__.all())
    ___1106 = conf.method()
    ___1107 = Cell((___1106 == Cell('POST')))
    __trace__.add_inputs(___1107.inputs)
    __stack__.push()
    __stack__.add(___1107.inputs, bot=True)
    affiliation.adopt(__stack__.all())
    affiliation.adopt(___1107.inputs)
    level.adopt(__stack__.all())
    level.adopt(___1107.inputs)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___1107.inputs)
    email.adopt(__stack__.all())
    email.adopt(___1107.inputs)
    conf.add_sql_inputs('user_profiles', __stack__.all())
    conf.add_sql_inputs('user_profiles', ___1107.inputs)
    acm_number.adopt(__stack__.all())
    acm_number.adopt(___1107.inputs)
    name.adopt(__stack__.all())
    name.adopt(___1107.inputs)
    if ___1107:
        ___1109 = conf.me()
        ___1110 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([___1109]))
        already_registered = ___1110
        already_registered.adopt(__stack__.all())
        ___1111 = db_len(already_registered)
        ___1112 = Cell((___1111 == Cell(0)))
        __stack__.push()
        __stack__.add(___1112.inputs)
        affiliation.adopt(__stack__.all())
        affiliation.adopt(___1112.inputs)
        level.adopt(__stack__.all())
        level.adopt(___1112.inputs)
        email.adopt(__stack__.all())
        email.adopt(___1112.inputs)
        conf.add_sql_inputs('user_profiles', __stack__.all())
        conf.add_sql_inputs('user_profiles', ___1112.inputs)
        acm_number.adopt(__stack__.all())
        acm_number.adopt(___1112.inputs)
        name.adopt(__stack__.all())
        name.adopt(___1112.inputs)
        if ___1112:
            ___1114 = conf.post('register', Cell('email'))
            email = ___1114
            email.adopt(__stack__.all())
            ___1115 = conf.post('register', Cell('name'))
            name = ___1115
            name.adopt(__stack__.all())
            ___1116 = conf.post('register', Cell('affiliation'))
            affiliation = ___1116
            affiliation.adopt(__stack__.all())
            ___1117 = conf.post('register', Cell('acm_number'))
            acm_number = ___1117
            acm_number.adopt(__stack__.all())
            level = Cell('normal')
            level.adopt(__stack__.all())
            ___1118 = conf.sql(Cell('INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level)\n                     VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([user_id, username, email, name, affiliation, acm_number, level]))
        __stack__.pop()
        ___1119 = conf.redirect(Cell('Service'), Cell('/accounts/do_login'))
        __r__ = Cell(___1119, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('username')]
    ___1120 = conf.render(Cell('registration/register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), username)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1120, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___1121 = conf.pop_session(Cell('loggedin'))
    ___1122 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
    __r__ = Cell(___1122, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)

def handle_comments_contents_deletion(id_):
    global __stack__
    ___1123 = conf.sql(Cell('DELETE FROM comments WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('comments', ['contents', 'paper_id'])
def _handle_comments_contents_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_comments_contents_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_comments_paper_id_rectification(id_, val, can):
    global __stack__
    ___1124 = conf.sql(Cell('DELETE FROM comments WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_rectification('comments', 'paper_id')
def _handle_comments_paper_id_rectification(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_comments_paper_id_rectification(id_, val, can)
    return (__c__, __r__, __s__, __trace__)

def handle_conf_papercoauthor_author_deletion(id_):
    global __stack__
    ___1125 = conf.sql(Cell('DELETE FROM conf_papercoauthor WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('conf_papercoauthor', ['author', 'paper_id'])
def _handle_conf_papercoauthor_author_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_conf_papercoauthor_author_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_conf_paperpcconflict_pc_id_deletion(id_):
    global __stack__
    ___1126 = conf.sql(Cell('DELETE FROM conf_paperpcconflict WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('conf_paperpcconflict', 'pc_id')
def _handle_conf_paperpcconflict_pc_id_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_conf_paperpcconflict_pc_id_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_conf_paperversion_title_deletion(id_):
    global __stack__
    ___1127 = conf.sql(Cell('DELETE FROM conf_paperversion WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('conf_paperversion', ['title', 'contents', 'abstract'])
def _handle_conf_paperversion_title_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_conf_paperversion_title_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_conf_paperversion_title_rectification(id_, val, can):
    global __stack__
    ___1128 = conf.sql(Cell('DELETE FROM conf_paperversion WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_rectification('conf_paperversion', ['title', 'contents', 'abstract'])
def _handle_conf_paperversion_title_rectification(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_conf_paperversion_title_rectification(id_, val, can)
    return (__c__, __r__, __s__, __trace__)

def handle_conf_userpcconflict_pc_id_deletion(id_):
    global __stack__
    ___1129 = conf.sql(Cell('DELETE FROM conf_userpcconflict WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('conf_userpcconflict', 'pc_id')
def _handle_conf_userpcconflict_pc_id_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_conf_userpcconflict_pc_id_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_review_assignments_paper_id_deletion(id_):
    global __stack__
    ___1130 = conf.sql(Cell('DELETE FROM review_assignments WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('review_assignments', ['paper_id', 'user_id'])
def _handle_review_assignments_paper_id_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_review_assignments_paper_id_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_reviews_contents_deletion(id_):
    global __stack__
    ___1131 = conf.sql(Cell('DELETE FROM reviews WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('reviews', ['contents', 'score_novelty', 'score_presentation', 'score_technical', 'score_confidence', 'paper_id'])
def _handle_reviews_contents_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_reviews_contents_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_tags_name_deletion(id_):
    global __stack__
    ___1132 = conf.sql(Cell('DELETE FROM tags WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('tags', ['name', 'paper_id'])
def _handle_tags_name_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_tags_name_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_user_profiles_username_deletion(id_):
    global __stack__
    ___1133 = conf.sql(Cell('DELETE FROM user_profiles WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.handle_field_deletion('user_profiles', ['username', 'email', 'name', 'affiliation', 'acm_number'])
def _handle_user_profiles_username_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_user_profiles_username_deletion(id_)
    return (__c__, __r__, __s__, __trace__)
