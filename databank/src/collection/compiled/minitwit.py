
from apps import minitwit
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___1293 = minitwit.get_session(Cell('loggedin'))
    __r__ = Cell(___1293, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___1294 = is_logged_in()[0]
    ___1295 = ___1294
    __trace__.add_inputs(___1295.inputs)
    __stack__.push()
    __stack__.add(___1295.inputs, bot=True)
    if ___1295:
        ___1297 = minitwit.me()
        ___1298 = minitwit.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___1297]))
        __r__ = Cell(___1298[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def filter_check(messages):
    global __stack__
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('message' not in locals()):
        message = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___1299 = messages
    for message in ___1299:
        ___1301 = minitwit.check('filter_check', __c__, __trace__, Cell('Service'), message)
        ___1302 = ___1301
        __stack__.push()
        __stack__.add(___1302.inputs)
        messages2.adopt(__stack__.all())
        messages2.adopt(___1302.inputs)
        if ___1302:
            messages2 = (messages2 + Cell([message]))
            messages2.adopt(__stack__.all())
        __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def get_user_id(username):
    global __stack__
    if ('rv' not in locals()):
        rv = Cell(None)
    ___1304 = minitwit.sql(Cell('SELECT id FROM user WHERE username = ?0'), Cell([username]))
    rv = ___1304
    rv.adopt(__stack__.all())
    ___1305 = db_len(rv)
    ___1306 = Cell((___1305 > Cell(0)))
    __trace__.add_inputs(___1306.inputs)
    __stack__.push()
    __stack__.add(___1306.inputs, bot=True)
    if ___1306:
        __r__ = Cell(rv[Cell(0)][Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def timeline():
    global __stack__
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('messages12' not in locals()):
        messages12 = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('messages1' not in locals()):
        messages1 = Cell(None)
    ___1308 = me_user()[0]
    user = ___1308
    user.adopt(__stack__.all())
    ___1309 = non(user)
    __trace__.add_inputs(___1309.inputs)
    __stack__.push()
    __stack__.add(___1309.inputs, bot=True)
    if ___1309:
        ___1311 = minitwit.redirect(Cell('Service'), Cell('/public'))
        __r__ = Cell(___1311, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1312 = minitwit.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       JOIN follower ON follower.whom_id = user.id\n       WHERE follower.who_id = ?0\n       ORDER BY message.pub_date DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages1 = ___1312
    messages1.adopt(__stack__.all())
    ___1313 = minitwit.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages2 = ___1313
    messages2.adopt(__stack__.all())
    ___1314 = db_sorted_by0((messages1 + messages2))
    ___1315 = filter_check(___1314)[0]
    messages12 = ___1315
    messages12.adopt(__stack__.all())
    messages = Cell([])
    messages.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___1316 = db_len(messages12)
    ___1317 = Cell((Cell((i < Cell(30))).value and Cell((i < ___1316)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___1316)).inputs))
    __stack__.push()
    __stack__.add(___1317.inputs)
    messages.adopt(__stack__.all())
    messages.adopt(___1317.inputs)
    i.adopt(__stack__.all())
    i.adopt(___1317.inputs)
    while ___1317:
        messages = (messages + Cell([messages12[i]]))
        messages.adopt(__stack__.all())
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___1317 = Cell((Cell((i < Cell(30))).value and Cell((i < ___1316)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___1316)).inputs))
        __stack__.add(___1317.inputs)
        messages.adopt(__stack__.all())
        messages.adopt(___1317.inputs)
        i.adopt(__stack__.all())
        i.adopt(___1317.inputs)
    __stack__.pop()
    __k__ = [Cell('messages'), Cell('user'), Cell('title'), Cell('flash')]
    ___1319 = minitwit.unflash()
    ___1320 = minitwit.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), user)),
        __k__[2].value: Cell((Cell('Service'), Cell('My Timeline'))),
        __k__[3].value: Cell((Cell('Service'), ___1319)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1320, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/')
def _timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = timeline()
    return (__c__, __r__, __s__, __trace__)

def public_timeline():
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    ___1321 = minitwit.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       ORDER BY message.pub_date DESC LIMIT 30'))
    messages = ___1321
    messages.adopt(__stack__.all())
    ___1322 = filter_check(messages)[0]
    messages = ___1322
    messages.adopt(__stack__.all())
    __k__ = [Cell('messages'), Cell('user'), Cell('title')]
    ___1323 = me_user()[0]
    ___1324 = minitwit.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), ___1323)),
        __k__[2].value: Cell((Cell('Service'), Cell('Public Timeline'))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1324, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/public')
def _public_timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = public_timeline()
    return (__c__, __r__, __s__, __trace__)

def user_timeline(username):
    global __stack__
    if ('followed' not in locals()):
        followed = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('profile_user' not in locals()):
        profile_user = Cell(None)
    ___1325 = minitwit.sql(Cell('SELECT * FROM user WHERE username = ?0'), Cell([username]))
    profile_user = ___1325
    profile_user.adopt(__stack__.all())
    ___1326 = db_len(profile_user)
    ___1327 = Cell((___1326 == Cell(0)))
    __trace__.add_inputs(___1327.inputs)
    __stack__.push()
    __stack__.add(___1327.inputs, bot=True)
    if ___1327:
        ___1329 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1329, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    followed = Cell(False)
    followed.adopt(__stack__.all())
    ___1330 = me_user()[0]
    user = ___1330
    user.adopt(__stack__.all())
    ___1331 = user
    __stack__.push()
    __stack__.add(___1331.inputs)
    followed.adopt(__stack__.all())
    followed.adopt(___1331.inputs)
    if ___1331:
        ___1333 = minitwit.sql(Cell('\n      SELECT 1 FROM follower WHERE follower.who_id = ?0 AND follower.whom_id = ?1'), Cell([user[Cell(0)], profile_user[Cell(0)][Cell(0)]]))
        ___1334 = db_len(___1333)
        followed = Cell((___1334 > Cell(0)))
        followed.adopt(__stack__.all())
    __stack__.pop()
    ___1335 = minitwit.sql(Cell('\n      SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n             user.user_id, user.username FROM message\n      JOIN user ON user.id = message.author_id\n      WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30'), Cell([profile_user[Cell(0)][Cell(0)]]))
    messages = ___1335
    messages.adopt(__stack__.all())
    ___1336 = filter_check(messages)[0]
    messages = ___1336
    messages.adopt(__stack__.all())
    __k__ = [Cell('messages'), Cell('followed'), Cell('profile_user'), Cell('user'), Cell('flash')]
    ___1337 = minitwit.unflash()
    ___1338 = minitwit.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), followed)),
        __k__[2].value: Cell((Cell('Service'), profile_user[Cell(0)])),
        __k__[3].value: Cell((Cell('Service'), user)),
        __k__[4].value: Cell((Cell('Service'), ___1337)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___1338, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/<username>')
def _user_timeline(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwit.register('user_timeline', 'username', username)
    (__r__, __s__) = user_timeline(username)
    return (__c__, __r__, __s__, __trace__)

def follow_user(username):
    global __stack__
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    ___1339 = me_user()[0]
    user = ___1339
    user.adopt(__stack__.all())
    ___1340 = non(user)
    __trace__.add_inputs(___1340.inputs)
    __stack__.push()
    __stack__.add(___1340.inputs, bot=True)
    if ___1340:
        ___1342 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1342, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1343 = get_user_id(username)[0]
    whom_id = ___1343
    whom_id.adopt(__stack__.all())
    ___1344 = non(whom_id)
    __trace__.add_inputs(___1344.inputs)
    __stack__.push()
    __stack__.add(___1344.inputs, bot=True)
    if ___1344:
        ___1346 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1346, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1347 = minitwit.sql(Cell('INSERT INTO follower (who_id, whom_id) VALUES (?0, ?1)'), Cell([user[Cell(0)], whom_id]))
    ___1348 = minitwit.flash(((Cell('You are now following "') + username) + Cell('"')))
    ___1349 = minitwit.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___1349, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/<username>/follow')
def _follow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwit.register('follow_user', 'username', username)
    (__r__, __s__) = follow_user(username)
    return (__c__, __r__, __s__, __trace__)

def unfollow_user(username):
    global __stack__
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    ___1350 = me_user()[0]
    user = ___1350
    user.adopt(__stack__.all())
    ___1351 = non(user)
    __trace__.add_inputs(___1351.inputs)
    __stack__.push()
    __stack__.add(___1351.inputs, bot=True)
    if ___1351:
        ___1353 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1353, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1354 = get_user_id(username)[0]
    whom_id = ___1354
    whom_id.adopt(__stack__.all())
    ___1355 = non(whom_id)
    __trace__.add_inputs(___1355.inputs)
    __stack__.push()
    __stack__.add(___1355.inputs, bot=True)
    if ___1355:
        ___1357 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1357, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1358 = minitwit.sql(Cell('DELETE FROM follower WHERE who_id = ?0 AND whom_id = ?1'), Cell([user[Cell(0)], whom_id]))
    ___1359 = minitwit.flash(((Cell('You are no longer following "') + username) + Cell('"')))
    ___1360 = minitwit.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___1360, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/<username>/unfollow')
def _unfollow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwit.register('unfollow_user', 'username', username)
    (__r__, __s__) = unfollow_user(username)
    return (__c__, __r__, __s__, __trace__)

def add_message():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1361 = me_user()[0]
    user = ___1361
    user.adopt(__stack__.all())
    ___1362 = non(user)
    __trace__.add_inputs(___1362.inputs)
    __stack__.push()
    __stack__.add(___1362.inputs, bot=True)
    if ___1362:
        ___1364 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1364, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1365 = minitwit.method()
    ___1366 = Cell((___1365 == Cell('POST')))
    __trace__.add_inputs(___1366.inputs)
    __stack__.push()
    __stack__.add(___1366.inputs, bot=True)
    minitwit.add_sql_inputs('message', __stack__.all())
    minitwit.add_sql_inputs('message', ___1366.inputs)
    if ___1366:
        ___1368 = minitwit.post('add_message', Cell('text'))
        ___1369 = minitwit.now_utc()
        ___1370 = minitwit.sql(Cell('INSERT INTO message (author_id, text, pub_date) \n          VALUES (?0, ?1, ?2)'), Cell([user[Cell(0)], ___1368, ___1369]))
        ___1371 = minitwit.flash(Cell('Your message was recorded'))
    __stack__.pop()
    ___1372 = minitwit.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___1372, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/add_message', methods=['POST'])
def _add_message():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_message()
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1373 = is_logged_in()[0]
    ___1374 = ___1373
    __trace__.add_inputs(___1374.inputs)
    __stack__.push()
    __stack__.add(___1374.inputs, bot=True)
    if ___1374:
        ___1376 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1376, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1377 = minitwit.me()
    ___1378 = minitwit.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___1377]))
    user = ___1378
    user.adopt(__stack__.all())
    ___1379 = db_len(user)
    ___1380 = Cell((___1379 > Cell(0)))
    __stack__.push()
    __stack__.add(___1380.inputs)
    minitwit.add_session_inputs('loggedin', __stack__.all())
    minitwit.add_session_inputs('loggedin', ___1380.inputs)
    if ___1380:
        ___1382 = minitwit.set_session(Cell('loggedin'), Cell(True))
        ___1383 = minitwit.flash(Cell('You were logged in'))
    else:
        ___1384 = minitwit.flash(Cell('Invalid user (register to access)'))
    __stack__.pop()
    ___1385 = minitwit.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___1385, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('new_user' not in locals()):
        new_user = Cell(None)
    if ('user_name' not in locals()):
        user_name = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    ___1386 = is_logged_in()[0]
    ___1387 = ___1386
    __trace__.add_inputs(___1387.inputs)
    __stack__.push()
    __stack__.add(___1387.inputs, bot=True)
    if ___1387:
        ___1389 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1389, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1390 = minitwit.me()
    user_id = ___1390
    user_id.adopt(__stack__.all())
    ___1391 = minitwit.name()
    user_name = ___1391
    user_name.adopt(__stack__.all())
    ___1392 = minitwit.method()
    ___1393 = Cell((___1392 == Cell('POST')))
    __trace__.add_inputs(___1393.inputs)
    __stack__.push()
    __stack__.add(___1393.inputs, bot=True)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___1393.inputs)
    minitwit.add_sql_inputs('user', __stack__.all())
    minitwit.add_sql_inputs('user', ___1393.inputs)
    new_user.adopt(__stack__.all())
    new_user.adopt(___1393.inputs)
    if ___1393:
        ___1395 = minitwit.me()
        ___1396 = minitwit.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___1395]))
        already_registered = ___1396
        already_registered.adopt(__stack__.all())
        ___1397 = db_len(already_registered)
        ___1398 = Cell((___1397 == Cell(0)))
        __stack__.push()
        __stack__.add(___1398.inputs)
        minitwit.add_sql_inputs('user', __stack__.all())
        minitwit.add_sql_inputs('user', ___1398.inputs)
        new_user.adopt(__stack__.all())
        new_user.adopt(___1398.inputs)
        if ___1398:
            ___1400 = minitwit.sql(Cell('INSERT INTO user (user_id, username) VALUES (?0, ?1)'), Cell([user_id, user_name]))
            new_user = ___1400
            new_user.adopt(__stack__.all())
            ___1401 = minitwit.flash(Cell('You were successfully registered'))
        else:
            ___1402 = minitwit.flash(Cell('You were already registered'))
        __stack__.pop()
        ___1403 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1403, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('name')]
    ___1404 = minitwit.render(Cell('register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), user_name)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1404, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___1405 = minitwit.flash(Cell('You were logged out'))
    ___1406 = minitwit.pop_session(Cell('loggedin'))
    ___1407 = minitwit.redirect(Cell('Service'), Cell('/public'))
    __r__ = Cell(___1407, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)

def handle_message_text_deletion(id_):
    global __stack__
    ___1408 = minitwit.sql(Cell('DELETE FROM message WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.handle_field_deletion('message', 'text')
def _handle_message_text_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_message_text_deletion(id_)
    return (__c__, __r__, __s__, __trace__)
