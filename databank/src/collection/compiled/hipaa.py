
from apps import hipaa
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___1134 = hipaa.get_session(Cell('loggedin'))
    __r__ = Cell(___1134, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___1135 = is_logged_in()[0]
    ___1136 = ___1135
    __trace__.add_inputs(___1136.inputs)
    __stack__.push()
    __stack__.add(___1136.inputs, bot=True)
    if ___1136:
        ___1138 = hipaa.me()
        ___1139 = hipaa.sql(Cell('SELECT * FROM UserProfile WHERE user_id = ?0'), Cell([___1138]))
        __r__ = Cell(___1139[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def error():
    global __stack__
    if ('msg' not in locals()):
        msg = Cell(None)
    msg = Cell('You are not allowed to access this page: Not a User')
    msg.adopt(__stack__.all())
    __k__ = [Cell('message')]
    ___1140 = hipaa.render(Cell('error.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), msg)),
    }, inputs=[__k__[0].inputs]))
    __r__ = Cell(___1140, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/error')
def _error():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = error()
    return (__c__, __r__, __s__, __trace__)

def filter_check(messages):
    global __stack__
    if ('L' not in locals()):
        L = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('checks' not in locals()):
        checks = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___1141 = db_print(Cell('check'))
    ___1142 = hipaa.check_all('filter_check', __c__, __trace__, Cell('Service'), messages)
    checks = ___1142
    checks.adopt(__stack__.all())
    ___1143 = db_print(Cell('end check'))
    ___1144 = db_len(messages)
    L = ___1144
    L.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___1145 = Cell((i < L))
    __stack__.push()
    __stack__.add(___1145.inputs)
    i.adopt(__stack__.all())
    i.adopt(___1145.inputs)
    while ___1145:
        ___1147 = db_print(i)
        ___1148 = checks[i]
        __stack__.push()
        __stack__.add(___1148.inputs)
        if ___1148:
            ___1150 = db_append(messages2, messages[i])
        __stack__.pop()
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___1145 = Cell((i < L))
        __stack__.add(___1145.inputs)
        i.adopt(__stack__.all())
        i.adopt(___1145.inputs)
    __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def profile():
    global __stack__
    if ('current_user' not in locals()):
        current_user = Cell(None)
    if ('profiletype' not in locals()):
        profiletype = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___1151 = me_user()[0]
    current_user = ___1151
    current_user.adopt(__stack__.all())
    ___1152 = non(current_user)
    __trace__.add_inputs(___1152.inputs)
    __stack__.push()
    __stack__.add(___1152.inputs, bot=True)
    if ___1152:
        ___1154 = hipaa.redirect(Cell('Service'), Cell('/error'))
        __r__ = Cell(___1154, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1155 = hipaa.method()
    ___1156 = Cell((___1155 == Cell('POST')))
    __trace__.add_inputs(___1156.inputs)
    __stack__.push()
    __stack__.add(___1156.inputs, bot=True)
    name.adopt(__stack__.all())
    name.adopt(___1156.inputs)
    hipaa.add_sql_inputs('UserProfile', __stack__.all())
    hipaa.add_sql_inputs('UserProfile', ___1156.inputs)
    email.adopt(__stack__.all())
    email.adopt(___1156.inputs)
    profiletype.adopt(__stack__.all())
    profiletype.adopt(___1156.inputs)
    if ___1156:
        ___1158 = hipaa.post('profile', Cell('email'))
        email = ___1158
        email.adopt(__stack__.all())
        ___1159 = hipaa.post('profile', Cell('profiletype'))
        profiletype = ___1159
        profiletype.adopt(__stack__.all())
        ___1160 = hipaa.post('profile', Cell('name'))
        name = ___1160
        name.adopt(__stack__.all())
        ___1161 = hipaa.me()
        ___1162 = hipaa.sql(Cell('DELETE FROM UserProfile WHERE user_id = ?0'), Cell([___1161]))
        ___1163 = hipaa.me()
        ___1164 = hipaa.name()
        ___1165 = hipaa.sql(Cell('INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name) \n                     VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([___1163, ___1164, Cell(1), email, profiletype, name]))
        ___1166 = hipaa.redirect(Cell('Service'), Cell('/accounts/profile'))
        __r__ = Cell(___1166, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1167 = me_user()[0]
    current_user = ___1167
    current_user.adopt(__stack__.all())
    __k__ = [Cell('profile'), Cell('which_page'), Cell('is_logged_in')]
    ___1168 = hipaa.render(Cell('profile.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), current_user)),
        __k__[1].value: Cell((Cell('Service'), Cell('profile'))),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1168, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/profile', methods=['GET', 'POST'])
def _profile():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = profile()
    return (__c__, __r__, __s__, __trace__)

def index():
    global __stack__
    if ('patients' not in locals()):
        patients = Cell(None)
    if ('current_user' not in locals()):
        current_user = Cell(None)
    if ('entities' not in locals()):
        entities = Cell(None)
    ___1169 = me_user()[0]
    current_user = ___1169
    current_user.adopt(__stack__.all())
    ___1170 = non(current_user)
    __trace__.add_inputs(___1170.inputs)
    __stack__.push()
    __stack__.add(___1170.inputs, bot=True)
    if ___1170:
        ___1172 = hipaa.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___1172, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1173 = db_print(Cell(1))
    ___1174 = hipaa.sql(Cell('SELECT * FROM Individual'))
    ___1175 = filter_check(___1174)[0]
    patients = ___1175
    patients.adopt(__stack__.all())
    ___1176 = db_print(Cell(2))
    ___1177 = hipaa.sql(Cell('SELECT * FROM CoveredEntity'))
    ___1178 = filter_check(___1177)[0]
    entities = ___1178
    entities.adopt(__stack__.all())
    ___1179 = db_print(Cell(3))
    __k__ = [Cell('patients'), Cell('entities'), Cell('name'), Cell('is_logged_in')]
    ___1180 = hipaa.render(Cell('index.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), patients)),
        __k__[1].value: Cell((Cell('Service'), entities)),
        __k__[2].value: Cell((Cell('Service'), current_user[Cell(6)])),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1180, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/')
def _index():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = index()
    return (__c__, __r__, __s__, __trace__)

def about_view():
    global __stack__
    __k__ = [Cell('which_page'), Cell('is_logged_in')]
    ___1181 = is_logged_in()[0]
    ___1182 = hipaa.render(Cell('about.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), Cell('about'))),
        __k__[1].value: Cell((Cell('Service'), ___1181)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1182, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/about')
def _about_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = about_view()
    return (__c__, __r__, __s__, __trace__)

def users():
    global __stack__
    if ('current_user' not in locals()):
        current_user = Cell(None)
    if ('user_profiles' not in locals()):
        user_profiles = Cell(None)
    ___1183 = me_user()[0]
    current_user = ___1183
    current_user.adopt(__stack__.all())
    ___1184 = Cell((current_user[Cell(5)] != Cell(3)))
    __trace__.add_inputs(___1184.inputs)
    __stack__.push()
    __stack__.add(___1184.inputs, bot=True)
    if ___1184:
        ___1186 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1186, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1187 = hipaa.sql(Cell('SELECT * FROM UserProfile'))
    ___1188 = filter_check(___1187)[0]
    user_profiles = ___1188
    user_profiles.adopt(__stack__.all())
    __k__ = [Cell('user_profiles'), Cell('which_page'), Cell('is_logged_in')]
    ___1189 = hipaa.render(Cell('users_view.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_profiles)),
        __k__[1].value: Cell((Cell('Service'), Cell('users'))),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1189, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/users', methods=['GET', 'POST'])
def _users():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = users()
    return (__c__, __r__, __s__, __trace__)

def set_level(level, user_id):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1190 = me_user()[0]
    user = ___1190
    user.adopt(__stack__.all())
    ___1191 = user
    __trace__.add_inputs(___1191.inputs)
    __stack__.push()
    __stack__.add(___1191.inputs, bot=True)
    hipaa.add_sql_inputs('UserProfile', __stack__.all())
    hipaa.add_sql_inputs('UserProfile', ___1191.inputs)
    user.adopt(__stack__.all())
    user.adopt(___1191.inputs)
    if ___1191:
        ___1193 = Cell((Cell((user[Cell(5)] == Cell(3))).value and level.in_(Cell([Cell('0'), Cell('1'), Cell('2'), Cell('3'), Cell('4'), Cell('5'), Cell('6')])).value), inputs=(Cell((user[Cell(5)] == Cell(3))).inputs + level.in_(Cell([Cell('0'), Cell('1'), Cell('2'), Cell('3'), Cell('4'), Cell('5'), Cell('6')])).inputs))
        __stack__.push()
        __stack__.add(___1193.inputs)
        hipaa.add_sql_inputs('UserProfile', __stack__.all())
        hipaa.add_sql_inputs('UserProfile', ___1193.inputs)
        user.adopt(__stack__.all())
        user.adopt(___1193.inputs)
        if ___1193:
            ___1195 = hipaa.sql(Cell('SELECT username, is_active, email, name\n                               FROM UserProfile WHERE user_id = ?0'), Cell([user_id]))
            user = ___1195[Cell(0)]
            user.adopt(__stack__.all())
            ___1196 = hipaa.sql(Cell('DELETE FROM UserProfile WHERE user_id = ?0'), Cell([user_id]))
            ___1197 = hipaa.sql(Cell('INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name)\n                      VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([user_id, user[Cell(0)], user[Cell(1)], user[Cell(2)], level, user[Cell(3)]]))
        __stack__.pop()
    __stack__.pop()
    ___1198 = hipaa.render(Cell('error.html'))
    __r__ = Cell(___1198, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/set_level/<level>/<user_id>', methods=['GET'])
def _set_level(level, user_id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    level = hipaa.register('set_level', 'level', level)
    user_id = hipaa.register('set_level', 'user_id', user_id)
    (__r__, __s__) = set_level(level, user_id)
    return (__c__, __r__, __s__, __trace__)

def patient_treatment(id):
    global __stack__
    if ('treatments' not in locals()):
        treatments = Cell(None)
    if ('p' not in locals()):
        p = Cell(None)
    ___1199 = is_logged_in()[0]
    ___1200 = non(___1199)
    __trace__.add_inputs(___1200.inputs)
    __stack__.push()
    __stack__.add(___1200.inputs, bot=True)
    if ___1200:
        ___1202 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1202, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1203 = hipaa.sql(Cell('SELECT * FROM Individual WHERE id = ?0'), Cell([id]))
    p = ___1203[Cell(0)]
    p.adopt(__stack__.all())
    ___1204 = hipaa.sql(Cell('SELECT * FROM Treatment WHERE patient_id = ?0'), Cell([p[Cell(0)]]))
    treatments = ___1204
    treatments.adopt(__stack__.all())
    __k__ = [Cell('first_name'), Cell('last_name'), Cell('treatments'), Cell('is_logged_in')]
    ___1205 = hipaa.render(Cell('treatments.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), p[Cell(0)])),
        __k__[1].value: Cell((Cell('Service'), p[Cell(1)])),
        __k__[2].value: Cell((Cell('Service'), treatments)),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1205, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/patients/<int:id>/treatments')
def _patient_treatment(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('patient_treatment', 'id', id)
    (__r__, __s__) = patient_treatment(id)
    return (__c__, __r__, __s__, __trace__)

def patient_diagnoses(id):
    global __stack__
    if ('newDiagnoses' not in locals()):
        newDiagnoses = Cell(None)
    if ('p' not in locals()):
        p = Cell(None)
    ___1206 = is_logged_in()[0]
    ___1207 = non(___1206)
    __trace__.add_inputs(___1207.inputs)
    __stack__.push()
    __stack__.add(___1207.inputs, bot=True)
    if ___1207:
        ___1209 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1209, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1210 = hipaa.sql(Cell('SELECT * FROM Individual WHERE id = ?0'), Cell([id]))
    p = ___1210[Cell(0)]
    p.adopt(__stack__.all())
    ___1211 = hipaa.sql(Cell('SELECT * FROM Diagnosis WHERE patient_id = ?0'), Cell([p[Cell(0)]]))
    ___1212 = filter_check(___1211)[0]
    newDiagnoses = ___1212
    newDiagnoses.adopt(__stack__.all())
    __k__ = [Cell('first_name'), Cell('last_name'), Cell('diagnoses'), Cell('is_logged_in')]
    ___1213 = hipaa.render(Cell('treatments.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), p[Cell(0)])),
        __k__[1].value: Cell((Cell('Service'), p[Cell(1)])),
        __k__[2].value: Cell((Cell('Service'), newDiagnoses)),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1213, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/patients/<int:id>/diagnoses')
def _patient_diagnoses(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('patient_diagnoses', 'id', id)
    (__r__, __s__) = patient_diagnoses(id)
    return (__c__, __r__, __s__, __trace__)

def info(id):
    global __stack__
    if ('dataset' not in locals()):
        dataset = Cell(None)
    if ('p' not in locals()):
        p = Cell(None)
    ___1214 = is_logged_in()[0]
    ___1215 = non(___1214)
    __trace__.add_inputs(___1215.inputs)
    __stack__.push()
    __stack__.add(___1215.inputs, bot=True)
    if ___1215:
        ___1217 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1217, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1218 = hipaa.sql(Cell('SELECT * FROM Individual WHERE id = ?0'), Cell([id]))
    p = ___1218[Cell(0)]
    p.adopt(__stack__.all())
    dataset = Cell([])
    dataset.adopt(__stack__.all())
    dataset = (dataset + Cell([Cell((Cell('Sex'), p[Cell(6)], Cell(False)))]))
    dataset.adopt(__stack__.all())
    __k__ = [Cell('patient'), Cell('dataset'), Cell('is_logged_in')]
    ___1219 = hipaa.render(Cell('info.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), p)),
        __k__[1].value: Cell((Cell('Service'), dataset)),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1219, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/patients/<int:id>/')
def _info(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('info', 'id', id)
    (__r__, __s__) = info(id)
    return (__c__, __r__, __s__, __trace__)

def entity_transaction(id):
    global __stack__
    if ('other_transactions' not in locals()):
        other_transactions = Cell(None)
    if ('entity' not in locals()):
        entity = Cell(None)
    if ('transactions' not in locals()):
        transactions = Cell(None)
    ___1220 = is_logged_in()[0]
    ___1221 = non(___1220)
    __trace__.add_inputs(___1221.inputs)
    __stack__.push()
    __stack__.add(___1221.inputs, bot=True)
    if ___1221:
        ___1223 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1223, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1224 = hipaa.sql(Cell('SELECT * FROM CoveredEntity WHERE id = ?0'), Cell([id]))
    entity = ___1224[Cell(0)]
    entity.adopt(__stack__.all())
    ___1225 = hipaa.sql(Cell('SELECT * FROM Transaction WHERE FirstParty = ?0'), Cell([entity[Cell(0)]]))
    ___1226 = filter_check(___1225)[0]
    transactions = ___1226
    transactions.adopt(__stack__.all())
    ___1227 = hipaa.sql(Cell('SELECT * FROM Transaction WHERE SecondParty = ?0'), Cell([entity[Cell(0)]]))
    ___1228 = filter_check(___1227)[0]
    other_transactions = ___1228
    other_transactions.adopt(__stack__.all())
    __k__ = [Cell('entity'), Cell('transactions'), Cell('other_transactions'), Cell('is_logged_in')]
    ___1229 = hipaa.render(Cell('transactions.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), entity)),
        __k__[1].value: Cell((Cell('Service'), transactions)),
        __k__[2].value: Cell((Cell('Service'), other_transactions)),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1229, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/entities/<int:id>/transactions')
def _entity_transaction(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('entity_transaction', 'id', id)
    (__r__, __s__) = entity_transaction(id)
    return (__c__, __r__, __s__, __trace__)

def entity_associate(id):
    global __stack__
    if ('entity' not in locals()):
        entity = Cell(None)
    if ('associates' not in locals()):
        associates = Cell(None)
    ___1230 = is_logged_in()[0]
    ___1231 = non(___1230)
    __trace__.add_inputs(___1231.inputs)
    __stack__.push()
    __stack__.add(___1231.inputs, bot=True)
    if ___1231:
        ___1233 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1233, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1234 = hipaa.sql(Cell('SELECT * FROM CoveredEntity WHERE id = ?0'), Cell([id]))
    entity = ___1234[Cell(0)]
    entity.adopt(__stack__.all())
    ___1235 = hipaa.sql(Cell('SELECT UserProfile.* FROM UserProfile JOIN BusinessAssociateAgreement \n    ON BusinessAssociateAgreement.BusinessAssociateID = UserProfile.id\n    WHERE BusinessAssociateAgreement.CoveredEntityID = ?0'), Cell([entity[Cell(0)]]))
    associates = ___1235
    associates.adopt(__stack__.all())
    ___1236 = filter_check(associates)[0]
    associates = ___1236
    associates.adopt(__stack__.all())
    __k__ = [Cell('entity'), Cell('associates'), Cell('is_logged_in')]
    ___1237 = hipaa.render(Cell('associates.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), entity)),
        __k__[1].value: Cell((Cell('Service'), associates)),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1237, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/entities/<int:id>/associates')
def _entity_associate(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('entity_associate', 'id', id)
    (__r__, __s__) = entity_associate(id)
    return (__c__, __r__, __s__, __trace__)

def entity_directory(id):
    global __stack__
    if ('visits' not in locals()):
        visits = Cell(None)
    if ('entity' not in locals()):
        entity = Cell(None)
    ___1238 = is_logged_in()[0]
    ___1239 = non(___1238)
    __trace__.add_inputs(___1239.inputs)
    __stack__.push()
    __stack__.add(___1239.inputs, bot=True)
    if ___1239:
        ___1241 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1241, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1242 = hipaa.sql(Cell('SELECT * FROM CoveredEntity WHERE id = ?0'), Cell([id]))
    entity = ___1242[Cell(0)]
    entity.adopt(__stack__.all())
    ___1243 = hipaa.sql(Cell('SELECT * FROM HospitalVisit WHERE hospitalID = ?0'), Cell([entity[Cell(0)]]))
    ___1244 = filter_check(___1243)[0]
    visits = ___1244
    visits.adopt(__stack__.all())
    __k__ = [Cell('entity'), Cell('visits'), Cell('is_logged_in')]
    ___1245 = hipaa.render(Cell('directory.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), entity)),
        __k__[1].value: Cell((Cell('Service'), visits)),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1245, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/entities/<int:id>/')
def _entity_directory(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('entity_directory', 'id', id)
    (__r__, __s__) = entity_directory(id)
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    ___1246 = is_logged_in()[0]
    ___1247 = ___1246
    __trace__.add_inputs(___1247.inputs)
    __stack__.push()
    __stack__.add(___1247.inputs, bot=True)
    if ___1247:
        ___1249 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1249, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1250 = hipaa.render(Cell('registration/login.html'))
    __r__ = Cell(___1250, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def do_login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1251 = is_logged_in()[0]
    ___1252 = ___1251
    __trace__.add_inputs(___1252.inputs)
    __stack__.push()
    __stack__.add(___1252.inputs, bot=True)
    if ___1252:
        ___1254 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1254, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1255 = hipaa.me()
    ___1256 = hipaa.sql(Cell('SELECT * FROM UserProfile WHERE user_id = ?0'), Cell([___1255]))
    user = ___1256
    user.adopt(__stack__.all())
    ___1257 = db_len(user)
    ___1258 = Cell((___1257 > Cell(0)))
    __trace__.add_inputs(___1258.inputs)
    __stack__.push()
    __stack__.add(___1258.inputs, bot=True)
    hipaa.add_session_inputs('loggedin', __stack__.all())
    hipaa.add_session_inputs('loggedin', ___1258.inputs)
    if ___1258:
        ___1260 = hipaa.set_session(Cell('loggedin'), Cell(True))
        ___1261 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1261, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('error')]
        ___1262 = hipaa.render(Cell('registration/login.html'), Cell({
            __k__[0].value: Cell((Cell('Service'), Cell('Invalid user (register to access)'))),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___1262, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@hipaa.route('/accounts/do_login')
def _do_login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = do_login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    if ('username' not in locals()):
        username = Cell(None)
    ___1263 = is_logged_in()[0]
    ___1264 = ___1263
    __trace__.add_inputs(___1264.inputs)
    __stack__.push()
    __stack__.add(___1264.inputs, bot=True)
    if ___1264:
        ___1266 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1266, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1267 = hipaa.me()
    user_id = ___1267
    user_id.adopt(__stack__.all())
    ___1268 = hipaa.name()
    username = ___1268
    username.adopt(__stack__.all())
    ___1269 = hipaa.method()
    ___1270 = Cell((___1269 == Cell('POST')))
    __trace__.add_inputs(___1270.inputs)
    __stack__.push()
    __stack__.add(___1270.inputs, bot=True)
    name.adopt(__stack__.all())
    name.adopt(___1270.inputs)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___1270.inputs)
    hipaa.add_sql_inputs('UserProfile', __stack__.all())
    hipaa.add_sql_inputs('UserProfile', ___1270.inputs)
    email.adopt(__stack__.all())
    email.adopt(___1270.inputs)
    if ___1270:
        ___1272 = hipaa.me()
        ___1273 = hipaa.sql(Cell('SELECT * FROM UserProfile WHERE user_id = ?0'), Cell([___1272]))
        already_registered = ___1273
        already_registered.adopt(__stack__.all())
        ___1274 = db_len(already_registered)
        ___1275 = Cell((___1274 == Cell(0)))
        __stack__.push()
        __stack__.add(___1275.inputs)
        name.adopt(__stack__.all())
        name.adopt(___1275.inputs)
        hipaa.add_sql_inputs('UserProfile', __stack__.all())
        hipaa.add_sql_inputs('UserProfile', ___1275.inputs)
        email.adopt(__stack__.all())
        email.adopt(___1275.inputs)
        if ___1275:
            ___1277 = hipaa.post('register', Cell('email'))
            email = ___1277
            email.adopt(__stack__.all())
            ___1278 = hipaa.post('register', Cell('name'))
            name = ___1278
            name.adopt(__stack__.all())
            ___1279 = hipaa.sql(Cell('INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name)\n                     VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([user_id, username, Cell(1), email, Cell(1), name]))
        __stack__.pop()
        ___1280 = hipaa.redirect(Cell('Service'), Cell('/accounts/do_login'))
        __r__ = Cell(___1280, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('username')]
    ___1281 = hipaa.render(Cell('registration/register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), username)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1281, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___1282 = hipaa.pop_session(Cell('loggedin'))
    ___1283 = hipaa.redirect(Cell('Service'), Cell('/accounts/login'))
    __r__ = Cell(___1283, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)

def handle_Address_deletion(id_):
    global __stack__
    ___1284 = hipaa.sql(Cell('DELETE FROM Address WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_deletion('Address', ['City', 'State', 'ZipCode'])
def _handle_Address_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Address_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_Diagnosis_deletion(id_):
    global __stack__
    ___1285 = hipaa.sql(Cell('DELETE FROM Diagnosis WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_deletion('Diagnosis', ['Manifestation', 'Diagnosis', 'DateRecognized', 'patient_id'])
def _handle_Diagnosis_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Diagnosis_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_Diagnosis_rectification(id_, val, can):
    global __stack__
    ___1286 = hipaa.sql(Cell('DELETE FROM Diagnosis WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_rectification('Diagnosis', ['Manifestation', 'Diagnosis', 'DateRecognized', 'patient_id'])
def _handle_Diagnosis_rectification(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Diagnosis_rectification(id_, val, can)
    return (__c__, __r__, __s__, __trace__)

def handle_HospitalVisit_deletion(id_, val, can):
    global __stack__
    ___1287 = hipaa.sql(Cell('DELETE FROM HospitalVisit WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_deletion('HospitalVisit', ['patientID', 'hospitalID', 'date_admitted', 'location', 'condition', 'date_released'])
def _handle_HospitalVisit_deletion(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_HospitalVisit_deletion(id_, val, can)
    return (__c__, __r__, __s__, __trace__)

def handle_HospitalVisit_rectification(id_, val, can):
    global __stack__
    ___1288 = hipaa.sql(Cell('DELETE FROM HospitalVisit WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_rectification('HospitalVisit', ['patientID', 'hospitalID', 'date_admitted', 'location', 'condition', 'date_released'])
def _handle_HospitalVisit_rectification(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_HospitalVisit_rectification(id_, val, can)
    return (__c__, __r__, __s__, __trace__)

def handle_Individual_deletion(id_):
    global __stack__
    ___1289 = hipaa.sql(Cell('DELETE FROM Individual WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_deletion('Individual', ['FirstName', 'LastName'])
def _handle_Individual_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Individual_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_Individual_rectification(id_, val, can):
    global __stack__
    ___1290 = hipaa.sql(Cell('DELETE FROM Individual WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_rectification('Individual', ['FirstName', 'LastName'])
def _handle_Individual_rectification(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Individual_rectification(id_, val, can)
    return (__c__, __r__, __s__, __trace__)

def handle_Transaction_deletion(id_):
    global __stack__
    ___1291 = hipaa.sql(Cell('DELETE FROM Transaction WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_deletion('Transaction', ['Standard', 'FirstPartyID', 'SecondPartyID', 'SharedInformationID', 'DateRequested', 'DateResponded', 'Purpose'])
def _handle_Transaction_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Transaction_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_Transaction_rectification(id_, val, can):
    global __stack__
    ___1292 = hipaa.sql(Cell('DELETE FROM Transaction WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.handle_field_rectification('Transaction', ['Standard', 'FirstPartyID', 'SecondPartyID', 'SharedInformationID', 'DateRequested', 'DateResponded', 'Purpose'])
def _handle_Transaction_rectification(id_, val, can):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_Transaction_rectification(id_, val, can)
    return (__c__, __r__, __s__, __trace__)
