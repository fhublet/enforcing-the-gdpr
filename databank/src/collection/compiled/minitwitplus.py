
from apps import minitwitplus
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___0 = minitwitplus.get_session(Cell('loggedin'))
    __r__ = Cell(___0, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___1 = is_logged_in()[0]
    ___2 = ___1
    __trace__.add_inputs(___2.inputs)
    __stack__.push()
    __stack__.add(___2.inputs, bot=True)
    if ___2:
        ___4 = minitwitplus.me()
        ___5 = minitwitplus.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___4]))
        __r__ = Cell(___5[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def filter_check(messages):
    global __stack__
    if ('L' not in locals()):
        L = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('checks' not in locals()):
        checks = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___6 = minitwitplus.check_all('filter_check', __c__, __trace__, Cell('Service'), messages)
    checks = ___6
    checks.adopt(__stack__.all())
    ___7 = db_len(messages)
    L = ___7
    L.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___8 = Cell((i < L))
    __stack__.push()
    __stack__.add(___8.inputs)
    i.adopt(__stack__.all())
    i.adopt(___8.inputs)
    while ___8:
        ___10 = checks[i]
        __stack__.push()
        __stack__.add(___10.inputs)
        if ___10:
            ___12 = db_append(messages2, messages[i])
        __stack__.pop()
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___8 = Cell((i < L))
        __stack__.add(___8.inputs)
        i.adopt(__stack__.all())
        i.adopt(___8.inputs)
    __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def get_user_id(username):
    global __stack__
    if ('rv' not in locals()):
        rv = Cell(None)
    ___13 = minitwitplus.sql(Cell('SELECT id FROM user WHERE username = ?0'), Cell([username]))
    rv = ___13
    rv.adopt(__stack__.all())
    ___14 = db_len(rv)
    ___15 = Cell((___14 > Cell(0)))
    __trace__.add_inputs(___15.inputs)
    __stack__.push()
    __stack__.add(___15.inputs, bot=True)
    if ___15:
        __r__ = Cell(rv[Cell(0)][Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell((- Cell(1)), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def generate_ad(messages):
    global __stack__
    if ('all_message_texts' not in locals()):
        all_message_texts = Cell(None)
    if ('message' not in locals()):
        message = Cell(None)
    all_message_texts = Cell('')
    all_message_texts.adopt(__stack__.all())
    ___17 = messages
    for message in ___17:
        all_message_texts = (all_message_texts + message[Cell(3)])
        all_message_texts.adopt(__stack__.all())
    ___19 = minitwitplus.check('generate_ad', __c__, __trace__, Cell('Marketing'), all_message_texts)
    ___20 = non(___19)
    __stack__.push()
    __stack__.add(___20.inputs)
    all_message_texts.adopt(__stack__.all())
    all_message_texts.adopt(___20.inputs)
    if ___20:
        all_message_texts = Cell('')
        all_message_texts.adopt(__stack__.all())
    __stack__.pop()
    ___22 = Cell('editor').in_(all_message_texts)
    __trace__.add_inputs(___22.inputs)
    __stack__.push()
    __stack__.add(___22.inputs, bot=True)
    if ___22:
        __k__ = [Cell('title'), Cell('text'), Cell('link')]
        __r__ = Cell(Cell({
            __k__[0].value: Cell('Emacs'),
            __k__[1].value: Cell('... is the best text editor'),
            __k__[2].value: Cell('https://emacs.org'),
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___24 = Cell('OS').in_(all_message_texts)
        __stack__.push()
        __stack__.add(___24.inputs)
        if ___24:
            __k__ = [Cell('title'), Cell('text'), Cell('link')]
            __r__ = Cell(Cell({
                __k__[0].value: Cell('Linux'),
                __k__[1].value: Cell('... has the best kernel'),
                __k__[2].value: Cell('https://linux.org'),
            }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]), adopt=__stack__.all())
            __s__ = __stack__.all()
            __stack__.pop()
            __stack__.pop()
            return (__r__, __s__)
        else:
            __k__ = [Cell('title'), Cell('text'), Cell('link')]
            __r__ = Cell(Cell({
                __k__[0].value: Cell('The Hague'),
                __k__[1].value: Cell('... has the best conferences'),
                __k__[2].value: Cell('https://denhaag.com'),
            }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]), adopt=__stack__.all())
            __s__ = __stack__.all()
            __stack__.pop()
            __stack__.pop()
            return (__r__, __s__)
        __stack__.pop()
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def timeline():
    global __stack__
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('messages12' not in locals()):
        messages12 = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('messages1' not in locals()):
        messages1 = Cell(None)
    ___26 = me_user()[0]
    user = ___26
    user.adopt(__stack__.all())
    ___27 = non(user)
    __trace__.add_inputs(___27.inputs)
    __stack__.push()
    __stack__.add(___27.inputs, bot=True)
    if ___27:
        ___29 = minitwitplus.redirect(Cell('Service'), Cell('/public'))
        __r__ = Cell(___29, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___30 = minitwitplus.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       JOIN follower ON follower.whom_id = user.user_id\n       WHERE follower.who_id = ?0\n       ORDER BY message.id DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages1 = ___30
    messages1.adopt(__stack__.all())
    ___31 = minitwitplus.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       WHERE user.id = ?0 ORDER BY message.id DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages2 = ___31
    messages2.adopt(__stack__.all())
    ___32 = db_sorted_by0((messages1 + messages2))
    ___33 = filter_check(___32)[0]
    messages12 = ___33
    messages12.adopt(__stack__.all())
    messages = Cell([])
    messages.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___34 = db_len(messages12)
    ___35 = Cell((Cell((i < Cell(30))).value and Cell((i < ___34)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___34)).inputs))
    __stack__.push()
    __stack__.add(___35.inputs)
    messages.adopt(__stack__.all())
    messages.adopt(___35.inputs)
    i.adopt(__stack__.all())
    i.adopt(___35.inputs)
    while ___35:
        messages = (messages + Cell([messages12[i]]))
        messages.adopt(__stack__.all())
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___35 = Cell((Cell((i < Cell(30))).value and Cell((i < ___34)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___34)).inputs))
        __stack__.add(___35.inputs)
        messages.adopt(__stack__.all())
        messages.adopt(___35.inputs)
        i.adopt(__stack__.all())
        i.adopt(___35.inputs)
    __stack__.pop()
    __k__ = [Cell('messages'), Cell('user'), Cell('title'), Cell('flash'), Cell('ad')]
    ___37 = minitwitplus.unflash()
    ___38 = generate_ad(messages)[0]
    ___39 = minitwitplus.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), user)),
        __k__[2].value: Cell((Cell('Service'), Cell('My Timeline'))),
        __k__[3].value: Cell((Cell('Service'), ___37)),
        __k__[4].value: Cell((Cell('Marketing'), ___38)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___39, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/')
def _timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = timeline()
    return (__c__, __r__, __s__, __trace__)

def public_timeline():
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    ___40 = minitwitplus.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       ORDER BY message.id DESC LIMIT 30'))
    messages = ___40
    messages.adopt(__stack__.all())
    ___41 = filter_check(messages)[0]
    messages = ___41
    messages.adopt(__stack__.all())
    __k__ = [Cell('messages'), Cell('user'), Cell('title'), Cell('ad')]
    ___42 = me_user()[0]
    ___43 = generate_ad(messages)[0]
    ___44 = minitwitplus.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), ___42)),
        __k__[2].value: Cell((Cell('Service'), Cell('Public Timeline'))),
        __k__[3].value: Cell((Cell('Marketing'), ___43)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___44, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/public')
def _public_timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = public_timeline()
    return (__c__, __r__, __s__, __trace__)

def user_timeline(username):
    global __stack__
    if ('followed' not in locals()):
        followed = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('profile_user' not in locals()):
        profile_user = Cell(None)
    ___45 = minitwitplus.sql(Cell('SELECT * FROM user WHERE username = ?0'), Cell([username]))
    profile_user = ___45
    profile_user.adopt(__stack__.all())
    ___46 = db_len(profile_user)
    ___47 = Cell((___46 == Cell(0)))
    __trace__.add_inputs(___47.inputs)
    __stack__.push()
    __stack__.add(___47.inputs, bot=True)
    if ___47:
        ___49 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___49, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    followed = Cell(False)
    followed.adopt(__stack__.all())
    ___50 = me_user()[0]
    user = ___50
    user.adopt(__stack__.all())
    ___51 = user
    __trace__.add_inputs(___51.inputs)
    __stack__.push()
    __stack__.add(___51.inputs)
    followed.adopt(__stack__.all())
    followed.adopt(___51.inputs)
    if ___51:
        ___53 = minitwitplus.sql(Cell('\n      SELECT 1 FROM follower WHERE follower.who_id = ?0 AND follower.whom_id = ?1'), Cell([user[Cell(0)], profile_user[Cell(0)][Cell(0)]]))
        ___54 = db_len(___53)
        followed = Cell((___54 > Cell(0)))
        followed.adopt(__stack__.all())
    __stack__.pop()
    ___55 = minitwitplus.sql(Cell('\n      SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n             user.user_id, user.username FROM message\n      JOIN user ON message.author_id = user.id\n      WHERE user.id = ?0 ORDER BY message.id DESC LIMIT 30'), Cell([profile_user[Cell(0)][Cell(0)]]))
    messages = ___55
    messages.adopt(__stack__.all())
    ___56 = filter_check(messages)[0]
    messages = ___56
    messages.adopt(__stack__.all())
    ___57 = minitwitplus.send('user_timeline', __c__, __trace__, Cell('trustedanalytics.com'), Cell('Analytics'), username)
    ___58 = minitwitplus.send('user_timeline', __c__, __trace__, Cell('evilanalytics.com'), Cell('Analytics'), username)
    __k__ = [Cell('messages'), Cell('followed'), Cell('profile_user'), Cell('user'), Cell('flash'), Cell('ad')]
    ___59 = minitwitplus.unflash()
    ___60 = generate_ad(messages)[0]
    ___61 = minitwitplus.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), followed)),
        __k__[2].value: Cell((Cell('Service'), profile_user[Cell(0)])),
        __k__[3].value: Cell((Cell('Service'), user)),
        __k__[4].value: Cell((Cell('Service'), ___59)),
        __k__[5].value: Cell((Cell('Marketing'), ___60)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs]))
    __r__ = Cell(___61, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/<username>')
def _user_timeline(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwitplus.register('user_timeline', 'username', username)
    (__r__, __s__) = user_timeline(username)
    return (__c__, __r__, __s__, __trace__)

def follow_user(username):
    global __stack__
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    ___62 = me_user()[0]
    user = ___62
    user.adopt(__stack__.all())
    ___63 = non(user)
    __trace__.add_inputs(___63.inputs)
    __stack__.push()
    __stack__.add(___63.inputs, bot=True)
    if ___63:
        ___65 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___65, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___66 = get_user_id(username)[0]
    whom_id = ___66
    whom_id.adopt(__stack__.all())
    ___67 = minitwitplus.sql(Cell('INSERT INTO follower (who_id, whom_id) VALUES (?0, ?1)'), Cell([user[Cell(0)], whom_id]))
    ___68 = non(whom_id)
    __trace__.add_inputs(___68.inputs)
    __stack__.push()
    __stack__.add(___68.inputs, bot=True)
    if ___68:
        ___70 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___70, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___71 = minitwitplus.flash(((Cell('You are now following "') + username) + Cell('"')))
    ___72 = minitwitplus.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___72, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/<username>/follow')
def _follow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwitplus.register('follow_user', 'username', username)
    (__r__, __s__) = follow_user(username)
    return (__c__, __r__, __s__, __trace__)

def unfollow_user(username):
    global __stack__
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    ___73 = me_user()[0]
    user = ___73
    user.adopt(__stack__.all())
    ___74 = non(user)
    __trace__.add_inputs(___74.inputs)
    __stack__.push()
    __stack__.add(___74.inputs, bot=True)
    if ___74:
        ___76 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___76, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___77 = get_user_id(username)[0]
    whom_id = ___77
    whom_id.adopt(__stack__.all())
    ___78 = minitwitplus.sql(Cell('DELETE FROM follower WHERE who_id = ?0 AND whom_id = ?1'), Cell([user[Cell(0)], whom_id]))
    ___79 = non(whom_id)
    __trace__.add_inputs(___79.inputs)
    __stack__.push()
    __stack__.add(___79.inputs, bot=True)
    if ___79:
        ___81 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___81, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___82 = minitwitplus.flash(((Cell('You are no longer following "') + username) + Cell('"')))
    ___83 = minitwitplus.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___83, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/<username>/unfollow')
def _unfollow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwitplus.register('unfollow_user', 'username', username)
    (__r__, __s__) = unfollow_user(username)
    return (__c__, __r__, __s__, __trace__)

def add_message():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___84 = me_user()[0]
    user = ___84
    user.adopt(__stack__.all())
    ___85 = non(user)
    __trace__.add_inputs(___85.inputs)
    __stack__.push()
    __stack__.add(___85.inputs, bot=True)
    if ___85:
        ___87 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___87, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___88 = minitwitplus.method()
    ___89 = Cell((___88 == Cell('POST')))
    __trace__.add_inputs(___89.inputs)
    __stack__.push()
    __stack__.add(___89.inputs, bot=True)
    minitwitplus.add_sql_inputs('message', __stack__.all())
    minitwitplus.add_sql_inputs('message', ___89.inputs)
    if ___89:
        ___91 = minitwitplus.post('add_message', Cell('text'))
        ___92 = minitwitplus.now_utc()
        ___93 = minitwitplus.sql(Cell('INSERT INTO message (author_id, text, pub_date) \n          VALUES (?0, ?1, ?2)'), Cell([user[Cell(0)], ___91, ___92]))
        ___94 = minitwitplus.flash(Cell('Your message was recorded'))
    __stack__.pop()
    ___95 = minitwitplus.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___95, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/add_message', methods=['POST'])
def _add_message():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_message()
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___96 = is_logged_in()[0]
    ___97 = ___96
    __trace__.add_inputs(___97.inputs)
    __stack__.push()
    __stack__.add(___97.inputs, bot=True)
    if ___97:
        ___99 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___99, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___100 = minitwitplus.me()
    ___101 = minitwitplus.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___100]))
    user = ___101
    user.adopt(__stack__.all())
    ___102 = db_len(user)
    ___103 = Cell((___102 > Cell(0)))
    __stack__.push()
    __stack__.add(___103.inputs)
    minitwitplus.add_session_inputs('loggedin', __stack__.all())
    minitwitplus.add_session_inputs('loggedin', ___103.inputs)
    if ___103:
        ___105 = minitwitplus.set_session(Cell('loggedin'), Cell(True))
        ___106 = minitwitplus.flash(Cell('You were logged in'))
    else:
        ___107 = minitwitplus.flash(Cell('Invalid user (register to access)'))
    __stack__.pop()
    ___108 = minitwitplus.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___108, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('new_user' not in locals()):
        new_user = Cell(None)
    if ('user_name' not in locals()):
        user_name = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    ___109 = is_logged_in()[0]
    ___110 = ___109
    __trace__.add_inputs(___110.inputs)
    __stack__.push()
    __stack__.add(___110.inputs, bot=True)
    if ___110:
        ___112 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___112, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___113 = minitwitplus.me()
    user_id = ___113
    user_id.adopt(__stack__.all())
    ___114 = minitwitplus.name()
    user_name = ___114
    user_name.adopt(__stack__.all())
    ___115 = minitwitplus.method()
    ___116 = Cell((___115 == Cell('POST')))
    __trace__.add_inputs(___116.inputs)
    __stack__.push()
    __stack__.add(___116.inputs, bot=True)
    new_user.adopt(__stack__.all())
    new_user.adopt(___116.inputs)
    minitwitplus.add_sql_inputs('user', __stack__.all())
    minitwitplus.add_sql_inputs('user', ___116.inputs)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___116.inputs)
    if ___116:
        ___118 = minitwitplus.me()
        ___119 = minitwitplus.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___118]))
        already_registered = ___119
        already_registered.adopt(__stack__.all())
        ___120 = db_len(already_registered)
        ___121 = Cell((___120 == Cell(0)))
        __stack__.push()
        __stack__.add(___121.inputs)
        new_user.adopt(__stack__.all())
        new_user.adopt(___121.inputs)
        minitwitplus.add_sql_inputs('user', __stack__.all())
        minitwitplus.add_sql_inputs('user', ___121.inputs)
        if ___121:
            ___123 = minitwitplus.sql(Cell('INSERT INTO user (user_id, username) VALUES (?0, ?1)'), Cell([user_id, user_name]))
            new_user = ___123
            new_user.adopt(__stack__.all())
            ___124 = minitwitplus.flash(Cell('You were successfully registered'))
        else:
            ___125 = minitwitplus.flash(Cell('You were already registered'))
        __stack__.pop()
        ___126 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___126, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('name')]
    ___127 = minitwitplus.render(Cell('register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), user_name)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___127, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___128 = minitwitplus.flash(Cell('You were logged out'))
    ___129 = minitwitplus.pop_session(Cell('loggedin'))
    ___130 = minitwitplus.redirect(Cell('Service'), Cell('/public'))
    __r__ = Cell(___130, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)

def handle_message_text_deletion(id_):
    global __stack__
    ___131 = minitwitplus.sql(Cell('DELETE FROM message WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.handle_field_deletion('message', 'text')
def _handle_message_text_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_message_text_deletion(id_)
    return (__c__, __r__, __s__, __trace__)

def handle_message_text_rectification(id_, val, can_delete):
    global __stack__
    if ('row' not in locals()):
        row = Cell(None)
    ___132 = minitwitplus.sql(Cell('SELECT author_id, text, pub_date FROM message WHERE id = ?0'), Cell([id_]))
    row = ___132[Cell(0)]
    row.adopt(__stack__.all())
    ___133 = minitwitplus.sql(Cell('INSERT INTO message (author_id, text, pub_date) VALUES (?0, ?1, ?2)'), Cell([row[Cell(0)], (val + Cell(' [edited]')), row[Cell(2)]]))
    ___134 = minitwitplus.sql(Cell('DELETE FROM message WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.handle_field_rectification('message', 'text')
def _handle_message_text_rectification(id_, val, can_delete):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_message_text_rectification(id_, val, can_delete)
    return (__c__, __r__, __s__, __trace__)

def handle_follower_whom_id_deletion(id_):
    global __stack__
    ___135 = minitwitplus.sql(Cell('DELETE FROM follower WHERE id = ?0'), Cell([id_]))
    __r__ = Cell(Cell(None), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.handle_field_deletion('follower', 'whom_id')
def _handle_follower_whom_id_deletion(id_):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = handle_follower_whom_id_deletion(id_)
    return (__c__, __r__, __s__, __trace__)
