# -*- coding: utf-8 -*-
###
#   Conf
#   ~~~~~~~~
#   
#   Originally a Jacqueline application from Yang et al. (2016)
#
#   Ported to Flask by Anonymous authors (c) 2023 
###

from apps import conf


def is_logged_in():
    return conf.get_session('loggedin')


def me_user():
    if is_logged_in():
        return conf.sql("SELECT * FROM user_profiles WHERE user_id = ?0", [conf.me()])[0]
    else:
        return None

def filter_check(messages): #sp
    messages2 = []
    checks = conf.check_all('Service', messages)
    L = len(messages)
    i = 0
    while i < L:
        if checks[i]:
            append(messages2, messages[i])
        i += 1
    return messages2

    
def is_admin():
    level = conf.sql("SELECT level FROM user_profiles WHERE user_id = ?0", [conf.me()])[0][0]
    return level == "chair"


# Baseline: about_view
@conf.route('/about')
def about_view():
  return conf.render('about.html', {'which_page': 'about'})


# Baseline: papers_view
@conf.route('/')
def papers_view():
    user = me_user()
    if not user:
        return conf.redirect('Service', '/accounts/login')

    user_name = user[2]

    papers = conf.sql('SELECT * FROM papers')
    paper_data = [None] * len(papers)

    i = 0
    for paper in papers:
        paper_versions = conf.sql('SELECT title FROM conf_paperversion WHERE paper_id = ?0', [paper[0]])
        author_name = conf.sql('SELECT name FROM user_profiles WHERE user_id = ?0', [paper[2]])[0][0]
        version = paper_versions[0][0]
        paper_data_1 = {
            'paper' : paper,
            'author_name': author_name,
            'latest' : version
        }
        paper_data[i] = paper_data_1
        i += 1

    paper_data = filter_check(paper_data) #sp
    
    return conf.render('papers.html', {'papers':       ('Service', papers),
                                       'which_page':   ('Service', 'home'),
                                       'paper_data':   ('Service', paper_data),
                                       'name':         ('Service', user_name),
                                       'is_logged_in': ('Service', True),
                                       'is_admin':     ('Service', is_admin())})


# Baseline: paper_view
@conf.route('/paper', methods=['GET', 'POST'])
def paper_view():
    user = me_user()
    if not user:
        return conf.redirect('Service', '/accounts/login')

    paper = conf.sql('SELECT * FROM papers WHERE id = ?0', [conf.get('id')])[0]

    if conf.method() == 'POST': # Δ: order to avoid propagation
        if paper != None:
            if conf.get('add_comment') == 'true':
                conf.sql('INSERT INTO comments (paper_id, user_id, contents) VALUES (?0, ?1, ?2)',
                           [paper[0], conf.me(), conf.get('comment')])
            elif conf.get('add_review') == 'true':
                conf.sql('''
                  INSERT INTO reviews (paper_id, reviewer_id, contents, score_novelty, 
                                       score_presentation, score_technical, score_confidence) 
                  VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)''',
                         [paper[0], conf.me(), conf.get('review'), int(conf.get('score_novelty')),
                          int(conf.get('score_presentation')), int(conf.get('score_technical')),
                          int(conf.get('score_confidence'))])
            elif conf.get('new_version') == 'true':
                contents = conf.post('contents')
                if contents != None:
                    conf.sql('''
                  INSERT INTO conf_paperversion (paper_id, title, contents, abstract, time)
                  VALUES (?0, ?1, ?2, ?3)''',
                               [paper[0], conf.get('title'), contents, conf.get('abstract'), conf.now_utc()])

    if paper != None:
        paper_versions = filter_check(conf.sql('SELECT * FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC', [paper[0]])) #sp
        coauthors = filter_check(conf.sql('SELECT * FROM conf_papercoauthor WHERE paper_id = ?0', [paper[0]])) #sp
        latest_abstract = None
        latest_title = None
        if len(paper_versions):
            latest_abstract = paper_versions[0][3]
            latest_title = paper_versions[0][1]
        if not conf.check('Service', latest_abstract): #sp
            latest_abstract = None #sp
        if not conf.check('Service', latest_title): #sp
            latest_title = None #sp
        link = conf.link(paper_versions[0][2])
        reviews = filter_check(conf.sql('SELECT * FROM reviews WHERE paper_id = ?0', [paper[0]])) #sp
        comments = filter_check(conf.sql('SELECT * FROM comments WHERE paper_id = ?0', [paper[0]])) #sp
        author = filter_check(conf.sql('SELECT * FROM user_profiles WHERE user_id = ?0', [paper[2]])[0]) #sp
    else:
        paper = None
        paper_versions = []
        coauthors = []
        latest_abstract = None
        latest_title = None
        reviews = []
        comments = []
        author = None

    return conf.render('paper.html', {'paper':           ('Service', paper),
                                      'paper_versions':  ('Service', paper_versions),
                                      'author':          ('Service', author),
                                      'coauthors':       ('Service', coauthors),
                                      'latest_abstract': ('Service', latest_abstract),
                                      'latest_title':    ('Service', latest_title),
                                      'reviews':         ('Service', reviews),
                                      'comments':        ('Service', comments),
                                      'profile':         ('Service', user),
                                      'which_page':      ('Service', 'paper'),
                                      'link':            ('Service', link),
                                      'review_score_fields': ('Service', [ ("Novelty", "score_novelty", 10)
                                                                           , ("Presentation", "score_presentation", 10)
                                                                           , ("Technical", "score_technical", 10)
                                                                           , ("Confidence", "score_confidence", 10) ]),
                                      'is_logged_in':    ('Service', True),
                                      'is_admin':        ('Service', is_admin())})


# Baseline: submit_view
@conf.route('/submit', methods=['GET', 'POST'])
def submit_view():
    if not is_logged_in():
        return conf.redirect('Service', '/accounts/login')

    if conf.method() == 'POST':
        coauthors = conf.getlist('coauthors[]')
        title = conf.post('title')
        abstract = conf.post('abstract')
        contents = conf.post('contents')

        # Δ: empty fields not checked here

        conf.sql('INSERT INTO papers (author_id, accepted) VALUES (?0, ?1)', [conf.me(), False])

        paper_id = conf.sql('SELECT id FROM papers WHERE author_id = ?0 ORDER BY id DESC LIMIT 1', [conf.me()])[0][0]

        for coauthor in coauthors:
            conf.sql('INSERT INTO conf_papercoauthor (paper_id, author) VALUES (?0, ?1)',
                     [paper_id, coauthor])

        conf.sql('''INSERT INTO conf_paperversion (paper_id, time, title, abstract, contents) 
                    VALUES (?0, ?1, ?2, ?3, ?4)''',
                 [paper_id, conf.now_utc(), title, abstract, contents])

        for conflict in conf.getlist('pc_conflicts[]'):
            new_pc_conflict = conf.sql('SELECT * FROM user_profiles WHERE username = ?0', [conflict])[0][0]
            conf.sql('INSERT INTO conf_paperpcconflict (paper_id, pc_id) values (?0, ?1)',
                     [paper_id, new_pc_conflict])

        return conf.redirect('Service', '/paper?id=' + str(paper_id)) # [FH] todo: fix this

    pcs = conf.sql("SELECT * FROM user_profiles WHERE level = 'pc'")
    pc_conflicts = []
    pcs2 = []
    for pc in conf.sql('SELECT pc_id FROM conf_userpcconflict WHERE id = ?0', [conf.me()]):
        pc_conflicts += [pc[0]]
    for pc in pcs:
        pcs2 += [{'pc': pc, 'conflict': pc in pc_conflicts}]
        
    return conf.render('submit.html', {'coauthors':    ('Service', []),
                                       'title':        ('Service', ''),
                                       'abstract':     ('Service', ''),
                                       'contents':     ('Service', ''),
                                       'error':        ('Service', ''),
                                       'pcs':          ('Service', pcs2),
                                       'pc_conflicts': ('Service', pc_conflicts),
                                       'which_page':   ('Service', 'submit'),
                                       'is_logged_in': ('Service', True),
                                       'is_admin':     ('Service', is_admin())})


# Baseline: profile_view
@conf.route('/accounts/profile/', methods=['GET', 'POST'])
def profile_view():
    if not is_logged_in():
        return conf.redirect('Service', '/accounts/login')
 
    profile = conf.sql('SELECT * FROM user_profiles where user_id = ?0', [conf.me()])[0]
    if profile:
        profile[6] = 'normal'

    pcs = conf.sql("SELECT * FROM user_profiles WHERE level = 'pc'")

    if conf.method() == 'POST':
        name = conf.post('name')
        affiliation = conf.post('affiliation')
        acm_number = conf.post('acm_number')
        email = conf.post('email')
        conf.sql('DELETE FROM user_profiles WHERE user_id = ?0', [conf.me()])
        conf.sql('''INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level, password)
                    VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6, ?7)''',
                 [conf.me(), profile[1], email, name, affiliation, acm_number, profile[6], profile[7]])
        pc_conflicts = []
        for confl in conf.getlist('pc_conflicts[]'):
            new_pc_conflict = conf.sql('SELECT id FROM user_profiles WHERE username = ?0', [confl])[0][0]
            conf.sql('INSERT INTO conf_userpcconflict (user_id, pc_id) VALUES (?0, ?1)', [conf.me(), new_pc_conflict])
            pc_conflicts += [new_pc_conflict]
    else:
        pc_conflicts = []
        for uppc in conf.sql('SELECT id FROM conf_userpcconflict WHERE id = ?0', [conf.me()]):
            pc_conflicts += [uppc[0]]

    pcs2 = []
    for pc in pcs:
        pcs2 += [{'pc': pc, 'conflict': pc in pc_conflicts}]

    return conf.render('profile.html', {'name':         ('Service', profile[3]),
                                        'affiliation':  ('Service', profile[4]),
                                        'acm_number':   ('Service', profile[5]),
                                        'pc_conflicts': ('Service', pc_conflicts),
                                        'email':        ('Service', profile[2]),
                                        'which_page':   ('Service', 'profile'),
                                        'pcs':          ('Service', pcs2),
                                        'is_logged_in': ('Service', True),
                                        'is_admin':     ('Service', is_admin())})


# untested
# Baseline: submit_review_view
@conf.route('/submit_review', methods=['GET', 'POST'])
def submit_review_view():
    user = me_user()
    if not user:
        return conf.redirect('Service', '/accounts/login')
    
    if conf.method() == 'GET':
        paper_id = int(conf.get('id'))
    elif conf.method() == 'POST':
        paper_id = int(conf.get('id'))
    paper = query_db('SELECT * FROM papers WHERE id = ?0', [paper_id])[0]
    if conf.method() == 'POST':
        contents = conf.get('contents')
        score_novelty = conf.get('score_novelty')
        score_presentation = conf.get('score_presentation')
        score_technical = conf.get('score_technical')
        score_confidence = conf.get('score_confidence')
        conf.sql(
            '''INSERT INTO reviews (contents, score_novelty, score_presentation, 
                                    score_technical, score_confidence, paper_id, reviewer_id)
               VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)''',
            [contents, score_novelty, score_presentation, score_technical, score_confidence,
             paper_id, reviewer_id])
        return conf.redirect('Service', "/paper?id=" + str(paper_id))

    return conf.render('submit_review.html', {'paper':              ('Service', paper),
                                              'contents':           ('Service', contents),
                                              'score_novelty':      ('Service', score_novelty),
                                              'score_presentation': ('Service', score_presentation),
                                              'score_technical':    ('Service', score_technical),
                                              'score_confidence':   ('Service', score_confidence),
                                              'paper_id':           ('Service', paper_id),
                                              'which_page':         ('Service', 'submit_review'),
                                              'is_logged_in':       ('Service', True),
                                              'is_admin':           ('Service', is_admin())})


# Baseline: users_view
@conf.route('/users', methods=['GET', 'POST'])
def users_view():
    user = me_user()
    if not user:
        return conf.redirect('Service', '/accounts/login')

    if user[-1] != 'chair':
        return conf.redirect('Service', '/papers_view')

    user_profiles = conf.sql('SELECT * FROM user_profiles')

    return conf.render('users_view.html', {'user_profiles': ('Service', user_profiles),
                                           'which_page':    ('Service', 'users'),
                                           'is_logged_in':  ('Service', True),
                                           'is_admin':      ('Service', True)})


@conf.route('/set_level/<level>/<user_id>', methods=['GET'])
def set_level(level, user_id):
    user = me_user()

    if user:
        if (user[-1] == 'chair') and (level in ['normal', 'pc', 'chair']):
            user = conf.sql('''SELECT (username, email, name, affiliation, acm_number, level)
                               FROM user_profiles WHERE user_id = ?0''', [user_id])[0]
            conf.sql('DELETE FROM user_profiles WHERE user_id = ?0', [user_id])
            conf.sql('''INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level)
                      VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)''',
                     [user_id, user[0], user[1], user[2], user[3], user[4], level])
    

# Baseline: submit_comment_view
@conf.route('/submit_comment', methods=['GET', 'POST'])
def submit_comment_view():
    user = me_user()
    if not user:
        return conf.redirect('Service', '/login')

    if conf.method() == 'GET':
        paper_id = int(conf.get('id'))
    elif conf.method() == 'POST':
        paper_id = int(conf.get('id'))
    paper = conf.sql('SELECT * FROM papers WHERE id = ?0', [paper_id])[0]
    if conf.method() == 'POST':
        contents = conf.get('contents')
        conf.sql('INSERT INTO comments (contents, paper_id, user_id) VALUES (?0, ?1, ?2)',
                 [contents, paper_id, conf.me()])
        return conf.redirect('Service', '/paper?id=' + str(paper_id))

    return conf.render('submit_comment.html', {'paper':        ('Service', paper),
                                               'contents':     ('Service', contents),
                                               'which_page':   ('Service', 'submit_comment'),
                                               'is_logged_in': ('Service', True),
                                               'is_admin':     ('Service', is_admin())})


# Baseline: assign_reviews_view
@conf.route('/assign_reviews', methods=['GET', 'POST'])
def assign_reviews_view():
    if not is_logged_in():
        return conf.redirect('Service', '/login')
    
    possible_reviewers = conf.sql("SELECT * FROM user_profiles WHERE level = 'pc'")

    reviewer_id = conf.get('reviewer_username')
    
    if reviewer_id:
        papers = conf.sql('SELECT * FROM papers')
        papers_data = []
        reviewers = conf.sql('SELECT * FROM reviewers WHERE id = ?0', [reviewer_id])
        for paper in papers:
            papers += [{
                'paper' : paper[0],
                'author_name': conf.sql('SELECT name FROM user_profiles WHERE user_id = ?0', [reviewer_id])[0][0],
                'latest_version' : conf.sql('SELECT * FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC', [paper[0]])[-1],
                'assignment' : conf.sql('SELECT * FROM review_assignments WHERE paper_id = ?0 AND user_id = ?1', [paper[0], reviewer_id]),
                'has_conflict' : conf.sql('SELECT * FROM conf_paperpcconflict WHERE pc_id = ?0 and paper_id = ?1', [reviewer_id, paper[0]])[0] != None}]
    else:
        reviewer = None
        papers_data = []

    return conf.render('assign_reviews.html', {'reviewer':           ('Service', reviewer),
                                               'possible_reviewers': ('Service', possible_reviewers),
                                               'papers_data':        ('Service', papers_data),
                                               'which_page':         ('Service', 'assign_reviews'),
                                               'is_logged_in':       ('Service', True),
                                               'is_admin':           ('Service', is_admin())})


@conf.route('/assign_reviewer/<paper_id>/<reviewer_id>/<value>', methods=['GET'])
def assign_reviewer(paper_id, reviewer_id, value):
    user = me_user()

    if user:
        if user[-1] == 'chair':
            if value == 'yes':
                conf.sql('INSERT INTO review_assignments (assign_type, paper_id, user_id) VALUES (?0, ?1, ?2)',
                         ['assigned', paper_id, reviewer_id])
            else:
                conf.sql('DELETE FROM review_assignments WHERE paper_id = ?0 AND reviewer_id = ?1',
                         [paper_id, reviewer_id])


# Baseline: search_view
@conf.route('/search')
def search_view():
    if not is_logged_in():
        return conf.redirect('Service', '/login')

    reviewers = conf.sql('SELECT * FROM user_profiles')
    authors = conf.sql('SELECT * FROM user_profiles')

    return conf.render('search.html', {'reviewers':    ('Service', reviewers),
                                       'authors':      ('Service', authors),
                                       'which_page':   ('Service', 'search'),
                                       'is_logged_in': ('Service', True),
                                       'is_admin':     ('Service', is_admin())})


@conf.route('/accounts/login') #nc
def login(): # Δ: different login infrastructure #nc
    """Logs the user in."""
    if is_logged_in():
        return conf.redirect('Service', '/')
    return conf.render('registration/login.html')


@conf.route('/accounts/do_login') #nc
def do_login(): # Δ: different login infrastructure #nc
    """Logs the user in."""
    if is_logged_in():
        return conf.redirect('Service', '/')
    user = conf.sql("SELECT * FROM user_profiles WHERE user_id = ?0", [conf.me()])
    if len(user) > 0:
        conf.set_session('loggedin', True)
        return conf.redirect('Service', '/')
    else:
        return conf.render('registration/login.html',
                           {'error': ('Service', 'Invalid user (register to access)')})


@conf.route('/accounts/register', methods=['GET', 'POST']) #nc
def register(): # Δ: different login infrastructure #nc
    """Registers the user."""
    if is_logged_in():
        return conf.redirect('Service', '/')
    user_id  = conf.me()
    username = conf.name()
    if conf.method() == 'POST':
        already_registered = conf.sql("SELECT * FROM user_profiles WHERE user_id = ?0", [conf.me()])
        if len(already_registered) == 0:
            email       = conf.post('email')
            name        = conf.post('name')
            affiliation = conf.post('affiliation')
            acm_number  = conf.post('acm_number')
            level       = 'normal'
            conf.sql('''INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level)
                     VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)''',
                     [user_id, username, email, name, affiliation, acm_number, level])
        return conf.redirect('Service', '/accounts/do_login')
    return conf.render('registration/register.html',
                       {'id':       ('Service', user_id),
                        "username": ('Service', username)})


@conf.route('/accounts/logout') #nc
def logout(): # Δ: different login infrastructure #nc
    conf.pop_session('loggedin')
    return conf.redirect('Service', '/accounts/login')

@conf.handle_field_deletion('comments', ['contents', 'paper_id']) #sp
def handle_comments_contents_deletion(id_):
    conf.sql("DELETE FROM comments WHERE id = ?0", [id_])
    return None

@conf.handle_field_rectification('comments', 'paper_id') #sp
def handle_comments_paper_id_rectification(id_, val, can):
    conf.sql("DELETE FROM comments WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('conf_papercoauthor', ['author', 'paper_id']) #sp
def handle_conf_papercoauthor_author_deletion(id_):
    conf.sql("DELETE FROM conf_papercoauthor WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('conf_paperpcconflict', 'pc_id') #sp
def handle_conf_paperpcconflict_pc_id_deletion(id_):
    conf.sql("DELETE FROM conf_paperpcconflict WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('conf_paperversion', ['title', 'contents', 'abstract']) #sp
def handle_conf_paperversion_title_deletion(id_):
    conf.sql("DELETE FROM conf_paperversion WHERE id = ?0", [id_])
    return None

@conf.handle_field_rectification('conf_paperversion', ['title', 'contents', 'abstract']) #sp
def handle_conf_paperversion_title_rectification(id_, val, can):
    conf.sql("DELETE FROM conf_paperversion WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('conf_userpcconflict', 'pc_id') #sp
def handle_conf_userpcconflict_pc_id_deletion(id_):
    conf.sql("DELETE FROM conf_userpcconflict WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('review_assignments', ['paper_id', 'user_id']) #sp
def handle_review_assignments_paper_id_deletion(id_):
    conf.sql("DELETE FROM review_assignments WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('reviews', ['contents', 'score_novelty', 'score_presentation', 'score_technical', 'score_confidence', 'paper_id']) #sp
def handle_reviews_contents_deletion(id_):
    conf.sql("DELETE FROM reviews WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('tags', ['name', 'paper_id']) #sp
def handle_tags_name_deletion(id_):
    conf.sql("DELETE FROM tags WHERE id = ?0", [id_])
    return None

@conf.handle_field_deletion('user_profiles', ['username', 'email', 'name', 'affiliation', 'acm_number']) #sp
def handle_user_profiles_username_deletion(id_):
    conf.sql("DELETE FROM user_profiles WHERE id = ?0", [id_])
    return None    

    
