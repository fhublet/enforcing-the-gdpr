# -*- coding: utf-8 -*-
###
#   Minitwitplus
#   ~~~~~~~~
#
#   A microblogging application written with Flask and sqlite3.
#
#   :copyright: (c) 2010 by Armin Ronacher.
#   :license: BSD, see LICENSE for more details.
#
#   Ported to TTC by Anonymous authors (c) 2023
###

from apps import minitwitplus

def is_logged_in():
    return minitwitplus.get_session('loggedin')


def me_user():
    if is_logged_in():
        return minitwitplus.sql("SELECT * FROM user WHERE user_id = ?0", [minitwitplus.me()])[0]
    else:
        return None

    
def filter_check(messages): #sp
    messages2 = []
    checks = minitwitplus.check_all('Service', messages)
    L = len(messages)
    i = 0
    while i < L:
        if checks[i]:
            append(messages2, messages[i])
        i += 1
    return messages2


# Baseline: get_user_id
def get_user_id(username):
    """Convenience method to look up the id for a username."""
    rv = minitwitplus.sql('SELECT id FROM user WHERE username = ?0', [username])
    if len(rv) > 0:
        return rv[0][0]
    else:
        return -1


# Baseline: generate_ad
def generate_ad(messages):
    """Generates personalized advertisement based on the user browsing."""
    all_message_texts = ""
    for message in messages:
        all_message_texts += message[3]
    if not minitwitplus.check('Marketing', all_message_texts):
        all_message_texts = ""
    if "editor" in all_message_texts:
        return {"title": "Emacs",
                "text":  "... is the best text editor",
                "link": "https://emacs.org"}
    elif "OS" in all_message_texts:
        return {"title": "Linux",
                "text": "... has the best kernel",
                "link": "https://linux.org"}
    else:
        return {"title": "The Hague",
                "text": "... has the best conferences",
                "link": "https://denhaag.com"}

# Baseline: timeline
@minitwitplus.route('/')
def timeline():
    """Shows a users timeline or if no user is logged in it will
    redirect to the public timeline.  This timeline shows the user's
    messages as well as all the messages of followed users.
    """
    user = me_user()
    if not user:
        return minitwitplus.redirect('Service', '/public')
    messages1 = minitwitplus.sql('''
       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
              user.user_id, user.username FROM message
       JOIN user ON message.author_id = user.id
       JOIN follower ON follower.whom_id = user.user_id
       WHERE follower.who_id = ?0
       ORDER BY message.id DESC LIMIT 30''', [user[0]])
    messages2 = minitwitplus.sql('''
       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
              user.user_id, user.username FROM message
       JOIN user ON message.author_id = user.id
       WHERE user.id = ?0 ORDER BY message.id DESC LIMIT 30''', [user[0]])
    messages12 = filter_check(sorted_by0(messages1 + messages2)) #sp
    messages = []
    i = 0
    while (i < 30) and (i < len(messages12)):
        messages += [messages12[i]]
        i += 1
    return minitwitplus.render('timeline.html', {"messages": ("Service", messages),
                                                 "user":     ("Service", user),
                                                 "title":    ("Service", "My Timeline"),
                                                 "flash":    ("Service", minitwitplus.unflash()),
                                                 "ad":       ("Marketing", generate_ad(messages))})

# Baseline: public_timeline
@minitwitplus.route('/public')
def public_timeline():
    """Displays the latest messages of all users."""
    messages = minitwitplus.sql('''
       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
              user.user_id, user.username FROM message
       JOIN user ON message.author_id = user.id
       ORDER BY message.id DESC LIMIT 30''')
    messages = filter_check(messages) #sp
    return minitwitplus.render('timeline.html', {"messages": ("Service", messages),
                                                 "user":     ("Service", me_user()),
                                                 "title":    ("Service", "Public Timeline"),
                                                 "ad":       ("Marketing", generate_ad(messages))})


# Baseline: user_timeline
@minitwitplus.route('/<username>')
def user_timeline(username):
    """Displays a user's tweets."""
    profile_user = minitwitplus.sql("SELECT * FROM user WHERE username = ?0", [username])
    if len(profile_user) == 0:
        return minitwitplus.redirect('Service', '/') # Δ: 404
    followed = False
    user = me_user()
    if user:
        followed = len(minitwitplus.sql('''
      SELECT 1 FROM follower WHERE follower.who_id = ?0 AND follower.whom_id = ?1''',
                                    [user[0], profile_user[0][0]])) > 0
    messages = minitwitplus.sql('''
      SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
             user.user_id, user.username FROM message
      JOIN user ON message.author_id = user.id
      WHERE user.id = ?0 ORDER BY message.id DESC LIMIT 30''',
                            [profile_user[0][0]])
    messages = filter_check(messages) #sp
    minitwitplus.send("trustedanalytics.com", "Analytics", username)
    minitwitplus.send("evilanalytics.com", "Analytics", username)
    return minitwitplus.render('timeline.html', {"messages":     ("Service", messages),
                                                 "followed":     ("Service", followed),
                                                 "profile_user": ("Service", profile_user[0]),
                                                 "user":         ("Service", user),
                                                 "flash":        ("Service", minitwitplus.unflash()),
                                                 "ad":           ("Marketing", generate_ad(messages))})

# Baseline: follow_user
@minitwitplus.route('/<username>/follow')
def follow_user(username):
    """Adds the current user as follower of the given user."""
    user = me_user()
    if not user:
        return minitwitplus.redirect('Service', '/') # Δ: 401
    whom_id = get_user_id(username)
    minitwitplus.sql('INSERT INTO follower (who_id, whom_id) VALUES (?0, ?1)',
                     [user[0], whom_id])
    if not whom_id:
        return minitwitplus.redirect('Service', '/') # Δ: 401
    minitwitplus.flash('You are now following "' + username + '"')
    return minitwitplus.redirect('Service', '/' + username)


# Baseline: unfollow_user
@minitwitplus.route('/<username>/unfollow')
def unfollow_user(username):
    """Removes the current user as follower of the given user."""
    user = me_user()
    if not user:
        return minitwitplus.redirect('Service', '/') # Δ: 401
    whom_id = get_user_id(username)
    minitwitplus.sql('DELETE FROM follower WHERE who_id = ?0 AND whom_id = ?1',
                     [user[0], whom_id])
    if not whom_id:
        return minitwitplus.redirect('Service', '/') # Δ: 401
    minitwitplus.flash('You are no longer following "' + username + '"')
    return minitwitplus.redirect('Service', '/' + username)


# Baseline: add_message
@minitwitplus.route('/add_message', methods=['POST'])
def add_message():
    """Registers a new message for the user."""
    user = me_user()
    if not user:
        return minitwitplus.redirect('Service', '/') # Δ: 401
    if minitwitplus.method() == 'POST':
        minitwitplus.sql('''INSERT INTO message (author_id, text, pub_date) 
          VALUES (?0, ?1, ?2)''', [user[0], minitwitplus.post('text'), minitwitplus.now_utc()])
        minitwitplus.flash("Your message was recorded")
    return minitwitplus.redirect('Service', '/')


# Baseline: login
@minitwitplus.route('/login') #nc
def login(): # Δ: different login infrastructure #nc
    """Logs the user in."""
    if is_logged_in():
        return minitwitplus.redirect('Service', '/')
    user = minitwitplus.sql("SELECT * FROM user WHERE user_id = ?0", [minitwitplus.me()])
    if len(user) > 0:
        minitwitplus.set_session('loggedin', True)
        minitwitplus.flash('You were logged in')
    else:
        minitwitplus.flash('Invalid user (register to access)')
    return minitwitplus.redirect('Service', '/')


# Baseline: register
@minitwitplus.route('/register', methods=['GET', 'POST']) #nc
def register(): # Δ: different login infrastructure #nc
    """Registers the user."""
    if is_logged_in():
        return minitwitplus.redirect('Service', '/')
    user_id   = minitwitplus.me()
    user_name = minitwitplus.name()
    if minitwitplus.method() == 'POST':
        already_registered = minitwitplus.sql("SELECT * FROM user WHERE user_id = ?0", [minitwitplus.me()])
        if len(already_registered) == 0:
            new_user = minitwitplus.sql("INSERT INTO user (user_id, username) VALUES (?0, ?1)",
                                    [user_id, user_name])
            minitwitplus.flash('You were successfully registered')
        else:
            minitwitplus.flash('You were already registered')
        return minitwitplus.redirect('Service', '/')
    return minitwitplus.render("register.html", {"id": ("Service", user_id),
                                             "name": ("Service", user_name)})

# Baseline: logout #nc
@minitwitplus.route('/logout') #nc
def logout():
    minitwitplus.flash('You were logged out')
    minitwitplus.pop_session('loggedin')
    return minitwitplus.redirect('Service', '/public')


@minitwitplus.handle_field_deletion('message', 'text') #sp
def handle_message_text_deletion(id_):
    minitwitplus.sql("DELETE FROM message WHERE id = ?0", [id_])
    return None

@minitwitplus.handle_field_rectification('message', 'text') #sp
def handle_message_text_rectification(id_, val, can_delete):
    row = minitwitplus.sql("SELECT author_id, text, pub_date FROM message WHERE id = ?0",
                           [id_])[0]
    minitwitplus.sql("INSERT INTO message (author_id, text, pub_date) VALUES (?0, ?1, ?2)",
                     [row[0], val + " [edited]", row[2]])
    minitwitplus.sql("DELETE FROM message WHERE id = ?0", [id_])
    return None

@minitwitplus.handle_field_deletion('follower', 'whom_id') #sp
def handle_follower_whom_id_deletion(id_):
    minitwitplus.sql("DELETE FROM follower WHERE id = ?0", [id_])
    return None
