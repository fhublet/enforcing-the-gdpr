from apps import hipaa

def is_logged_in():
    return hipaa.get_session('loggedin')

def me_user():
    if is_logged_in():
        return hipaa.sql("SELECT * FROM UserProfile WHERE user_id = ?0", [hipaa.me()])[0]
    else:
        return None


@hipaa.route('/error')
def error():
    msg = "You are not allowed to access this page: Not a User"
    return hipaa.render('error.html', {'message': ('Service', msg)})


def filter_check(messages): #sp
    messages2 = []
    print('check')
    checks = hipaa.check_all('Service', messages)
    print('end check')
    L = len(messages)
    i = 0
    while i < L:
        print(i)
        if checks[i]:
            append(messages2, messages[i])
        i += 1
    return messages2


@hipaa.route('/accounts/profile', methods=['GET', 'POST'])
def profile():
    """Displaying and updating profiles.
    """
    current_user = me_user()
    if not current_user:
        return hipaa.redirect('Service', '/error')

    if hipaa.method() == 'POST':
        email = hipaa.post('email')
        profiletype = hipaa.post('profiletype')
        name = hipaa.post('name')
        hipaa.sql('DELETE FROM UserProfile WHERE user_id = ?0', [hipaa.me()])
        hipaa.sql('''INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name) 
                     VALUES (?0, ?1, ?2, ?3, ?4, ?5)''',
                  [hipaa.me(), hipaa.name(), 1, email, profiletype, name])
        return hipaa.redirect('Service', '/accounts/profile')

    current_user = me_user()

    return hipaa.render("profile.html", {"profile": ('Service', current_user),
                                         "which_page": ('Service', "profile"),
                                         "is_logged_in": ('Service', True)})


@hipaa.route('/')
def index():
    """The main page shows patients and entities.
    """
    current_user = me_user()
    if not current_user:
        return hipaa.redirect('Service', '/accounts/login')

    print(1)
    patients = filter_check(hipaa.sql('SELECT * FROM Individual'))
    print(2)
    entities = filter_check(hipaa.sql('SELECT * FROM CoveredEntity'))
    print(3)
    return hipaa.render("index.html", {"patients": ('Service', patients),
                                       "entities": ('Service', entities),
                                       'name':     ('Service', current_user[6]),
                                       "is_logged_in": ('Service', True)})


@hipaa.route('/about')
def about_view():
    """About the system.
    """
    return hipaa.render("about.html", {'which_page' : ('Service', "about"),
                                       "is_logged_in": ('Service', is_logged_in())})


@hipaa.route('/users', methods=['GET', 'POST'])
def users():
    """Viewing all users.
    """
    current_user = me_user()
    if current_user[5] != 3:
        return hipaa.redirect('Service', '/')

    user_profiles = filter_check(hipaa.sql('SELECT * FROM UserProfile')) #sp

    return hipaa.render("users_view.html", {'user_profiles': ('Service', user_profiles),
                                            'which_page':    ('Service', "users"),
                                            "is_logged_in":  ('Service', True)})


@hipaa.route('/set_level/<level>/<user_id>', methods=['GET'])
def set_level(level, user_id):
    user = me_user()

    if user:
        if (user[5] == 3) and (level in ['0', '1', '2', '3', '4', '5', '6']):
            user = hipaa.sql('''SELECT username, is_active, email, name
                               FROM UserProfile WHERE user_id = ?0''', [user_id])[0]
            hipaa.sql('DELETE FROM UserProfile WHERE user_id = ?0', [user_id])
            hipaa.sql('''INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name)
                      VALUES (?0, ?1, ?2, ?3, ?4, ?5)''',
                     [user_id, user[0], user[1], user[2], level, user[3]])

    return hipaa.render('error.html')
            
@hipaa.route('/patients/<int:id>/treatments')
def patient_treatment(id):
    """Treatments.
    """
    if not is_logged_in():
        return hipaa.redirect('Service', '/')
    
    p = hipaa.sql('SELECT * FROM Individual WHERE id = ?0', [id])[0]
    treatments = hipaa.sql('SELECT * FROM Treatment WHERE patient_id = ?0', [p[0]])

    return hipaa.render("treatments.html", {"first_name": ('Service', p[0]),
                                            "last_name":  ('Service', p[1]),
                                            "treatments": ('Service', treatments),
                                            "is_logged_in": ('Service', True)})


@hipaa.route('/patients/<int:id>/diagnoses')
def patient_diagnoses(id):
    """Diagnoses.
    """
    if not is_logged_in():
        return hipaa.redirect('Service', '/')

    p = hipaa.sql('SELECT * FROM Individual WHERE id = ?0', [id])[0]
    newDiagnoses = filter_check(hipaa.sql('SELECT * FROM Diagnosis WHERE patient_id = ?0', [p[0]])) #sp

    return hipaa.render("treatments.html", {"first_name": ('Service', p[0]),
                                            "last_name":  ('Service', p[1]),
                                            "diagnoses":  ('Service', newDiagnoses),
                                            "is_logged_in": ('Service', True)})


@hipaa.route('/patients/<int:id>/')
def info(id):
    """Viewing information about an individual.
    """
    if not is_logged_in():
        return hipaa.redirect('Service', '/')

    p = hipaa.sql('SELECT * FROM Individual WHERE id = ?0', [id])[0]
    dataset = []
    dataset += [("Sex", p[6], False)]
    return hipaa.render("info.html", {"patient": ('Service', p),
                                      "dataset": ('Service', dataset),
                                      "is_logged_in": ('Service', True)})


@hipaa.route('/entities/<int:id>/transactions')
def entity_transaction(id):
    """
    Viewing transactions.
    """
    if not is_logged_in():
        return hipaa.redirect('Service', '/')

    entity = hipaa.sql('SELECT * FROM CoveredEntity WHERE id = ?0', [id])[0]
    transactions = filter_check(hipaa.sql('SELECT * FROM Transaction WHERE FirstParty = ?0', [entity[0]])) #sp
    other_transactions = filter_check(hipaa.sql('SELECT * FROM Transaction WHERE SecondParty = ?0', [entity[0]])) #sp
 
    return hipaa.render("transactions.html", {"entity": ('Service', entity),
                                              "transactions": ('Service', transactions),
                                              "other_transactions": ('Service', other_transactions),
                                              "is_logged_in": ('Service', True)})


@hipaa.route('/entities/<int:id>/associates')
def entity_associate(id):
    if not is_logged_in():
        return hipaa.redirect('Service', '/')
    
    entity = hipaa.sql('SELECT * FROM CoveredEntity WHERE id = ?0', [id])[0]
    associates = hipaa.sql('''SELECT UserProfile.* FROM UserProfile JOIN BusinessAssociateAgreement 
    ON BusinessAssociateAgreement.BusinessAssociateID = UserProfile.id
    WHERE BusinessAssociateAgreement.CoveredEntityID = ?0''', [entity[0]])
    associates = filter_check(associates) #sp
    return hipaa.render("associates.html", {"entity":     ('Service', entity),
                                            "associates": ('Service', associates),
                                            "is_logged_in": ('Service', True)})


@hipaa.route('/entities/<int:id>/')
def entity_directory(id):
    """Viewing covered entities.
    """
    if not is_logged_in():
        return hipaa.redirect('Service', '/')

    entity = hipaa.sql('SELECT * FROM CoveredEntity WHERE id = ?0', [id])[0]
    visits = filter_check(hipaa.sql('SELECT * FROM HospitalVisit WHERE hospitalID = ?0', [entity[0]])) #sp

    return hipaa.render("directory.html", {"entity":  ('Service', entity),
                                           "visits":  ('Service', visits),
                                           "is_logged_in": ('Service', True)})


# Baseline: login
@hipaa.route('/accounts/login') #nc
def login(): # Δ: different login infrastructure #nc
    """Logs the user in."""
    if is_logged_in():
        return hipaa.redirect('Service', '/')
    return hipaa.render('registration/login.html')


@hipaa.route('/accounts/do_login') #nc
def do_login(): # Δ: different login infrastructure #nc
    """Logs the user in."""
    if is_logged_in():
        return hipaa.redirect('Service', '/')
    user = hipaa.sql("SELECT * FROM UserProfile WHERE user_id = ?0", [hipaa.me()])
    if len(user) > 0:
        hipaa.set_session('loggedin', True)
        return hipaa.redirect('Service', '/')
    else:
        return hipaa.render('registration/login.html',
                            {'error': ('Service', 'Invalid user (register to access)')})

    
# Baseline: register
@hipaa.route('/accounts/register', methods=['GET', 'POST']) #nc
def register(): # Δ: different login infrastructure #nc
    """Registers the user."""
    if is_logged_in():
        return hipaa.redirect('Service', '/')
    user_id  = hipaa.me()
    username = hipaa.name()
    if hipaa.method() == 'POST':
        already_registered = hipaa.sql("SELECT * FROM UserProfile WHERE user_id = ?0", [hipaa.me()])
        if len(already_registered) == 0:
            email       = hipaa.post('email')
            name        = hipaa.post('name')
            hipaa.sql('''INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name)
                     VALUES (?0, ?1, ?2, ?3, ?4, ?5)''',
                     [user_id, username, 1, email, 1, name])
        return hipaa.redirect('Service', '/accounts/do_login')
    return hipaa.render('registration/register.html',
                       {'id':       ('Service', user_id),
                        "username": ('Service', username)})

# Baseline: logout
@hipaa.route('/accounts/logout') #nc
def logout(): # Δ: different login infrastructure #nc
    hipaa.pop_session('loggedin')
    return hipaa.redirect('Service', '/accounts/login')


@hipaa.handle_field_deletion('Address', ['City', 'State', 'ZipCode']) #sp
def handle_Address_deletion(id_):
    hipaa.sql("DELETE FROM Address WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_deletion('Diagnosis', ['Manifestation', 'Diagnosis', 'DateRecognized', 'patient_id']) #sp
def handle_Diagnosis_deletion(id_):
    hipaa.sql("DELETE FROM Diagnosis WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_rectification('Diagnosis', ['Manifestation', 'Diagnosis', 'DateRecognized', 'patient_id']) #sp
def handle_Diagnosis_rectification(id_, val, can):
    hipaa.sql("DELETE FROM Diagnosis WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_deletion('HospitalVisit', ['patientID', 'hospitalID', 'date_admitted', 'location', 'condition', 'date_released']) #sp
def handle_HospitalVisit_deletion(id_, val, can):
    hipaa.sql("DELETE FROM HospitalVisit WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_rectification('HospitalVisit', ['patientID', 'hospitalID', 'date_admitted', 'location', 'condition', 'date_released']) #sp
def handle_HospitalVisit_rectification(id_, val, can):
    hipaa.sql("DELETE FROM HospitalVisit WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_deletion('Individual', ['FirstName', 'LastName']) #sp
def handle_Individual_deletion(id_):
    hipaa.sql("DELETE FROM Individual WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_rectification('Individual', ['FirstName', 'LastName']) #sp
def handle_Individual_rectification(id_, val, can):
    hipaa.sql("DELETE FROM Individual WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_deletion('Transaction', ['Standard', 'FirstPartyID', 'SecondPartyID', 'SharedInformationID', 'DateRequested', 'DateResponded', 'Purpose']) #sp
def handle_Transaction_deletion(id_):
    hipaa.sql("DELETE FROM Transaction WHERE id = ?0", [id_])
    return None

@hipaa.handle_field_rectification('Transaction', ['Standard', 'FirstPartyID', 'SecondPartyID', 'SharedInformationID', 'DateRequested', 'DateResponded', 'Purpose']) #sp
def handle_Transaction_rectification(id_, val, can):
    hipaa.sql("DELETE FROM Transaction WHERE id = ?0", [id_])
    return None





