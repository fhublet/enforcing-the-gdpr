# -*- coding: utf-8 -*-
###
#   MiniTwit
#   ~~~~~~~~
#
#   A microblogging application written with Flask and sqlite3.
#
#   :copyright: (c) 2010 by Armin Ronacher.
#   :license: BSD, see LICENSE for more details.
#
#   Ported to TTC by Anonymous authors (c) 2023
###

from apps import minitwit

    
def is_logged_in():
    return minitwit.get_session('loggedin')


def me_user():
    if is_logged_in():
        return minitwit.sql("SELECT * FROM user WHERE user_id = ?0", [minitwit.me()])[0]
    else:
        return None

    
def filter_check(messages): #sp
    messages2 = []
    for message in messages:
        if minitwit.check('Service', message):
            messages2 += [message]
    return messages2


# Baseline: get_user_id
def get_user_id(username):
    """Convenience method to look up the id for a username."""
    rv = minitwit.sql('SELECT id FROM user WHERE username = ?0', [username])
    if len(rv) > 0:
        return rv[0][0]
    else:
        return None

    
# Baseline: timeline
@minitwit.route('/')
def timeline():
    """Shows a users timeline or if no user is logged in it will
    redirect to the public timeline.  This timeline shows the user's
    messages as well as all the messages of followed users.
    """
    user = me_user()
    if not user:
        return minitwit.redirect('Service', '/public')
    messages1 = minitwit.sql('''
       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
              user.user_id, user.username FROM message
       JOIN user ON message.author_id = user.id
       JOIN follower ON follower.whom_id = user.id
       WHERE follower.who_id = ?0
       ORDER BY message.pub_date DESC LIMIT 30''', [user[0]])
    messages2 = minitwit.sql('''
       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
              user.user_id, user.username FROM message
       JOIN user ON message.author_id = user.id
       WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30''', [user[0]])
    messages12 = filter_check(sorted_by0(messages1 + messages2)) #sp
    messages = []
    i = 0
    while (i < 30) and (i < len(messages12)):
        messages += [messages12[i]]
        i += 1
    return minitwit.render('timeline.html', {"messages": ("Service", messages),
                                             "user":     ("Service", user),
                                             "title":    ("Service", "My Timeline"),
                                             "flash":    ("Service", minitwit.unflash())})

# Baseline: public_timeline
@minitwit.route('/public')
def public_timeline():
    """Displays the latest messages of all users."""
    messages = minitwit.sql('''
       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
              user.user_id, user.username FROM message
       JOIN user ON message.author_id = user.id
       ORDER BY message.pub_date DESC LIMIT 30''')
    messages = filter_check(messages) #sp
    return minitwit.render('timeline.html', {"messages": ("Service", messages),
                                             "user":     ("Service", me_user()),
                                             "title":    ("Service", "Public Timeline")})


# Baseline: user_timeline
@minitwit.route('/<username>')
def user_timeline(username):
    """Displays a user's tweets."""
    profile_user = minitwit.sql("SELECT * FROM user WHERE username = ?0", [username])
    if len(profile_user) == 0:
        return minitwit.redirect('Service', '/') # Δ: 404
    followed = False
    user = me_user()
    if user:
        followed = len(minitwit.sql('''
      SELECT 1 FROM follower WHERE follower.who_id = ?0 AND follower.whom_id = ?1''',
                                    [user[0], profile_user[0][0]])) > 0
    messages = minitwit.sql('''
      SELECT message.pub_date, message.id, message.author_id, message.text, user.id, 
             user.user_id, user.username FROM message
      JOIN user ON user.id = message.author_id
      WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30''',
                            [profile_user[0][0]])
    messages = filter_check(messages) #sp
    return minitwit.render('timeline.html', {"messages":     ("Service", messages),
                                             "followed":     ("Service", followed),
                                             "profile_user": ("Service", profile_user[0]),
                                             "user":         ("Service", user),
                                             "flash":        ("Service", minitwit.unflash())})

# Baseline: follow_user
@minitwit.route('/<username>/follow')
def follow_user(username):
    """Adds the current user as follower of the given user."""
    user = me_user()
    if not user:
        return minitwit.redirect('Service', '/') # Δ: 401
    whom_id = get_user_id(username)
    if not whom_id:
        return minitwit.redirect('Service', '/') # Δ: 401
    minitwit.sql('INSERT INTO follower (who_id, whom_id) VALUES (?0, ?1)',
                 [user[0], whom_id])
    minitwit.flash('You are now following "' + username + '"')
    return minitwit.redirect('Service', '/' + username)


# Baseline: unfollow_user
@minitwit.route('/<username>/unfollow')
def unfollow_user(username):
    """Removes the current user as follower of the given user."""
    user = me_user()
    if not user:
        return minitwit.redirect('Service', '/') # Δ: 401
    whom_id = get_user_id(username)
    if not whom_id:
        return minitwit.redirect('Service', '/') # Δ: 401
    minitwit.sql('DELETE FROM follower WHERE who_id = ?0 AND whom_id = ?1',
                 [user[0], whom_id])
    minitwit.flash('You are no longer following "' + username + '"')
    return minitwit.redirect('Service', '/' + username)


# Baseline: add_message
@minitwit.route('/add_message', methods=['POST'])
def add_message():
    """Registers a new message for the user."""
    user = me_user()
    if not user:
        return minitwit.redirect('Service', '/') # Δ: 401
    if minitwit.method() == 'POST':
        minitwit.sql('''INSERT INTO message (author_id, text, pub_date) 
          VALUES (?0, ?1, ?2)''', [user[0], minitwit.post('text'), minitwit.now_utc()])
        minitwit.flash("Your message was recorded")
    return minitwit.redirect('Service', '/')


# Baseline: login
@minitwit.route('/login') #nc
def login(): # Δ: different login infrastructure #nc
    """Logs the user in."""
    if is_logged_in():
        return minitwit.redirect('Service', '/')
    user = minitwit.sql("SELECT * FROM user WHERE user_id = ?0", [minitwit.me()])
    if len(user) > 0:
        minitwit.set_session('loggedin', True)
        minitwit.flash('You were logged in')
    else:
        minitwit.flash('Invalid user (register to access)')
    return minitwit.redirect('Service', '/')


# Baseline: register
@minitwit.route('/register', methods=['GET', 'POST']) #nc
def register(): # Δ: different login infrastructure #nc
    """Registers the user."""
    if is_logged_in():
        return minitwit.redirect('Service', '/')
    user_id   = minitwit.me()
    user_name = minitwit.name()
    if minitwit.method() == 'POST':
        already_registered = minitwit.sql("SELECT * FROM user WHERE user_id = ?0", [minitwit.me()])
        if len(already_registered) == 0:
            new_user = minitwit.sql("INSERT INTO user (user_id, username) VALUES (?0, ?1)",
                                    [user_id, user_name])
            minitwit.flash('You were successfully registered')
        else:
            minitwit.flash('You were already registered')
        return minitwit.redirect('Service', '/')
    return minitwit.render("register.html", {"id": ("Service", user_id),
                                             "name": ("Service", user_name)})


# Baseline: logout
@minitwit.route('/logout') #nc
def logout(): #nc
    minitwit.flash('You were logged out')
    minitwit.pop_session('loggedin')
    return minitwit.redirect('Service', '/public')


@minitwit.handle_field_deletion('message', 'text')
def handle_message_text_deletion(id_):
    minitwit.sql("DELETE FROM message WHERE id = ?0", [id_])
    return None











