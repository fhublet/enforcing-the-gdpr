from settings import bank, server, is_set

from databank.application import Application, PassiveApplication

if is_set("passive"):
    ApplicationClass = PassiveApplication
else:
    ApplicationClass = Application

meetup = ApplicationClass(bank, server, 2, "meetup", "127.0.0.1", "../db/database.db", "The answer is no")
minitwit = ApplicationClass(bank, server, 3, "minitwit", "127.0.0.1", "../db/database.db", "The answer is no")
minitwitplus = ApplicationClass(bank, server, 31, "minitwitplus", "127.0.0.1", "../db/database.db", "The answer is no")
conf = ApplicationClass(bank, server, 5, "conf", "127.0.0.1", "../db/database.db", "The answer is no")
hipaa = ApplicationClass(bank, server, 6, "hipaa", "127.0.0.1", "../db/database.db", "The answer is no")
