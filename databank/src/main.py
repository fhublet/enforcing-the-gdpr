from flask import Flask, render_template, request, session, url_for, redirect, \
    escape, send_file, jsonify
import subprocess
import os
import sys
from time import sleep
import uuid

from settings import *

if __name__ == '__main__':
    for param in sys.argv[1:]:
        if param == "-m":
            # show monitor trace
            set_option('verbose_monitor')
        elif param == "-d":
            # show database trace
            set_option('verbose_db')
        elif param == "-p":
            # make application passive (no labelling or monitoring)
            set_option('passive')
        elif param == "-sp":
            # make application semi-passive (labelling but no monitoring)
            set_option('semipassive')
        elif param == "-t":
            # compute monitoring trace and dump it on failure
            set_option('trace')
        elif param == "-nc":
            # do not compile
            set_option('no_compile')

from catalog import user_catalog, catalog_by_name
from databank.monitor import monitor, generate_ut, Monitor, loginput_backdoor, \
    retrieve_inputs, retrieve_outputs, retrieve_consent, retrieve_deletions, retrieve_rectifications, \
    retrieve_accesses, retrieve_restrictions, retrieve_legal_grounds, \
    log_ds_consent, log_ds_revoke, log_ds_erase, log_ds_rectify, log_ds_access, log_ds_restrict, log_ds_repeal

## Index, login, logout

@bank.route('/', methods=['GET'])
def index():
    # Data from current session
    user_id = session.get('user_id', None)
    user_name = session.get('user_name', None)
    # Failed authentication
    wrong_pass = request.args.get('wrong_pass', None)
    # Catalog
    if user_id is not None:
        apps = user_catalog(user_id)
    # Render index
    return render_template("index.html", **locals())

@bank.route('/login', methods=['POST'])
def login():
    # Should not be accessed by an already logged-in user
    if 'user_id' in session:
        return redirect(url_for('index'))
    # If not logged in
    if request.method == 'POST':
        name  = request.form['name']
        hash_ = hashing.hash_value(request.form['password'])
        query = "SELECT * FROM users WHERE name = ? AND hash = ?"
        cur = get_db().execute(query, [name, hash_])
        res = cur.fetchall()
        cur.close()
        if res:
            session['user_id']   = res[0][0]
            session['user_name'] = res[0][1]
            session['user_apps'] = [app.id_ for app in user_catalog(res[0][0])]
            # Log in, redirect to index
            return redirect(url_for('index'))
    # Login failed, redirect to index
    return redirect(url_for('index', wrong_pass=True))

@bank.route('/logout')
def logout():
    if 'user_id' in session:
        session.pop('user_id', None)
        session.pop('user_name', None)
    return redirect(url_for('index'))

## Profile editor

@bank.route('/profile')
def profile():
    if 'user_id' not in session:
        return redirect(url_for('index'))
    status = request.args.get('status', "0")
    name = session.get('user_name')
    user_id = session['user_id']
    query = "SELECT token FROM users WHERE id = ?"
    cur = get_db().execute(query, [user_id])
    res = cur.fetchall()
    cur.close()
    if res[0][0] is not None: 
        token = res[0][0]
    else:
        return reset_token()
    return render_template("profile.html", **locals())

@bank.route('/profile/change_name', methods=['POST'])
def change_name():
    if 'user_id' not in session:
        return redirect(url_for('index'))
    name = request.form['name']
    query = "UPDATE users SET name = ? WHERE id = ?"
    con = get_db()
    con.execute(query, [name, session['user_id']])
    con.commit()
    session['user_name'] = name
    return redirect(url_for('profile', status=2))

@bank.route('/profile/change_password', methods=['POST'])
def change_password():
    if 'user_id' not in session:
        return redirect(url_for('index'))
    password = request.form['password']
    repeat = request.form['repeat']
    if password == repeat:
        hash_ = hashing.hash_value(request.form['password'])
        query = "UPDATE users SET hash = ? WHERE id = ?"
        con = get_db()
        con.execute(query, [hash_, session['user_id']])
        con.commit()
        return redirect(url_for('profile', status=1))
    return redirect(url_for('profile', status=-1))

@bank.route('/profile/reset_token', methods=['GET'])
def reset_token():
    if 'user_id' not in session:
        return redirect(url_for('index'))
    token = str(uuid.uuid4())
    query = "UPDATE users SET token = ? WHERE id = ?"
    con = get_db()
    con.execute(query, [token, session['user_id']])
    con.commit()
    return redirect(url_for('profile'))

## Record of processing activities

@bank.route('/record')
def record():
    if 'user_id' not in session:
        return redirect(url_for('index'))    
    name = session.get('user_name')
    user_id = session.get('user_id')
    inputs = retrieve_inputs(user_id)
    outputs = retrieve_outputs(user_id)
    consents = retrieve_consent(user_id, inputs=inputs)
    deletions = retrieve_deletions(user_id, inputs=inputs)
    rectifications = retrieve_rectifications(user_id, inputs=inputs)
    accesses = retrieve_accesses(user_id, inputs=inputs, requests=False)
    accesses2 = retrieve_accesses(user_id, inputs=inputs)
    restrictions = retrieve_restrictions(user_id, inputs=inputs)
    legal_grounds = retrieve_legal_grounds(user_id, inputs=inputs)
    return render_template("record.html", **locals())

def set_user_id_from_token(get=False):
    args = request.args if get else request.form
    if 'token' not in args:
        return False
    query = "SELECT id FROM users WHERE token = ?"
    cur = get_db().execute(query, [args['token']])
    res = cur.fetchall()
    cur.close()
    if res:
        session['user_id'] = res[0][0]
        return True
    return False

@bank.route('/record/add_consent', methods=['POST'])
def add_consent():
    if 'user_id' not in session and 'token' not in request.form:
        return redirect(url_for('index'))
    if 'app' not in request.form or 'purpose' not in request.form \
       or 'ut' not in request.form:
        return redirect(url_for('record'))
    by_token = False
    if 'token' in request.form:
        by_token = True
        if not set_user_id_from_token():
            return jsonify(ok=False)
    log_ds_consent(
        session['user_id'], request.form['app'],
        request.form['purpose'], request.form['ut']
    )
    if by_token:
        return jsonify(ok=True)
    else:
        sleep(1)
        return redirect(url_for('record'))

@bank.route('/record/revoke_consent', methods=['GET'])
def revoke_consent():
    if 'user_id' not in session and 'token' not in request.args:
        return redirect(url_for('index'))
    if 'u' not in request.args or 'app' not in request.args \
       or 'purpose' not in request.args or 'ut' not in request.args:
        return redirect(url_for('record'))
    by_token = False
    if 'token' in request.args:
        by_token = True
        if not set_user_id_from_token(get=True):
            return jsonify(ok=False)
    log_ds_revoke(
        request.args.get('u'),
        request.args.get('app'),
        request.args.get('purpose'), request.args.get('ut')
    )
    if by_token:
        return jsonify(ok=True)
    else:
        return redirect(url_for('record'))

@bank.route('/record/object', methods=['GET'])
def object():
    if 'user_id' not in session and 'token' not in request.args:
        return redirect(url_for('index'))
    if 'u' not in request.args \
       or 'app' not in request.args or 'ground' not in request.args \
       or 'purpose' not in request.args or 'ut' not in request.args:
        return redirect(url_for('record'))
    by_token = False
    if 'token' in request.args:
        by_token = True
        if not set_user_id_from_token(get=True):
            return jsonify(ok=False)
    log_ds_object(
        request.args.get('u'),
        request.args.get('app'), request.args.get('ground'),
        request.args.get('purpose'), request.args.get('ut')
    )
    if by_token:
        return jsonify(ok=True)
    else:
        sleep(1)
        return redirect(url_for('record'))

def log_with_app_ut(log_f, get=False):
    args = request.args if get else request.form
    if 'user_id' not in session and 'token' not in args:
        return redirect(url_for('index'))
    if 'app' not in args  or 'ut' not in args:
        return redirect(url_for('record'))
    by_token = False
    if 'token' in args:
        by_token = True
        if not set_user_id_from_token():
            return jsonify(ok=False)
    log_f(
        session['user_id'], args.get('app'),
        args.get('ut')
    )
    if by_token:
        return jsonify(ok=True)
    else:
        sleep(1)
        return redirect(url_for('record'))

@bank.route('/record/rectify', methods=['POST'])
def rectify():
    if 'user_id' not in session and 'token' not in request.form:
        return redirect(url_for('index'))
    if 'app' not in request.form  or 'ut' not in request.form \
       or 'value' not in request.form:
        return redirect(url_for('record'))
    by_token = False
    if 'token' in request.form:
        by_token = True
        if not set_user_id_from_token():
            return jsonify(ok=False)
    log_ds_rectify(
        session['user_id'], request.form.get('app'),
        request.form.get('ut'), request.form.get('value')
    )
    if by_token:
        return jsonify(ok=True)
    else:
        sleep(1)
        return redirect(url_for('record'))

@bank.route('/record/erase', methods=['POST'])
def delete():
    return log_with_app_ut(log_ds_erase)

@bank.route('/record/access', methods=['POST'])
def access():
    return log_with_app_ut(log_ds_access)

@bank.route('/record/restrict', methods=['GET'])
def restrict():
    return log_with_app_ut(log_ds_restrict, get=True)

@bank.route('/record/repeal', methods=['GET'])
def repeal():
    return log_with_app_ut(log_ds_repeal, get=True)

@bank.route('/register_inputs', methods=['POST'])
def register_inputs():
    if not set_user_id_from_token():
        return jsonify(ok=False)
    n = min(256, int(request.form.get("n", 0)))
    return jsonify(ok=True, uts=[generate_ut() for _ in range(n)])

## Varia

@bank.route('/file_link/<filename>', methods=['GET'])
def file_link(filename):
    return send_file(full_filename(filename))

# Testing only!
@bank.route('/loginput', methods=['POST'])
def loginput():
    f = request.form['f']
    a = request.form['a']
    u = int(request.form['u'])
    return loginput_backdoor(f, a, u)

if __name__ == '__main__':
    bank.run(debug=True, use_reloader=False)
