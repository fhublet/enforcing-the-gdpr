import json
from copy import deepcopy
import operator
from flask import session
import matplotlib.pyplot as plt
import matplotlib
from time import time_ns
import os

from settings import LOG_FOLDER

matplotlib.use('agg')

def copy_setdict(d):
    return {k: set(v) for (k, v) in d.items()}

def union(d1, d2):
    return dict(list(d1.items()) + list(d2.items()))

class Dag:

    def __init__(self, copy=None, singleton=None, singletons=None, json=None):
        #print('Dag.__init__ 1', copy)
        if copy is not None:
            self.uts  = [set(s) for s in copy.uts]
            self.all_ = set(copy.all_)
            self.len_ = copy.len_
        elif json is not None:
            self.load_from_json(json)
            self.len_ = None
        elif singleton is not None:
            self.uts  = [{singleton}]
            self.all_ = {singleton}
            self.len_ = 1
        elif singletons is not None:
            self.uts  = [set(singletons)]
            self.all_ = set(singletons)
            self.len_ = len(set(singletons))
        else:
            self.uts  = []
            self.all_ = set()
            self.len_ = None

    def __eq__(self, other):
        return self.uts == other.uts

    def __len__(self):
        if self.len_ is None:
            l = sum([len(s) for s in self.uts])
            self.len_ = l
        #print('__len__', self, self.len_)
        return self.len_

    def is_empty(self):
        return self.uts == []

    def collect(self):
        return set(self.all_)
        
    def purge(self):
        all_s = set()
        for (i, s) in enumerate(self.uts):
            self.uts[i] -= all_s
            all_s |= s
        self.uts  = [s for s in self.uts if len(s)]
        self.len_ = None

    #def add_inplace(self, other):
        #for i in range(min(len(self.uts), len(other.uts))):
            #self.uts[i] |= other.uts[i]
        #for i in range(len(self.uts), len(other.uts)):
            #self.uts.append(set(other.uts[i]))
        #self.all_ |= other.all_
        
    def __add__(self, other):
        m = min(len(self.uts), len(other.uts))
        M = max(len(self.uts), len(other.uts))
        if m == len(self.uts):
            a, b = self, other
        else:
            a, b = other, self
        c = Dag()
        for i in range(m):
            c.uts.append(a.uts[i] | b.uts[i])
        for i in range(m, M):
            c.uts.append(set(b.uts[i]))
        c.all_ = a.all_ | b.all_
        c.purge()
        return c

    def __sub__(self, other):
        c = Dag(self)
        for i in range(len(c.uts)):
            c.uts[i] -= other.all_
        c.all_ -= other.all_
        c.purge()
        return c

    def __truediv__(self, other):
        c = Dag()
        c.uts = [set(s) for s in self.uts] + [set(s) for s in other.uts]
        c.all_ = self.all_ | other.all_
        c.purge()
        #print(f"{self}/{other}={c}")
        return c

    def __repr__(self):
        return repr(self.uts)

    def to_json(self):
        return json.dumps([list(s) for s in self.uts])

    def load_from_json(self, json_):
        json_ = json.loads(json_)
        self.uts  = [{tuple(t) for t in s} for s in json_]
        self.all_ = set(i for s in self.uts for i in s)
        #print(self.uts)

    def inputs_in_order(self):
        return self.uts


class Cell:

    def __init__(self, value=None, inputs=None, valuei=None, adopt=None, json=None):
        if value is None and json is not None:
            self.load(json)
        else:
            if type(value) is list:
                self.value = [Cell(e) for e in value]
            elif type(value) is dict:
                self.value = {k: Cell(v) for (k, v) in value.items()}
            elif type(value) is tuple:
                self.value = tuple(Cell(e) for e in value)
            elif not isinstance(value, Cell):
                self.value  = value
                self.inputs = Dag()
            if isinstance(value, Cell):
                self.value  = value.value
                self.inputs = Dag(copy=value.inputs)
            elif type(inputs) is list:
                self.inputs = Dag()
                for input_ in inputs:
                    self.inputs = self.inputs + input_
            elif inputs is not None:
                self.inputs = inputs
            else:
                self.inputs = Dag()
            self.valuei = valuei
            if isinstance(value, Cell):
                self.valuei = valuei or value.valuei
        if adopt:
            self.inputs = adopt / self.inputs

    def __iter__(self):
        if type(self.value) in [list, tuple, dict]:
            return (Cell(x, adopt=self.inputs) for x in self.value)
        else:
            return iter()

    def all_inputs(self):
        inputs = Dag(self.inputs)
        if type(self.value) in [list, tuple]:
            inputs2 = Dag()
            if self.value:
                for e in self.value:
                    inputs2 = inputs2 + e.all_inputs()
            return inputs / inputs2
        elif type(self.value) is dict:
            inputs2 = Dag()
            if self.value:
                for e in self.value.values():
                    inputs2 = inputs2 + e.all_inputs()
            return inputs / inputs2
        return inputs

    def all_inputs_set(self):
        inputs = self.inputs.collect()
        if type(self.value) in [list, tuple]:
            if self.value:
                for e in self.value:
                    inputs |= e.all_inputs_set()
            return inputs
        elif type(self.value) is dict:
            if self.value:
                for e in self.value.values():
                    inputs |= e.all_inputs_set()
            return inputs
        return inputs

    def inputs_in_order(self):
        return self.inputs.inputs_in_order()
    
    def dump(self):
        if type(self.value) is list:
            return {"type": "list",
                    "value": [e._dump() for e in self.value],
                    "valuei": self.valuei or "",
                    "inputs": self.inputs.to_json()}
        elif type(self.value) is dict:
            return {"type": "dict",
                    "value": {k: v._dump() for (k, v) in self.value.items()},
                    "valuei": self.valuei or "",
                    "inputs": self.inputs.to_json()}
        elif type(self.value) is tuple:
            return {"type": "tuple",
                    "value": tuple(e._dump() for e in self.value),
                    "valuei": self.valuei or "",
                    "inputs": self.inputs.to_json()}
        else:
            return {"type": "value",
                    "value": self.value,
                    "valuei": self.valuei or "",
                    "inputs": self.inputs.to_json()}

    def load(self, dic):
        if dic["type"] == "list":
            self.value = [Cell(json=e) for e in dic["value"]]
            self.valuei = dic["valuei"] or None
            self.inputs = Dag(json=dic["inputs"])
        elif dic["type"] == "dict": 
            self.value = {k: Cell(json=v) for (k, v) in dic["value"].items()}
            self.valuei = dic["valuei"] or None
            self.inputs = Dag(json=dic["inputs"])
        elif dic["type"] == "tuple":
            self.value = tuple(Cell(json=e) for e in dic["value"])
            self.valuei = dic["valuei"] or None
            self.inputs = Dag(json=dic["inputs"])
        else:
            self.value = dic["value"]
            self.valuei = dic["valuei"] or None
            self.inputs = Dag(json=dic["inputs"])

    def untaint(self):
        if type(self.value) is list:
            return [e.untaint() for e in self.value]
        elif type(self.value) is dict:
            return {k: v.untaint() for (k, v) in self.value.items()}
        elif type(self.value) is tuple:
            return tuple(e.untaint() for e in self.value)
        else:
            return self.value

    def untaint_value(self):
        untainted = self.untaint()
        if type(untainted) in [list, dict, tuple]:
            return None
        else:
            return untainted or ""

    def adopt(self, inputs):
        self.inputs = inputs / self.inputs

    def add_inputs(self, inputs):
        self.inputs = self.inputs / inputs
        
    def __repr__(self):
        return ("<databank.memory.Cell value={} inputs={} valuei={}>").format(
            self.value, self.inputs, self.valuei)

    def __bool__(self):
        return bool(self.untaint())

    def __getitem__(self, key):
        if type(self.value) in [list, tuple]:
            if (isinstance(key, Cell) and type(key.value) is int \
                and key.value >= 0 and key.value < len(self.value)) \
               or (type(key) is int and key >= 0 and key < len(self.value)):
                if isinstance(key, Cell):
                    cell = Cell(self.value[key.value], adopt=key.inputs)
                else:
                    cell = Cell(self.value[key])
                cell.inputs = self.inputs / cell.inputs
                return cell
        elif type(self.value) is dict:
            if (isinstance(key, Cell) and key.value in self.value) or key in self.value:
                if isinstance(key, Cell):
                    cell = Cell(self.value[key.value], adopt=key.inputs)
                else:
                    cell = Cell(self.value[key])
                cell.inputs = self.inputs / cell.inputs
                return cell
        if isinstance(key, Cell):
            return Cell(None, inputs=self.inputs, adopt=key.inputs)
        else:
            return Cell(None, inputs=self.inputs)

    def __setitem__(self, key, value):
        if type(self.value) in [list, dict] and \
           (isinstance(key, Cell) and type(key.value) not in [list, dict, tuple] or \
            type(key) not in [list, dict, tuple]):
            if isinstance(key, Cell):
                self.value[key.value] = value
            else:
                self.value[key] = value

    def __delitem__(self, key):
        if type(self.value) in [list, dict] and \
           (isinstance(key, Cell) and type(key.value) not in [list, dict, tuple] or \
            type(key) not in [list, dict, tuple]):
            if isinstance(key, Cell):
                del self.value[key.value]
            else:
                del self.value[key]

    def in_(self, other):
        return Cell(self.untaint() in other.untaint(), self.inputs + other.inputs)
            
## Additional functions

def _len(cell):
    if type(cell.value) in [list, tuple, dict]:
        return Cell(len(cell.value), inputs=cell.inputs)
    else:
        return Cell(None, inputs=cell.inputs)
    
def _keys(cell):
    if type(cell.value) is dict:
        return Cell(list(cell.value.keys()), inputs=cell.inputs)
    else:
        return Cell(None, inputs=cell.inputs)

def _int(cell):
    try:
        assert(type(cell.value) not in [list, tuple, dict])
        return Cell(int(cell.value), inputs=cell.inputs)
    except:
        return Cell(None, inputs=cell.inputs)

def _float(cell):
    try:
        assert(type(cell.value) not in [list, tuple, dict])
        return Cell(float(cell.value), inputs=cell.inputs)
    except:
        return Cell(None, inputs=cell.inputs)
    
def _str(cell):
    try:
        assert(type(cell.value) not in [list, tuple, dict])
        return Cell(str(cell.value), inputs=cell.inputs)
    except:
        return Cell(None, inputs=cell.inputs)

def _print(cell):
    print(repr(cell))

def _sorted_by0(cell):
    try:
        assert(type(cell.value) is list)
        all_0_inputs = Dag()
        for v in cell.value:
            all_0_inputs += v[0].inputs
        return Cell(sorted(cell.value, key=lambda x: x[0].value, reverse=True), inputs=all_0_inputs)
    except:
        return Cell(None, inputs=cell.inputs)

def _append(l, e):
    try:
        assert(type(l.value) is list)
        l.value.append(e)
    except:
        return l

def non(cell):
    return Cell(not cell.value, inputs=cell.inputs)

## Add overloaded operators to Cell

BINOPS = ["__add__", "__sub__", "__mul__", "__truediv__", "__mod__", "__divmod__", "__pow__", "__lshift__",
          "__rshift__", "__and__", "__xor__", "__or__", "__radd__", "__rsub__", "__rmul__", "__rdiv__",
          "__rmod__", "__rdivmod__", "__rpow__", "__rlshift__", "__rrshift__", "__rand__", "__rxor__",
          "__ror__", "__lt__", "__le__", "__eq__", "__ne__", "__gt__", "__ge__", "__contains__"]

for o in BINOPS:
    def wrapper(self, other, o=o):
        if isinstance(other, Cell):
            try:
                return Cell(operator.__dict__[o](self.value, other.value), inputs=(self.inputs + other.inputs))
            except:
                return Cell(None, inputs=(self.inputs + other.inputs))
        else:
            try:
                return Cell(operator.__dict__[o](self.value, other), inputs=self.inputs)
            except:
                return Cell(None, inputs=(self.inputs + other.inputs))
    setattr(Cell, o, wrapper)

UNOPS = ["__neg__", "__pos__", "__invert__"]
        
for o in UNOPS:
    def wrapper(self, o=o):
        try:
            return Cell(operator.__dict__[o](self.value), inputs=self.inputs)
        except:
            return Cell(None, inputs=self.inputs)
    setattr(Cell, o, wrapper)

class Stack:

    def __init__(self):
        self.stack = [Dag()]

    def pop(self):
        self.stack.pop()

    def push(self):
        self.stack.append(Dag())

    def add(self, inputs, bot=False):
        self.stack[0 if bot else -1] = self.stack[0] / inputs

    def all(self):
        inputs = Dag()
        for inputs_ in self.stack:
            inputs = inputs / inputs_
        return inputs

    def __repr__(self):
        return ("<databank.memory.Stack stack={}>").format(self.stack)
    
class Counter:

    def __init__(self):
        self.c = 0

    def next(self, inc=True):
        c = self.c
        if inc:
            self.c += 1
        return c
                
def load(value):
    if value["type"] == "list":
        return Cell([load(e) for e in value["value"]], inputs=Dag(json=value["inputs"]), valuei=value["valuei"])
    elif value["type"] == "dict":
        return Cell({k: load(v) for (k, v) in value["value"].items()}, inputs=Dag(json=value["inputs"]), valuei=value["valuei"])
    elif value["type"] == "tuple":
        return Cell(tuple(load(e) for e in value["value"]), inputs=Dag(json=value["inputs"]), valuei=value["valuei"])
    else:
        return Cell(value["value"], inputs=Dag(json=value["inputs"]), valuei=value["valuei"])
