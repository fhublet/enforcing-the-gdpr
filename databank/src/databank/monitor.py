from abc import ABC, abstractmethod
from flask import session
import re
import time
import uuid
import requests
import json
import io

from settings import *
from catalog import catalog_by_name

from lark import Lark

from databank.memory import Cell, Dag
from databank.timing import log_time, Timer

# Printing function

def _print_monitor(s, end='\n'):
    if is_set('verbose_monitor'):
        print(s, end=end)

# (v, u) -> "v:u"

def _str_of_label(w):
    return str(w[0]) + ":" + str(w[1])

# "v:u" -> (v, u)

def _label_of_str(s):
    s = s.split(":")
    return s[0], int(s[1])
    
# Predicates

class Predicate(ABC):

    def __str__(self):
        return name + "(" + ', '.join('\"' + arg + '\"') + ")"

    @property
    def json(self):
        return {"name": self.name(), "occurrences": [self.arguments()]}

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def arguments(self):
        pass

class Collect(Predicate):

    def __init__(self, u, a, ut, sp):
        self.u = u
        self.a = a
        self.ut = ut
        self.sp = sp

    @property
    def name(self):
        return "Collect"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut, self.sp]

class Use(Predicate):

    def __init__(self, a, p, ut, q):
        self.a = a
        self.p = p
        self.ut = ut
        self.q = q

    @property
    def name(self):
        return "Use"

    @property
    def arguments(self):
        return [self.a, self.p, self.ut, self.q]

class ShareWith(Predicate):

    def __init__(self, a, u, ut, q):
        self.a = a
        self.u = u
        self.ut = ut
        self.q = q

    @property
    def name(self):
        return "ShareWith"

    @property
    def arguments(self):
        return [self.a, self.u, self.ut, self.q]

class NotifyProc(Predicate):

    def __init__(self, u, ut):
        self.u = u
        self.ut = ut

    @property
    def name(self):
        return "NotifyProc"

    @property
    def arguments(self):
        return [self.u, self.ut]

class DSConsent(Predicate):

    def __init__(self, u, a, p, ut):
        self.u = u
        self.a = a
        self.p = p
        self.ut = ut

    @property
    def name(self):
        return "DSConsent"

    @property
    def arguments(self):
        return [self.u, self.a, self.p, self.ut]

class DSRevoke(Predicate):

    def __init__(self, u, a, p, ut):
        self.u = u
        self.a = a
        self.p = p
        self.ut = ut

    @property
    def name(self):
        return "DSRevoke"

    @property
    def arguments(self):
        return [self.u, self.a, self.p, self.ut]

class DSErase(Predicate):

    def __init__(self, u, a, ut):
        self.u = u
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "DSErase"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut]

class DSRectify(Predicate):

    def __init__(self, u, a, ut, v):
        self.u = u
        self.a = a
        self.ut = ut
        self.v = v

    @property
    def name(self):
        return "DSRectify"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut, self.v]

class DSAccess(Predicate):

    def __init__(self, u, a, ut):
        self.u = u
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "DSAccess"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut]

class Erase(Predicate):

    def __init__(self, a, ut):
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "Erase"

    @property
    def arguments(self):
        return [self.a, self.ut]

class Rectify(Predicate):

    def __init__(self, a, ut, v):
        self.a = a
        self.ut = ut
        self.v = v

    @property
    def name(self):
        return "Rectify"

    @property
    def arguments(self):
        return [self.a, self.ut, self.v]

class Access(Predicate):

    def __init__(self, u, a, ut):
        self.u = u
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "Access"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut]

class DSRestrict(Predicate):

    def __init__(self, u, a, ut):
        self.u = u
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "DSRestrict"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut]

class DSRepeal(Predicate):

    def __init__(self, u, a, ut):
        self.u = u
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "DSRepeal"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut]

class DSObject(Predicate):

    def __init__(self, u, a, ut):
        self.u = u
        self.a = a
        self.ut = ut

    @property
    def name(self):
        return "DSObject"

    @property
    def arguments(self):
        return [self.u, self.a, self.ut]

class LegalGround(Predicate):

    def __init__(self, a, g, sp, ut):
        self.a = a
        self.g = g
        self.sp = sp
        self.ut = ut

    @property
    def name(self):
        return "LegalGround"

    @property
    def arguments(self):
        return [self.a, self.g, self.sp, self.ut]



class Predicates:

    def __init__(self, preds):
        self.occurrences_by_name = {}
        for pred in preds:
            if not pred.name in self.occurrences_by_name:
                self.occurrences_by_name[pred.name] = [pred.arguments]
            else:
                self.occurrences_by_name[pred.name].append(pred.arguments)

    @property
    def json(self):
        predicates_json = []
        for (name, occurrences) in self.occurrences_by_name.items():
            predicates_json.append({"name": name, "occurrences": occurrences})
        return predicates_json
                
class Monitor:

    def __init__(self, url):
        self.url = url
        self.s = requests.Session()
        self.parser = self.get_parser()
        
    def tp(self):
        return int(time.time())

    def stop(self):
        self.s.close()

    def get_parser(self):
        with open("databank/events.lark") as f:
            events_lark = f.read()
        return Lark(events_lark)

    ROK = re.compile(r"^\[Enforcer\] OK.")
    RSUPPRESS = re.compile(r"^\[Enforcer\] Suppress")
    RCAUSE = re.compile(r"^\[Enforcer\] Cause: ([^\[]*)$")

    LOG = "log-events"
    QUERY = "db-query"
        
    def log(self, preds, check=False, multiple=False):
        if is_set("semipassive"):
            return {}
        if multiple:
            dic_input = [{"predicates": Predicates(p).json, "check": check} for p in preds]
        else:
            dic_input = [{"predicates": Predicates(preds).json, "check": check}]
        str_input = json.dumps(dic_input)
        files = {"events": io.StringIO(str_input)}
        _print_monitor(f"→ {str_input}")
        #with Timer('../monitor_files/time.json', 'monitor'):
        resp = self.s.post(self.url + "/" + self.LOG, files=files)
        json_output = resp.json()
        log_time('../monitor_files/time.json', 'monitor', resp.elapsed.total_seconds())
        _print_monitor(f"← {json_output} [elapsed={resp.elapsed.total_seconds()}]")
        reactions = json_output.get("reactions", [])
        if not check:
            for reaction in reactions:
                to_cause = reaction.get("cause", [])
                if to_cause:
                    self.handle_events(self.parse(to_cause))
        if multiple:
            return [("suppress" not in reaction) \
                    or (not reaction["suppress"]) for reaction in reactions]
        else:
            return ("suppress" not in reactions[0]) \
                or (not reactions[0]["suppress"])

    def query(self, query):
        resp = self.s.post(self.url + "/" + self.QUERY, data={"query": query})
        return resp.json()

    def parse(self, events):
        parsed_events = []
        for event in events:
            name, occurrences = event["name"], event["occurrences"]
            if event["name"] == "Erase":
                for args in occurrences:
                    parsed_events.append(Erase(*args))
            elif event["name"] == "Rectify":
                for args in occurrences:
                    parsed_events.append(Rectify(*args))
            elif event["name"] == "Access":
                for args in occurrences:
                    parsed_events.append(Access(*args))
            elif event["name"] == "NotifyProc":
                for args in occurrences:
                    parsed_events.append(NotifyProc(*args))
        return parsed_events
            
    def handle_events(self, events):
        for event in events:
            if isinstance(event, Erase):
                app = catalog_by_name[event.a]
                #print('cause_deletion', event.ut)
                app.cause_deletion(event.ut)
                #print('did cause_deletion')
            if isinstance(event, Rectify):
                app = catalog_by_name[event.a]
                app.cause_rectification(event.ut, event.v)
            if isinstance(event, Access):
                app = catalog_by_name[event.a]
                app.cause_access(int(event.u), event.ut)
            if isinstance(event, NotifyProc):
                # notify proc (simulated)
                pass

# Load monitors

monitor = Monitor(MONITOR_URL)

# Handle SIGINT

def save_all(signum, sigframe):
    monitor.stop()
    os._exit(0)

# Monitor control functions (used by applications)

class Stop(Exception):
    pass

def generate_ut():
    return str(uuid.uuid4())

def logreturn(app, u, p, d, t, check=False, share=False):
    out_u = str(u.value)
    out_i = str(uuid.uuid4())
    multiple = type(d) in [tuple, list]
    if not multiple:
        inputs_in_order = [d]
    else:
        inputs_in_order = d
    if type(p) not in [tuple, list]:
        purposes = [p] * len(inputs_in_order)
    else:
        purposes = p
    if check:
        inputs_in_order = list(map(lambda x: x.inputs_in_order(), inputs_in_order))
    else:
        inputs_in_order = list(map(lambda x: [x.collect()], inputs_in_order))
    res = Cell([Cell(True) for i in inputs_in_order])
    m = max([0] + [len(i) for i in inputs_in_order])
    for i in range(m):
        indices = [j for j in range(len(inputs_in_order)) if len(inputs_in_order[j]) > i and res.value[j].value]
        uts_all = [inputs_in_order[j][i] for j in indices]
        ps_all = [purposes[j] for j in indices]
        qs_all = [{ut[0]: str(uuid.uuid4()) for ut in uts} for uts in uts_all]
        preds_all = [[Use(app.app_name, ps_all[i], ut[0], qs_all[i][ut[0]]) for ut in uts] \
                     + ([ShareWith(app.app_name, out_u, ut[0], qs_all[i][ut[0]]) for ut in uts]
                        if share else [])
                     for (i, uts) in enumerate(uts_all)]
        bs = monitor.log(preds_all, check=check, multiple=True)
        for (j, b) in zip(indices, bs):
            if not b:
                res[j] = Cell(False, inputs=res[j].inputs)
            else:
                res[j].inputs = res[j].inputs / Dag(singletons=inputs_in_order[j][i])
    if multiple:
        return res
    else:
        return res[0]

def loginput(app, fun, a, u, ts, v, ut=None, owners="", sp=False):
    f = app.app_name + "/" + fun
    ut = ut or str(uuid.uuid4())
    if not owners:
        owners = [str(u)]
    else:
        owners = [o.strip() for o in owners.split(",")]
    monitor.log([Collect(o, app.app_name, ut, str(sp)) for o in owners])
    return Cell(v, valuei=(ut, u), inputs=Dag(singleton=(ut, u)))

def loginput_backdoor(f, a, u):
    ut = str(uuid.uuid4())
    monitor.log([Collect(str(u), a, ut, "False")])
    return ut

def loglegal_ground(app, g, p, sp, v):
    monitor.log([LegalGround(app.app_name, g, p, sp, ut) in v.all_inputs()])

# Record of processing activities (used by Databank)

def where_dates(start_date, end_date):
    if start_date is not None and end_date is not None:
        # BETWEEN is inclusive
        return f"time_stamp BETWEEN '{start_date}' AND '{end_date}'"
    elif start_date is not None:
        return f"time_stamp >= '{start_date}'"
    elif end_date is not None:
        return f"time_stamp <= '{end_date}'"
    else:
        return ""

def retrieve_inputs(u, start_date=None, end_date=None):
    query = f"""SELECT Collect.time_stamp, Collect.x2, Collect.x3, Collect.x4, Use.x4 FROM Collect 
LEFT JOIN Use ON Use.x3 = Collect.x3 
WHERE Collect.x1 = '{u}'"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += query.format(" AND " + where_d)
    query += " ORDER BY Collect.time_stamp DESC"
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = {}
    for row in resp["response"]:
        ut = row[2]
        app = row[1]
        if ut not in response:
            response[ut] = {"timestamp": row[0],
                            "UT":        ut,
                            "special":   True if row[3] == "True" else False,
                            "outputs":   [row[4]] if row[4] else [],
                            "consents":  [],
                            "accesses":  [],
                            "access_requests": [],
                            "restricted": False,
                            "app":       app,
                            "legal_grounds": [],
                            "rectifications": []}
        else:
            response[ut]["outputs"].append(row[4])
    resp["response"] = list(response.values())
    return resp
    
def retrieve_outputs(u, start_date=None, end_date=None):
    query = f"""SELECT Use.time_stamp, Use.x1, Use.x2, Use.x3, Use.x4, ShareWith.time_stamp FROM Use
JOIN Collect ON Collect.x3 = Use.x3
LEFT JOIN ShareWith ON Use.x4 = ShareWith.x4
WHERE Collect.x1 = '{u}'"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    query += " ORDER BY Use.time_stamp DESC"
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = {}
    for row in resp["response"]:
        id_ = row[4]
        if id_ not in response:
            response[id_] = {"timestamp": row[0],
                             "app":       row[1],
                             "purpose":   row[2],
                             "id":        id_,
                             "inputs":    [row[3]],
                             "is_sharing": bool(row[5])}
        else:
            response[id_]["inputs"].append(row[3])
    resp["response"] = list(response.values())
    return resp

def retrieve_consent(u, inputs=None, start_date=None, end_date=None):
    query = f"""SELECT DSConsent.time_stamp, DSConsent.x2, DSConsent.x3, DSConsent.x4 FROM DSConsent
LEFT JOIN DSRevoke ON DSConsent.x1 = DSRevoke.x1 AND DSConsent.x2 = DSRevoke.x2
                      AND DSConsent.x3 = DSRevoke.x3 AND DSConsent.x4 = DSRevoke.x4
                      AND DSConsent.time_stamp <= DSRevoke.time_stamp
WHERE DSConsent.x1 = '{u}' AND DSRevoke.x1 IS NULL"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = []
    inputs = {row["UT"]: row for row in inputs["response"]} \
        if "response" in inputs else {}
    c = 0
    for row in resp["response"]:
        ut = row[3]
        response.append({"timestamp": row[0],
                         "app":       row[1],
                         "purpose":   row[2],
                         "UT":        ut})
        if ut in inputs:
            inputs[ut]["consents"].append(c)
        c += 1
    resp["response"] = response
    return resp

def retrieve_restrictions(u, inputs=None, start_date=None, end_date=None):
    query = f"""SELECT DSRestrict.time_stamp, DSRestrict.x2, DSRestrict.x3 FROM DSRestrict
LEFT JOIN DSRepeal ON DSRestrict.x1 = DSRepeal.x1 AND DSRestrict.x2 = DSRepeal.x2
                  AND DSRestrict.x3 = DSRepeal.x3 
                  AND DSRestrict.time_stamp <= DSRepeal.time_stamp
WHERE DSRestrict.x1 = '{u}' AND DSRepeal.x1 IS NULL"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = []
    inputs = {row["UT"]: row for row in inputs["response"]} \
        if "response" in inputs else {}
    c = 0
    for row in resp["response"]:
        ut = row[2]
        response.append({"timestamp": row[0],
                         "app":       row[1],
                         "UT":        ut})
        if ut in inputs:
            inputs[ut]["restricted"] = True
        c += 1
    resp["response"] = response
    return resp

def retrieve_deletions(u, inputs=None, start_date=None, end_date=None):
    query = f"""SELECT DSErase.time_stamp, Erase.time_stamp, DSErase.x2, DSErase.x3 FROM DSErase
LEFT JOIN Erase ON DSErase.x3 = Erase.x2
WHERE DSErase.x1 = '{u}'"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = []
    inputs = {row["UT"]: row for row in inputs["response"]} \
        if "response" in inputs else {}
    c = 0
    for row in resp["response"]:
        ut = row[3]
        response.append({"timestamp1": row[0],
                         "timestamp2": row[1],
                         "app":        row[2],
                         "UT":         ut})
        if ut in inputs:
            inputs[ut]["deletion"] = c
            inputs[ut]["deleted"]  = row[1] is not None
        c += 1
    resp["response"] = response
    return resp

def retrieve_rectifications(u, inputs=None, start_date=None, end_date=None):
    query = f"""SELECT d1.time_stamp, Rectify.time_stamp, d1.x2, d1.x3 FROM DSRectify AS d1
LEFT JOIN Rectify ON d1.x3 = Rectify.x2 
LEFT JOIN DSRectify AS d2 ON d1.x3 = d2.x3 AND d2.time_stamp > d1.time_stamp AND d2.time_stamp <= Rectify.time_stamp
WHERE d1.x1 = '{u}' AND Rectify.time_stamp >= d1.time_stamp AND d2.x1 IS NULL"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = []
    inputs = {row["UT"]: row for row in inputs["response"]} \
        if "response" in inputs else {}
    c = 0
    for row in resp["response"]:
        ut = row[3]
        response.append({"timestamp1": row[0],
                         "timestamp2": row[1],
                         "app":        row[2],
                         "UT":         ut})
        if ut in inputs:
            inputs[ut]["rectifications"].append(c)
        c += 1
    resp["response"] = response
    return resp

def retrieve_accesses(u, inputs=None, start_date=None, end_date=None, requests=True):
    if requests:
        query = f"""SELECT time_stamp, x2, x3 FROM DSAccess WHERE x1 = '{u}'"""
    else:
        query = f"""SELECT time_stamp, x2, x3 FROM Access WHERE x1 = '{u}'"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = []
    inputs = {row["UT"]: row for row in inputs["response"]} \
        if "response" in inputs else {}
    c = 0
    for row in resp["response"]:
        ut = row[2]
        response.append({"timestamp1": row[0],
                         "app":        row[1],
                         "UT":         ut,
                         "link":       f"{catalog_by_name[row[1]].id_}_access/{ut}"})
        if ut in inputs:
            if requests:
                inputs[ut]["access_requests"].append(c)
            else:
                inputs[ut]["accesses"].append(c)
        c += 1
    resp["response"] = response
    return resp

def retrieve_legal_grounds(u, inputs=None, start_date=None, end_date=None):
    query = f"""SELECT LegalGround.time_stamp, DSObject.time_stamp, LegalGround.x1, LegalGround.x2, LegalGround.x3, LegalGround.x4 FROM LegalGround
LEFT JOIN DSObject ON LegalGround.x1 = DSObject.x2 
                   AND LegalGround.x4 = DSObject.x3
                   AND LegalGround.time_stamp <= DSObject.time_stamp
JOIN Collect ON Collect.x3 = LegalGround.x4
WHERE Collect.x1 = '{u}'"""
    where_d = where_dates(start_date, end_date)
    if where_d:
        query += " AND " + where_d
    resp = monitor.query(query)
    if "error" in resp:
        return resp
    response = []
    inputs = {row["UT"]: row for row in inputs["response"]} \
        if "response" in inputs else {}
    c = 0
    for row in resp["response"]:
        ut = row[5]
        response.append({"timestamp1": row[0],
                         "timestamp2": row[1],
                         "app":        row[2],
                         "special":    True if row[3] == "True" else False,
                         "ground":     row[4],
                         "UT":         ut})
        if ut in inputs:
            inputs[ut]["legal_grounds"].append(c)
        c += 1
    resp["response"] = response
    return resp
        
def log_ds_consent(u, a, p, ut):
    monitor.log([DSConsent(str(u), a, p, ut)])

def log_ds_revoke(u, a, p, ut):
    monitor.log([DSRevoke(str(u), a, p, ut)])

def log_ds_erase(u, a, ut):
    monitor.log([DSErase(str(u), a, ut)])

def log_ds_rectify(u, a, ut, v):
    monitor.log([DSRectify(str(u), a, ut, v)])
    
def log_ds_access(u, a, ut):
    monitor.log([DSAccess(str(u), a, ut)])

def log_ds_restrict(u, a, ut):
    monitor.log([DSRestrict(str(u), a, ut)])

def log_ds_repeal(u, a, ut):
    monitor.log([DSRepeal(str(u), a, ut)])

def log_ds_object(u, a, ut):
    monitor.log([DSObject(str(u), a, ut)])
