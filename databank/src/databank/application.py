from flask import Flask, request, session, render_template, url_for, redirect, make_response
from werkzeug.utils import secure_filename
import sqlite3
import datetime
import socket
import pickle
import time
from threading import Thread, get_ident
from copy import deepcopy
from Crypto.Cipher import AES
import os.path
import random
import string
import re
import json

from databank.monitor import logreturn, loginput, loglegal_ground
from databank.memory import Cell, load, Dag, Stack
from databank.calls import Server, do_call, join_route, pad
#from databank.timing import timing
import databank.sql as sql
from databank.cfg import InputDependent
from settings import get_db, file_save, file_remove, file_exists, FUNDAMENTAL, is_set

class Cur:
    def __init__(self, db, select_only=False):
        self.db = db
        self.select_only = select_only
    def __enter__(self):
        self.cur = self.db.cursor()
        return self.cur
    def __exit__(self, type, value, traceback):
        self.cur.close()
        if not self.select_only:
            self.db.commit()

class Redirect:
    def __init__(self, purp, rule):
        self.purp = purp
        self.rule = rule

class Template:
    def __init__(self, template, args):
        self.template = template
        self.args     = args
            
## Application wrapper
            
class Application:

    def __init__(self, flask, server, id_, app_name, sp_IP, database, key, databank_side=True):
        self.flask = flask
        self.id_ = id_
        self.app_name = app_name
        if databank_side:
            self.server = server
        else:
            self.server = Server(databank_side=False)            
        self.server.register_app(id_, sp_IP, key)
        self.database = database
        self.aes = AES.new(bytes(key, 'utf8'), AES.MODE_EAX)
        self.databank_side = databank_side
        self.db = sqlite3.connect(database, check_same_thread=False)
        self.db.cursor().execute("PRAGMA synchronous = OFF")
        self.sql_asts = {}
        self.tables = self.get_tables()
        self.update_tables()
        self.handlers = {}
        self.register_access_functions()

    def __del__(self):
        self.db.close()

    def get_tables(self):
        with self.db_cur(select_only=True) as cur:
            tables_q = "SELECT name FROM sqlite_master WHERE type='table'"
            tables = [row[0] for row in cur.execute(tables_q).fetchall()
                      if row[0].split("_")[0] == str(self.id_) and not row[0].endswith("_")]
        return tables

    def update_tables(self):
        with self.db_cur(select_only=True) as cur:
            # retrieve current tables
            tables_q = "SELECT name FROM sqlite_master WHERE type='table'"
            tables = [row[0] for row in cur.execute(tables_q).fetchall()]
            indices_q = "SELECT name FROM sqlite_master WHERE type='index'"
            indices = [row[0] for row in cur.execute(indices_q).fetchall()]
            triggers_q = "SELECT name FROM sqlite_master WHERE type='trigger'"
            triggers = [row[0] for row in cur.execute(triggers_q).fetchall()]
            # add _access_, _access_data_, _access_tables_
            if f"{self.id_}_access_" not in tables:
                access_create_q = f'''CREATE TABLE "{self.id_}_access_" (
	"id"	   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"user_id"  INTEGER NOT NULL,
	"datetime" TEXT NOT NULL,
	"ut"	   TEXT NOT NULL)'''
                cur = cur.execute(access_create_q)
            if f"{self.id_}_access_data_" not in tables:
                access_data_create_q = f'''CREATE TABLE "{self.id_}_access_data_" (
	"id"	   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"table_id" INTEGER NOT NULL,
	"row_id"   INTEGER NOT NULL,
	"field"	   TEXT NOT NULL,
	"data"	   BLOB NOT NULL)'''
                cur = cur.execute(access_data_create_q)
            if f"{self.id_}_access_tables_" not in tables:
                access_tables_create_q = f'''CREATE TABLE "{self.id_}_access_tables_" (
	"id"         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"access_id"  INTEGER NOT NULL,
	"table_name" TEXT NOT NULL)'''
                cur = cur.execute(access_tables_create_q)
            # add _inputs_, _inputs2_, _new_, indices, & triggers
            for table in self.tables + [f"{self.id_}_access_", f"{self.id_}_access_data_", f"{self.id_}_access_tables_"]:
                if table + "_inputs2_" not in tables:
                    inputs2_create_q = f'''CREATE TABLE "{table}_inputs2_" (
	"id"	 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"entry"	 INTEGER,
	"field"	 TEXT,
	"inputs" TEXT NOT NULL,
	"owner"	 TEXT NOT NULL)'''
                    cur = cur.execute(inputs2_create_q)
                if table + "_inputs2_index" not in indices:
                    inputs2_index_q = f'''CREATE INDEX "{table}_inputs2_index" ON "{table}_inputs2_" (
	"entry"	 ASC,
	"field"	 ASC,
	"inputs" ASC)'''
                    cur = cur.execute(inputs2_index_q)
                if table + "_inputs_" not in tables:
                    inputs_create_q = f'''CREATE TABLE "{table}_inputs_" (
	"id"	 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"entry"	 INTEGER,
	"field"	 TEXT,
	"inputs" BLOB NOT NULL,
	"valuei" BLOB)'''
                    cur = cur.execute(inputs_create_q)
                if table + "_inputs2_index" not in indices:
                    inputs_index_q = f'''CREATE INDEX "{table}_inputs_index" ON "{table}_inputs_" (
	"entry"	ASC,
	"field"	ASC)'''
                    cur = cur.execute(inputs_index_q)
                if table + "_new_" not in tables:
                    new_create_q = f'CREATE TABLE "{table}_new_" ( `id` INTEGER NOT NULL )'
                    cur = cur.execute(new_create_q)
                if table + "_new" not in triggers:
                    new_new_q = f'''CREATE TRIGGER "{table}_new" AFTER INSERT ON "{table}" BEGIN INSERT INTO "{table}_new_" (id) VALUES (new.id); END'''
                    cur = cur.execute(new_new_q)
                


    # Function decorators

    def callback(self, rule):
        def inner(f):
            def wrapper(*args, **kw):
                x = f(*args, **kw)
                return x.untaint()
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            self.server.register_callback(self.id_, rule, self, wrapper)
            return wrapper
        return inner
    
    def route(self, rule, **kwargs):
        def inner(f):
            f_name = str(self.id_) + "__" + f.__name__
            def wrapper(*args, **kw):
                i, ret, stack, trace = f(*args, **kw)
                ret.adopt(stack)
                page = None
                if isinstance(ret.value, Redirect):
                    verdict = logreturn(self, self.me(), ret.value.purp, ret.inputs, trace)
                    if not verdict.value:
                        page = render_template("violation.html")
                    else:
                        page = redirect(join_route(str(self.id_), ret.value.rule))
                elif isinstance(ret.value, Template):
                    final_args = {}
                    if ret.value.args:
                        arg_names, args = zip(*ret.value.args.items())
                        purposes, values, uts = zip(*args)
                    else:
                        arg_names, args = [], []
                        purposes, values, uts = [], [], []
                    verdicts = logreturn(self, self.me(), purposes, uts, trace)
                    for arg_name, verdict, val in zip(arg_names, verdicts, values):
                        if not verdict.value:
                            final_args[arg_name] = "<strong>Content suppressed to avoid a privacy violation.</strong>"
                        else:
                            final_args[arg_name] = val
                    page = render_template(os.path.join("collection", self.app_name, ret.value.template), **final_args)
                else:
                    if type(ret.value) is tuple and len(ret.value) == 2:
                        purp = ret.value[0].untaint_value()
                        val  = ret.value[1].untaint_value()
                        uts  = ret.inputs / ret.value.inputs / ret.value[0].inputs / ret.value[1].inputs
                        verdict = logreturn(self, self.me(), purp, uts, trace)
                        if not verdict.value:
                            page = render_template("violation.html", **locals())
                        else:
                            page = val
                resp = make_response(page)
                resp.headers['TTC-Application-ID']   = self.id_
                resp.headers['TTC-Application-Name'] = self.app_name
                return resp
            wrapper.__name__ = f_name
            return self.flask.route(join_route(self.id_, rule), **kwargs)(wrapper)
        return inner

    def handle_table_deletion(self, table):
        def inner(f, table=table):
            self.handlers[('delete', 'table', table)] = f
            return f
        return inner

    def handle_row_deletion(self, table):
        def inner(f, table=table):
            self.handlers[('delete', 'row', table)] = f
            return f
        return inner

    def handle_field_deletion(self, table, field):
        def inner(f, table=table, field=field):
            if type(field) is list:
                fields = field
            else:
                fields = [field]
            for field in fields:
                self.handlers[('delete', 'field', table, field)] = f
            return f
        return inner

    def handle_table_rectification(self, table):
        def inner(f, table=table):
            self.handlers[('rectify', 'table', table)] = f
            return f
        return inner
    
    def handle_row_rectification(self, table):
        def inner(f, table=table):
            self.handlers[('rectify', 'row', table)] = f
            return f
        return inner

    def handle_field_rectification(self, table, field):
        def inner(f, table=table, field=field):
            if type(field) is list:
                fields = field
            else:
                fields = [field]
            for field in fields:
                self.handlers[('rectify', 'field', table, field)] = f
            return f
        return inner

    # Distant call

    def send(self, f, i, t, a, p, arg):
        verdict = logreturn(self, a, p.value, arg.all_inputs(), t, share=True)
        if verdict.value:
            print(f"Sent {arg.value} to {a}!")

    # Check with monitor

    def check(self, f, i, t, p, arg, a=None):
        if isinstance(p, Cell) and not p.inputs.is_empty():
            raise InputDependent("purpose")
        a = a or self.me()
        #u = time.time()
        r = logreturn(self, a, p.value, arg.all_inputs(), t, check=True)
        return r

    def check_all(self, f, i, t, p, arg, a=None):
        if isinstance(p, Cell) and not p.inputs.is_empty():
            raise InputDependent("purpose")
        a = a or self.me()
        #u = time.time()
        rs = []
        if type(arg.value) is list:
            rs = logreturn(self, a, p.value, [c.all_inputs() for c in arg.value], t, check=True)
        return Cell(rs)

    # Claim legal gorunds

    def claim_legal_ground(self, g, p, sp, arg):
        if isinstance(g, Cell) and not g.inputs.is_empty():
            raise InputDependent("legal ground")
        if isinstance(p, Cell) and not p.inputs.is_empty():
            raise InputDependent("purpose")
        if isinstance(sp, Cell) and not sp.inputs.is_empty():
            raise InputDependent("special-data flag")
        loglegal_ground(self, g.value, p.value, str(sp.value), arg)

    # Exposed user input methods

    def method(self):
        return Cell(request.method)

    def _generate_filename(self, file_):
        return ''.join(random.choices(string.ascii_letters, k=16)) + "_" + secure_filename(file_.filename)

    def post(self, func_name, key):
        if isinstance(key, Cell) and not key.inputs.is_empty():
            raise InputDependent("POST key")
        if key.value in request.files:
            file_ = request.files.get(key.value)
            value = self._generate_filename(file_)
            file_save(value, file_)
        else:
            value = request.form.get(key.value, None)
        return self.register(func_name, key.value, value)

    def getlist(self, func_name, key):
        if isinstance(key, Cell) and not key.inputs.is_empty(): # TODO: do this statically
            raise InputDependent("POST key")
        values = request.form.getlist(key.value, None)
        for value in range(len(values)):
            values[value] = self.register(func_name, key.value, values[value])
        return Cell(values)

    def get(self, func_name, key):
        if isinstance(key, Cell) and not key.inputs.is_empty():
            raise InputDependent("GET key")
        value = request.args.get(key.value, None)
        return self.register(func_name, key.value, value)

    def link(self, value):
        if type(value.value) is str and file_exists(value.value):
            return Cell(url_for('file_link', filename=value.value), inputs=value.inputs)
        return Cell(None, inputs=value.inputs)

    def remove(self, value):
        if type(value.value) is str and file_exists(value.value):
            file_remove(value.value)
        return Cell(None)

    # Session

    def get_session(self, key):
        if isinstance(key, Cell):
            key = key.value
        if key in session and key[:4] != "user":
            return load(session[key])
        else:
            return Cell(None)

    def pop_session(self, key):
        if isinstance(key, Cell):
            key = key.value
        if key in session and key[:4] != "user":
            session.pop(key, None)
        return Cell(None)

    def set_session(self, key, value):
        if isinstance(key, Cell):
            key = key.value
        session[key] = value.dump()
        return Cell(None)

    def add_session_inputs(self, key, inputs):
        if key in session:
            cell = load(session[key])
            cell.add_inputs(inputs)
            session[key] = cell.dump()
        else:
            session[key] = Cell(None, inputs=inputs).dump()
        return Cell(None)

    # extension: COOKIES

    def _me(self):
        try:
            return session['user_id']
        except RuntimeError:
            # no active HTTP request → callback
            return self.callback_user_id
    
    def me(self):
        value = self._me()
        return Cell(value)#, inputs={value: value})

    def name(self):
        try:
            value = session['user_name']
            return Cell(value)#, inputs={value: self._me()})
        except RuntimeError:
            # no active HTTP request → callback
            return Cell(self.callback_user_name)#, inputs={value: self._me()})

    # extension: access name and other personal information

    def now(self):
        return int(time.time())

    def now_utc(self):
        return datetime.datetime.utcfromtimestamp(self.now()).strftime('%Y-%m-%d @ %H:%M')
            
    # Database operations

    def db_cur(self, select_only=False):
        return Cur(self.db, select_only=select_only)

    def _print_db(self, s):
        if is_set('verbose_db'):
            print(s)

    def _execute_print_db(self, cur, s):
        self._print_db(">> " + s)
        return cur.execute(s)

    #@timing('../evaluation/time.json', 'database')
    def sql(self, query, args=Cell(None), stack=None):
        if query.value in self.sql_asts:
            ast, where_vars, tables = deepcopy(self.sql_asts[query.value])
        else:
            ast = sql.parse(query.value)
            ast.table = f"{self.id_}_{ast.table}"
            if isinstance(ast, sql.Select):
                # read variables in WHERE clause
                where_vars = set()
                for join in ast.joins:
                    join.table = f"{self.id_}_{join.table}"
                    where_vars |= join.get_vars()
                if ast.where is not None:
                    where_vars |= ast.where.get_vars()
                if ast.order_by is not None:
                    where_vars |= ast.order_by.get_vars()
                # list used tables
                tables = {ast.table} | {join.table for join in ast.joins}
            else:
                where_vars, tables = set(), set()
            self.sql_asts[query.value] = deepcopy(ast), where_vars, tables
        if args is not None:
            ast.set_params(args)
        args_cell = Cell(None, inputs=args.all_inputs())
        if isinstance(ast, sql.Select):
            # retrieve results
            with self.db_cur(select_only=True) as cur:
                self._print_db(">> " + str(ast))
                cur = cur.execute(str(ast))
                results = cur.fetchall()
                self._print_db("<< " + repr(results))
                # retrieve ids
                ast_ids = deepcopy(ast)
                ast_ids.result_columns = sql.ResultColumns(
                    columns=[sql.Col(table=table, name="id") for table in tables])
                self._print_db(">> " + str(ast_ids))
                cur = cur.execute(str(ast_ids))
                indices = cur.fetchall()
                self._print_db("<< " + repr(indices))
                idx = {}
                inputs = {}
                valuei = {}
                # for each used table
                for t, table in enumerate(tables):
                    ids = [i[t] for i in indices if i[t] is not None]
                    # fill in index
                    idx[table] = {}
                    for j, id_ in enumerate(ids):
                        if id_ not in idx[table]:
                            idx[table][id_] = []
                        idx[table][id_].append(j)
                    # identify relevant vars
                    rel_vars = list(filter(lambda x: "." not in x or x.split('.')[0] == f'"{table}"',
                                                where_vars))
                    rel_vars = [var.split('.')[1] if '.' in var else var for var in rel_vars]
                    # retrieve table-level inputs
                    table_inputs_q = ("SELECT i.inputs FROM \"{}_inputs_\" AS i "
                                      "WHERE i.entry IS NULL").format(table)
                    self._print_db(">> " + table_inputs_q)
                    cur = cur.execute(table_inputs_q)
                    inputs[table] = Dag()
                    for i in cur.fetchall():
                        inputs[table] = inputs[table] + Dag(json=i[0])
                    # retrieve field-level inputs (from WHERE clauses)
                    field_inputs_q = ("SELECT i.inputs FROM \"{}_inputs_\" AS i "
                                      "WHERE i.field IN ({})") \
                                      .format(table,
                                              ", ".join(map(lambda x: "'{}'".format(x), rel_vars)))
                    self._print_db(">> " + field_inputs_q)
                    cur = cur.execute(field_inputs_q)
                    inputs2 = Dag()
                    for i in cur.fetchall():
                        inputs2 = inputs2 + Dag(json=i[0])
                    inputs[table] = inputs[table] / inputs2
                    # retrieve entry-level inputs (from entries)
                    entry_inputs_q = ("SELECT i.entry, i.field, i.inputs, i.valuei FROM \"{}_inputs_\" AS i "
                                      "WHERE i.entry IN ({})") \
                                      .format(table, ", ".join(map(str, list(set(ids)))))
                    self._print_db(">> " + entry_inputs_q)
                    cur = cur.execute(entry_inputs_q)
                    for i in cur.fetchall():
                        key = (table, i[0], i[1])
                        if key not in inputs:
                            inputs[key] = Dag()
                        inputs[key] = inputs[key] + Dag(json=i[2])
                        valuei[key] = json.loads(i[3] or "null")
                        if valuei[key] is not None:
                            valuei[key] = tuple(valuei[key])
                    # show all inputs retrieved
                    self._print_db("<< " + repr(inputs))
                if ast.result_columns.type_ in ["star", "starred"]:
                    star_table = ast.table if ast.result_columns.type_ == "star" \
                        else str(ast.result_columns).split('.')[0].replace('"', '')
                    # read list of columns
                    col_q = "PRAGMA table_info(\"{}\")".format(star_table)
                    self._print_db(">> " + col_q)
                    cur = cur.execute(col_q)
                    columns = [n[1] for n in cur.fetchall()]
                    self._print_db("<< " + repr(columns))
                    col_idx = {'"' + star_table + '".' + col: i for (i, col) in enumerate(columns)}
                elif ast.result_columns.type_ == "countstar":
                    col_idx = {}
                else:
                    col_idx = {str(col): i for (i, col) in enumerate(ast.result_columns.columns)}
            # build answer
            cell = Cell([Cell(result) for result in results], inputs=args_cell.inputs)
            # add inputs
            u = time.time()
            for (key, the_inputs) in inputs.items():
                # add table-level inputs and inputs from columns in WHERE clause
                t = time.time()
                if type(key) is str:
                    cell.inputs = cell.inputs + the_inputs
                else:
                    table, entry, field = key
                    #print(table, entry, field, idx[table], col_idx)
                    if field is None and entry in idx and idx[entry] in cell.value:
                        # add row-level inputs
                        for j in idx[table][entry]:
                            cell.value[j].inputs += the_inputs
                    elif field is not None and entry in idx[table] \
                         and (f'"{table}".{field}' in col_idx or field in col_idx):
                        # add field-level inputs
                        for j in idx[table][entry]:
                            if field in col_idx:
                                cell.value[j].value[col_idx[field]].inputs += the_inputs
                                cell.value[j].value[col_idx[field]].valuei = valuei[key]
                            else:
                                cell.value[j].value[col_idx[f'"{table}".{field}']].inputs += the_inputs
                                cell.value[j].value[col_idx[f'"{table}".{field}']].valuei = valuei[key]
            return cell
        elif isinstance(ast, sql.Insert):
            # insert entries
            # requires a trigger
            with self.db_cur() as cur:
                # delete content of new table (to avoid bugs)
                delete_new_q = "DELETE FROM \"{}_new_\"".format(ast.table)
                self._print_db(">> " + delete_new_q)
                cur = cur.execute(delete_new_q)
                # execute insert query
                self._print_db(">> " + str(ast))
                cur = cur.execute(str(ast))
                # read new entries' ids
                new_q = "SELECT * FROM \"{}_new_\"".format(ast.table)
                self._print_db(">> " + new_q)
                cur = cur.execute(new_q)
                entry_ids = [n[0] for n in cur.fetchall()]
                self._print_db("<< " + repr(entry_ids))
                # read list of columns
                col_q = "PRAGMA table_info(\"{}\")".format(ast.table)
                self._print_db(">> " + col_q)
                cur = cur.execute(col_q)
                columns = [n[1] for n in cur.fetchall()]
                self._print_db("<< " + repr(columns))
                # retrieve new entries
                new2_q = "SELECT * FROM \"{}\" WHERE id IN ({})".format(ast.table, ", ".join(map(str, entry_ids)))
                self._print_db(">> " + new2_q)
                cur = cur.execute(new2_q)
                entries = cur.fetchall()
                self._print_db("<< " + str(entries))
                # put inserted entries into memory cell
                ins_cells = {entry[0]: Cell({k: v for (k, v) in zip(columns[1:], entry[1:])},
                                            inputs=args.inputs)
                             for entry in entries}
                # set inputs
                if ast.column_list:
                    column_list = ast.column_list.columns
                else:
                    column_list = columns
                for entry in entries:
                    for (value, column) in zip(ast.values.values, column_list):
                        ins_cells[entry[0]].value[column].inputs += value.get_inputs()
                        ins_cells[entry[0]].value[column].valuei = value.get_valuei()
                        #print(value.get_valuei())
                log_cell = Cell(None, inputs=args.inputs)
                # retrieve table-level inputs
                table_inputs_old_q = "SELECT inputs FROM \"{}_inputs_\" WHERE entry IS NULL AND field IS NULL" \
                    .format(ast.table)
                old_inputs = cur.execute(table_inputs_old_q).fetchall()
                if old_inputs:
                    old_inputs = Dag(json=old_inputs[0][0])
                else:
                    old_inputs = Dag()
                # insert new table-level inputs (from stack)
                if stack:
                    new_inputs = stack.all()
                    if not new_inputs.is_empty():
                        table_inputs_old_q = "SELECT inputs FROM \"{}_inputs_\" WHERE entry IS NULL AND field IS NULL" \
                            .format(ast.table)
                        old_inputs = cur.execute(table_inputs_old_q).fetchall()
                        # _inputs_
                        if old_inputs:
                            new_inputs = old_inputs / new_inputs
                            table_inputs_q = "UPDATE \"{}_inputs_\" SET inputs = '{}' WHERE entry IS NULL AND field IS NULL" \
                                .format(ast.table, new_inputs.to_json())
                        else:
                            table_inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs, valuei) VALUES (NULL, NULL, '{}', NULL)" \
                                .format(ast.table, new_inputs.to_json())
                        self._print_db(">> " + table_inputs_q)
                        cur = cur.execute(table_inputs_q)
                        # _inputs2_
                        if old_inputs:
                            table_inputs2_v = ", ".join([f"(NULL, NULL, '{ut[0]}', '{ut[1]}')"
                                                     for ut in new_inputs.collect() - old_inputs.collect()])
                        else:
                            table_inputs2_v = ", ".join([f"(NULL, NULL, '{ut[0]}', '{ut[1]}')"
                                                         for ut in new_inputs.collect()])
                        table_inputs2_q = f"INSERT INTO \"{ast.table}_inputs2_\" (entry, field, inputs, owner) VALUES {table_inputs2_v}"
                        self._print_db(">> " + table_inputs2_q)
                        cur = cur.execute(table_inputs2_q)
                else:
                    new_inputs = old_inputs
                # for each new entry
                for (i, ins_cell) in ins_cells.items():
                    # insert row-level inputs
                    if not ins_cell.inputs.is_empty():
                        # _inputs_
                        row_inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs, valuei) VALUES ({}, NULL, '{}', NULL)" \
                            .format(ast.table, i, (ins_cell.inputs - new_inputs).to_json())
                        self._print_db(">> " + row_inputs_q)
                        cur = cur.execute(row_inputs_q)
                        # _inputs2_
                        row_inputs2_v = ", ".join([f"({i}, NULL, '{ut[0]}', '{ut[1]}')"
                                                   for ut in (ins_cell.inputs - new_inputs).collect()])
                        row_inputs2_q = f"INSERT INTO \"{ast.table}_inputs2_\" (entry, field, inputs, owner) VALUES {table_inputs2_v}"
                        self._print_db(">> " + row_inputs2_q)
                        cur = cur.execute(row_inputs2_q)
                    # for each field
                    for (field, value) in ins_cell.value.items():
                        # insert field-level inputs
                        #print(value)
                        if not value.inputs.is_empty():
                            # _inputs_
                            field_inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs, valuei) VALUES ({}, '{}', '{}', '{}')" \
                                .format(ast.table, i, field,
                                        ((value.inputs - ins_cell.inputs) - new_inputs).to_json(),
                                        json.dumps(value.valuei))
                            self._print_db(">> " + field_inputs_q)
                            cur = cur.execute(field_inputs_q)
                            # _inputs2_
                            field_inputs2_v = ", ".join([f"({i}, '{field}', '{ut[0]}', '{ut[1]}')"
                                                         for ut in ((value.inputs - ins_cell.inputs) - new_inputs).collect()])
                            field_inputs2_q = f"INSERT INTO \"{ast.table}_inputs2_\" (entry, field, inputs, owner) VALUES {field_inputs2_v}"
                            self._print_db(">> " + field_inputs2_q)
                            cur = cur.execute(field_inputs2_q)
                delete_new_q = "DELETE FROM \"{}_new_\"".format(ast.table)
                self._print_db(">> " + delete_new_q)
                cur = cur.execute(delete_new_q)
                return Cell(entry_ids, inputs=log_cell.inputs)
        elif isinstance(ast, sql.Delete):
            with self.db_cur() as cur:
                # select ids to be deleted
                ast_ids = sql.Select(result_columns=sql.ResultColumns(
                    type_="list", columns=[sql.Var(name="id")]),
                                     table=ast.table,
                                     where=ast.where)
                self._print_db(">> " + str(ast_ids))
                cur = cur.execute(str(ast_ids))
                ids = [i[0] for i in cur.fetchall()]
                self._print_db("<< " + repr(ids))
                # select inputs to be deleted
                inputs_ids_q = "SELECT id FROM \"{}_inputs_\" WHERE entry IN ({})" \
                               .format(ast.table, ", ".join(map(str, ids)))
                self._print_db(">> " + inputs_ids_q)
                cur = cur.execute(inputs_ids_q)
                inputs_ids = [i[0] for i in cur.fetchall()]
                self._print_db("<< " + repr(inputs_ids))
                log_cell = Cell(None, inputs=args.all_inputs())
                # delete everything
                delete_q = "DELETE FROM \"{}\" WHERE id in ({})" \
                           .format(ast.table, ", ".join(map(str, ids)))
                self._print_db(">> " + delete_q)
                cur = cur.execute(delete_q)
                # _inputs_
                delete_inputs_q = "DELETE FROM \"{}_inputs_\" WHERE id in ({})" \
                                  .format(ast.table, ", ".join(map(str, inputs_ids)))
                self._print_db(">> " + delete_inputs_q)
                cur = cur.execute(delete_inputs_q)
                # _inputs2_
                delete_inputs2_q = "DELETE FROM \"{}_inputs2_\" WHERE id in ({})" \
                                  .format(ast.table, ", ".join(map(str, inputs_ids)))
                self._print_db(">> " + delete_inputs2_q)
                cur = cur.execute(delete_inputs2_q)
                return Cell(ids, inputs=log_cell.all_inputs())
        else:
            assert(False)

    def add_sql_inputs(self, table, inputs):
        if not inputs:
            return
        table = f"{self.id_}_{table}"
        # insert input
        if not inputs.is_empty():
            with self.db_cur() as cur:
                old_inputs_q = "SELECT inputs FROM \"{}_inputs_\" WHERE entry IS NULL AND field IS NULL" \
                    .format(table)
                old_inputs = cur.execute(old_inputs_q).fetchall()
                # _inputs_
                if old_inputs:
                    inputs = Dag(json=old_inputs[0][0]) / inputs
                    inputs_q = "UPDATE \"{}_inputs_\" SET inputs = '{}' WHERE entry IS NULL AND field IS NULL" \
                        .format(table, inputs.to_json())
                else:
                    inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs, valuei) VALUES (NULL, NULL, '{}', NULL)" \
                        .format(table, inputs.to_json())
                self._print_db(">> " + inputs_q)
                cur.execute(inputs_q)
                # _inputs2_
                if old_inputs:
                    inputs2_v = ", ".join([f"(NULL, NULL, '{ut[0]}')"
                                                 for ut in (inputs - old_inputs[0][0]).collect()])
                else:
                    inputs2_v = ", ".join([f"(NULL, NULL, '{ut[0]}')"
                                                 for ut in inputs.collect()])
                inputs2_q = f"INSERT INTO \"{ast.table}_inputs2_\" (entry, field, inputs) VALUES {inputs2_v}"
                self._print_db(">> " + inputs2_q)
                cur.execute(inputs2_q)

    # Register inputs
    
    def register(self, func_name, key, value):
        args = request.form if request.method == 'POST' else request.args
        ut = args.get(key + "__uts", None) or args.get("the__uts", None)
        owners = args.get(key + "__owners", "") or args.get("the__owners", None)
        sp = args.get(key + "__sp", False) or args.get("the__sp", None)
        return loginput(self, func_name, key, self._me(), self.now(), value,
                        ut=ut, owners=owners, sp=sp)

    # Template operations

    def _parse_render_args(self, the_args):
        args = {}
        if type(the_args.value) is dict:
            for arg, v in the_args.value.items():
                if type(v.value) is tuple:
                    if len(v.value) == 2:
                        value_inputs = Dag(singletons=v.value[1].all_inputs_set())
                        inputs = the_args.inputs / v.inputs / v.value[0].inputs / value_inputs
                        args[arg] = (v.value[0].untaint_value(),
                                     v.value[1].untaint(),
                                     inputs)
        return args

    def render(self, template, args=Cell({})):
        args = self._parse_render_args(args) # collect everything, without order
        ret = Cell(Template(template.value, args))
        return ret

    ## Redirect

    def redirect(self, purp, rule):
        return Cell(Redirect(purp.value, rule.value), inputs=purp.inputs / rule.inputs)

    ## Flash

    def flash(self, msg):
        return self.set_session('flash', msg)

    def unflash(self):
        c = self.get_session('flash')
        self.pop_session('flash')
        return c

    ## Causation of various enforcement actions

    def cause_deletion(self, ut, rectify=None):
        deleted = []
        with self.db_cur() as cur:
            for table in self.tables:
                # if ut labels table, clear table
                table_inputs_q = f"SELECT COUNT(*) FROM \"{table}_inputs2_\" WHERE entry IS NULL AND field IS NULL AND inputs = '{ut}'"
                self._print_db(">> " + table_inputs_q)
                entries = cur.execute(table_inputs_q).fetchall()
                self._print_db("<< " + str(entries))
                if entries[0][0]:
                    table_delete_q1 = f"DELETE FROM \"{table}\" WHERE 1=1"
                    table_delete_q2 = f"DELETE FROM \"{table}_inputs_\" WHERE 1=1"
                    table_delete_q3 = f"DELETE FROM \"{table}_inputs2_\" WHERE 1=1"
                    self._print_db(">> " + str(table_delete_q1))
                    cur.execute(table_delete_q1)
                    self._print_db(">> " + str(table_delete_q2))
                    cur.execute(table_delete_q2)
                    self._print_db(">> " + str(table_delete_q3))
                    cur.execute(table_delete_q3)
                    if rectify is not None:
                        deleted.append((('rectify', 'table', table, rectify), []))
                    else:
                        deleted.append((('delete', 'table', table), []))
                # remove all rows labeled with ut
                row_inputs_q = f"SELECT entry FROM \"{table}_inputs2_\" WHERE entry IS NOT NULL AND field IS NULL AND inputs = '{ut}'"
                self._print_db(">> " + row_inputs_q)
                entries = cur.execute(row_inputs_q).fetchall()
                self._print_db("<< " + str(entries))
                for entry in entries:
                    row_delete_q1 = f"DELETE FROM \"{table}\" WHERE id = {entry[0]}"
                    row_delete_q2 = f"DELETE FROM \"{table}_inputs_\" WHERE entry = {entry[0]}"
                    row_delete_q3 = f"DELETE FROM \"{table}_inputs2_\" WHERE entry = {entry[0]}"
                    self._print_db(">> " + str(row_delete_q1))
                    cur.execute(row_delete_q1)
                    self._print_db(">> " + str(row_delete_q2))
                    cur.execute(row_delete_q2)
                    self._print_db(">> " + str(row_delete_q3))
                    cur.execute(row_delete_q3)
                    if rectify is not None:
                        deleted.append((('rectify', 'row', table, rectify), [entry[0]]))
                    else:
                        deleted.append((('delete', 'row', table), [entry[0]]))
                # remove all fields labeled with ut
                field_inputs_q = f"""SELECT i2.entry, i2.field, i1.valuei, i2.owner FROM \"{table}_inputs2_\" AS i2
JOIN \"{table}_inputs_\" AS i1 ON i1.entry = i2.entry and i1.field = i2.field
WHERE i2.entry IS NOT NULL AND i2.field IS NOT NULL AND i2.inputs = '{ut}'"""
                self._print_db(">> " + field_inputs_q)
                results = cur.execute(field_inputs_q).fetchall()
                self._print_db("<< " + str(results))
                for result in results:
                    entry, field, valuei, owner = result
                    can_rectify = rectify is not None and json.loads(valuei) is not None and json.loads(valuei)[0] == ut
                    if can_rectify:
                        field_delete_q1 = f"UPDATE \"{table}\" SET {field} = {rectify} WHERE id = {entry}"
                    else:
                        field_delete_q1 = f"UPDATE \"{table}\" SET {field} = NULL WHERE id = {entry}"
                        field_delete_q2 = f"DELETE FROM \"{table}_inputs_\" WHERE entry = {entry} AND field = '{field}'"
                        field_delete_q3 = f"DELETE FROM \"{table}_inputs2_\" WHERE entry = {entry} AND field = '{field}'"
                    self._print_db(">> " + str(field_delete_q1))
                    try:
                        cur.execute(field_delete_q1)
                    except:
                        try:
                            if can_rectify:
                                field_delete_q1 = f"UPDATE \"{table}\" SET {field} = '{rectify}' WHERE id = {entry}"
                            else:
                                field_delete_q1 = f"UPDATE \"{table}\" SET {field} = '<Deleted>' WHERE id = {entry}"
                            self._print_db(">> " + str(field_delete_q1))
                            cur.execute(field_delete_q1)
                        except:
                            field_delete_q1 = f"UPDATE \"{table}\" SET {field} = 0 WHERE id = {entry}"
                            self._print_db(">> " + str(field_delete_q1))
                            cur.execute(field_delete_q1)
                    if not can_rectify:
                        self._print_db(">> " + str(field_delete_q2))
                        cur.execute(field_delete_q2)
                        self._print_db(">> " + str(field_delete_q3))
                        cur.execute(field_delete_q3)
                    if rectify is not None:
                        deleted.append((('rectify', 'field', "_".join(table.split("_")[1:]), field),
                                        [entry, Cell(rectify, inputs=Dag(singleton=(ut, int(owner)))), Cell(can_rectify)]))
                    else:
                        deleted.append((('delete', 'field', "_".join(table.split("_")[1:]), field), [entry]))
        for (handler, args) in deleted:
            if handler in self.handlers:
                #print(handler, args)
                self.handlers[handler](*args)

    def cause_rectification(self, ut, value):
        return self.cause_deletion(ut, rectify=value)
        
    def cause_access(self, user, ut):
        dump = {}
        with self.db_cur() as cur:
            # add entry to _access_, get access_id
            delete_access_new_q = f"DELETE FROM \"{self.id_}_access__new_\""
            cur = self._execute_print_db(cur, delete_access_new_q)
            access_q = f"INSERT INTO \"{self.id_}_access_\" (user_id, datetime, ut) VALUES ({user}, '{self.now_utc()}', '{ut}')"
            cur = self._execute_print_db(cur, access_q)
            new_access_q = f"SELECT * FROM \"{self.id_}_access__new_\""
            cur = self._execute_print_db(cur, new_access_q)
            access_id = cur.fetchall()[0][0]
            self._print_db("<< " + str(access_id))
            # dump tables
            for table in self.tables:
                # add table to _access_tables_, get table_id
                delete_access_tables_new_q = f"DELETE FROM \"{self.id_}_access_tables__new_\""
                cur = self._execute_print_db(cur, delete_access_tables_new_q)
                access_tables_q = f"INSERT INTO \"{self.id_}_access_tables_\" (access_id, table_name) VALUES ({access_id}, '{table}')"
                cur = self._execute_print_db(cur, access_tables_q)
                new_access_tables_q = f"SELECT * FROM \"{self.id_}_access_tables__new_\""
                cur = self._execute_print_db(cur, new_access_tables_q)
                table_id = cur.fetchall()[0][0]
                self._print_db("<< " + str(table_id))
                # if ut labels table, set tags_table
                table_inputs_q = f"SELECT COUNT(*) FROM \"{table}_inputs2_\" WHERE entry IS NULL AND field IS NULL AND inputs = '{ut}'"
                cur = self._execute_print_db(cur, table_inputs_q)
                table_ut_count = cur.fetchall()[0][0]
                self._print_db("<< " + str(table_ut_count))
                tags_table = table_ut_count > 0
                if tags_table:
                    # add table-level uts to _access_tables_inputs_ and _access_tables_inputs2_
                    table_uts_q = f"SELECT inputs FROM \"{table}_inputs_\" WHERE entry IS NULL AND field IS NULL"
                    cur = self._execute_print_db(cur, table_uts_q)
                    to_reinsert = [f"({table_id}, NULL, {row[0]}, NULL)" for row in cur.fetchall()]
                    cur = cur.execute(f"INSERT INTO \"{self.id_}_access_tables__inputs_\" (entry, field, inputs, valuei) VALUES {' '.join(to_reinsert)}")
                    table_uts2_q = f"SELECT inputs, owner FROM \"{table}_inputs2_\" WHERE entry IS NULL AND field IS NULL"
                    cur = self._execute_print_db(cur, table_uts2_q)
                    to_reinsert = [f"({table_id}, NULL, {row[0]}, {row[1]})" for row in cur.fetchall()]
                    cur = cur.execute(f"INSERT INTO \"{self.id_}_access_tables__inputs2_\" (entry, field, inputs, owner) VALUES {' '.join(to_reinsert)}")
                if not tags_table:
                    # select relevant rows
                    relevant_rows_q = f"SELECT entry, field FROM \"{table}_inputs2_\" WHERE inputs = '{ut}'"
                    cur = self._execute_print_db(cur, relevant_rows_q)
                    relevant_rows = cur.fetchall()
                    self._print_db("<< " + str(relevant_rows))
                    if relevant_rows:
                        entries = list(map(str, set(list(zip(*relevant_rows))[0])))
                    else:
                        entries = []
                # collect row-level and field-level uts
                if tags_table:
                    other_uts_q = f"SELECT entry, field, inputs FROM \"{table}_inputs_\" WHERE entry IS NOT NULL"
                else:
                    other_uts_q = f"SELECT entry, field, inputs FROM \"{table}_inputs_\" WHERE entry IN ({', '.join(entries)})"
                cur = self._execute_print_db(cur, other_uts_q)
                uts = {}
                for row in cur.fetchall():
                    idx = (row[0], row[1])
                    if tags_table \
                       and list(idx) not in relevant_rows \
                       and [row[0], None] not in relevant_rows:
                        continue
                    if idx not in uts:
                        uts[idx] = set()
                    uts[idx].add(row[2])
                if tags_table:
                    other_uts2_q = f"SELECT entry, field, inputs, owner FROM \"{table}_inputs2_\" WHERE entry IS NOT NULL"
                else:
                    other_uts2_q = f"SELECT entry, field, inputs, owner FROM \"{table}_inputs2_\" WHERE entry IN ({', '.join(entries)})"
                cur = self._execute_print_db(cur, other_uts2_q)
                uts2 = {}
                for row in cur.fetchall():
                    idx = (row[0], row[1])
                    if tags_table \
                       and list(idx) not in relevant_rows \
                       and [row[0], None] not in relevant_rows:
                        continue
                    if idx not in uts2:
                        uts2[idx] = set()
                    uts2[idx].add((row[2], row[3]))
                # retrieve columns of table
                col_q = "PRAGMA table_info(\"{}\")".format(table)
                cur = self._execute_print_db(cur, col_q)
                columns = [n[1] for n in cur.fetchall()]
                self._print_db("<< " + repr(columns))
                # dump content of table to _access_data_
                if tags_table:
                    table_content_q = f"SELECT * FROM \"{table}\""
                else:
                    table_content_q = f"SELECT * FROM \"{table}\" WHERE id IN ({', '.join(entries)})"
                cur = self._execute_print_db(cur, table_content_q)
                to_reinsert_uts = []
                to_reinsert_uts2 = []
                for row in cur.fetchall():
                    row_id = row[0]
                    row2 = list(zip(columns, row))[1:]
                    to_reinsert = []
                    for (field, data) in row2:
                        to_reinsert.append(f"({table_id}, {row_id}, '{field}', '{data}')")
                    if to_reinsert:
                        delete_data_new_q = f"DELETE FROM \"{self.id_}_access_data__new_\""
                        cur = self._execute_print_db(cur, delete_data_new_q)
                        data_q = f"INSERT INTO \"{self.id_}_access_data_\" (table_id, row_id, field, data) VALUES {', '.join(to_reinsert)}"
                        cur = self._execute_print_db(cur, data_q)
                        new_data_q = f"SELECT * FROM \"{self.id_}_access_data__new_\""
                        cur = self._execute_print_db(cur, new_data_q)
                        data_ids = [c[0] for c in cur.fetchall()]
                    else:
                        data_ids = []
                    for ut in uts.get((row_id, None), set()):
                        for data_id in data_ids:
                            to_reinsert_uts.append(f"({data_id}, NULL, '{ut}')")
                    for (ut, owner) in uts2.get((row_id, None), set()):
                        for data_id in data_ids:
                            to_reinsert_uts2.append(f"({data_id}, NULL, '{ut}', '{owner}')")
                    row3 = list(zip(columns[1:], row[1:], data_ids))
                    for (field, data, data_id) in row3:
                        for ut in uts.get((row_id, field), set()):
                            to_reinsert_uts.append(f"({data_id}, NULL, '{ut}', NULL)")
                        for (ut, owner) in uts2.get((row_id, field), set()):
                            to_reinsert_uts2.append(f"({data_id}, NULL, '{ut}', '{owner}')")
                if to_reinsert_uts:
                    cur = self._execute_print_db(cur, f"INSERT INTO \"{self.id_}_access_data__inputs_\" (entry, field, inputs, valuei) VALUES {', '.join(to_reinsert_uts)}")
                if to_reinsert_uts2:
                    cur = self._execute_print_db(cur, f"INSERT INTO \"{self.id_}_access_data__inputs2_\" (entry, field, inputs, owner) VALUES {', '.join(to_reinsert_uts2)}")
    
    ## Access functions

    def access_index(self, ut):
        with self.db_cur() as cur:
            datasets = cur.execute(f"SELECT * FROM \"{self.id_}_access_\" WHERE ut = ? AND user_id = ?", [ut, self.me().value]).fetchall()
        return render_template(os.path.join("access", "index.html"), app_name=self.app_name, ut=ut, datasets=datasets, url_prefix=f"/{self.id_}_access")

    def access_dataset(self, id, json=False):
        with self.db_cur() as cur:
            dataset = cur.execute(f"SELECT datetime, ut FROM \"{self.id_}_access_\" WHERE id = ? AND user_id = ?", [id, self.me().value]).fetchall()
        if not dataset:
            return redirect("record")
        else:
            datetime, ut = dataset[0]
        data = self.sql(Cell("SELECT id, table_name FROM access_tables_ WHERE access_id = ?0"),
                        Cell([Cell(id)]))
        tables = []
        has_hidden = False
        for row in data:
            verdict = logreturn(self, self.me(), "Service", Cell(row).all_inputs(), Cell())
            if not verdict.value:
                has_hidden = True
            else:
                tables.append((row[0].value, row[1].value))
        if json:
            return {"datetime": datetime,
                    "ut": ut,
                    "tables": {row[0]: {"table_name": row[1]} for row in tables}}
        else:
            return render_template(os.path.join("access", "dataset.html"), app_name=self.app_name,
                                   datetime=datetime, ut=ut, tables=tables, has_hidden=has_hidden,
                                   url_prefix=f"/{self.id_}_access", id_=id)

    def access_table(self, id, json=False):
        with self.db_cur() as cur:
            table = cur.execute(f"SELECT access_id, table_name FROM \"{self.id_}_access_tables_\" WHERE id = ?", [id]).fetchall()
            if not table:
                return redirect("record")
            access_id, table_name = table[0]
            dataset = cur.execute(f"SELECT datetime, ut FROM  \"{self.id_}_access_\" WHERE id = ? AND user_id = ?", [access_id, self.me().value]).fetchall()
        if not dataset:
            return redirect("record")
        else:
            datetime, ut = dataset[0]
        data = self.sql(Cell("SELECT row_id, field, data FROM access_data_ WHERE table_id = ?0"), Cell([Cell(id)]))
        rows = {}
        has_hidden = False
        for row in data:
            verdict = logreturn(self, self.me(), "Service", Cell(row).all_inputs(), Cell())
            if not verdict.value:
                has_hidden = True
            else:
                row_id = row[0].value
                if row_id not in rows:
                    rows[row_id] = []
                rows[row_id].append((row.untaint())[1:])
        if json and id in json["tables"]:
            dic = json["tables"][id]
            dic["rows"] = {row_id: [{"field": field[0], "data": field[1]} for field in fields]
                           for (row_id, fields) in rows.items()}
        else:
            return render_template(os.path.join("access", "table.html"), app_name=self.app_name,
                                   datetime=datetime, ut=ut, rows=list(rows.items()),
                                   access_id=access_id, table_name=table_name, 
                                   has_hidden=has_hidden, url_prefix=f"/{self.id_}_access", id_=id)

    def access_dump(self, id):
        json = self.access_dataset(id, json=True)
        for table_id in json["tables"]:
            self.access_table(table_id, json=json)
        return json

    
    def register_access_functions(self):
        self.flask.add_url_rule(f"/{self.id_}_access/<ut>", f"{self.id_}_access", self.access_index)
        self.flask.add_url_rule(f"/{self.id_}_access/dataset/<id>", f"{self.id_}_access_dataset", self.access_dataset)
        self.flask.add_url_rule(f"/{self.id_}_access/table/<id>", f"{self.id_}_access_table", self.access_table)
        self.flask.add_url_rule(f"/{self.id_}_access/dump/<id>", f"{self.id_}_access_dump", self.access_dump)
    
## Passive application wrapper
            
class PassiveApplication:

    def __init__(self, flask, server, id_, app_name, sp_IP, database, key, databank_side=True):
        self.flask = flask
        self.id_ = id_
        self.app_name = app_name
        if databank_side:
            self.server = server
        else:
            self.server = Server(databank_side=False)            
        self.server.register_app(id_, sp_IP, key)
        self.database = database
        self.aes = AES.new(bytes(key, 'utf8'), AES.MODE_EAX)
        self.databank_side = databank_side
        self.db = sqlite3.connect(database, check_same_thread=False)
        self.db.cursor().execute("PRAGMA synchronous = OFF")
        self.sql_asts = {}

    # Function decorators

    def callback(self, rule):
        def inner(f):
            def wrapper(*args, **kw):
                x = f(*args, **kw)
                return x
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            self.server.register_callback(self.id_, rule, self, wrapper)
            return wrapper
        return inner
    
    def route(self, rule, **kwargs):
        def inner(f):
            def wrapper(*args, **kw):
                return f(*args, **kw)
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            return self.flask.route(join_route(self.id_, rule), **kwargs)(wrapper)
        return inner

    # Distant call

    def call(self, addr, function, *args, stack=None, **kwargs):
        dump = pickle.dumps((function, args.untaint(), kwargs.untaint()))
        msg = self.aes.encrypt(pad(dump))
        ret = do_call(msg, addr, databank_side=self.databank_side) 
        data = pickle.loads(self.aes.decrypt(ret).strip())
        return data

    # Check with monitor

    def check(self, p, arg):
        return True

    # Claim legal gorunds

    def claim_legal_ground(self, g, p, arg):
        # ...
        pass

    # Exposed user input methods

    def method(self):
        return request.method
    
    def post(self, key):
        if key in request.files:
            file_ = request.files.get(key)
            value = self._generate_filename(file_)
            file_Save(value, file_)
        else:
            value = request.form.get(key, None)
        return value

    def get(self, key):
        return request.args.get(key, None)

    def link(self, value):
        if type(value) is str and file_exists(value):
            return url_for('file_link', filename=value)
        return None

    def remove(self, value):
        if type(value) is str and file_exists(value):
            file_remove(value)
        return None

    # Session

    def get_session(self, key):
        if key in session and key[:4] != "user":
            cell = session[key]
            return Cell(json=cell)
        else:
            return None

    def pop_session(self, key):
        if key in session and key[:4] != "user":
            session.pop(key, None)
        return None

    def set_session(self, key, value):
        session[key] = value.dump()
        return None

    # extension: COOKIES

    def _me(self):
        try:
            return session['user_id']
        except RuntimeError:
            # no active HTTP request → callback
            return self.callback_user_id
    
    def me(self):
        value = self._me()
        return value

    def name(self):
        try:
            value = session['user_name']
            return value
        except RuntimeError:
            # no active HTTP request → callback
            return self.callback_user_name

    # extension: access name and other personal information

    def now(self):
        return int(time.time())

    def now_utc(self):
        return datetime.utcfromtimestamp(self.now()).strftime('%Y-%m-%d @ %H:%M')
            
    # Database operations

    def db_cur(self, select_only=False):
        return Cur(self.db, select_only=select_only)

    def _print_db(self, s):
        if is_set('verbose_db'):
            print(s)

    #@timing('../evaluation/time.json', 'database')
    def sql(self, query, args=None, stack=None, assigned=None):
        if query in self.sql_asts:
            ast, select_only = deepcopy(self.sql_asts[query])
        else:
            ast = sql.parse(query)
            ast.table = f"{self.id_}_{ast.table}"
            if isinstance(ast, sql.Select):
                for join in ast.joins:
                    join.table = f"{self.id_}_{join.table}"
                select_only = True
            else:
                select_only = False
            self.sql_asts[query] = deepcopy(ast), select_only
        t2 = time.time()
        if args is not None:
            ast.set_params(args)
        with self.db_cur(select_only=select_only) as cur:
            self._print_db(">> " + str(ast))
            cur = cur.execute(str(ast))
            results = cur.fetchall()
            self._print_db("<< " + repr(results))
        t3 = time.time()
        return results

    # Template operations
    
    def render(self, template, args):
        return render_template(template, **args)

    # Redirect
    
    def redirect(self, purp, rule):
        return redirect(rule)

    ## Flash

    def flash(self, msg):
        return self.set_session('flash', msg)

    def unflash(self):
        return self.get_session('flash')

# TODO: remove files when they are no longer needed
