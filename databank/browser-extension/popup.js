const checks = document.querySelectorAll('.form-check-input');
const token = document.getElementById('token');
const owners = document.getElementById('owners');
const resetLink = document.getElementById('reset');

async function getCurrentApplicationName() {
    const currentTabs = await browser.tabs.query({currentWindow: true, active: true});
    const url = currentTabs[0].url;
    let app_names = (await browser.storage.local.get({app_names: new Map()})).app_names;
    console.log(app_names);
    if(app_names.get(url) !== undefined) {
	return app_names.get(url)
    }
    else {
	let req = new XMLHttpRequest();
	req.open('GET', url, false);
	req.send(null);
	let name = req.getResponseHeader("ttc-application-name");
	console.log(name);
	app_names.set(url, name);
	await browser.storage.local.set({app_names: app_names});
	return name;
    }
    /*console.log(headers);
    if (/^.*:\/\/localhost(:.*)?\/.*\/.*$/.test(url))
	return currentTabs[0].url.split("/")[3];
    else
	return undefined;*/
}

function updateApplicationName() {
    getCurrentApplicationName().then(
	name => {
	    if (name === undefined) {
		document.querySelectorAll('.form-check-input').forEach(
		    check => {
			if (check.id.startsWith("cur-")) {
			    console.log(check); check['disabled'] = true;
			}
		    }
		);
	    }
	    else
		document.getElementById('app-name').innerHTML = name;
	});
}

function getCurrentConsent(checked, id) {
    const pref = id.slice(0, 3);
    if (pref === "cur") { // App consent
	const option = id.slice(4);
	return getCurrentApplicationName().then( 
	    name => {
		if (name === undefined) // If app not supported, no consent
		    return false;
		else {
		    if (checked.has("visited-" + name))  // If app consent exists
			return checked.has(name + "-" + option);
		    else                                 // Else default consent
			return checked.has("def-" + option);
		}
	    });
    }
    else if (pref === "def") // Default consent
	return new Promise ( (resolve) => resolve(checked.has(id)) );
}


async function updateSelect() {
    console.log("Update select");
    let warning = document.getElementById('warning');
    let name = await getCurrentApplicationName();
    browser.storage.local.get({checked: new Set()}).then(
	items => {
	    const checked = items.checked;
	    document.querySelectorAll('.form-check-input').forEach(
		check => getCurrentConsent(checked, check.id).then(
		    checkedBool => check.checked = checkedBool));
	    if (Array.from(checked).some(c => c.includes(name + ":")))
		warning.style.display = 'block';
	    else
		warning.style.display = 'none';
	});
}

function updateToken() {
    browser.storage.local.get({token: ""}).then(
	items => {
	    const tokenValue = items.token;
	    document.getElementById('token').value = tokenValue;
	});
}

function updateOwners() {
    browser.storage.local.get({owners: ""}).then(
	items => {
	    const ownersValue = items.owners;
	    document.getElementById('owners').value = ownersValue;
	});
}

async function reset() {
    console.log("Reset");
    let name = await getCurrentApplicationName();
    await browser.storage.local.get({checked: new Set()}).then(
	items => {
	    let checked = items.checked;
	    console.log(checked);
	    checked = new Set(Array.from(checked).filter(c => !c.includes(name + ":")));
	    console.log(checked);
	    browser.storage.local.set({checked: checked}).then();
	    updateSelect();
	});
}


checks.forEach(check => {
    check.addEventListener('click', event =>  {
	console.log("Reacting to click");
	console.log(event);
	browser.storage.local.get({checked: new Set()}).then(
	    async items => {
		var checked = items.checked;
		const pref = check.id.slice(0, 3);
		const option = check.id.slice(4);
		if (pref === "cur") { // App consent
		    const name = await getCurrentApplicationName();
		    if (name !== undefined) { // If app supported
			if (!checked.has("visited-" + name)) {
			    checked.add("visited-" + name); // App consent now exists
			    checked.forEach(
				c => {
				    if (c.startsWith("def-"))
					checked.add(name + "-" + c.slice(4));
				});
			}
			if (check.checked)            // Consent given
			    checked.add(name + "-" + option);
			else                          // Consent not given
			    checked.delete(name + "-" + option);
		    }
		}
                else if (pref === "def") { // Default consent
		    if (check.checked)  // Consent given
			checked.add(check.id);
		    else                // Consent not given
			checked.delete(check.id);
		}
		console.log(checked);
    		await browser.storage.local.set({checked: checked});
		console.log("Finished reacting to click");
		console.log(checked);
		/*updateSelect();*/
	    }
	);
    });
});

token.addEventListener('input', event => {
    browser.storage.local.set({token: token.value}).then();
});

owners.addEventListener('input', event => {
    browser.storage.local.set({owners: owners.value}).then();
});

resetLink.addEventListener('click', event => reset());

updateApplicationName();
updateSelect();
updateToken();
updateOwners();


