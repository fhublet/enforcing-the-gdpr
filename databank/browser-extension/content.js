let myPort = browser.runtime.connect();

inputsRegistered = null;

function handleMessage(message) {
    console.log("In content script, received message from background script: ");
    console.log(message);
    if (message.type === "inputsConfirmed") {
	inputsRegistered(message);
    } else if (message.type === "getTargetElementId") {
	getTargetElementId(message);
    }
    else {
	console.log("Message of unknown type.");
    }
}

myPort.onMessage.addListener(handleMessage);

function connected(p) {
    p.onMessage.addListener(handleMessage);
}

browser.runtime.onConnect.addListener(connected);

function computeCurrentPurposes(name, func, field) {
    return browser.storage.local.get({checked: new Set()}).then(
	items => {
	    const checked = items.checked;
	    if (!checked.has("visited-" + name))
		return Array.from(items.checked)
		    .filter(value => value.split('-')[0] == 'def')
		    .map(value => value.split('-')[1]);
	    else if ((field === undefined) ||
		     !checked.has("visited-" + name + ":" + func + "." + field))
		return Array.from(items.checked)
		    .filter(value => value.split('-')[0] == name)
		    .map(value => value.split('-')[1]);
	    else
		return Array.from(items.checked)
		.filter(value => value.split('-')[0] == name + ":" + func + "." + field)
		.map(value => value.split('-')[1]);
	});
}

async function registerInputs(formKeys) {
    message = {
	type: "registerInputs",
	keys: formKeys,
    };
    myPort.postMessage(message);
    return new Promise( (resolve) => {
	inputsRegistered = (reply) => { resolve(reply); }
    });
}

async function getOwners() {
    return (await browser.storage.local.get({owners: ''})).owners;
}

async function sendConsent(app, ut, purpose) {
    message = {
	type:    "sendConsent",
	app:     app,
	ut:      ut,
	purpose: purpose,
    },
    myPort.postMessage(message);
}

async function interceptForm(e) {
    e.preventDefault();
    console.log("Intercepting submission of target form:");
    console.log(e.target);
    const formContent = Object.values(e.target);
    const formKeys = formContent.map(obj => obj["name"])
 	  .filter(name => name != undefined && name != "");
    let registeredInputs = await registerInputs(formKeys);
    let owners = await getOwners();
    const currentApp = await getCurrentApplicationName();
    const currentFunc = window.location.href.split("?")[0].split("/").slice(4).join("/");
    console.log(owners);
    console.log(formKeys);
    const uts = registeredInputs["uts"];
    let sp = await Promise.all(formKeys.map(
	async (key, i) =>
	    { const purposes = await computeCurrentPurposes(currentApp, currentFunc, key);
	      return purposes.includes("Special"); }));
    console.log('hello');
    console.log(sp);
    formKeys.forEach((key, i) => {
	let newInput = document.createElement("input");
	newInput["type"] = "hidden";
	newInput["name"] = key + "__uts";
	newInput["value"] = uts[i];
	e.target.appendChild(newInput);
	newInput = document.createElement("input");
	newInput["type"] = "hidden";
	newInput["name"] = key + "__owners";
	newInput["value"] = owners;
	e.target.appendChild(newInput);
	newInput = document.createElement("input");
	newInput["type"] = "hidden";
	newInput["name"] = key + "__sp";
	newInput["value"] = sp[i] ? "True" : "False";
	e.target.appendChild(newInput);
    });
    console.log("Updated target form:");
    console.log(e.target);
    formKeys.forEach(
	async (key, i) =>
	    { const purposes = await computeCurrentPurposes(currentApp, currentFunc, key);
	      console.log(key, i, purposes)
	      purposes.forEach(
		  async purpose => {
		      if (purpose != "Special")
			  await sendConsent(currentApp, uts[i], purpose);
		  });
	    });
    console.log("bar");
    e.target.submit();
    console.log("Submitted updated target form.");
}

async function interceptA(e) {
    e.preventDefault();
    console.log("Intercepting link click:");
    console.log(e.target);
    const a = e.target;
    const currentApp = await getCurrentApplicationName();
    const currentFunc = window.location.href.split("?")[0].split("/").slice(4).join("/");
    const purposes = await computeCurrentPurposes(currentApp, currentFunc, undefined);
    const registeredInputs = await registerInputs(["the"]);
    console.log(registeredInputs);
    const ut = registeredInputs["uts"][0];
    const owners = await getOwners();
    const sp = purposes.includes("Special") ? "True" : "False";
    console.log(purposes)
    purposes.forEach(
	async purpose => {
	    if (purpose != "Special")
		await sendConsent(currentApp, ut, purpose);
	});
    const suffix = `the__uts=${ut}&the__owners=${owners}&the__sp=${sp}&the=True`;
    if(a["href"].includes("?"))
	a["href"] += "&" + suffix
    else
	a["href"] += "?" + suffix
    window.location = a["href"];
}
    

document.querySelectorAll('form').forEach(
    form => {
	if (form.attachEvent)
	    form.attachEvent("submit", interceptForm);
	else
	    form.addEventListener("submit", interceptForm);
    });

document.querySelectorAll('input').forEach(
    e => { if (e.id === "") e.id = e.name; }
);

document.querySelectorAll('a').forEach(
    a => { console.log(a); a.addEventListener("click", interceptA); }
);

console.log("GDPR enforcement content script loaded.");


// Context menu stuff

function getTargetElementId(message) {
    message = {
	type:  "sendTargetElementId",
	id:   browser.menus.getTargetElement(message.id).id
    };
    myPort.postMessage(message);
}

async function getCurrentApplicationName() {
    const url = window.location.href;
    let app_names = (await browser.storage.local.get({app_names: new Map()})).app_names;
    if(app_names.get(url) !== undefined) {
	return app_names.get(url)
    }
    else {
	let req = new XMLHttpRequest();
	req.open('GET', url, false);
	req.send(null);
	let name = req.getResponseHeader("ttc-application-name");
	app_names.set(url, name);
	await browser.storage.local.set({app_names: app_names});
	return name;
    }
}

