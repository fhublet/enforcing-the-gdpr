


MONITOR_URL = "http://localhost:5000";


async function getToken() {
    const token = (await browser.storage.local.get({token: ''})).token;
    return token;
}

async function getOwners() {
    const owners = (await browser.storage.local.get({owners: ''})).owners;
    return owners;
}

function sendConsent(app, ut, purpose) {
    console.log("Sending consent");
    getToken().then(
	token => {
	    fetch(`${MONITOR_URL}/record/add_consent`, {
		method: 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		body: `app=${app}&ut=${ut}&purpose=${purpose}&token=${token}`, //
	    })
	});
}

async function registerInputs(formKeys, receiverID) {
    console.log("Registering inputs");
    const n = formKeys.length.toString();
    const token = await getToken();
    const response = await fetch(`${MONITOR_URL}/register_inputs`, {
	method: 'POST',
	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	body: `n=${n}&token=${token}`
    });
    const responseJson = await response.json();
    responseJson.type = "inputsConfirmed";
    sendReply(receiverID, responseJson);
}


function handleMessage(receiverID, message) {
  console.log("In background script, received message from content script: ");
  console.log(message);
  if (message.type === "registerInputs") {
      registerInputs(message.keys, receiverID);
  } else if (message.type === "sendConsent") {
      sendConsent(message.app, message.ut, message.purpose);
  } else if (message.type == "sendTargetElementId") {
      targetElementIds[receiverID](message.id);
  }
    else {
      console.log("Message of unknown type.");
  }

}

let ports = [];

function connected(p) {
  const replyID = p.sender.tab.id;
  ports[replyID] = p;
  p.onMessage.addListener((message) => {
    handleMessage(replyID, message)
  });
}

browser.runtime.onConnect.addListener(connected);

function sendReply(receiverID, message) {
    const port = ports[receiverID];
    port.postMessage(message);
}

let targetElementIds = [];


console.log("GDPR enforcement background script loaded.");

// Context menu stuff

function getCurrentConsent(checked, id) {
    const option = id.split("-")[1];
    const name = id.split("-")[0].split(":")[0];
    const field = id.split("-")[0].split(":")[1];
    if (checked.has("visited-" + name + ":" + field))  // If field consent exists
	return checked.has(name + ":" + field + "-" + option);
    else if (checked.has("visited-" + name)) // If app consent exists
	return checked.has(name + "-" + option);
    else
	return checked.has("def"- + option);
}

async function getCurrentApplicationName(tab) {
    const url = tab.url;
    let app_names = (await browser.storage.local.get({app_names: new Map()})).app_names;
    if(app_names.get(url) !== undefined) {
	return app_names.get(url)
    }
    else {
	let req = new XMLHttpRequest();
	req.open('GET', url, false);
	req.send(null);
	let name = req.getResponseHeader("ttc-application-name");
	app_names.set(url, name);
	await browser.storage.local.set({app_names: app_names});
	return name;
    }
}

function getCurrentFunctionName(tab) {
    const url = tab.url;
    if (/^.*:\/\/localhost(:.*)?\/.*\/.*$/.test(url))
	return tab.url.split("?")[0].split("/").slice(4).join("/");
    else
	return undefined;
}

browser.contextMenus.create(
  {
      id: "input-consent",
      title: "Enforcing the GDPR: Custom field consent",
      contexts: ["editable"],
  }
);

PURPOSE_OPTIONS = ["Service", "Analytics", "Marketing", "Special"];


browser.contextMenus.create(
    {
	id: "input-consent",
	title: "Consent",
	contexts: ["editable"],
    }
);

PURPOSE_OPTIONS.forEach(
    option => {
	browser.contextMenus.create(
	    {
		id: "input-consent-" + option,
		title: option,
		parentId: "input-consent",
		type: "checkbox"
	    })
    });


async function updateMenus(info, tab, checked) {
    console.log(info);
    console.log("Update menus");
    let targetId = await getTargetElementId(tab.id, info.targetElementId);
    PURPOSE_OPTIONS.forEach(
	async option => {
	    const name = await getCurrentApplicationName(tab);
	    const func = getCurrentFunctionName(tab);
	    const checkedBool = getCurrentConsent(
		checked, name + ":" + func + "." + targetId + "-" + option);
	    console.log(name + ":" + func + "." + targetId + "-" + option);
	    console.log(checked);
	    console.log(checkedBool);
	    browser.contextMenus.update("input-consent-" + option, { checked: checkedBool });
	    browser.contextMenus.refresh();
	});
}

function getTargetElementId(receiverID, id) {
    message = {
	type: "getTargetElementId",
	id:   id
    };
    console.log("Sending request to resolve targetElementId");
    console.log(message);
    let port = browser.tabs.connect(receiverID);
    port.postMessage(message);
    return new Promise( (resolve) => {
	targetElementIds[receiverID] = (reply) => { resolve(reply); }
    });
}

async function registerConsentMenu(info, tab, checked) {
    if (info.menuItemId.startsWith("input-consent-")) {
	const option = info.menuItemId.slice(14);
	let targetId = await getTargetElementId(tab.id, info.targetElementId);
	console.log("Updating field consent");
	console.log(targetId);
	console.log(info.checked);
	console.log(info.menuId);
	console.log(checked);
	let name = await getCurrentApplicationName(tab);
	let func = getCurrentFunctionName(tab);
	if (name !== undefined) { // If app supported
	    if (!checked.has("visited-" + name + ":" + func + "." + targetId)) {
		checked.add("visited-" + name + ":" + func + "." + targetId);
		checked.forEach(
		    c => {
			if (c.startsWith(name + "-"))
			    checked.add(name + ":" + func + "." + targetId + "-" + c.split("-")[1]);
		    });
	    }
	    // Field consent now exists
	    if (info.checked)          // Consent given
		checked.add(name + ":" + func + "." + targetId + "-" + option);
	    else                          // Consent not given
		checked.delete(name + ":" + func + "." + targetId + "-" + option);
	}
    }
    console.log(checked);
    await browser.storage.local.set({checked: checked});
    updateMenus(info, tab, checked);
}

browser.contextMenus.onShown.addListener(
    (info, tab) => {
	browser.storage.local.get({checked: new Set()}).then(
	    items => updateMenus(info, tab, items.checked));
    });

browser.contextMenus.onClicked.addListener(
    async (info, tab) => {
	browser.storage.local.get({checked: new Set()}).then(
	    items => registerConsentMenu(info, tab, items.checked));
    });

