from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests

from werkzeug.security import generate_password_hash
from lorem_text import lorem


from tools import Task, Subtask


class Scenario:

    login_url = "http://localhost:5000/login"
    timeline_url = "http://localhost:5000/public"
    add_message_url = "http://localhost:5000/add_message"
    follow_url = "http://localhost:5000/{username}/follow"

    def __init__(self, sc, app_cmd, database):
        self.sc = sc
        self.app_cmd = app_cmd
        self.database = database

    def generate_random_message(self, users):
        author = random.choice(users)[0]
        text = lorem.paragraph()
        date = int(round(datetime.now().timestamp()))
        return f'("{author}", "{text}", "{date}")'
        
    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'minitwitplus.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Initializing database', cplx=True) as task:
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                users = cur.execute("SELECT user_id FROM user").fetchall()
                messages = cur.execute("SELECT message_id FROM message").fetchall()

            if len(users) > self.u:
                with Subtask(f'Deleting {len(users)-self.u} users', task):
                    cur.execute(f"DELETE FROM user ORDER BY user_id DESC LIMIT {len(users)-self.u}")
                    users = cur.execute("SELECT user_id FROM user").fetchall()
                    assert(len(users) == self.u)

            elif len(users) < self.u:
                with Subtask(f'Adding {self.u-len(users)} users', task):
                    for i in range(len(users), self.u):
                        to_insert = f'("user{i}", "user{i}@mail.com", "' + generate_password_hash(f'password{i}') + '")'
                        data = cur.execute(f"INSERT INTO user (username, email, pw_hash) VALUES {to_insert} returning user_id")
                        user_id = data.fetchone()[0]
                        cur.execute('insert into consent (user, purpose, data, value) values (?,?,\'messages\',1)', [user_id, 'generate advertisements'])
                        cur.execute('insert into consent (user, purpose, data, value) values (?,?,\'follower\',1)', [user_id, 'display relevant timeline'])
                    users = cur.execute("SELECT username FROM user").fetchall()
                    consents = cur.execute("SELECT consent_id FROM consent").fetchall()
                    assert(len(users) == self.u)
                    assert(len(consents) == self.u*2)

            if len(messages) > self.n:
                with Subtask(f'Deleting {len(messages)-self.n} messages', task):
                    cur.execute(f"DELETE FROM message ORDER BY message_id DESC LIMIT {len(messages)-self.n}")
                    messages = cur.execute("SELECT message_id FROM message").fetchall()
                    assert(len(messages) == self.n)
                    
            elif len(messages) < self.n:
                with Subtask(f'Adding {self.n-len(messages)} messages', task):
                    to_insert = [self.generate_random_message(users) for _ in range(len(messages), self.n)]
                    cur.execute(f"INSERT INTO message (author_id, text, pub_date) VALUES {','.join(to_insert)}")
                    messages = cur.execute("SELECT message_id FROM message").fetchall()
                    assert(len(messages) == self.n)
            
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Starting Minitwit Flask application'):
            self.proc = Popen(self.app_cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True)
            sleep(1)

        with Task(pref, 'Logging in with user 1') as task:
            user1 = users[0]
            self.session = requests.Session()
            r = self.session.post(self.login_url, data={"username": "user0", "password": "password0"})
            assert(r.ok)
            # Adding followings
            for i in range(1,len(users)):
                r = self.session.get(self.follow_url.format(username=f"user{i}"))
                assert(r.ok)

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        if self.sc == 'add_message':
            with Task('minitwitplus.run', 'Deleting last message'):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                cur.execute(f"DELETE FROM message ORDER BY message_id DESC LIMIT 1")
                cur.close()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'add_message':
            with Task('minitwitplus.run', 'Measurement for scenario "Add message"'):
                r = self.session.post(self.add_message_url, data={"text": lorem.paragraph()})
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'timeline':
            with Task('minitwitplus.run', 'Measurement for scenario "Timeline"'):
                r = self.session.get(self.timeline_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('minitwitplus.finalize', 'Killing Minitwit Flask application'):
            self.proc.kill()

class Application:
    
    app_cmd  = ["python3", "baseline/minitwitplus_secured/minitwit.py"]
    database = "baseline/minitwitplus_secured/db/minitwitplus.db"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('minitwitplus.start', 'Starting evaluation') as task:
            
            with Subtask('Opening database', task):
                
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f"DELETE FROM user WHERE 1=1")
                cur.execute(f"DELETE FROM consent WHERE 1=1")
                cur.execute(f"DELETE FROM message WHERE 1=1")
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('minitwitplus.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('minitwitplus.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('add_message', self.app_cmd, self.database),
                         Scenario('timeline', self.app_cmd, self.database),
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('minitwitplus.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 1
            us = [4**i for i in range(0, N_u+1)]
            # n: number of messages
            N_n = 7
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us x ns
            configs = [(u, n) for u in us for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
