from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
from requests.exceptions import ReadTimeout, ConnectTimeout
import shutil
import os
import signal
import json
from tqdm import tqdm

from werkzeug.security import generate_password_hash
from lorem_text import lorem
import uuid

from flask_hashing import Hashing
from flask import Flask
from lorem_text import lorem

from tools import Task, Subtask

bank = Flask("The Databank")
bank.secret_key = "E4kB3BUlTXivYtkaKnCb9XHGIr9erSEIX0n0MWOnAqlqr2PGKWjPgWp2834M5PmDqx2dEvI2EV7YdriY"
hashing = Hashing(bank)

class Scenario:

    login_url = "http://localhost:5000/login"
    loginput_url = "http://localhost:5000/loginput"
    logout_url = "http://localhost:5000/logout"
    register_url = "http://localhost:5000/6/accounts/register"
    login_url2 = "http://localhost:5000/6/accounts/do_login"
    view_patient_url = "http://localhost:5000/6/patients/{}"
    view_index_url = "http://localhost:5000/6/"
    add_consent_url = "http://localhost:5000/record/add_consent"
    record_url = "http://localhost:5000/record"
    erase_url = "http://localhost:5000/record/erase"
    rectify_url = "http://localhost:5000/record/rectify"
    revoke_consent_url = "http://localhost:5000/record/revoke_consent"

    reset_monitor_url = "http://localhost:5678/reset-everything"
    set_signature_url = "http://localhost:5678/set-signature"
    set_policy_url = "http://localhost:5678/set-policy"
    start_monitor_url = "http://localhost:5678/start-monitor"
    log_events_url = "http://localhost:5678/log-events"
    get_events_url = "http://localhost:5678/get-events"

    def __init__(self, sc, app_cmd, cwd, database, database2, db_folder, databank_log,
                 state_folder, app_name, monitor_files_folder, time_log):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database
        self.database2 = database2
        self.db_folder = db_folder
        self.databank_log = databank_log
        self.state_folder = state_folder
        self.app_name = app_name
        self.monitor_files_folder = monitor_files_folder
        self.time_log = os.path.join(self.monitor_files_folder, time_log)
        
    def reset_monitor(self):
        r = requests.get(self.reset_monitor_url)
        assert(r.ok)

    def set_signature(self):
        r = requests.post(self.set_signature_url,
                          files={"signature": open(os.path.join(self.monitor_files_folder, 'gdpr.sig'), "rb")})
        assert(r.ok)

    def set_policy(self):
        r = requests.post(self.set_policy_url,
                          files={"policy": open(os.path.join(self.monitor_files_folder, 'gdpr.mfotl'), "rb")})
        assert(r.ok)

    def start_monitor(self):
        r = requests.post(self.start_monitor_url, data={"enforce": True})
        assert(r.ok)

    def get_monitor_time(self):
        try:
            with open(self.time_log, 'r') as f:
                return json.load(f)['monitor']
        except:
            return 0

    def ut_with_service_consent(self, u):
        ut = str(uuid.uuid4())
        r = self.session.post(self.add_consent_url, data={"app": self.app_name, "ut": ut,
                                                          "purpose": "Service", "token": f"token{u}"})
        r.raise_for_status()
        return ut, r.elapsed.total_seconds()

    def view_patient(self, i, u):
        ut_id, t_id = self.ut_with_service_consent(u)
        random_id = {"the__uts": ut_id,
                     "the__owners": str(u),
                     "the__sp": "False",
                     "the": "True"}
        if os.path.exists(self.time_log):
            os.remove(self.time_log)
        r = self.session.get(self.view_patient_url.format(i), params=random_id, timeout=10)
        r.raise_for_status()    
        return t_id, r.elapsed.total_seconds(), ut_id


    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'hipaa.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Setting up users and policies', cplx=True) as task:
            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                users2 = cur.execute('SELECT id FROM users').fetchall()

            if len(users2) > self.u:
                with Subtask(f'Deleting {len(users2)-self.u} users', task):
                    cur.execute(f'DELETE FROM users ORDER BY id DESC LIMIT {len(users2)-self.u}')
                    users2 = cur.execute('SELECT name FROM users').fetchall()
                    assert(len(users2) == self.u)
                
            if len(users2) < self.u:
                with Subtask(f'Adding {self.u-len(users2)} users', task):
                    f = lambda i: hashing.hash_value(f"password{i}")
                    g = lambda i: self.p.format(i).replace('"', '""')
                    to_insert = [f'({i}, "user{i}", "{f(i)}", "token{i}")' for i in range(len(users2), self.u)]
                    cur.execute(f'INSERT INTO users (id, name, hash, token) VALUES {",".join(to_insert)}')
                    users2 = cur.execute('SELECT id FROM users').fetchall()
                    assert(len(users2) == self.u)

            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Loading test database', cplx=True) as task:
            db_fn = os.path.join(self.db_folder, f'{self.u}_{self.u*self.n}.db')
            mfotl_fn = os.path.join(self.state_folder, 'gdpr.mfotl')
            sig_fn = os.path.join(self.state_folder, 'gdpr.sig')
            state_fn = os.path.join(self.state_folder, f'{self.u}_{self.u*self.n}.json')
            loaded = False

            with Subtask(f'Trying to load existing database', task):
                # If it exists, load it
                if os.path.exists(db_fn):
                    self.reset_monitor()
                    self.set_signature()
                    self.set_policy()
                    self.start_monitor()
                    r = requests.post(self.log_events_url, files={"events": open(state_fn, "rb")})
                    assert(r.ok)
                    shutil.copy(db_fn, self.database)
                    loaded = True
                    
            if not loaded:
                with Subtask(f'Opening and resetting database', task):
                    db = sqlite3.connect(self.database)
                    cur = db.cursor()
                    cur.execute(f'DELETE FROM "6_UserProfile" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_UserProfile_inputs_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_UserProfile_inputs2_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_Individual" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_Individual_inputs_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_Individual_inputs2_" WHERE 1=1')
                    cur.close()
                    db.commit()
                    db.close()

        with Task(pref, 'Initializing database', cplx=True) as task:
            if not loaded:
                with Subtask('Cleaning monitor states', task):
                    self.reset_monitor()
                    self.set_signature()
                    self.set_policy()
                    self.start_monitor()

                with Subtask('Starting Databank', task):
                    with open(self.databank_log, 'w') as f:
                        self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)

                self.session = requests.Session()
                with Subtask('Generating UTs', task):
                    uts = []
                    for i in tqdm(list(range(self.u*self.n))):
                        uts.append([])
                        for a in ["FirstName", "LastName", "Email", "Sex", "BirthDate"]:
                            while True:
                                try:
                                    ut, _ = self.ut_with_service_consent(random.randint(0, self.u-1))
                                    r = requests.post(self.loginput_url,
                                                      data={"f": "6/Individual", "a": "hipaa",
                                                            "u": random.randint(0, self.u-1),
                                                            "ut": ut})
                                    assert(r.ok)
                                    uts[i].append(ut)
                                except Exception as e:
                                    sleep(1)
                                else:
                                    break
                            assert(r.ok)

                with Subtask('Killing Databank', task):
                    self.proc.send_signal(signal.SIGINT)
                    #self.proc.wait()
                    self.proc.kill()
                    self.proc.wait()
                    
                with Subtask(f'Saving states', task):
                    r = requests.post(self.get_events_url)
                    assert(r.ok)
                    with open(state_fn, "w") as f:
                        json.dump(r.json(), f)
                    shutil.copy(self.database, db_fn)

                with Subtask('Opening database', task):
                    db = sqlite3.connect(self.database)
                    cur = db.cursor()

                with Subtask(f'Adding {self.u} users', task):
                    to_insert = [f'({i}, {i}, "user{i}", 1, "user{i}@example.com", 1, "name")'
                                  for i in range(self.u)]
                    cur.execute(f'INSERT INTO "6_UserProfile" (id, user_id, username, is_active, email, profiletype, name) ' +\
                                f"VALUES {','.join(to_insert)}")
                    users = cur.execute('SELECT id FROM "6_UserProfile"').fetchall()
                    assert(len(users) == self.u)

                with Subtask(f'Adding {self.u*self.n} individuals', task):
                    to_insert = [f'({i}, "firstname", "lastname", "user{i}@example.com", ' + \
                                 f'"{random.choice(["Male", "Female"])}", "2023-01-01")'
                                 for i in range(self.u*self.n)]
                    cur.execute(f'INSERT INTO "6_Individual" (id, FirstName, Lastname, Email, Sex, BirthDate) ' + \
                                f"VALUES {','.join(to_insert)}")
                    to_insert_inputs_ = \
                        [f"({i}, 'FirstName', '[[[\"{uts[i][0]}\",{i}]]]', '[\"{uts[i][0]}\", {i}]')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'LastName', '[[[\"{uts[i][1]}\",{i}]]]', '[\"{uts[i][1]}\", {i}]')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'Email', '[[[\"{uts[i][2]}\",{i}]]]', '[\"{uts[i][2]}\", {i}]')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'Sex', '[[[\"{uts[i][3]}\",{i}]]]', '[\"{uts[i][3]}\", {i}]')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'BirthDate', '[[[\"{uts[i][4]}\",{i}]]]', '[\"{uts[i][4]}\", {i}]')"
                         for i in range(self.u*self.n)]
                    cur.execute(f'INSERT INTO "6_Individual_inputs_" (entry, field, inputs, valuei) ' + \
                                f"VALUES {','.join(to_insert_inputs_)}")
                    to_insert_inputs2_ = \
                        [f"({i}, 'FirstName', '{uts[i][0]}', '{i}')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'LastName', '{uts[i][1]}', '{i}')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'Email', '{uts[i][2]}', '{i}')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'Sex', '{uts[i][3]}', '{i}')"
                         for i in range(self.u*self.n)] + \
                        [f"({i}, 'BirthDate', '{uts[i][4]}', '{i}')"
                         for i in range(self.u*self.n)]
                    cur.execute(f'INSERT INTO "6_Individual_inputs2_" (entry, field, inputs, owner) ' + \
                                f"VALUES {','.join(to_insert_inputs2_)}")
                    individuals = cur.execute('SELECT id FROM "6_Individual"').fetchall()
                    assert(len(individuals) == self.u*self.n)

                with Subtask('Committing and closing database', task):
                    cur.close()
                    db.commit()
                    db.close()

                with Subtask(f'Saving database', task):
                    shutil.copy(self.database, db_fn)

        with Task(pref, 'Starting Databank'):
            with open(self.databank_log, 'w') as f:
                self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)

        with Task(pref, 'Logging in as user0', cplx=True) as task:
            user1 = users2[0]
            c = 0
            self.session = requests.Session()
            while True:
                with Subtask(f'Attempt {c}', task):
                    try:
                        r = self.session.post(self.login_url,
                                              data={"name": "user0", "password": "password0"})
                        assert(r.ok)
                        r = self.session.get(self.login_url2, allow_redirects=False)
                        #print(r, r.text)
                        assert(r.ok)
                    except Exception as e:
                        print(e, r.ok)
                        c += 1
                        sleep(1)
                    else:
                        break
            try:
                assert(r.ok)
            except:
                print(r.text)

        with Task('minitwitplus.run', 'Resetting time log'):
            if os.path.exists(self.time_log):
                os.remove(self.time_log)

                
    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        pass
                
    def run(self):
        """Function performing the measurement"""
        if self.sc == 'view_index':
            with Task('conf.run', 'Resetting time counter'):
                if os.path.exists(self.time_log):
                    os.remove(self.time_log)
            with Task('hipaa.run', 'Measurement for scenario "View index"'):
                try:
                    r = self.session.get(self.view_index_url, timeout=5)
                except ReadTimeout:
                    print('Timeout')
                    t = 5
                except ConnectTimeout:
                    print('Timeout')
                    t = 5
                else:
                    t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_monitor': self.get_monitor_time(),
                    't_other': t-self.get_monitor_time(), 't': t}
        elif self.sc == 'view_patient':
            with Task('conf.run', 'Resetting time counter'):
                if os.path.exists(self.time_log):
                    os.remove(self.time_log)
            with Task('hipaa.run', 'Measurement for scenario "View patient"'):
                t1, t2, _ = self.view_patient(random.randint(0, self.u*self.n-1), 0)
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_consent': t1, 't_monitor': self.get_monitor_time(),
                    't_other': t2-self.get_monitor_time(), 't': t1 + t2}
        
    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('hipaa.finalize', 'Killing Hipaa Databank application'):
            self.proc.kill()

class Application:

    app_cmd  = ["python3", "main.py"]
    database = "../db/database.db"
    database2 = "../db/fundamental.db"
    cwd      = "../src"
    db_folder = "databank_db/hipaa"
    databank_log = "logs/databank_log"
    state_folder = "monitor_db/hipaa"
    app_name = "hipaa"
    monitor_files_folder = "../monitor_files"
    time_log = "time.json"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('hipaa.start', 'Starting evaluation') as task:

            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f'DELETE FROM "6_UserProfile" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_UserProfile_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_UserProfile_inputs2_" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_Individual" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_Individual_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_Individual_inputs2_" WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                
            with Subtask('Cleaning users', task):
                cur.execute(f'DELETE FROM users WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()
                
    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('hipaa.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('hipaa.scenarios', 'Computing scenarios'):
            args = [self.app_cmd, self.cwd, self.database, self.database2,
                    self.db_folder, self.databank_log, self.state_folder, self.app_name,
                    self.monitor_files_folder, self.time_log]
            scenarios = [#Scenario('view_index', *args),
                         Scenario('view_patient', *args),
            ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('hipaa.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 4
            u_def = 4**4
            us = [4**i for i in range(0, N_u+1)]
            # n: number of individuals
            N_n = 4
            n_def = 4**4
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us x ns x ps
            configs = [(u, n_def) for u in us] + [(u_def, n) for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t_monitor", "t_other"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
    
