from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import shutil
import os

from bs4 import BeautifulSoup

from werkzeug.security import generate_password_hash
from lorem_text import lorem


from tools import Task, Subtask


class Scenario:

    login_url = "http://localhost:5000/accounts/login"
    submit_url = "http://localhost:5000/submit"
    view_papers_url = "http://localhost:5000/papers"
    view_paper_url = "http://localhost:5000/paper"

    def __init__(self, sc, app_cmd, database, default_pdf):
        self.sc = sc
        self.app_cmd = app_cmd
        self.database = database
        self.default_pdf = default_pdf

    def generate_random_paper1(self, id_, users):
        accepted = 0
        author_id = random.choice(users)[0]
        return f'({id_}, {accepted}, {author_id})'

    def generate_random_paper2(self, id_, users):
        title = lorem.sentence()
        contents = f"papers/paper{id_}.pdf"
        shutil.copy(self.default_pdf, os.path.join("media", f"paper{id_}.pdf"))
        abstract = lorem.paragraph()
        time = "2023-01-01 00:00:00.000000"
        return f'({id_}, "{title}", "{contents}", "{abstract}", "{time}", {id_})'

    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'conf.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Initializing database', cplx=True) as task:
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                users = cur.execute("SELECT id FROM user_profiles").fetchall()
                papers = cur.execute("SELECT id FROM papers").fetchall()

            if len(users) > self.u:
                with Subtask(f'Deleting {len(users)-self.u} users', task):
                    cur.execute(f"DELETE FROM user_profiles ORDER BY id DESC LIMIT {len(users)-self.u}")
                    users = cur.execute("SELECT id FROM user_profiles").fetchall()
                    assert(len(users) == self.u)

            elif len(users) < self.u:
                with Subtask(f'Adding {self.u-len(users)} users', task):
                    password_hashes = [generate_password_hash(f'password{i}') for i in range(len(users), self.u)]
                    to_insert = [f'({i}, "user{i}", "user{i}@example.com", "user{i}", "ABCD", "acmnumber", "chair", ' + \
                                 f'"{password_hashes[i-len(users)]}")' for i in range(len(users), self.u)]
                    cur.execute((f"INSERT INTO user_profiles (id, username, email, name, affiliation, acm_number, level, password) "
                                 f"VALUES {','.join(to_insert)}"))
                    users = cur.execute("SELECT id FROM user_profiles").fetchall()
                    assert(len(users) == self.u)
                    
            if len(papers) > self.n*self.u:
                with Subtask(f'Deleting {len(papers)-self.n*self.u} papers', task):
                    cur.execute(f"DELETE FROM papers ORDER BY id DESC LIMIT {len(papers)-self.n*self.u}")
                    cur.execute(f"DELETE FROM conf_paperversion ORDER BY id DESC LIMIT {len(papers)-self.n*self.u}")
                    papers = cur.execute("SELECT id FROM papers").fetchall()
                    versions = cur.execute("SELECT id FROM conf_paperversion").fetchall()
                    assert(len(papers) == self.n*self.u)
                    assert(len(versions) == self.n*self.u)
                    
            elif len(papers) < self.n*self.u:
                with Subtask(f'Adding {self.n*self.u-len(papers)} papers', task):
                    to_insert1 = [self.generate_random_paper1(i, users) for i in range(len(papers), self.n*self.u)]
                    to_insert2 = [self.generate_random_paper2(i, users) for i in range(len(papers), self.n*self.u)]
                    cur.execute(f"INSERT INTO papers (id, accepted, author_id) VALUES {','.join(to_insert1)}")
                    cur.execute(f"INSERT INTO conf_paperversion (id, title, contents, abstract, time, paper_id) VALUES {','.join(to_insert2)}")
                    papers = cur.execute("SELECT id FROM papers").fetchall()
                    versions = cur.execute("SELECT id FROM conf_paperversion").fetchall()
                    assert(len(papers) == self.n*self.u)
                    assert(len(versions) == self.n*self.u)
            
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Starting Conf Flask application'):
            self.proc = Popen(self.app_cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True)
            sleep(1)

        with Task(pref, 'Logging in with user 0') as task:
            user0 = users[0]
            self.session = requests.Session()
            while True:
                try:
                    r = self.session.post(self.login_url, data={"username": "user0", "password": "password0"})
                    assert(r.ok)
                except:
                    print(r.text)
                    pass
                else:
                    break

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        if self.sc == 'submit_paper':
            with Task('conf.run', 'Deleting last paper'):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                cur.execute(f"DELETE FROM papers ORDER BY id DESC LIMIT 1")
                cur.execute(f"DELETE FROM conf_paperversion ORDER BY id DESC LIMIT 1")
                cur.close()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'submit_paper':
            with Task('conf.run', 'Measurement for scenario "Submit paper"'):
                r = self.session.post(self.submit_url,
                                      data={"title": lorem.sentence(), "abstract": lorem.paragraph()},
                                      files={"contents": open(self.default_pdf, 'rb')})
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'view_paper':
            with Task('conf.run', 'Measurement for scenario "View paper"'):
                r = self.session.get(self.view_paper_url, params={"id": random.randint(0, self.n*self.u-1)})
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'view_papers':
            with Task('conf.run', 'Measurement for scenario "View papers"'):
                r = self.session.get(self.view_papers_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('conf.finalize', 'Killing Minitwit Flask application'):
            self.proc.kill()

class Application:
    
    app_cmd  = ["python3", "baseline/cms_flask/conf.py"]
    database = "db/conf.db"
    default_pdf = "mathgen.pdf"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('conf.start', 'Starting evaluation') as task:
            
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f"DELETE FROM user_profiles WHERE 1=1")
                cur.execute(f"DELETE FROM papers WHERE 1=1")
                cur.execute(f"DELETE FROM conf_paperversion WHERE 1=1")
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('conf.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('conf.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('submit_paper', self.app_cmd, self.database, self.default_pdf),
                         Scenario('view_paper', self.app_cmd, self.database, self.default_pdf), 
                         Scenario('view_papers', self.app_cmd, self.database, self.default_pdf),
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('conf.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 4
            u_def = 4**4
            us = [4**i for i in range(0, N_u+1)]
            # n: number of papers
            N_n = 1
            n_def = 4**1
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us x ns
            configs = [(u, n_def) for u in us]# + [(u_def, n) for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
    
