from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import os
import uuid
from tqdm import tqdm
import shutil
import json
import signal

from lorem_text import lorem
from flask_hashing import Hashing
from flask import Flask

from tools import Task, Subtask

bank = Flask("The Databank")
bank.secret_key = "E4kB3BUlTXivYtkaKnCb9XHGIr9erSEIX0n0MWOnAqlqr2PGKWjPgWp2834M5PmDqx2dEvI2EV7YdriY"
hashing = Hashing(bank)

class Scenario:

    login_url = "http://localhost:5000/login"
    logout_url = "http://localhost:5000/logout"
    register_url = "http://localhost:5000/31/register"
    login_url2 = "http://localhost:5000/31/login"
    timeline_url = "http://localhost:5000/31/public"
    add_message_url = "http://localhost:5000/31/add_message"
    add_consent_url = "http://localhost:5000/record/add_consent"
    record_url = "http://localhost:5000/record"
    erase_url = "http://localhost:5000/record/erase"
    rectify_url = "http://localhost:5000/record/rectify"
    revoke_consent_url = "http://localhost:5000/record/revoke_consent"

    reset_monitor_url = "http://localhost:5678/reset-everything"
    set_signature_url = "http://localhost:5678/set-signature"
    set_policy_url = "http://localhost:5678/set-policy"
    start_monitor_url = "http://localhost:5678/start-monitor"
    log_events_url = "http://localhost:5678/log-events"
    get_events_url = "http://localhost:5678/get-events"


    def __init__(self, sc, app_cmd, cwd, database, database2, db_folder, databank_log,
                 state_folder, app_name, monitor_files_folder, time_log):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database
        self.database2 = database2
        self.db_folder = db_folder
        self.databank_log = databank_log
        self.state_folder = state_folder
        self.app_name = app_name
        self.monitor_files_folder = monitor_files_folder
        self.time_log = os.path.join(self.monitor_files_folder, time_log)

    def reset_monitor(self):
        r = requests.get(self.reset_monitor_url)
        assert(r.ok)

    def set_signature(self):
        r = requests.post(self.set_signature_url,
                          files={"signature": open(os.path.join(self.monitor_files_folder, 'gdpr.sig'), "rb")})
        assert(r.ok)

    def set_policy(self):
        r = requests.post(self.set_policy_url,
                          files={"policy": open(os.path.join(self.monitor_files_folder, 'gdpr.mfotl'), "rb")})
        assert(r.ok)

    def start_monitor(self):
        r = requests.post(self.start_monitor_url, data={"enforce": True})
        assert(r.ok)

    def get_monitor_time(self):
        with open(self.time_log, 'r') as f:
            return json.load(f)['monitor']

    def post_random_message(self, u):
        ut = str(uuid.uuid4())
        r1 = self.session.post(self.add_consent_url, data={"app": self.app_name, "ut": ut, "purpose": "Service", "token": f"token{u}"})
        r1.raise_for_status()
        random_message = {"text": lorem.paragraph(),
                          "text__uts": ut,
                          "text__owners": str(u),
                          "text__sp": "False"}
        if os.path.exists(self.time_log):
            os.remove(self.time_log)
        r2 = self.session.post(self.add_message_url, data=random_message, allow_redirects=False)
        r2.raise_for_status()
        return r1.elapsed.total_seconds(), r2.elapsed.total_seconds(), ut
        
    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'minitwitplus.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Setting up users and policies', cplx=True) as task:
            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                users2 = cur.execute('SELECT id FROM users').fetchall()

            if len(users2) > self.u:
                with Subtask(f'Deleting {len(users2)-self.u} users', task):
                    cur.execute(f'DELETE FROM users ORDER BY id DESC LIMIT {len(users2)-self.u}')
                    users2 = cur.execute('SELECT name FROM users').fetchall()
                    assert(len(users2) == self.u)
                
            if len(users2) < self.u:
                with Subtask(f'Adding {self.u-len(users2)} users', task):
                    f = lambda i: hashing.hash_value(f"password{i}")
                    to_insert = [f'({i}, "user{i}", "{f(i)}", "token{i}")' for i in range(len(users2), self.u)]
                    cur.execute(f'INSERT INTO users (id, name, hash, token) VALUES {",".join(to_insert)}')
                    users2 = cur.execute('SELECT id FROM users').fetchall()
                    assert(len(users2) == self.u)

            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Loading test database', cplx=True) as task:
            db_fn = os.path.join(self.db_folder, f'{self.u}_{self.u*self.n}.db')
            mfotl_fn = os.path.join(self.state_folder, 'gdpr.mfotl')
            sig_fn = os.path.join(self.state_folder, 'gdpr.sig')
            state_fn = os.path.join(self.state_folder, f'{self.u}_{self.u*self.n}.json')
            loaded = False

            with Subtask(f'Trying to load existing database', task):
                # If it exists, load it
                if os.path.exists(db_fn):
                    self.reset_monitor()
                    self.set_signature()
                    self.set_policy()
                    self.start_monitor()
                    r = requests.post(self.log_events_url, files={"events": open(state_fn, "rb")})
                    assert(r.ok)
                    shutil.copy(db_fn, self.database)
                    loaded = True
                    
            if not loaded:
                with Subtask(f'Opening and resetting database', task):
                    db = sqlite3.connect(self.database)
                    cur = db.cursor()
                    cur.execute('DELETE FROM "31_user" WHERE 1=1')
                    cur.execute('DELETE FROM "31_user_inputs_" WHERE 1=1')
                    cur.execute('DELETE FROM "31_user_inputs2_" WHERE 1=1')
                    cur.execute('DELETE FROM "31_message" WHERE 1=1')
                    cur.execute('DELETE FROM "31_message_inputs_" WHERE 1=1')
                    cur.execute('DELETE FROM "31_message_inputs2_" WHERE 1=1')
                    cur.execute('DELETE FROM "31_follower" WHERE 1=1')
                    cur.execute('DELETE FROM "31_follower_inputs_" WHERE 1=1')
                    cur.execute('DELETE FROM "31_follower_inputs2_" WHERE 1=1')
                    cur.close()
                    db.commit()
                    db.close()
                
        with Task(pref, 'Loading test database (2)', cplx=True) as task:
            if not loaded:
                with Subtask('Starting Databank', task):
                    with open(self.databank_log, 'w') as f:
                        self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)
                with Subtask('Cleaning monitor states', task):
                    self.reset_monitor()
                    self.set_signature()
                    self.set_policy()
                    self.start_monitor()
                    #self.reset_monitor()
                with Subtask(f'Registering users', task):
                    # For each user
                    for i in range(self.u):
                        # Log in into Databank
                        c = 0
                        while True:
                            try:
                                self.session = requests.Session()
                                r = self.session.post(self.login_url, data={"name": f"user{i}", "password": f"password{i}"})
                            except:
                                c += 1
                                print(c, end='')
                                sleep(5)
                            else:
                                break
                        assert(r.ok)
                        # Register user into Minitwitplus
                        r = self.session.post(self.register_url)
                        assert(r.ok)
                with Subtask(f'Posting messages', task):
                    authors = random.choices(range(self.u), k=self.u*self.n)
                    messages_by_author = {k: authors.count(k) for k in range(self.u)}
                    for a, mn in messages_by_author.items():
                        # Log in into Databank as usera
                        self.session = requests.Session()
                        r = self.session.post(self.login_url, data={"name": f"user{a}", "password": f"password{a}"})
                        assert(r.ok)
                        # Log in into Minitwitplus
                        r = self.session.get(self.login_url2)
                        assert(r.ok)
                        # Insert mn random messages
                        for _ in tqdm(list(range(mn))):
                            while True:
                                try:
                                    self.post_random_message(a)
                                except requests.exceptions.HTTPError as err:
                                    print(err)
                                else:
                                    break
                with Subtask('Killing Databank', task):
                    self.proc.send_signal(signal.SIGINT)
                    #self.proc.wait()
                    self.proc.kill()
                    self.proc.wait()
                with Subtask(f'Saving database', task):
                    r = requests.post(self.get_events_url)
                    assert(r.ok)
                    with open(state_fn, "w") as f:
                        json.dump(r.json(), f)
                    shutil.copy(self.database, db_fn)

        with Task(pref, f'Starting Databank'):
            with open(self.databank_log, 'w') as f:
                self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)

        with Task(pref, 'Logging in as user0', cplx=True) as task:
            user1 = users2[0]
            self.session = requests.Session()
            c = 0
            while True:
                with Subtask(f'Attempt {c}', task):
                    try:
                        r = self.session.post(self.login_url, data={"name": "user0", "password": "password0"})
                    except:
                        c += 1
                        sleep(1)
                    else:
                        break
            assert(r.ok)
            r = self.session.get(self.login_url2)
            try:
                assert(r.ok)
            except:
                print(r.text)

        with Task('minitwitplus.run', 'Resetting time log'):
            if os.path.exists(self.time_log):
                os.remove(self.time_log)


    def continue_(self):
        """Function to be executed between two instances of the same measurement"""

        if self.sc == 'add_message':
            with Task('minitwitplus.run', 'Deleting last message'):
                # should reset the states as well
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                id_to_delete = cur.execute(f'SELECT MAX(id) FROM "31_message"').fetchall()[0][0]
                cur.execute(f'DELETE FROM "31_message" WHERE id = {id_to_delete}')
                cur.execute(f'DELETE FROM "31_message_inputs_" WHERE entry = {id_to_delete}')
                cur.execute(f'DELETE FROM "31_message_inputs2_" WHERE entry = {id_to_delete}')
                db.commit()
                db.close()
                
        with Task('minitwitplus.run', 'Resetting time log'):
            if os.path.exists(self.time_log):
                os.remove(self.time_log)


    def run(self):
        """Function performing the measurement"""
        if self.sc == 'add_message':
            with Task('minitwitplus.run', 'Measurement for scenario "Add message"'):
                t1, t2, _ = self.post_random_message(0)
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_consent': t1, 't_monitor': self.get_monitor_time(),
                    't_other': t2-self.get_monitor_time(), 't': t1 + t2}
        elif self.sc == 'timeline':
            with Task('minitwitplus.run', 'Measurement for scenario "Timeline"'):
                r = self.session.get(self.timeline_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_monitor': self.get_monitor_time(),
                    't_other': t-self.get_monitor_time(), 't': t}
        elif self.sc == 'record':
            with Task('minitwitplus.run', 'Measurement for scenario "Record"'):
                r = self.session.get(self.record_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
                return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'erase':
            with Task('minitwitplus.run', 'Measurement for scenario "Erase"'):
                ut = self.post_random_message(0)[2]
                r = self.session.post(self.erase_url,
                                      data={"token": "token0", "app": "minitwitplus", "ut": ut})
                assert(r.ok)
                t = r.elapsed.total_seconds()
                return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_monitor': self.get_monitor_time(),
                        't_other': t-self.get_monitor_time(), 't': t}
        elif self.sc == 'rectify':
            with Task('minitwitplus.run', 'Measurement for scenario "Rectify"'):
                ut = self.post_random_message(0)[2]
                r = self.session.post(self.rectify_url,
                                      data={"token": "token0", "app": "minitwitplus", "ut": ut,
                                            "value": lorem.paragraph()})
                assert(r.ok)
                t = r.elapsed.total_seconds()
                return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_monitor': self.get_monitor_time(),
                        't_other': t-self.get_monitor_time(), 't': t}
        elif self.sc == 'revoke':
            with Task('minitwitplus.run', 'Measurement for scenario "Revoke"'):
                ut = self.post_random_message(0)[2]
                r = self.session.get(self.revoke_consent_url,
                                     params={"u": "0", "app": "minitwitplus", "ut": ut,
                                             "purpose": "Service", "token": "token0"},
                                     allow_redirects=False)
                assert(r.ok)
                t = r.elapsed.total_seconds()
                return {'sc': self.sc, 'u': self.u, 'n': self.n, 't_monitor': self.get_monitor_time(),
                        't_other': t-self.get_monitor_time(), 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('minitwitplus.finalize', 'Killing Databank'):
            self.proc.send_signal(signal.SIGINT)
            #self.proc.wait()
            self.proc.kill()
            self.proc.wait()

class Application:
    
    app_cmd  = ["python3", "main.py"]
    database = "../db/database.db"
    database2 = "../db/fundamental.db"
    cwd      = "../src"
    db_folder = "databank_db/minitwitplus"
    databank_log = "logs/databank_log"
    state_folder = "monitor_db/minitwitplus"
    app_name = "minitwitplus"
    monitor_files_folder = "../monitor_files"
    time_log = "time.json"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('minitwitplus.start', 'Starting evaluation') as task:

            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f'DELETE FROM "31_user" WHERE 1=1')
                cur.execute(f'DELETE FROM "31_user_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "31_user_inputs2_" WHERE 1=1')
                cur.execute(f'DELETE FROM "31_message" WHERE 1=1')
                cur.execute(f'DELETE FROM "31_message_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "31_message_inputs2_" WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                
            with Subtask('Cleaning users', task):
                cur.execute(f'DELETE FROM users WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('minitwitplus.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('minitwitplus.scenarios', 'Computing scenarios'):
            args = [self.app_cmd, self.cwd, self.database, self.database2,
                    self.db_folder, self.databank_log, self.state_folder, self.app_name,
                    self.monitor_files_folder, self.time_log]
            scenarios = [Scenario('revoke', *args),
                         Scenario('add_message', *args),
                         Scenario('timeline', *args),
                         Scenario('record', *args),
                         Scenario('erase', *args),
                         Scenario('rectify', *args),
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('minitwitplus.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 4
            u_def = 4**4
            us = [4**i for i in range(0, N_u+1)]
            # n: number of messages
            N_n = 4
            n_def = 4**4
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us + ns
            configs = [(u, n_def) for u in us] + [(u_def, n) for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t_consent", "t_monitor", "t_other"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]

