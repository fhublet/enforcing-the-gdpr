from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import shutil
import os

from bs4 import BeautifulSoup

from werkzeug.security import generate_password_hash
from lorem_text import lorem


from tools import Task, Subtask


class Scenario:

    login_url = "http://localhost:8000/accounts/login/"
    view_patient = "http://localhost:8000/patients/{uid}" #Show info of a patient
    #view_covered_entity = "http://localhost:8000/entities/{eid}" #The page show info of a covered entity
    view_index = "http://localhost:8000/index" #The main page shows patients and entities.
    # Add more if you want additional scenarios
    # consult hipaa/urls.py for more endpoints

    def __init__(self, sc, app_cmd, cwd, database, default_pdf):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database
        self.default_pdf = default_pdf

    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n  = config
        pref = f'hipaa.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Initializing database', cplx=True) as task:
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                users = cur.execute("SELECT id FROM auth_user").fetchall()
                profiles = cur.execute("SELECT id FROM UserProfile").fetchall()
                individuals = cur.execute("SELECT id FROM Individual").fetchall()

            if len(users) > self.u:
                with Subtask(f'Deleting {len(users)-self.u} users', task):
                    cur.execute(f"DELETE FROM auth_user ORDER BY id DESC LIMIT {len(users)-self.u}")
                    cur.execute(f"DELETE FROM UserProfile ORDER BY id DESC LIMIT {len(users)-self.u}")
                    users = cur.execute("SELECT id, username FROM auth_user").fetchall()
                    profiles = cur.execute("SELECT id FROM UserProfile").fetchall()
                    assert(len(users) == self.u)
                    assert(len(users) == len(profiles))

            elif len(users) < self.u:
                with Subtask(f'Adding {self.u-len(users)} users', task):
                    to_insert = [f'({i}, "pbkdf2_sha256$12000$iV0sZ7R8KrVJ$xcIuLw2+ucijlFbCtpRFIy3DxlIANgGjZxJP1pa4kVo=", ' + \
                                 '"2023-01-01 00:00:00.000000", ' + \
                                 f'1, "user{i}", "firstname", "lastname", "user{i}@example.com", 1, 1, "2023-01-01 00:00:00.000000")'
                                 for i in range(len(users), self.u)]
                    cur.execute((f"INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, "
                                f"last_name, email, is_staff, is_active, date_joined) VALUES {','.join(to_insert)}"))
                    to_insert3 = [f'(1, "ZoVcQUlZnhtk9XwKE3XanV4g832Y1eMs{i}", ";UserProfile__userprofilelabel__ZoVcQUlZnhtk9XwKE3XanV4g832Y1eMs{i}=1;", "user{i}", "user{i}@example.com", "name", {i})' for i in range(len(users), self.u)]
                    cur.execute(f"INSERT INTO UserProfile (profiletype, jeeves_id, jeeves_vars, username, email, name, individual_id) VALUES {','.join(to_insert3)}")
                    users = cur.execute("SELECT id, username FROM auth_user").fetchall()
                    profiles = cur.execute("SELECT id FROM UserProfile").fetchall()
                    assert(len(users) == self.u)
                    assert(len(users) == len(profiles))

            if len(individuals) > self.n:
                with Subtask(f'Deleting {len(individuals)-self.n} users', task):
                    cur.execute(f'DELETE FROM Individual ORDER BY id DESC LIMiT {len(individuals)-self.n}')
                    individuals = cur.execute("SELECT id FROM Individual").fetchall()
                    assert(len(individuals) == self.n)
                
            elif len(individuals) < self.n:
                with Subtask(f'Adding {self.n-len(individuals)} individuals', task):
                    to_insert2 = [f'({i}, "OtPUfgXiq03CkGaVSduEfEjfmvJB8CcB{i}", ";Individual__Individuallabel1__OtPUfgXiq03CkGaVSduEfEjfmvJB8CcB{i}=1;Individual__Individuallabel_ForReligiousAffiliation__OtPUfgXiq03CkGaVSduEfEjfmvJB8CcB{i}=1;", "firstname", "lastname", "user{i}@example.com", "{random.choice(["Male", "Female"])}", "2023-01-01")'
                                  for i in range(len(individuals), self.n)]
                    cur.execute(f"INSERT INTO Individual (id, jeeves_id, jeeves_vars, FirstName, Lastname, Email, Sex, BirthDate) VALUES {','.join(to_insert2)}")
                    individuals = cur.execute("SELECT id FROM Individual").fetchall()
                    assert(len(individuals) == self.n)
                    
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Starting Hipaa Jacqueline application'):
            self.proc = Popen(self.app_cmd, cwd=self.cwd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True)
            sleep(1)

        with Task(pref, 'Logging in with user 0') as task:
            self.session = requests.Session()
            soup = BeautifulSoup(self.session.get(self.login_url).content, features='html.parser')
            csrftoken = soup.find('input', {"name": 'csrfmiddlewaretoken'})['value']
            r = self.session.post(self.login_url, data={"username": "user0", "password": "password",
                                                        "csrfmiddlewaretoken": csrftoken})
            assert(r.ok)

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        if self.sc == 'submit_paper':
            with Task('hipaa.run', 'Deleting last entity'):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                cur.execute(f"DELETE FROM CoveredEntity ORDER BY id DESC LIMIT 1")
                cur.close()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'view_index':
            with Task('hipaa.run', 'Measurement for scenario "View index"'):
                r = self.session.get(self.view_index)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'view_patient':
            with Task('hipaa.run', 'Measurement for scenario "View patient"'):
                r = self.session.get(self.view_patient.format(uid=random.randint(0, self.u-1)))
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('hipaa.finalize', 'Killing Hipaa Jacqueline application'):
            self.proc.kill()

class Application:
    
    app_cmd  = ["python2", "manage.py", "runserver"]
    cwd      = "baseline/hipaa"
    database = "baseline/hipaa/jelf/jelf.db"
    default_pdf = "mathgen.pdf"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('hipaa.start', 'Starting evaluation') as task:
            
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                # "Transaction" seems to be some other sqlite3 tables, need to be quoted.
                tables = ['Address','BusinessAssociateAgreement','CoveredEntity','Diagnosis','DiagnosisTransfer',
                          'HospitalVisit','HospitalVisitTransfer','Individual','InformationTransferSet',
                          'PersonalRepresentative','PersonalRepresentative','\"Transaction\"','Treatment', 'UserProfile',
                          'auth_group','auth_group_permissions','auth_permission','auth_user','auth_user_groups',
                          'auth_user_user_permissions']
                for t in tables:
                    cur.execute(f"DELETE FROM {t} WHERE 1=1")
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('hipaa.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('hipaa.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('view_patient', self.app_cmd, self.cwd, self.database, self.default_pdf),
                         Scenario('view_index', self.app_cmd, self.cwd, self.database, self.default_pdf)
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('hipaa.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 5
            u_def = 256
            us = [4**i for i in range(0, N_u+1)]
            # u: number of users
            N_n = 5
            n_def = 256
            ns = [4**i for i in range(0, N_u+1)]
            # configs = us
            configs = [(u_def, n) for n in ns] + [(u, n_def) for u in us]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
    
