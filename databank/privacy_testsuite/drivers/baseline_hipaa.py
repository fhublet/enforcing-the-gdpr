from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import shutil
import os

from bs4 import BeautifulSoup

from werkzeug.security import generate_password_hash

from tools import Task, Subtask

class Scenario:

    login_url = "http://localhost:5000/user/sign-in"
    view_patient = "http://localhost:5000/patients/{uid}/" #Show info of a patient
    view_covered_entity = "http://localhost:5000/entities/{eid}/" #The page show info of a covered entity
    view_index = "http://localhost:5000/index" #The main page shows patients and entities.
    # Add more if you want additional scenarios
    # consult hipaa/urls.py for more endpoints

    def __init__(self, sc, app_cmd, cwd, database):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database

    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'hipaa.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Initializing database', cplx=True) as task:
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                individuals = cur.execute("SELECT id FROM Individual").fetchall()
                users = cur.execute("SELECT username FROM UserProfile").fetchall()

            if len(users) > self.u:
                with Subtask(f'Deleting {len(users)-self.u} users', task):
                    cur.execute(f"DELETE FROM UserProfile ORDER BY id DESC LIMIT {len(users)-self.u}")
                    users = cur.execute("SELECT id FROM UserProfile").fetchall()
                    assert(len(users) == self.u)

            elif len(users) < self.u:
                with Subtask(f'Adding {self.u-len(users)} users', task):
                    to_insert = [f'("user{i}", "$2b$12$i7QHBmsx9isVVoVCb1V5m.jlgoFfquuxj6L7G9ft8ScGrdtVYmAVC"' + \
                                 f', "user{i}@example.com", "name", {i})' for i in range(len(users), self.u)]
                    cur.execute(f"INSERT INTO UserProfile (username, password, email, name, individualID) VALUES {','.join(to_insert)}")
                    users = cur.execute("SELECT id, username FROM UserProfile").fetchall()
                    assert(len(users) == self.u)

            if len(individuals) > self.u*self.n:
                with Subtask(f'Deleting {len(individuals)-self.u*self.n} users', task):
                    cur.execute(f"DELETE FROM Individual ORDER BY id DESC LIMIT {len(individuals)-self.u*self.n}")
                    individuals = cur.execute("SELECT id FROM Individual").fetchall()
                    assert(len(individuals) == self.u*self.n)

            elif len(individuals) < self.u*self.n:
                with Subtask(f'Adding {self.u*self.n-len(individuals)} individuals', task):
                    to_insert = [f'({i}, "firstname", "lastname", "user{i}@example.com", "{random.choice(["Male", "Female"])}", "2023-01-01")'
                                 for i in range(len(individuals), self.u*self.n)]
                    cur.execute(f"INSERT INTO Individual (id, FirstName, Lastname, Email, Sex, BirthDate) VALUES {','.join(to_insert)}")
                    individuals = cur.execute("SELECT id FROM Individual").fetchall()
                    assert(len(individuals) == self.u*self.n)
                                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Starting Flask HIPAA application'):
            self.proc = Popen(self.app_cmd, cwd=self.cwd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True)
            sleep(1)

        with Task(pref, 'Logging in with user 0') as task:
            self.session = requests.Session()
            soup = BeautifulSoup(self.session.get(self.login_url).content, features='html.parser')
            csrftoken = soup.find('input', {"name": 'csrf_token'})['value']
            r = self.session.post(self.login_url, data={"username": f"user0", "password": f"5NWSM8USeUZBGxc",
                                                        "csrf_token": csrftoken})
            assert(r.ok)

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        if self.sc == 'submit_paper':
            with Task('hipaa.run', 'Deleting last entity'):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                cur.execute(f"DELETE FROM CoveredEntity ORDER BY id DESC LIMIT 1")
                cur.close()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'view_index':
            with Task('hipaa.run', 'Measurement for scenario "View index"'):
                r = self.session.get(self.view_index)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'view_patient':
            with Task('hipaa.run', 'Measurement for scenario "View patient"'):
                r = self.session.get(self.view_patient.format(uid=random.randint(0, self.u*self.n-1)))
                with open('test.html', 'w') as f:
                    f.write(r.text)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('hipaa.finalize', 'Killing Hipaa Flask application'):
            self.proc.kill()

class Application:
    
    app_cmd  = ["python3", "src/app.py"]
    cwd      = "baseline/hipaa-flask"
    database = "baseline/hipaa-flask/instance/app.db"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('hipaa.start', 'Starting evaluation') as task:
            
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                # "Transaction" seems to be some other sqlite3 tables, need to be quoted.
                tables = ['Address','BusinessAssociateAgreement','CoveredEntity','Diagnosis','DiagnosisTransfer',
                          'HospitalVisit','HospitalVisitTransfer','Individual','InformationTransferSet',
                          'PersonalRepresentative','PersonalRepresentative','\"Transaction\"','Treatment', 'UserProfile']
                          #'auth_group','auth_group_permissions','auth_permission','auth_user','auth_user_groups',
                          #'auth_user_user_permissions']
                for t in tables:
                    cur.execute(f"DELETE FROM {t} WHERE 1=1")
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('hipaa.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('hipaa.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('view_patient', self.app_cmd, self.cwd, self.database),
                         Scenario('view_index', self.app_cmd, self.cwd, self.database)
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('hipaa.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 4
            u_def = 4**4
            us = [4**i for i in range(0, N_u+1)]
            # n: number of individuals
            N_n = 4
            n_def = 4**4
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us x ns
            configs = [(u, n_def) for u in us] + [(u_def, n) for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
    
