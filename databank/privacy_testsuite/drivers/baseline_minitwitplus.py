from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests

from werkzeug.security import generate_password_hash
from lorem_text import lorem


from tools import Task, Subtask


class Scenario:

    login_url = "http://localhost:5000/login"
    timeline_url = "http://localhost:5000/public"
    add_message_url = "http://localhost:5000/add_message"

    def __init__(self, sc, app_cmd, database):
        self.sc = sc
        self.app_cmd = app_cmd
        self.database = database

    def generate_random_message(self, users):
        author = random.choice(users)[0]
        text = lorem.paragraph()
        date = int(round(datetime.now().timestamp()))
        return f'("{author}", "{text}", "{date}")'
        
    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'minitwitplus.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Initializing database', cplx=True) as task:
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                users = cur.execute("SELECT user_id FROM user").fetchall()
                messages = cur.execute("SELECT message_id FROM message").fetchall()

            if len(users) > self.u:
                with Subtask(f'Deleting {len(users)-self.u} users', task):
                    cur.execute(f"DELETE FROM user ORDER BY user_id DESC LIMIT {len(users)-self.u}")
                    users = cur.execute("SELECT user_id FROM user").fetchall()
                    assert(len(users) == self.u)

            elif len(users) < self.u:
                with Subtask(f'Adding {self.u-len(users)} users', task):
                    to_insert = [f'("user{i}", "user{i}@mail.com", "' + generate_password_hash(f'password{i}') + '")'
                                 for i in range(len(users), self.u)]
                    cur.execute(f"INSERT INTO user (username, email, pw_hash) VALUES {','.join(to_insert)}")
                    users = cur.execute("SELECT username FROM user").fetchall()
                    assert(len(users) == self.u)

            if len(messages) > self.n*self.u:
                with Subtask(f'Deleting {len(messages)-self.n*self.u} messages', task):
                    cur.execute(f"DELETE FROM message ORDER BY message_id DESC LIMIT {len(messages)-self.n*self.u}")
                    messages = cur.execute("SELECT message_id FROM message").fetchall()
                    assert(len(messages) == self.n*self.u)
                    
            elif len(messages) < self.n*self.u:
                with Subtask(f'Adding {self.n*self.u-len(messages)} messages', task):
                    to_insert = [self.generate_random_message(users) for _ in range(len(messages), self.n*self.u)]
                    cur.execute(f"INSERT INTO message (author_id, text, pub_date) VALUES {','.join(to_insert)}")
                    messages = cur.execute("SELECT message_id FROM message").fetchall()
                    assert(len(messages) == self.n*self.u)
            
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Starting Minitwit Flask application'):
            self.proc = Popen(self.app_cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True)
            sleep(1)

        with Task(pref, 'Logging in with user 1') as task:
            user1 = users[0]
            self.session = requests.Session()
            r = self.session.post(self.login_url, data={"username": "user0", "password": "password0"})
            assert(r.ok)

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        if self.sc == 'add_message':
            with Task('minitwitplus.run', 'Deleting last message'):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                cur.execute(f"DELETE FROM message ORDER BY message_id DESC LIMIT 1")
                cur.close()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'add_message':
            with Task('minitwitplus.run', 'Measurement for scenario "Add message"'):
                r = self.session.post(self.add_message_url, data={"text": lorem.paragraph()}, allow_redirects=False)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'timeline':
            with Task('minitwitplus.run', 'Measurement for scenario "Timeline"'):
                r = self.session.get(self.timeline_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('minitwitplus.finalize', 'Killing Minitwit Flask application'):
            self.proc.kill()

class Application:
    
    app_cmd  = ["python3", "baseline/minitwitplus/minitwit.py"]
    database = "db/minitwitplus.db"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('minitwitplus.start', 'Starting evaluation') as task:
            
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f"DELETE FROM user WHERE 1=1")
                cur.execute(f"DELETE FROM message WHERE 1=1")
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('minitwitplus.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('minitwitplus.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('add_message', self.app_cmd, self.database),
                         Scenario('timeline', self.app_cmd, self.database),
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('minitwitplus.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 4
            u_def = 4**4
            us = [4**i for i in range(0, N_u+1)]
            # n: number of messages
            N_n = 4
            n_def = 4**4
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us + ns
            configs = [(u, n_def) for u in us] + [(u_def, n) for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
