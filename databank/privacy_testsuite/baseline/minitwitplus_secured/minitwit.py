# -*- coding: utf-8 -*-
"""
    MiniTwit
    ~~~~~~~~

    A microblogging application written with Flask and sqlite3.

    :copyright: (c) 2015 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.

    [FH] From Flask release 0.12.4
         https://github.com/pallets/flask/releases/tag/0.12.4
"""

import time
from sqlite3 import dbapi2 as sqlite3
from hashlib import md5
from datetime import datetime
from flask import Flask, request, session, url_for, redirect, \
     render_template, abort, g, flash, _app_ctx_stack
from werkzeug.security import check_password_hash, generate_password_hash


# configuration
DATABASE = 'db/minitwitplus.db'
PER_PAGE = 30
DEBUG = True
SECRET_KEY = 'development key'

# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('MINITWIT_SETTINGS', silent=True)

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    db = getattr(g, 'sqlite_db', None)
    if db is None:
        db = g._database = sqlite3.connect(app.config['DATABASE'])
    return db


@app.teardown_appcontext
def close_database(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'sqlite_db'):
        top.sqlite_db.close()
        

def query_db(query, args=(), one=False):
    """Queries the database and returns a list of dictionaries."""
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    return (rv[0] if rv else None) if one else rv


def get_user_id(username):
    """Convenience method to look up the id for a username."""
    rv = query_db('select user_id from user where username = ?',
                  [username], one=True)
    return rv[0] if rv else None


def format_datetime(timestamp):
    """Format a timestamp for display."""
    return datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d @ %H:%M')


@app.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = query_db('select * from user where user_id = ?',
                          [session['user_id']], one=True)

        
def generate_ad(messages):
    """Generates personalized advertisement based on the user browsing."""
    # Checking if there is a user in session:
    # if not, indicate as guest, showing ads as normal
    if g.user:
        # otherwise, assuming that email is used
        # checking if there is a consent for reading email
        consented = False
        consented = query_db('''select 1 from consent c 
                                where c.user = ? and c.purpose = ? and value == 1''',
            [session['user_id'], 'generate advertisements'],
            one=True) is not None
        if not consented:
            return None
    all_message_texts = "".join([message[2] for message in messages])
    if "editor" in all_message_texts:
        return {"title": "Emacs",
                "text":  "... is the best text editor",
                "link": "https://emacs.org"}
    elif "OS" in all_message_texts:
        return {"title": "Linux",
                "text": "... has the best kernel",
                "link": "https://linux.org"}
    else:
        return {"title": "Lausanne",
                "text": "... has the best conferences",
                "link": "https://lausanne.ch"}

    
@app.route('/')
def timeline():
    """Shows a users timeline or if no user is logged in it will
    redirect to the public timeline.  This timeline shows the user's
    messages as well as all the messages of followed users.
    """
    if not g.user:
        return redirect(url_for('public_timeline'))
    # Check if the user allow his followship info to be use for better display
    consented = False
    consented = query_db('''select 1 from consent c 
                            where c.user = ? and c.purpose = ? and value == 1''',
        [session['user_id'], 'display relevant timeline'],
        one=True) is not None
    if not consented:
        return redirect(url_for('public_timeline'))
    messages = query_db('''
        select message.*, user.* from message, user
        where message.author_id = user.user_id and (
            user.user_id = ? or
            user.user_id in (select whom_id from follower
                                    where who_id = ?))
        order by message.pub_date desc limit ?''',
       [session['user_id'], session['user_id'], PER_PAGE])
    return render_template('timeline.html', messages=messages,
                           ad=generate_ad(messages))


# works
@app.route('/public')
def public_timeline():
    """Displays the latest messages of all users."""
    messages = query_db('''
        select message.*, user.* from message, user
        where message.author_id = user.user_id
        order by message.pub_date desc limit ?''', [PER_PAGE])
    return render_template('timeline.html', messages=messages, ad=generate_ad(messages))

#works
@app.route('/<username>')
def user_timeline(username):
    """Display's a users tweets."""
    profile_user = query_db('select * from user where username = ?',
                            [username], one=True)
    if profile_user is None:
        abort(404)
    followed = False
    if g.user:
        followed = query_db('''select 1 from follower where
            follower.who_id = ? and follower.whom_id = ?''',
            [session['user_id'], profile_user[0]],
            one=True) is not None
    messages = query_db('''
            select message.*, user.* from message, user where
            user.user_id = message.author_id and user.user_id = ?
            order by message.pub_date desc limit ?''',
            [profile_user[0], PER_PAGE])
    return render_template('timeline.html', messages=messages, followed=followed,
                           profile_user=profile_user, ad=generate_ad(messages))

# works
@app.route('/<username>/follow')
def follow_user(username):
    """Adds the current user as follower of the given user."""
    if not g.user:
        abort(401)
    whom_id = get_user_id(username)
    if whom_id is None:
        abort(404)
    db = get_db()
    db.execute('insert into follower (who_id, whom_id) values (?, ?)',
              [session['user_id'], whom_id])
    db.commit()
    flash('You are now following "%s"' % username)
    return redirect(url_for('user_timeline', username=username))

# works
@app.route('/<username>/unfollow')
def unfollow_user(username):
    """Removes the current user as follower of the given user."""
    if not g.user:
        abort(401)
    whom_id = get_user_id(username)
    if whom_id is None:
        abort(404)
    db = get_db()
    db.execute('delete from follower where who_id=? and whom_id=?',
              [session['user_id'], whom_id])
    db.commit()
    flash('You are no longer following "%s"' % username)
    return redirect(url_for('user_timeline', username=username))

# works
@app.route('/add_message', methods=['POST'])
def add_message():
    """Registers a new message for the user."""
    if 'user_id' not in session:
        abort(401)
    if request.form['text']:
        db = get_db()
        db.execute('''insert into message (author_id, text, pub_date)
          values (?, ?, ?)''', (session['user_id'], request.form['text'],
                                int(time.time())))
        db.commit()
        flash('Your message was recorded')
    return redirect(url_for('timeline'))

# works for both
@app.route('/login', methods=['GET', 'POST'])
def login():
    """Logs the user in."""
    if g.user:
        return redirect(url_for('timeline'))
    error = None
    if request.method == 'POST':
        user = query_db('''select * from user where
            username = ?''', [request.form['username']], one=True)
        if user is None:
            error = 'Invalid username'
        elif not check_password_hash(user[3],
                                     request.form['password']):
            error = 'Invalid password'
        else:
            flash('You were logged in')
            session['user_id'] = user[0]
            return redirect(url_for('timeline'))
    return render_template('login.html', error=error)


# works for GET and POST
@app.route('/register', methods=['GET', 'POST'])
def register():
    """Registers the user."""
    if g.user:
        return redirect(url_for('timeline'))
    error = None
    if request.method == 'POST':
        if not request.form['username']:
            error = 'You have to enter a username'
        elif not request.form['email'] or \
                '@' not in request.form['email']:
            error = 'You have to enter a valid email address'
        elif not request.form['password']:
            error = 'You have to enter a password'
        elif request.form['password'] != request.form['password2']:
            error = 'The two passwords do not match'
        elif get_user_id(request.form['username']) is not None:
            error = 'The username is already taken'
        else:
            db = get_db()
            cursor = db.execute('''insert into user (
              username, email, pw_hash) values (?, ?, ?) returning user_id''',
              [request.form['username'], request.form['email'],
               generate_password_hash(request.form['password'])])
            # Also need to add consent for email
            user_id = cursor.fetchone()[0]
            db.execute('insert into consent (user, purpose, data, value) values (?,?,\'messages\',1)', [user_id, 'generate advertisements'])
            db.execute('insert into consent (user, purpose, data, value) values (?,?,\'follower\',1)', [user_id, 'display relevant timeline'])
            db.commit()
            flash('You were successfully registered and can login now')
            return redirect(url_for('login'))
    return render_template('register.html', error=error)

# works
@app.route('/logout')
def logout():
    """Logs the user out."""
    flash('You were logged out')
    session.pop('user_id', None)
    return redirect(url_for('public_timeline'))

# works
@app.route('/privacy', methods=['GET', 'POST'])
def privacy():
    """Displays the privacy of a user."""
    if request.method == 'GET':
        consents = query_db('''
            select consent.* from consent
            where user = ?''', [session['user_id']])
        return render_template('privacy.html', consents=consents)
    else:
        db = get_db()
        consents = query_db('''
            select consent.* from consent
            where user = ?''', [session['user_id']])
        for c in consents:
            c_id = c[0]
            keys=[i for i in request.form.to_dict().keys()]
            if str(c_id) in keys:
                db.execute('''update consent set value = ? where consent_id = ?''', [1, c_id])
            else:
                db.execute('''update consent set value = ? where consent_id = ?''', [0, c_id])
        db.commit()
        return redirect(url_for('public_timeline'))

# add some filters to jinja
app.jinja_env.filters['datetimeformat'] = format_datetime

if __name__ == '__main__':
    app.run()
