To run, follow these steps:

1. Create a db directory.
mkdir db

2. Initialize database
sqlite3 db/minitwitplus_secured.db -init schema.sql

3. set running app @ minitwit.py
export FLASK_APP=minitwit.py
set FLASK_APP=minitwit.py

4. Run
flask run