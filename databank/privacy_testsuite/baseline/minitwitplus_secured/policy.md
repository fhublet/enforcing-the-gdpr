# Security policy

User class: User

A user can create a message.
A user can initialize the authorship of the message with his own credential.
A user can initialize the text and pub_date of the message of his own message.

A user can read any message pub_date.
A user can read the text and authorship of a message if he is (i) its owner or (ii) its owner's followers or (iii) it is an advertisement message.

==> This is tangledly done within the business logic already - todo: add line no.

# Privacy policy

User class: User
Personal Data: follow, email. (add to db - done)
Purpose: Advertisement, Analytics (add to db - done)

Personal data x User: 
The follower is the ownership of the personal data follow. ---> When follow(), unfollow() add/delete personaldata and consent (done)
The user owns their own email information. ---> when register add personaldata and consent (done)
Need to have a simple page to add/remove consent (todo)

We use your information about whom you follow to display relevant tweets. ---> Done.
We use your email to display relevant advertisement. ---> Done.
