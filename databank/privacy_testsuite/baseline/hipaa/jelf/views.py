"""Views for HIPAA case study.

    :synopsis: Code for displaying HIPAA demo pages.

.. moduleauthor:: Ariel Jacobs <arielj@mit.edu> #nc
.. moduleauthor:: Jean Yang <jeanyang@csail.mit.edu> #nc
"""
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect #, HttpResponse
#from django.contrib.auth.models import User
#from django.views import generic
from datetime import date
#import urllib
#import random

from jelf.models import Diagnosis, Individual, CoveredEntity, UserProfile, \
    Transaction, Treatment

from sourcetrans.macro_module import macros, jeeves
import JeevesLib

def register_account(request): #nc
    """Account registration.
    """
    if request.user.is_authenticated():
        return HttpResponseRedirect("index")

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            #print user.username
            #print request.POST.get('email', '')
            #print request.POST.get('name', '')
            user.save()

            profiletype = request.POST.get('profiletype', '')
            UserProfile.objects.create(
                  username=user.username
                , email=request.POST.get('email', '')
                , name=request.POST.get('name', '')
                , profiletype=int(profiletype))
            user = authenticate(username=request.POST['username'],
            password=request.POST['password1'])
            login(request, user)
            return HttpResponseRedirect("index")
    else:
        form = UserCreationForm()

    return render_to_response("registration/account.html"
        , RequestContext(request,
		        {'form' : form
           , 'which_page' : "register"}))

@jeeves
def add_to_context(context_dict, request, template_name, profile, concretize):
    """Adds relevant arguments to the context.
    """
    template_name = concretize(template_name)
    context_dict['concretize'] = concretize
    context_dict['profile'] = profile
    context_dict['is_logged_in'] = (request.user and
                                    request.user.is_authenticated() and
                                    (not request.user.is_anonymous()))

def request_wrapper(view_fn): #nc
    """Wraps requests by setting the current viewing context and fetching the
    profile associated with that context.
    """
    def real_view_fn(request, *args, **kwargs):
        try:
            profile = UserProfile.objects.get(username=request.user.username)
            ans = view_fn(request, profile, *args, **kwargs)
            template_name = ans[0]
            context_dict = ans[1]

            if template_name == "redirect":
                path = context_dict
                return HttpResponseRedirect(JeevesLib.concretize(profile, path))

            concretizeState = \
                JeevesLib.jeevesState.policyenv.getNewSolverState(profile)
            def concretize(val):
                return concretizeState.concretizeExp(
                    val, JeevesLib.jeevesState.pathenv.getEnv())
            add_to_context(
                context_dict, request, template_name, profile, concretize)
            return render_to_response(
                template_name, RequestContext(request, context_dict))
        except Exception:
            import traceback
            #traceback.print_exc()
            raise

    real_view_fn.__name__ = view_fn.__name__
    return real_view_fn

@login_required
@request_wrapper
@jeeves
def index(request, profile):
    """The main page shows patients and entities.
    """
    patients = Individual.objects.all()
    entities = CoveredEntity.objects.all()

    # TODO: Filter out ones you can't see?

    data = {"patients": patients
          , "entities": entities
          , 'name': profile.name}
    return ("index.html", data)

@request_wrapper
@jeeves
def about_view(request, user):
    """About the system.
    """
    # TODO: This doesn't work.
    return ("about.html"
            , {'which_page' : "about"})

@login_required
@request_wrapper
@jeeves
def profile_view(request, profile):
    """Displaying and updating profiles.
    """
    if profile == None:
        profile = UserProfile(username=request.user.username)

    if request.method == 'POST':
        profile.name = request.POST.get('name', '')
        profile.email = request.POST.get('email', '')
        profile.save()

    return ("profile.html", {
          "profile": profile
        , "which_page": "profile"})

@login_required
@request_wrapper
@jeeves
def users_view(request, profile):
    """Viewing all users.
    """
    profile = UserProfile(username=request.user.username)

    if profile.profiletype != 3: 
        return ("redirect", "/index")

    user_profiles = UserProfile.objects.all()

    if request.method == 'POST':
        for profile in user_profiles:
            query_param_name = 'level-' + profile.username
            level = request.POST.get(query_param_name, '')
            if level in ['individual', 'coveredEntity', 'businessAssociate', 'government', 'research', 'clergy']:
                profile.level = level
                profile.save()
#    profiletype = IntegerField(help_text="Type of body this user is about. \
#        0=None, 1=Individual, 2=CoveredEntity, 3=BusinessAssociate, \
#        4=Government, 5=Research, 6=Clergy")

    return ("users_view.html", {
        'user_profiles': user_profiles,
        'which_page' : "users"
    })

@login_required
@request_wrapper
@jeeves
def treatments_view(request, profile, patient):
    """Treatments.
    """
    p = Individual.objects.get(jeeves_id=patient)
    treatments = Treatment.objects.filter(patient=p)
    return ("treatments.html"
        , {"first_name" : p.FirstName
         , "last_name" : p.LastName
         , "treatments" : treatments})

@login_required
@request_wrapper
@jeeves
def diagnoses_view(request, profile, patient):
    """Diagnoses.
    """
    p = Individual.objects.get(jeeves_id=patient)
    newDiagnoses = Diagnosis.objects.filter(Patient=p)
    return ("diagnoses.html"
            , {"first_name" : p.FirstName
             , "last_name" : p.LastName
             , "diagnoses" : newDiagnoses})

@login_required
@request_wrapper
@jeeves
def info_view(request, profile, patient): # view_patient
    """Viewing information about an individual.
    """
    p = Individual.objects.get(id=patient)

    dataset = []
    dataset.append(("Sex", p.Sex, False))
    return ("info.html"
            , {"patient": p
               , "dataset": dataset
             })

@login_required
@request_wrapper
@jeeves
def directory_view(request, profile, entity):
    """Viewing covered entities.
    """
    entity = CoveredEntity.objects.get(ein=entity)
    visits = entity.Patients.filter(date_released=None)

    return ("directory.html"
            , {"entity":entity, "visits":visits})

@login_required
@request_wrapper
@jeeves
def transactions_view(request, profile, entity):
    """
    Viewing transactions.
    """
    entity = CoveredEntity.objects.get(ein=entity)
    transactions = Transaction.objects.filter(FirstParty=entity)
    other_transactions = Transaction.objects.filter(SecondParty=entity)
    return ("transactions.html"
            , {"entity": entity
             , "transactions":transactions
             , "other_transactions": other_transactions})

@login_required
@request_wrapper
@jeeves
def associates_view(request, profile, entity):
    entity = CoveredEntity.objects.get(ein=entity)
    associates = entity.Associations.all()
    return ("associates.html"
        , {"entity":entity, "associates":associates})
