from model import *
from datetime import date

def initData(db):
    """
    sampleData.py from Jeeves
    @HN: I have mapped the dummy data into this
    """
    #joesHouse=Address.objects.create(
    #	City="Cambridge",
    #    State="MA",
    #    Street="41 Home St.",
    #    ZipCode="14830")
    joesHouse = Address(
        City="Cambridge",
        State="MA",
        Street="41 Home St.",
        ZipCode="14830")
    db.session.add(joesHouse)
    db.session.commit()

    #arielsHouse=Address.objects.create(
    #	City="Cambridge",
    #	State="MA",
    #	Street="5 Y St.",
    #	ZipCode="14830")
    arielsHouse=Address(
	City="Cambridge",
	State="MA",
	Street="5 Y St.",
	ZipCode="14830")
    db.session.add(arielsHouse)
    db.session.commit()

    #suesHouse=Address.objects.create(
    #	City="Boston",
    #	State="MA",
    #	Street="1840 Healer",
    #	ZipCode="14830")
    suesHouse=Address(
	City="Boston",
	State="MA",
	Street="1840 Healer",
	ZipCode="14830")
    db.session.add(suesHouse)
    db.session.commit()

    #house1=Address.objects.create(
    #	City="Boston",
    #	State="MA",
    #	Street="625 Frost Ln.",
    #	ZipCode="14830")
    house1=Address(
	City="Boston",
	State="MA",
	Street="625 Frost Ln.",
	ZipCode="14830")
    db.session.add(house1)
    db.session.commit()

    #house2=Address.objects.create(
    #	City="Boston",
    #	State="MA",
    #	Street="9 Xing",
    #	ZipCode="14830")
    house2=Address(
	City="Boston",
	State="MA",
	Street="9 Xing",
	ZipCode="14830")
    db.session.add(house2)
    db.session.commit()

    #house3=Address.objects.create(
    #	City="Boston",
    #	State="MA",
    #	Street="13 Healer",
    #	ZipCode="14830")
    house3=Address(
	City="Boston",
	State="MA",
	Street="13 Healer",
	ZipCode="14830")
    db.session.add(house3)
    db.session.commit()

    #house4=Address.objects.create(
    #	City="Seattle",
    #	State="WA",
    #	Street="1747 Gray Rd.",
    #	ZipCode="14830")
    house4=Address(
	City="Seattle",
	State="WA",
	Street="1747 Gray Rd.",
	ZipCode="14830")
    db.session.add(house4)
    db.session.commit()

    #house5=Address.objects.create(
    #	City="Cambridge",
    #	State="MA",
    #	Street="7 Backends",
    #	ZipCode="14830")
    house5=Address(
	City="Cambridge",
	State="MA",
	Street="7 Backends",
	ZipCode="14830")
    db.session.add(house5)
    db.session.commit()

    '''
    Individuals.
    '''
    #jean=Individual.objects.create(
    #    FirstName="Jean"
    #  , LastName="Yang"
    #  , Email="jean@example.com"
    #  , Sex="Female"
    #  , BirthDate=date(1900,01,01)
    #  , Address=house1)
    jean=Individual(
        FirstName="Jean"
      , LastName="Yang"
      , Email="jean@example.com"
      , Sex="Female"
      , BirthDate = date(1979,2,13)
      , Address=house1)
    db.session.add(jean)
    db.session.commit()

    #ariel=Individual.objects.create(
    #	FirstName="Ariel",
    #	LastName="Jacobs",
    #	Email="ariel@example.com",
    #	Sex="Male",
    #	BirthDate=date(1993,03,21),
    #	Address=arielsHouse)
    ariel=Individual(
	FirstName="Ariel",
	LastName="Jacobs",
	Email="ariel@example.com",
	Sex="Male",
	BirthDate = date(1979,2,13),
	Address=arielsHouse)
    db.session.add(ariel)
    db.session.commit()

    #joe=Individual.objects.create(FirstName="Joe",
    #    LastName="McGrand",
    #    BirthDate = date(1979,2,13),
    #    Address = joesHouse,
    #    FaxNumber = "2054291012",
    #    TelephoneNumber = "1431439883",
    #    ReligiousAffiliation = "None",
    #    SSN = "193248873",
    #    Sex = "Male",
    #    Email = "joe@example.com",
    #    DriversLicenseNumber = "J5938532984",
    #    Employer = "Macro Creations, Inc.")
    joe=Individual(FirstName="Joe",
    LastName="McGrand",
    BirthDate = date(1979,2,13),
    Address = joesHouse,
    FaxNumber = "2054291012",
    TelephoneNumber = "1431439883",
    ReligiousAffiliation = "None",
    SSN = "193248873",
    Sex = "Male",
    Email = "joe@example.com",
    DriversLicenseNumber = "J5938532984",
    Employer = "Macro Creations, Inc.")
    db.session.add(joe)
    db.session.commit()

    #sue=Individual.objects.create(FirstName="Sue",
    #    LastName="Huff",
    #    Email="sue@example.com",
    #    Address=suesHouse,
    #    Sex="Female",
    #    SSN="582081439")
    sue=Individual(FirstName="Sue",
    LastName="Huff",
    Email="sue@example.com",
    Address=suesHouse,
    Sex="Female",
    SSN="582081439")
    db.session.add(sue)
    db.session.commit()

    #greg=Individual.objects.create(FirstName="Greg",
    #    LastName="Huff",
    #    Address=suesHouse,
    #    Sex="Male",
    #    Email="greg@mit.example.com",
    #    TelephoneNumber="2845030411",
    #    ReligiousAffiliation="Christian")
    greg=Individual(FirstName="Greg",
    LastName="Huff",
    Address=suesHouse,
    Sex="Male",
    Email="greg@mit.example.com",
    TelephoneNumber="2845030411",
    ReligiousAffiliation="Christian")
    db.session.add(greg)
    db.session.commit()

    #bob=Individual.objects.create(
    #	FirstName="Bob",
    #	LastName="Dreck",
    #	Email="bob@national.example.com",
    #	Sex="Male",
    #	Address=house1,
    #	TelephoneNumber="4543398401",
    #	Employer = "Installations and Repair")
    bob=Individual(
	FirstName="Bob",
	LastName="Dreck",
	Email="bob@national.example.com",
	Sex="Male",
	Address=house1,
	TelephoneNumber="4543398401",
	Employer = "Installations and Repair")
    db.session.add(bob)
    db.session.commit()

    #justin = Individual.objects.create(FirstName="Justin",
    #    LastName="Foo",
    #    Address=house2,
    #    Email="justin@example.com",
    #    Sex="Female",
    #    TelephoneNumber="5240198310",
    #    ReligiousAffiliation="Catholic",
    #    Employer = "Foo Market")
    justin = Individual(FirstName="Justin",
    LastName="Foo",
    Address=house2,
    Email="justin@example.com",
    Sex="Female",
    TelephoneNumber="5240198310",
    ReligiousAffiliation="Catholic",
    Employer = "Foo Market")
    db.session.add(justin)
    db.session.commit()

    '''
    Covered Entities.
    '''
    #vision= CoveredEntity.objects.create(ein = "01GBS253DV"
    #    , name = "Vision National")
    vision= CoveredEntity(ein = "01GBS253DV"
    , name = "Vision National")
    db.session.add(vision)
    db.session.commit()

    #health = CoveredEntity.objects.create(ein = "0424D3294N"
    #    , name = "Covered Health")
    health = CoveredEntity(ein = "0424D3294N"
    , name = "Covered Health")
    db.session.add(health)
    db.session.commit()

    '''
    Hospital visits.
    '''
    #visit1 = HospitalVisit.objects.create(patient=joe,
    #    date_admitted=date(2003,4,1),
    #    date_released=date(2003,9,13),
    #    condition="Good",
    #    location="Third room on the left",
    #    hospital=vision)
    visit1 = HospitalVisit(patient=joe,
    date_admitted=date(2003,4,1),
    date_released=date(2003,9,13),
    condition="Good",
    location="Third room on the left",
    hospital=vision)
    db.session.add(visit1)
    db.session.commit()

    '''
    Treatments.
    '''
    #treat1 = Treatment.objects.create(patient=ariel,
    #    date_performed=date(2014,1,1),
    #    performing_entity = health,
    #    prescribing_entity = health,
    #    service = "W4-491")
    treat1 = Treatment(patient=ariel,
    date_performed=date(2014,1,1),
    performing_entity = health,
    prescribing_entity = health,
    service = "W4-491")
    db.session.add(treat1)
    db.session.commit()

    '''
    Diagnoses.
    '''
    #diag1 = Diagnosis.objects.create(
    #    DateRecognized=date(2012,9,2),
    #    RecognizingEntity = vision,
    #    Diagnosis = "Positive",
    #    Manifestation = "Obesity",
    #    Patient = ariel,
    #)
    diag1 = Diagnosis(
    DateRecognized=date(2012,9,2),
    Diagnosis = "Positive",
    Manifestation = "Obesity")
    diag1.RecognizingEntity = vision
    diag1.Patient = ariel
    db.session.add(diag1)
    db.session.commit()
    

    '''
    Some other stuff.
    TODO: Figure out what this is.
    '''
    #data = InformationTransferSet.objects.create()
    #data.save()
    data = InformationTransferSet()
    db.session.add(data)
    db.session.commit()

    #dataVisit = HospitalVisitTransfer.objects.create(Set=data, Visit=visit1)
    #dataVisit.save()
    dataVisit = HospitalVisitTransfer(Set=data, Visit=visit1)
    db.session.add(dataVisit)
    db.session.commit()

    #dataTreatment = TreatmentTransfer.objects.create(Set=data, Treatment=treat1)
    #dataTreatment.save()
    dataTreatment = TreatmentTransfer(Set=data, Treatment=treat1)
    db.session.add(dataTreatment)
    db.session.commit()

    #dataDiagnosis = DiagnosisTransfer.objects.create(Set=data, Diagnosis=diag1)
    #dataDiagnosis.save()
    dataDiagnosis = DiagnosisTransfer(Set=data, Diagnosis=diag1)
    db.session.add(dataDiagnosis)
    db.session.commit()

    '''
    Transactions.
    '''
    #trans1 = Transaction.objects.create(
    #	Standard = "H-78F2",
    #	SharedInformation = data,
    #	FirstParty = vision,
    #	SecondParty = health,
    #	DateRequested = date(2014,3,4),
    #	DateResponded = date(2014,4,1),
    #	Purpose = "Synthesis of records")
    trans1 = Transaction(
	Standard = "H-78F2",
	SharedInformation = data,
	FirstParty = vision,
	SecondParty = health,
	DateRequested = date(2014,3,4),
	DateResponded = date(2014,4,1),
	Purpose = "Synthesis of records")
    db.session.add(trans1)
    db.session.commit()
    """Up to here"""

    #assoc = BusinessAssociate()
    #assoc.Covered=False
    #assoc.Name="The Best Tax Filers"
    #assoc.save()
    assoc = BusinessAssociate()
    assoc.Covered=False
    assoc.Name="The Best Tax Filers"
    db.session.add(assoc)
    db.session.commit()
    
    #association = assoc.Associations.create(SharedInformation=data, CoveredEntity = vision, Purpose="Tax Filing")
    association = BusinessAssociateAgreement(BusinessAssociate = assoc, SharedInformation = data, CoveredEntity = vision, Purpose = "Tax Filing")
    db.session.add(association)
    db.session.commit()

    #visit2=HospitalVisit()
    #visit2.Patient=justin
    #visit2.DateAdmitted=date(2014,4,10)
    #visit2.Hospital=vision
    #visit2.Location="369A"
    #visit2.Condition="Good"
    visit2=HospitalVisit(patient=justin, 
        date_admitted=date(2014,4,10),
        hospital=vision,
        location="369A",
        condition="Good")
    db.session.add(visit2)
    db.session.commit()

    #visit3=HospitalVisit()
    #visit3.Patient=greg
    #visit3.DateAdmitted=date(2011,8,10)
    #visit3.Hospital=vision
    #visit3.Location="245B"
    #visit3.Condition = "Improving"
    #visit3.save()
    visit3=HospitalVisit(patient=greg, 
        date_admitted=date(2011,8,10),
        hospital=vision,
        location="245B",
        condition="Improving")
    db.session.add(visit3)
    db.session.commit()

    #visit4=HospitalVisit()
    #visit4.Patient=sue
    #visit4.DateAdmitted=date(2013,8,10)
    #visit4.Hospital=vision
    #visit4.Location="454A"
    #visit4.Condition = "Steady"
    #visit4.DateReleased=date(2013,9,2)
    #visit4.save()
    visit4=HospitalVisit(patient=sue, 
        date_admitted=date(2013,8,10),
        hospital=vision,
        location="454A",
        condition="Steady",
        date_released=date(2013,9,2))
    db.session.add(visit4)
    db.session.commit()

    dataVisit1 = HospitalVisitTransfer(Set = data, Visit = visit2)
    db.session.add(dataVisit1)
    db.session.commit()
    dataVisit2 = HospitalVisitTransfer(Set = data, Visit = visit3)
    db.session.add(dataVisit2)
    db.session.commit()
    dataVisit3 = HospitalVisitTransfer(Set = data, Visit = visit4)
    db.session.add(dataVisit3)
    db.session.commit()
    #dataVisit = data.Visits.create(Visit = visit2)
    #dataVisit = data.Visits.create(Visit = visit3)
    #dataVisit = data.Visits.create(Visit = visit4)


    ###USERS###
    #jeanyang=User.objects.create_user(
    #      username="jeanyang"
    #    , password="hi")

    #jeanyangProfile=UserProfile.objects.create(
    #      profiletype=1
    #    , username="jeanyang"
    #    , email = "jeanyang@example.com"
    #    , name="Jean Yang"
    #    , individual=jean)
    jeanyang = UserProfile(username="jeanyang"
                           , password="hi"
                           , active=1, profiletype=1
                           , email="jeanyang@example.com"
                           , name="Jean Yang"
                           , individual=jean)
    db.session.add(jeanyang)
    db.session.commit()

    #arielj=User.objects.create_user(
    #      username="arielj321"
    #    , password="hipaaRules")

    #arielProfile=UserProfile.objects.create(
    #      profiletype=1
    #    , username="arielj321"
    #    , email="ariel@example.com"
    #    , name="Ariel Jacobs"
    #    , individual=ariel)
    arielj = UserProfile(username="arielj321", password="hipaaRules"
                           , active=1, profiletype=1
                           , email="ariel@example.com"
                           , name="Ariel Jacobs"
                           , individual=ariel)
    db.session.add(arielj)
    db.session.commit()

    #admin=User.objects.create_user(
    #    username="admin"
    #    , password="admin"
    #    )
    #adminProfile=UserProfile.objects.create(
    #    profiletype=3
    #    , username="admin"
    #    , email="admin@example.com"
    #    , name="Admin")
    admin = UserProfile(username="admin", password="admin"
                           , active=1, profiletype=3
                           , email="admin@example.com"
                           , name="Admin")
    db.session.add(admin)
    db.session.commit()

#mcJoe=User()
#mcJoe.username="mcjoe"
#mcJoe.first_name="Joe"
#mcJoe.last_name="McGrand"
#mcJoe.email="joe@example.com"
#mcJoe.set_password("hipaaRules")
#mcJoe.save()

#joeProfile=UserProfile()
#joeProfile.type=1
#joeProfile.user=mcJoe
#joeProfile.individual=joe
#joeProfile.save()
#'''

#'''
#vnational=User()
#vnational.username="vnational"
#vnational.set_password("hipaaRules")
#vnational.save()

#visionProfile=UserProfile()
#visionProfile.type=2
#visionProfile.user=vnational
#visionProfile.entity=vision
#visionProfile.save()

#bestTax = User()
#bestTax.username = "bestTax"
#bestTax.set_password("hipaaRules")
#bestTax.save()

#bestTaxProfile = UserProfile()
#bestTaxProfile.type = 3
#bestTaxProfile.user = bestTax
#bestTaxProfile.associate = assoc
#bestTaxProfile.save()
