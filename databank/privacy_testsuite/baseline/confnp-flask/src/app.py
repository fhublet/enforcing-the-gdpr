from flask import Flask, render_template, redirect, url_for, Response, request
from flask_user import UserManager, user_registered, current_user, login_required
from model import db, UserProfile, UserPCConflict, Paper, PaperCoauthor, PaperVersion, PaperPCConflict
from datetime import date

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['USER_APP_NAME'] = "Conference Management System"
app.config['USER_ENABLE_EMAIL'] = False      
app.config['USER_ENABLE_USERNAME'] = True    
app.config['USER_REQUIRE_RETYPE_PASSWORD'] = False
app.config['SECRET_KEY'] = '_5#yfasQ8sansaxec/][#1'
app.config['USER_UNAUTHORIZED_ENDPOINT'] = 'error'

@app.route('/error')
def error():
    msg = "You are not allowed to access this page: Not a User"
    return render_template('error.html', message = msg)

@user_registered.connect_via(app)
def after_user_registered_hook(sender, user, **extra):
    pass

db.init_app(app)

with app.app_context():
    db.drop_all()
    db.create_all()
    
user_manager = UserManager(app, db, UserProfile)

    #url(r'^submit$', views.submit_view),
    #url(r'^papers$', views.papers_view),
    #url(r'^paper$', views.paper_view),
    #url(r'^submit_review$', views.submit_review_view),
    #url(r'^submit_comment$', views.submit_comment_view),
    #url(r'^assign_reviews$', views.assign_reviews_view),
    #url(r'^search$', views.search_view),

@app.route('/accounts/login')
def login():
    return redirect(url_for('user.login'))

@app.route('/accounts/register')
def register():
    return redirect(url_for('user.register'))

@app.route('/accounts/logout')
def logout():
    return redirect(url_for('user.logout'))

@app.route('/accounts/profile')
@login_required
def profile():    
    pcs = UserProfile.query.filter_by(level='pc').all()
    if request.method == 'POST':
        current_user.email = request.form["email"]
        current_user.name = request.form["name"]
        current_user.affiliation = request.form["affiliation"]
        db.session.update(current_user)
        db.session.commit()
        return redirect(url_for("/accounts/profile"))
    else:
        pc_conflicts = [uppc.pc for uppc in UserPCConflict.query.filter_by(user=current_user.id).all()]
        data = {
        "name": current_user.name,
        "affiliation": current_user.affiliation,
        "acm_number": current_user.acm_number,
        "pc_conflicts": pc_conflicts,
        "email": current_user.email,
        "which_page": "profile",
        "pcs": [{'pc':pc, 'conflict':pc in pc_conflicts} for pc in pcs],
        }
        return render_template("profile.html", **data)

@app.route('/')
@app.route('/index')
def index():
    if not current_user.is_authenticated:
        return redirect(url_for('user.login'))

    data = { 'name' : current_user.name 
             , 'which_page': "home" }
    return render_template("index.html"
           , **data)

@app.route('/about')
def about_view():
    """About the system.
    """
    data = {'which_page' : "about"}
    return render_template("about.html", **data)

@app.route('/users')
@login_required
def users():
    if current_user.level != 'chair':
        return (   "redirect", "/index")

    user_profiles = UserProfile.query.all()

    if request.method == 'POST':
        for profile in user_profiles:
            query_param_name = 'level-' + profile.username
            level = request.form[query_param_name]
            if level in ['normal', 'pc', 'chair']:
                profile.level = level
                db.session.update(profile)
                db.session.commit()

    data = {
        'user_profiles': user_profiles,
        'which_pages' : "users"
    }
    return render_template("users_view.html", **data)

@app.route('/submit')
@login_required
def submit():
    if request.method == 'POST':
        coauthors = request.form.getlist("coauthors[]")        
        title = request.form['title']
        abstract = request.form['abstract']
        contents = request.form['contents']

        if title == None or abstract == None or contents == None:
            data = {
                'coauthors' : coauthors,
                'title' : title,
                'abstract' : abstract,
                'contents' : contents.name,
                'error' : 'Please fill out all fields',
                'which_page' : "submit",
            }
            return render_template("submit.html", **data)

        paper = Paper(author_user=current_user.id, accepted=False)
        db.session.add(paper)
        for coauthor in coauthors:
            if coauthor != "":
                papercoauthor = PaperCoauthor(coauthor_paper=paper,author=coauthor)
                db.session.add(papercoauthor)
        set_random_name(contents)
        version = PaperVersion(Paper=paper,title=title,abstract=abstract,contents=contents,time=date.today())
        db.session.add(version)

        for conf in request.form.getlist('pc_conflicts[]'):
            new_pc_conflict = UserProfile.query.get(username=conf)
            conflict = PaperPCConflict(conflict_paper=paper, conflict_pc=new_pc_conflict)
            db.session.add(conflict)
        db.session.commit()
        return redirect("redirect", "paper?id=" + str(paper.id))

    pcs = UserProfile.query.filter_by(level='pc').all()
    pc_conflicts = [uppc.pc for uppc in UserPCConflict.query.filter_by(user=current_user.id).all()]
    data = {
        'coauthors' : [],
        'title' : '',
        'abstract' : '',
        'contents' : '',
        'error' : '',
        "pcs": [{'pc':pc, 'conflict':pc in pc_conflicts} for pc in pcs],
        'pc_conflicts' : pc_conflicts,
        'which_page': "submit",
    }
    return render_template("submit.html", **data)

def set_random_name(contents):
    import random
    contents.name = '%030x' % random.randrange(16**30) + ".pdf"