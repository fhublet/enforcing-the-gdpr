"""Models for Conference Management System
@author: Hoang Nguyen

Similar to the ones in Jeeves
"""

from flask_sqlalchemy import SQLAlchemy
from flask_user import UserMixin
from sqlalchemy_file import FileField


db = SQLAlchemy()

class UserProfile(db.Model, UserMixin):
    __tablename__ = "user_profiles"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(1024))
    password = db.Column(db.String(255), nullable=False, server_default='')
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')
    email = db.Column(db.String(1024))
    name = db.Column(db.String(1024))
    affiliation = db.Column(db.String(1024))
    acm_number = db.Column(db.String(1024))
    level = db.Column(db.String(12))
    # choices = normal, pc, chair

class UserPCConflict(db.Model):
    __tablename__ = "user_pc_conflicts"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    userpcconflict_user = db.relationship('UserProfile', backref='userpcconflict_user', foreign_keys = [user])
    pc = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    userpcconflict_pc = db.relationship('UserProfile', backref='userpcconflict_pc', foreign_keys = [pc])

class PaperVersion(db.Model):
    __tablename__ = "paper_versions"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    paper = db.Column(db.Integer, db.ForeignKey("papers.id"), nullable=True)
    Paper = db.relationship('Paper', backref='versions', foreign_keys = [paper])
    title = db.Column(db.String(1024))
    contents = db.Column(FileField)
    abstract = db.Column(db.Text)
    time = db.Column(db.Date)
    
class Paper(db.Model):
    __tablename__ = "papers"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    author = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    author_user = db.relationship('UserProfile', backref='author')
    accepted = db.Column(db.Boolean)
    latest_version = db.Column(db.Integer, db.ForeignKey(PaperVersion.id), nullable=True)
    latest_version_of = db.relationship('PaperVersion', backref='latest_version_of', foreign_keys = [latest_version])

class PaperPCConflict(db.Model):
    __tablename__ = "paper_pc_conflicts"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    paper = db.Column(db.Integer, db.ForeignKey(Paper.id), nullable=True)
    conflict_paper = db.relationship('Paper', backref='conflict_papers')
    pc = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    conflict_pc = db.relationship('UserProfile', backref='conflict_pcs')
    
class PaperCoauthor(db.Model):
    __tablename__ = "paper_coauthors"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    paper = db.Column(db.Integer, db.ForeignKey(Paper.id), nullable=True)
    coauthor_paper = db.relationship('Paper', backref='coauthor_papers')
    author = db.Column(db.String(1024))

class ReviewAssignment(db.Model):
    __tablename__ = "review_assignments"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    paper = db.Column(db.Integer, db.ForeignKey(Paper.id), nullable=True)
    review_paper = db.relationship('Paper', backref='review_papers')
    user = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    review_assignment = db.relationship('UserProfile', backref='review_assignments')
    assign_type = db.Column(db.String(8))
    # choices = none, assigned, conflict

class Tag(db.Model):
    __tablename__ = "tags"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(32))
    paper = db.Column(db.Integer, db.ForeignKey(Paper.id), nullable=True)
    Paper = db.relationship('Paper', backref='tags')

class Review(db.Model):
    __tablename__ = "reviews"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    time = db.Column(db.Date)
    paper = db.Column(db.Integer, db.ForeignKey(Paper.id), nullable=True)
    Paper = db.relationship('Paper', backref='reviews')
    reviewer = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    Reviewer = db.relationship('UserProfile', backref='tag_reviewers')
    contents = db.Column(db.Text)
    score_novelty = db.Column(db.Integer)
    score_presentation = db.Column(db.Integer)
    score_technical = db.Column(db.Integer)
    score_confidence = db.Column(db.Integer)

class Comment(db.Model):
    __tablename__ = "comments"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    time = db.Column(db.Date)
    paper = db.Column(db.Integer, db.ForeignKey(Paper.id), nullable=True)
    Paper = db.relationship('Paper', backref='comments')
    user = db.Column(db.Integer, db.ForeignKey(UserProfile.id), nullable=True)
    User = db.relationship('UserProfile', backref='commenters')
    contents = db.Column(db.Text)

""" end """
