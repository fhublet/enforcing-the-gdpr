# -*- coding: utf-8 -*-
"""
    Conf
    ~~~~~~~~
    
    Originally a Jacqueline application from Yang et al. (2016)

    Ported to Flask by Anonymous authors (c) 2023
"""

import time
from sqlite3 import dbapi2 as sqlite3
from hashlib import md5
from datetime import datetime
from flask import Flask, request, session, url_for, redirect, \
     render_template, abort, g, flash, _app_ctx_stack, send_from_directory
from werkzeug.security import check_password_hash, generate_password_hash
import random
import os.path

DATABASE = 'db/conf.db' #nc
DEBUG = True #nc
SECRET_KEY = 'development key' #nc

app = Flask(__name__) #nc
app.config.from_object(__name__) #nc
app.config.from_envvar('CONF_SETTINGS', silent=True) #nc
app.config['UPLOAD_PATH'] = 'media' #nc


def get_db(): #nc
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    db = getattr(g, 'sqlite_db', None)
    if db is None:
        db = g._database = sqlite3.connect(app.config['DATABASE'])
    return db


@app.teardown_appcontext #nc
def close_database(exception): #nc
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'sqlite_db'):
        top.sqlite_db.close()
        

def query_db(query, args=(), one=False):
    """Queries the database and returns a list of dictionaries."""
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    return (rv[0] if rv else None) if one else rv


def get_user_id(username):
    """Convenience method to look up the id for a username."""
    rv = query_db('select id from user_profiles where username = ?',
                  [username], one=True)
    return rv[0] if rv else None


def is_admin():
    return g.user and g.user[6] == 'chair'


def format_datetime(timestamp):
    """Format a timestamp for display."""
    return datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d @ %H:%M')


@app.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = query_db('select * from user_profiles where id = ?',
                          [session['user_id']], one=True)

@app.route('/media/<filename>')
def media(filename):
    return send_from_directory(os.path.join('..', '..', app.config['UPLOAD_PATH']), filename)


@app.route('/about')
def about_view():
  return render_template('about.html', which_page='about')


@app.route('/papers')
@app.route('/')
@app.route('/index')
def papers_view():
    if not g.user:
        return redirect(url_for('login'))
    
    user_name = query_db('select name from user_profiles WHERE id = ?', [g.user[0]], one=True)[0]

    papers = query_db('select * from papers')
    paper_data = []

    for paper in papers:
        paper_versions = query_db('select title from conf_paperversion where paper_id = ? order by time desc', [paper[0]])
        latest_version_title = None
        if len(paper_versions):
            latest_version_title = paper_versions[0][0]
        paper_data.append({
            'paper' : paper,
            'author_name': query_db('select name from user_profiles where id = ?', [str(paper[2])], one=True)[0][0],
            'latest' : latest_version_title
        })

    return render_template('papers.html', papers=papers, which_page='home', paper_data=paper_data,
                           name=user_name, is_logged_in=True, is_admin=is_admin())


def set_random_name():
    return '%030x' % random.randrange(16**30) + ".pdf"


@app.route('/paper', methods=['GET', 'POST'])
def paper_view():
    if not g.user:
        return redirect(url_for('login'))

    paper = query_db('select * from papers where id = ?', [request.args.get('id', '')], one=True)
    if paper != None:
        if request.method == 'POST':
            if request.form.get('add_comment', 'false') == 'true':
                db = get_db()
                db.execute('insert into comments (paper_id, id, contents) values (?, ?, ?)',
                           [paper[0], g.user[0], request.form.get('comment', '')])
                db.commit()
            elif request.form.get('add_review', 'false') == 'true':
                db = get_db()
                db.execute('''insert into reviews (paper_id, reviewer_id, contents, score_novelty, 
                  score_presentation, score_technical, score_confidence) values (?, ?, ?, ?, ?, ?, ?)''',
                           [paper[0], g.user[0], request.form.get('review', ''), int(request.form.get('score_novelty', '1')),
                            int(request.form.get('score_presentation', '1')), int(request.form.get('score_technical', '1')),
                            int(request.form.get('score_confidence', '1'))])
                db.commit()
            elif request.form.get('new_version', 'false') == 'true' and id == paper[2]:
                contents = request.files.get('contents', None)
                if contents != None:
                    random_filename = set_random_name()
                    contents.save(os.path.join(app.config['UPLOAD_PATH'], random_filename))
                    db = get_db()
                    db.execute('''insert into conf_paperversion (paper_id, title, contents, abstract)
                      values (?, ?, ?, ?)''', [paper[0], request.form.get('title', ''), random_filename,
                                               request.form.get('abstract', '')])
                    db.commit()

        paper_versions = query_db('select * from conf_paperversion where paper_id = ? order by time desc', [paper[0]])
        coauthors = query_db('select * from conf_papercoauthor where paper_id = ?', [paper[0]])
        latest_abstract, latest_title = None, None
        if len(paper_versions):
            latest_abstract, latest_title = paper_versions[0][3], paper_versions[0][1]
        reviews = query_db('select * from reviews where paper_id = ?', [paper[0]])
        comments = query_db('select * from comments where paper_id = ?', [paper[0]])
        author = query_db('select * from user_profiles where id = ?', [paper[2]], one=True)
    else:
        paper = None
        paper_versions = []
        coauthors = []
        latest_abstract = None
        latest_title = None
        reviews = []
        comments = []
        author = None

    return render_template('paper.html', paper=paper, paper_versions=paper_versions, author=author,
                           coauthors=coauthors, latest_abstract=latest_abstract, latest_title=latest_title,
                           reviews=reviews, comments=comments, profile=g.user, which_page='paper',
                           review_score_fields=[ ("Novelty", "score_novelty", 10)
                                                 , ("Presentation", "score_presentation", 10)
                                                 , ("Technical", "score_technical", 10)
                                                 , ("Confidence", "score_confidence", 10) ],
                           is_logged_in=True, is_admin=is_admin())


@app.route('/submit', methods=['GET', 'POST'])
def submit_view():
    if not g.user:
        return redirect(url_for('login'))

    if request.method == 'POST':
        coauthors = request.form.getlist('coauthors[]')
        title = request.form.get('title', None)
        abstract = request.form.get('abstract', None)
        contents = request.files.get('contents', None)

        if title == None or abstract == None or contents == None:
            return render_template('submit.html', coauthors=coauthors, title=title, abstract=abstract,
                                   contents=contents.name, error='Please fill out all fields', which_page='submit',
                                   is_logged_in=True, is_admin=is_admin())

        db = get_db()
        db.execute('insert into papers (author_id, accepted) values (?, ?)', [g.user[0], False])
        db.commit()
        paper_id = query_db('select MAX(id) from papers where author_id = ?', [g.user[0]], one=True)[0]
        
        db = get_db()
        for coauthor in coauthors:
            if coauthor != "":
                db.execute('insert into conf_papercoauthor (paper_id, author_id) values (?, ?)',
                           [paper_id, g.user[0]])
        db.commit()

        random_filename = set_random_name()
        if contents != None:
            contents.save(os.path.join(app.config['UPLOAD_PATH'], random_filename))
            db = get_db()
            db.execute('insert into conf_paperversion (paper_id, time, title, abstract, contents) values (?, ?, ?, ?, ?)',
                       [paper_id, format_datetime(time.time()), title, abstract, random_filename])
            db.commit()

        db = get_db()
        for conf in request.form.getlist('pc_conflicts[]'):
            new_pc_conflict = query_db('select * from user_profiles where username = ?', [conf], one=True)
            db.execute('insert into conf_paperpcconflict (paper_id, pc_id) values (?, ?)',
                       [paper_id, new_paper_conflict])

        return redirect('paper?id=' + str(paper_id))

    pcs = query_db('select * from user_profiles where level = "pc"')
    pc_conflicts = [p[0] for p in query_db('select pc_id from conf_userpcconflict where id = ?', [g.user[0]])]
    
    return render_template('submit.html', coauthors=[], title='', abstract='', contents='', error='',
                           pcs=[{'pc': pc, 'conflict': pc in pc_conflicts} for pc in pcs],
                           pc_conflicts=pc_conflicts, which_page='submit',
                           is_logged_in=True, is_admin=is_admin())


@app.route('/accounts/profile/', methods=['GET', 'POST'])
def profile_view():
    if not g.user:
        return redirect(url_for('login'))
    
    profile = list(query_db('select * from user_profiles where id = ?', [g.user[0]], one=True))
    if profile:
        profile[6] = 'normal'

    pcs = query_db('select * from user_profiles where level = "pc"')

    if request.method == 'POST':
        name = request.form.get('name', '')
        affiliation = request.form.get('affiliation', '')
        acm_number = request.form.get('acm_number', '')
        email = request.form.get('email', '')
        db = get_db()
        db.execute('update user_profiles set name = ?, affiliation = ?, acm_number = ?, email = ? where id = ?',
                   [name, affiliation, acm_number, email, g.user[0]])
        db.commit()
        pc_conflicts = []
        for conf in request.form.getlist('pc_conflicts[]'):
            new_pc_conflict = query_db('select id from user_profiles where username = ?', [conf], one=True)
            db = get_db()
            db.execute('insert into conf_userpcconflict (id, pc_id) values (?, ?)', [profile[0], new_pc_conflict])
            db.commit()
            pc_conflicts.append(new_pc_conflict)
    else:
        pc_conflicts = [uppc[0] for uppc in query_db('select id from conf_userpcconflict where id = ?', [profile[0]])]

    return render_template('profile.html', name=profile[3], affiliation=profile[4], acm_number=profile[5],
                           pc_conflicts=pc_conflicts, email=profile[2], which_page='profile',
                           pcs=[{'pc': pc, 'conflict': pc in pc_conflicts} for pc in pcs],
                           is_logged_in=True, is_admin=is_admin())


# untested
@app.route('/submit_review', methods=['GET', 'POST'])
def submit_review_view():
    if not g.user:
        return redirect(url_for('login'))

    try:
        if request.method == 'GET':
            paper_id = int(request.args['id'])
        elif request.method == 'POST':
            paper_id = int(request.args['id'])
        paper = query_db('select * from papers where id = ?', [paper_id], one=True)
        if request.method == 'POST':
            contents = request.form.get('contents', '')
            score_novelty = request.form.get('score_novelty', 1)
            score_presentation = request.form.get('score_presentation', 1)
            score_technical = request.form.get('score_technical', 1)
            score_confidence = request.form.get('score_confidence', 1)
            db = get_db()
            db.execute('''insert into reviews (contents, score_novelty, score_presentation, 
              score_technical, score_confidence, paper_id, reviewer_id) values (?, ?, ?, ?, ?, ?, ?)''',
                       [contents, score_novelty, score_presentation, score_technical, score_confidence,
                        paper_id, reviewer_id])
            db.commit()
            return redirect("paper?id=" + str(paper_id))
    except (ValueError, KeyError, Paper.DoesNotExist):
        paper = None
        contents, score_novelty, score_presentation, score_technical, score_confidence, paper_id = \
            None, None, None, None, None, None, None

    return render_template('submit_review.html', paper=paper, contents=contents, score_novelty=score_novelty,
                           score_presentation=score_presentation, score_technical=score_technical,
                           score_confidence=score_confidence, paper_id=paper_id, which_page='submit_review',
                           is_logged_in=True, is_admin=is_admin())


# untested
@app.route('/users', methods=['GET', 'POST'])
def users_view():
    if not g.user:
        return redirect(url_for('login'))

    user_level = query_db('select level from user_profile where id = ?', [g.user[0]], one=True)[0]
    if user_level != 'chair':
        return redirect(url_for('papers_view'))

    user_profiles = query_db('select * from user_profile')

    if request.method == 'POST':
        for profile in user_profiles:
            query_param_name = 'level-' + profile[1]
            level = request.forms.get(query_param_name, '')
            if level in ['normal', 'pc', 'chair']:
                db = get_db()
                db.execute('update user_profile set level = ? where id = ?', [level, g.user[0]])
                db.commit()

    return render_template('users_view.html', user_profiles=user_profiles, which_page='users',
                           is_logged_in=True, is_admin=is_admin())


# untested
@app.route('/submit_comment', methods=['GET', 'POST'])
def submit_comment_view():
    if not g.user:
        return redirect(url_for('login'))

    try:
        if request.method == 'GET':
            paper_id = int(request.GET['id'])
        elif request.method == 'POST':
            paper_id = int(request.POST['id'])
        paper = query_db('select * from papers where id = ?', [paper_id], one=True)
        if request.method == 'POST':
            contents = request.form.get('contents', '')
            db = get_db()
            db.execute('insert into comments (contents, paper_id, id) values (?, ?, ?)',
                       [contents, paper_id, g.user[0]])
            db.commit()
            return redirect("paper?id=" + str(paper_id))
    except (ValueError, KeyError, Paper.DoesNotExist):
        paper = None
        contents, paper_id = None, None

    return render_template('submit_comment.html', paper=paper, contents=contents, which_page='submit_comment',
                           is_logged_in=True, is_admin=is_admin())


# untested
@app.route('/assign_reviews', methods=['GET', 'POST'])
def assign_reviews_view():
    if not g.user:
        return redirect(url_for('login'))
    
    possible_reviewers = query_db('select * from user_profiles where level = "pc"')

    if 'reviewer_username' in request.GET:
        papers = query_db('select * from papers')

        if request.method == 'POST':
            db = get_db()
            db.execute('delete from review_assignments where id = ?', [g.user[0]])
            for paper in papers:
                db.execute('insert into review_assignments (assign_type, paper_id, id) values (?, ?, ?)',
                           ['assigned' if request.POST.get('assignment-' + paper.id, '') == 'yes' else 'none',
                            paper[0], g.user[0]])
            db.commit()
        papers_data = [{
            'paper' : paper[0],
            'latest_version' : query_db('select from conf_paperversion where paper_id = ? order by time desc', [paper[0]])[-1],
            'assignment' : query_db('select from review_assignments where paper_id = ? and user_id = ?', [paper[0], g.user[0]]),
            'has_conflict' : query_db('select from conf_paperpcconflict where pc_id = ? and paper_id = ?', [g.user[0], paper[0]],
                                      one=True) != None,
        } for paper in papers]
    else:
        reviewer = None
        papers_data = []

    return render_template('assign_reviews.html', reviewer=reviewer, possible_reviewers=possible_reviewers,
                           papers_data=papers_data, which_page='assign_reviews',
                           is_logged_in=True, is_admin=is_admin())


# untested
@app.route('/search')
def search_view():
    if not g.user:
        return redirect(url_for('login'))

    reviewers = query_db('select * from user_profiles')
    authors = query_db('select * from user_profiles')

    return render_template('search.html', reviewers=reviewers, authors=authors, which_page='search',
                           is_logged_in=True, is_admin=is_admin())

@app.route('/register', methods=['GET', 'POST']) #nc
def register_account(): #nc
    if g.user:
        return redirect(url_for("index"))

    error, username, name, affiliation, email = '', '', '', '', ''

    if request.method == 'POST':
        username = request.form['username']
        name = request.form.get('name', '')
        affiliation = request.form.get('affiliation', '')
        email = request.form.get('email', '')
        password1 = request.form.get('password1', '')
        password2 = request.form.get('password2', '')
        if not username:
            error = 'You have to enter a username'
        elif get_user_id(username) is not None:
            error = 'The username is already taken'
        elif '@' not in email:
            error = 'You have to enter a valid email address'
        elif not password1:
            error = 'You have to enter a password'
        elif password1 != password2:
            error = 'The two passwords do not match'
        elif get_user_id(username) is not None:
            error = 'The username is already taken'
        else:
            db = get_db()
            username = request.form['username']
            db.execute('''insert into user_profiles (
              username, name, affiliation, level, email, password, acm_number) values (?, ?, ?, ?, ?, ?, "")''',
                       [username, name, affiliation, 'normal', email, generate_password_hash(password1)])
            db.commit()
            id = get_user_id(username)
            session['user_id'] = id
            return redirect(url_for('papers_view'))

    return render_template('registration/account.html',
                           username=username, name=name, affiliation=affiliation, email=email, error=error,
                           which_page='register')

@app.route('/accounts/login', methods=['GET', 'POST']) #nc
def login(): #nc
    """Logs the user in."""
    if g.user:
        return redirect(url_for('papers_view'))
    error = None
    if request.method == 'POST':
        user = query_db('''select * from user_profiles where
            username = ?''', [request.form['username']], one=True)
        if user is None:
            error = 'Invalid username'
        elif not check_password_hash(user[7],
                                     request.form['password']):
            error = 'Invalid password'
        else:
            flash('You were logged in')
            session['user_id'] = user[0]
            return redirect(url_for('papers_view'))
    return render_template('registration/login.html', error=error)


@app.route('/accounts/logout') #nc
def logout(): #nc
    """Logs the user out."""
    flash('You were logged out')
    session.pop('user_id', None)
    return redirect(url_for('login'))


# add some filters to jinja
app.jinja_env.filters['datetimeformat'] = format_datetime #nc

if __name__ == '__main__': #nc
    app.run()
