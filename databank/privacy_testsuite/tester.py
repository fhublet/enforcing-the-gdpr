import os.path

from tqdm import tqdm

class Tester:

    DEFAULT_N1 = 10
    DEFAULT_N2 = 10
    
    def __init__(self, title, App, Reporter, root_folder):
        self.App = App
        self.Reporter = Reporter
        self.title = title
        self.root_folder = root_folder
        self.reporter = None
        
    def test(self, N1=DEFAULT_N1, N2=DEFAULT_N2, folder=None):
        data = []
        app = self.App()
        configs = app.configurations()
        scenarios = app.scenarios()
        cs = [(config, scenario) for config in configs for scenario in scenarios]
        for i in range(N1):
            while True:
                print(f"--- Started iteration #{i+1}/{N1} ---")
                try:
                    data_ = []
                    app.start()
                    for (config, scenario) in tqdm(cs):
                        scenario.initialize(config)
                        for _ in range(N2):
                            data_.append(scenario.run())
                            scenario.continue_()
                        scenario.finalize()
                    app.stop()
                except Exception as e:
                    print(f"--- Restarting iteration {i} on exception: {e} ---")
                    input("--- Reset wrapper and press enter...")
                else:
                    data = data_ + data
                    print(f"--- Finished iteration #{i+1}/{N1} ---")
                    break
        self.reporter = self.Reporter(self.title, data, app.dep_vars(), app.indep_vars())
        if folder is not None:
            self.reporter.save(folder)
        self.N1 = N1
        self.N2 = N2

    def load(self, N1=DEFAULT_N1, N2=DEFAULT_N2):
        self.reporter = self.Reporter(self.title, data=os.path.join(self.root_folder))
        self.N1 = N1
        self.N2 = N2
            
    def generate(self, baseline=None):
        assert(self.reporter is not None)
        self.reporter.generate(self.root_folder, N=self.N1*self.N2, baseline_folder=baseline)

