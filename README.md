# Enforcing the GDPR 

This Git repository contains the artifacts of the paper "Enforcing the GDPR."

## Paper

The paper is located in the `doc/paper` folder, together with the technical report [25].

## Setup instructions

The following setup procedure should work on Linux Ubuntu/Debian:

1. Fetch the last QuestDB version from [the QuestDB website](https://questdb.io/get-questdb/) and copy it into a new folder called `questdb` at the root of this repo. Note that testing was performed using QuestDB version 7.0.0.

2. Run
```
  cd monpoly-server
  git clone https://bitbucket.com/jshs/monpoly
  cd monpoly 
  git checkout enfpoly_logging_backend
```

3. Follow the instructions at https://bitbucket.com/jshs/monpoly to complete the installation of Enfpoly/MonPoly within folder `monpoly`

4. Go back to the root folder, create and open a virtual environment, and install the Python dependencies with
```
   cd ../..
   virtualenv env
   source env/bin/activate
   pip3 install -r requirements.txt
```

5. You can now start the enforcement backend with
```
  ./init.sh start
```
and run the app with
```
  cd databank/src
  python3 main.py
```
If needed, the backend can be stopped with

```
  ./init.sh stop
```

## Evaluation

The evaluation scripts are located at `databank/privacy_testsuite`. See README file there.
You can find the data used to generate the graphics in the paper at `databank/privacy_testsuite/reports`.

