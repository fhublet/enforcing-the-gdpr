if [ "$1" = "start" ]; then
    
    ./questdb/bin/questdb.sh start
    python3.9 monpoly-server/src/app.py &
    sleep 2
    curl http://localhost:5678/reset-everything
    curl http://localhost:5678/set-signature -F "signature=@./databank/monitor_files/gdpr.sig"
    curl http://localhost:5678/set-policy -F "policy=@./databank/monitor_files/gdpr.mfotl"
    curl http://localhost:5678/start-monitor -d "enforce=True"
	

elif [ "$1" = "stop" ]; then

    ./questdb/bin/questdb.sh stop
    killall python3.9

else

    echo "Incorrect option $1" 
    
fi
